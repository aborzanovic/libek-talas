/**
 * Pads a string on the left side, with a specified padding string and up to the
 * specified length.
 *
 * Behaves similarly to the PHP `lpad` function.
 *
 * Taken from the linked page and fixed as per the first comment.
 *
 * @param {String} padString
 * @param {Number} length
 * @return {String}
 * @link http://stackoverflow.com/a/10073737
 */
String.prototype.lpad = function( padString, length ) {
	var str = this;

	while ( str.length < length ) {
		str = padString + str;
	}

	return str.slice(-length);
};
