/**
 * Casts a string to an integer, with an optional base to use for the conversion
 * (defaults to base 10).
 *
 * @param {Number} base
 * @return {String}
 */
String.prototype.toInteger = function( base ) {
	if ( typeof base === "undefined" ) {
		base = 10;
	}

	return parseInt( this, base );
};
