/**
 * This plugin initializes the Redactor editor on all text areas with the
 * `redactor` CSS class.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );
var errorDialog = require( "creitive/core/errorDialog" );
var csrf = require( "creitive/core/csrf" );
var language = require( "creitive/core/language" );

var translator = language.getTranslator();

translator.addTranslations({
	"imageUploadError": {
		"en_US": "Image upload error! Please check if you have uploaded the correct file type!",
		"sr_RS": "Greška prilikom slanja slike! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!",
		"bs_BA": "Greška prilikom slanja slike! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!"
	},
	"fileUploadError": {
		"en_US": "File upload error! Please check if you have uploaded the correct file type!",
		"sr_RS": "Greška prilikom slanja fajla! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!",
		"bs_BA": "Greška prilikom slanja fajla! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!"
	}
});

/**
 * Attempts to find a label for the passed text area object.
 *
 * Returns `null` if no such label is found.
 *
 * @param {Object} $textarea
 * @return {Object|null}
 */
var getLabel = function( $textarea ) {

	/**
	 * First, we'll attempt to find a label element that wraps this textarea.
	 */

	var $parentLabel = $textarea.closest( "label" );

	if ( $parentLabel.length ) {
		return $parentLabel;
	}

	/**
	 * Otherwise, we'll try to find a matching label that has its `for`
	 * attribute set to the textarea's `id`, assuming the textarea has an `id`
	 * attribute.
	 */

	var id = $textarea.attr( "id" );

	if ( ! id ) {
		return null;
	}

	var $relatedLabel = $( "label" ).filterByAttr( "for", id );

	if ( $relatedLabel.length ) {
		return $relatedLabel;
	}

	/**
	 * If all fails, we'll assume there is no related label, and we'll return
	 * `null`.
	 */

	return null;
};

/**
 * Initializes the Redactor plugin on a textarea element.
 *
 * @param {Object} textarea
 * @return {Void}
 */
var initializeRedactor = function( textarea ) {
	var $textarea = $( textarea );
	var $label = getLabel( $textarea );
	var minHeight = $textarea.attr( "data-min-height" );

	if ( minHeight ) {
		minHeight = minHeight.toInteger();
	} else {
		minHeight = 400;
	}

	$textarea.redactor({
		/**
		 * This setting allows to set minimum height for Redactor.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/height/#setting-minHeight
		 */
		"minHeight": minHeight,

		/**
		 * This setting turns on pasting as plain text. The pasted text will be
		 * stripped of any tags, line breaks will be marked with tag. With this
		 * set to 'true' and 'enterKey' set to 'false', line breaks will be
		 * replaced by spaces.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/clean/#setting-pastePlainText
		 */
		"pastePlainText": true,

		/**
		 * This setting makes Redactor to convert all divs in a text into
		 * paragraphs. With 'linebreaks' set to 'true', all div tags will be
		 * removed, and text will be marked up with tag.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/clean/#setting-replaceDivs
		 */
		"replaceDivs": false,

		/**
		 * Sets URL path to the image upload script.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/images/#setting-imageUpload
		 */
		"imageUpload": "/admin/redactor/images",

		/**
		 * This callback is triggered on successful image upload (including via
		 * drag and drop).
		 *
		 * @link http://imperavi.com/redactor/docs/callbacks/image/#callback-imageUploadCallback
		 * @param {HTMLElement} image
		 * @return {Void}
		 */
		"imageUploadCallback": function( image ) {
			$( image ).addClass( "img-responsive" );
		},

		/**
		 * This callback is triggered whenever there is an error in image
		 * upload.
		 *
		 * @link http://imperavi.com/redactor/docs/callbacks/image/#callback-imageUploadErrorCallback
		 * @return {Void}
		 */
		"imageUploadErrorCallback": function() {
			errorDialog.show( translator.get( "imageUploadError" ) );
		},

		/**
		 * This setting allows to pass additional parameters with image upload.
		 * Parameters will be passed using POST method.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/images/#setting-uploadImageFields
		 */
		"uploadImageFields": csrf.inject({}),

		/**
		 * Sets path URL to the file upload script.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/files/#setting-fileUpload
		 */
		"fileUpload": "/admin/redactor/files",

		/**
		 * This callback is triggered whenever there is an error in file upload.
		 *
		 * @link http://imperavi.com/redactor/docs/callbacks/file/#callback-fileUploadErrorCallback
		 * @return {Void}
		 */
		"fileUploadErrorCallback": function() {
			errorDialog.show( translator.get( "fileUploadError" ) );
		},

		/**
		 * This setting allows to pass additional parameters with uploaded file.
		 * Parameters will be passed using POST method.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/files/#setting-uploadFileFields
		 */
		"uploadFileFields": csrf.inject({}),

		/**
		 * This callback is triggered after a table been inserted to Redactor
		 * using table plugin.
		 *
		 * @link http://imperavi.com/redactor/docs/callbacks/table/#callback-insertedTableCallback
		 * @param {HTMLElement} table
		 * @return {Void}
		 */
		"insertedTableCallback": function( table ) {
			$( table ).addClass( "table" ).wrap( "<div class='table-responsive'></div>" );
		},

		/**
		 * This setting adds HTML code view button to the toolbar.
		 *
		 * @link http://imperavi.com/redactor/docs/settings/buttons/#setting-buttonSource
		 */
		"buttonSource": true,

		/**
		 * The plugins in use.
		 *
		 * The linked article shows how to create plugins, but it also shows how
		 * plugins are loaded by their name.
		 *
		 * @link http://imperavi.com/redactor/docs/how-to-create-plugin/
		 */
		"plugins": [
			/**
			 * Insert and format tables with ease.
			 *
			 * @link http://imperavi.com/redactor/plugins/table/
			 */
			"table",

			/**
			 * Enrich text with embedded video.
			 *
			 * @link http://imperavi.com/redactor/plugins/video/
			 */
			"video",

			/**
			 * Expand Redactor to fill the whole screen. Also known as
			 * "distraction free" mode.
			 *
			 * @link http://imperavi.com/redactor/plugins/fullscreen/
			 */
			"fullscreen"
		]
	});

	/**
	 * When the related label is clicked, assuming it exists, we want to set the
	 * focus to the end of the editor.
	 */

	if ( $label !== null ) {
		$label.on( "click", function( event ) {
			event.preventDefault();

			$textarea.redactor( "focus.setEnd" );
		});
	}
};

module.exports = {
	initialize: function() {
		$( ".redactor" ).each( function( index, textarea ) {
			initializeRedactor( textarea );
		});
	}
};
