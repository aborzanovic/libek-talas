/**
 * jQuery UI datepicker initialization.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );

module.exports = {
	initialize: function() {
		$( ".datepicker" ).datepicker();
	}
};
