/**
 * Initializes plugins on document.ready
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var cms = {};

cms.autoSortables = require( "./autoSortables" );
cms.datepicker = require( "./datepicker" );
cms.dropzone = require( "./dropzone" );
cms.redactor = require( "./redactor" );
cms.sidebarNavToggler = require( "./sidebarNavToggler" );

cms.initialize= function() {
	cms.autoSortables.initialize();
	cms.datepicker.initialize();
	cms.dropzone.initialize();
	cms.redactor.initialize();
	cms.sidebarNavToggler.initialize();
};

module.exports = cms;
