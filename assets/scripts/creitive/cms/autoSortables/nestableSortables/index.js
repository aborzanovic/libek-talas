/**
 * Initializes the nestableSortable logic for each instance found on the current
 * page.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );
var nestableSortable = require( "./nestableSortable" );

module.exports = {
	/**
	 * Initializes the module.
	 *
	 * @return {Void}
	 */
	initialize: function() {
		$( ".itemList--nestableSortable" ).each( function( index, container ) {
			nestableSortable.initialize( container );
		});
	}
};
