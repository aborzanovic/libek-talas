/**
 * Initializes the sortable logic for each instance found on the current page.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );
var sortable = require( "./sortable" );

module.exports = {
	/**
	 * Initializes the module.
	 *
	 * @return {Void}
	 */
	initialize: function() {
		$( ".itemList--sortable" ).each( function( index, container ) {
			sortable.initialize( container );
		});
	}
};
