/**
 * Initializes sortables and nestable sortables for the CMS.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var autoSortables = {};

autoSortables.sortables = require( "./sortables" );
autoSortables.nestableSortables = require( "./nestableSortables" );

autoSortables.initialize = function() {
	autoSortables.sortables.initialize();
	autoSortables.nestableSortables.initialize();
};

module.exports = autoSortables;
