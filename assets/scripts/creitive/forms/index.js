/**
 * Initializes form-related modules.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var forms = {};

forms.bootstrapButtonLoadingText = require( "./bootstrapButtonLoadingText" );

forms.initialize= function() {
	forms.bootstrapButtonLoadingText.initialize();
};

module.exports = forms;
