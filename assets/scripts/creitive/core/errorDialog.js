/**
 * This module enables showing customized error messages by calling
 * `errorDialog.show()`, as well as passing an optional callback to be executed
 * after the dialog is closed.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );
var BootstrapDialog = require( "BootstrapDialog" );
var language = require( "./language" );

var translator = language.getTranslator();

translator.addTranslations({
	"defaultTitle": {
		"en_US": "Error",
		"sr_RS": "Greška",
		"bs_BA": "Greška"
	},
	"defaultMessage": {
		"en_US": "An unexpected error occured. We apologize for the inconvenience. Please contact the administrator about the problem. Thank you for your patience!",
		"sr_RS": "Došlo je do nepoznate greške. Izvinjavamo se na neprijatnosti. Molimo Vas obavestite administratora o problemu. Hvala Vam na razumevanju!",
		"bs_BA": "Došlo je do nepoznate greške. Izvinjavamo se na neprijatnosti. Molimo Vas obavestite administratora o problemu. Hvala Vam na razumevanju!"
	},
	"defaultButton": {
		"en_US": "OK",
		"sr_RS": "OK",
		"bs_BA": "OK"
	}
});

module.exports = {

	/**
	 * Shows a modal error dialog using the jQuery UI dialog plugin.
	 *
	 * Accepts an error message as the first argument, which, if provided, will
	 * be injected as-is into the dialog (ie. HTML tags will be left unescaped).
	 * If not provided, a generic error message will be used instead.
	 *
	 * If the optional second argument is provided, and if it is a function, it
	 * will be called right after the dialog is closed.
	 *
	 * The method returns the object which was made into a dialog, to enable
	 * chaining in case any custom modifications are required.
	 *
	 * @param {String} errorMessage
	 * @param {Function} callback
	 * @return {Object}
	 */
	show: function( errorMessage, callback ) {
		var titleText = translator.get( "defaultTitle" );
		var buttonText = translator.get( "defaultButton" );

		errorMessage = errorMessage || translator.get( "defaultMessage" );

		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": titleText,
			"message": errorMessage,
			"buttons": [
				{
					"label": buttonText,
					"action": function( dialog ) {
						dialog.close();

						if ( $.isFunction( callback ) ) {
							callback();
						}
					}
				}
			]
		});
	}

};
