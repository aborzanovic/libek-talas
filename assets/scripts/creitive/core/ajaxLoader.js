/**
 * Some methods for displaying a global AJAX loader indicator.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );

/**
 * We will store the AJAX loader `<div>` in this variable, to prevent
 * unnecessary DOM lookups.
 *
 * @var {Object}
 */
var ajaxLoader;

/**
 * Stores the number of currently active AJAX requests, so that the animations
 * would only be triggered when needed.
 *
 * @type {Number}
 */
var activeAjaxRequests = 0;

var ajaxLoaderManager = {

	/**
	 * Animates a `<div>` that's expected to be hidden just below the bottom of
	 * the page, in the form of a "Status bar", to make it slide into the page.
	 * This div is included in the "head" view of the EWS CMS framework, by
	 * default.
	 *
	 * (Usually, it's located above the page, but for this particular project,
	 * we needed to have the "Status bar"-like design).
	 *
	 * @return {Object} The jQuery object containing the `<div>`
	 */
	show: function() {
		++activeAjaxRequests;

		if ( activeAjaxRequests === 1 ) {
			return ajaxLoader.animate( { top: "0px" }, 200 );
		} else {
			return ajaxLoader;
		}
	},

	/**
	 * Animates the `<div>` shown by the `EWS.ajaxLoader.show()` function to
	 * make it slide out of the page.
	 *
	 * @return {Object} The jQuery object containing the `<div>`
	 */
	hide: function() {
		--activeAjaxRequests;

		if ( ! activeAjaxRequests ) {
			return ajaxLoader.animate( { top: "-25px" }, 200 );
		} else {
			return ajaxLoader;
		}
	},

	/**
	 * Initializes the module.
	 *
	 * @return {Void}
	 */
	initialize: function() {
		ajaxLoader = $( "#ajax-loader" );
	}

};

module.exports = ajaxLoaderManager;
