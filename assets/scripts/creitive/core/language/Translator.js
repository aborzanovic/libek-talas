/**
 * A Translator class, for creating translation dictionaries.
 *
 * This class should only be used in conjunction with the provided "language"
 * plugin.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var Translator = function() {

	this.dictionary = {};

	this.languageResolver = {};

	var that = this;

	/**
	 * Adds a new translation string into the internal dictionary.
	 *
	 * @param {String} translationName
	 * @param {Object} definitions
	 */
	var addTranslation = function( translationName, definitions ) {
		var languageCode;

		for ( languageCode in definitions ) {
			if ( definitions.hasOwnProperty( languageCode ) ) {

				if ( ! that.dictionary.hasOwnProperty( languageCode ) ) {
					that.dictionary[languageCode] = {};
				}

				// We should stop if we encounter a duplicate definition
				if ( that.dictionary[languageCode].hasOwnProperty( translationName ) ) {
					throw new Error( "Duplicate language definition: Key \"" + translationName + "\" already exists in the dictionary for language: " + languageCode );
				}

				that.dictionary[languageCode][translationName] = definitions[languageCode];
			}
		}
	};

	this.setLanguageResolver = function( languageResolver ) {
		this.languageResolver = languageResolver;
	};

	/**
	 * Adds an object of translation strings into the internal dictionary.
	 *
	 * The first and only argument is a list of translation objects to import
	 * into the internal dictionary. A complete translation object is expected
	 * to be structured with property names as specific names for the
	 * translation string in question, and the associated values are the
	 * translations for that key, or, to be more specific, an object with
	 * properties corresponding to ISO 639-1 language codes, and their values
	 * being the actual translations.
	 *
	 * @param {Object} translations
	 * @return {Void}
	 */
	this.addTranslations = function( translations ) {
		var translationName;

		for ( translationName in translations ) {
			if ( translations.hasOwnProperty( translationName ) ) {
				addTranslation( translationName, translations[translationName] );
			}
		}
	};

	/**
	 * Returns a string by its translation name (key), in the specified
	 * language, or the language currently in use, if one wasn't specified.
	 *
	 * @param {string} translationName
	 * @param {string} languageCode
	 * @return {string}
	 */
	this.get = function( translationName, languageCode ) {
		languageCode = languageCode || this.languageResolver.getLanguage();

		if ( ! this.dictionary.hasOwnProperty( languageCode ) ) {
			throw new Error( "Unknown language requested: The requested language (" + languageCode + ") wasn't found in the currently active dictionary." );
		}

		if ( ! this.dictionary[languageCode].hasOwnProperty( translationName ) ) {
			throw new Error( "Unknown translation item: The requested item (" + translationName + ") wasn't found in the currently active dictionary for the language \"" + languageCode + "\"." );
		}

		return this.dictionary[languageCode][translationName] || "";
	};

};

module.exports = Translator;
