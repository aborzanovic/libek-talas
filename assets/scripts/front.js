/**
 * These are the main front-end scripts.
 *
 * @copyright ©2011-2014 CreITive (http://www.creitive.rs)
 */

"use strict";

var $ = require( "jquery" );

var creitive = {};

creitive.core = require( "creitive/core" );
creitive.forms = require( "creitive/forms" );

creitive.initialize = function() {
	creitive.core.initialize();
	creitive.forms.initialize();

	/**
	 * In a non-production environment, the complete library will be attached to
	 * the global `window` object, so as to be able to access it from the
	 * browser console.
	 */

	if ( ! creitive.core.environment.is( "production" ) ) {
		global.window.creitive = creitive;
	}

	/**
	 * As a sanity check for development, we'll log a "Ready" message to the
	 * console.
	 */

	creitive.core.logger.info( "Ready!" );
};

$( document ).ready( function() {
	creitive.initialize();
});
