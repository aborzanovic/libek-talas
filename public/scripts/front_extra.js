var body = document.querySelector('body');
var listElm = document.querySelector('.news-padding');

var nextPage = 1;
var loadingResults = false;
var loadNextPage = function() {
    loadingResults = true;
    var ajaxUrl = window.location.protocol +"//"+ window.location.hostname + window.location.pathname
    $.ajax(ajaxUrl + '/' + (window.infiniteScrollPrefix ? window.infiniteScrollPrefix : '') + nextPage + window.location.search).success(function (items) {
        if (items !== '') {
            listElm.innerHTML += items;
        }
        else {
            body.removeEventListener('scroll', handleScroll, true);
        }
        loadingResults = false;
        nextPage++;
    });
}

function handleScroll() {
    if (body.scrollTop + body.clientHeight >= body.scrollHeight - 500) {
        if (! loadingResults) {
            loadNextPage();
        }
    }
}

// Detect when scrolled to bottom.
body.addEventListener('scroll', handleScroll, true);

$(document).mouseup(function(e) 
{
    var search = $("#search");
    var icon = $("#icon");
    // if the target of the click isn't the container nor a descendant of the container
    if (!search.is(e.target) && search.has(e.target).length === 0) 
    {
        search.css("display","none");
        icon.css("display","block");
    }

});

$(document).mouseup(function(e) 
{
    var menu_content = $("#menu-content");
    var flag = $("#menu_open_shadow").css('display');
    // if the target of the click isn't the container nor a descendant of the container
    if (!menu_content.is(e.target) && menu_content.has(e.target).length === 0) 
    {
       if (flag !== 'none')
       hide_show_menu();
    }

});


document.addEventListener('DOMContentLoaded', function() {
    $('#tags').selectize({ maxItems: 1 });
});

$("#newsletter-subscribe").click(function(e) {
    e.preventDefault();
    $.post("/newsletter", {
        _token: $('meta[name="csrf-token"]').attr('content'),
        email: $('#newsletter-email').val()
    }).done(function (response) {
        if (response === "SUCC") {
            $.notify("Uspeh!", "success");
        }
        else {
            $.notify(response, "error");
        }
    });
});