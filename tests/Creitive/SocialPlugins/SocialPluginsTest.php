<?php namespace Creitive\SocialPlugins;

use Creitive\SocialPlugins\SocialPlugins;
use Symfony\Component\DomCrawler\Crawler;
use TestCase;
use Way\Tests\Assert;

class SocialPluginsTest extends TestCase {

	public function testAddPlugins()
	{
		$s = new SocialPlugins('http://localhost');
		$s->addPlugins(
			new \Creitive\SocialPlugins\Facebook\Like(),
			new \Creitive\SocialPlugins\Google\PlusOne()
		);

		Assert::count(2, $s->getPlugins());
	}

	public function testSetUrl()
	{
		$url = 'http://localhost';
		$s = new \Creitive\SocialPlugins\SocialPlugins();
		$s->setUrl($url);

		Assert::same($url, $s->getUrl());
	}

	public function testFacebookLike()
	{
		$like = new \Creitive\SocialPlugins\Facebook\Like();
		$crawler = new Crawler($like->render());

		Assert::count(1, $crawler->filter('.fb-like'));
		Assert::count(1, $crawler->filter('[data-action=like]'));
	}

	public function testGooglePlusOne()
	{
		$plusOne = new \Creitive\SocialPlugins\Google\PlusOne();
		$crawler = new Crawler($plusOne->render());

		Assert::count(1, $crawler->filter('.g-plusone'));
	}

	public function testTwitterTweet()
	{
		$tweet = new \Creitive\SocialPlugins\Twitter\Tweet();
		$crawler = new Crawler($tweet->render());

		Assert::count(1, $crawler->filter('.twitter-share-button'));
	}

	public function testLinkedInShare()
	{
		$share = new \Creitive\SocialPlugins\LinkedIn\Share();
		$crawler = new Crawler($share->render());

		Assert::count(1, $crawler->filter('script[type="IN/Share"]'));
	}

	public function testPinterestPinIt()
	{
		$pinIt = new \Creitive\SocialPlugins\Pinterest\PinIt();
		$crawler = new Crawler($pinIt->render());

		Assert::count(1, $crawler->filter('[data-pin-do = buttonPin]'));
	}

}
