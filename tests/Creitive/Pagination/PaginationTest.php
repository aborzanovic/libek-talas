<?php namespace Creitive\Pagination;

use Creitive\Pagination\Pagination;
use Symfony\Component\DomCrawler\Crawler;
use TestCase;
use Way\Tests\Assert;

class PaginationTest extends TestCase {

	public function provider()
	{
		return array(
			array(new Pagination(1, 50, 6, '/')),
			array(new Pagination(5, 70, 3, '/')),
		);
	}

	/**
	 * @dataProvider provider
	 */
	public function testTotalPages($pagination)
	{
		Assert::greaterThan($pagination->getTotalPages(), $pagination->getCurrentPage());
	}

	/**
	 * @dataProvider provider
	 */
	public function testGetFirstVisiblePage($pagination)
	{
		if ($pagination->getCurrentPage() > $pagination->getVisiblePageLinks())
		{
			Assert::eq($pagination->getFirstVisiblePage(), $pagination->getCurrentPage() - $pagination->getVisiblePageLinks());
		}
		else
		{
			Assert::eq($pagination->getFirstVisiblePage(), 0);
		}
	}

	/**
	 * @dataProvider provider
	 */
	public function testGetLastVisiblePage($pagination)
	{
		if ($pagination->getCurrentPage() < $pagination->getTotalPages())
		{
			Assert::eq($pagination->getLastVisiblePage(), $pagination->getCurrentPage() + $pagination->getVisiblePageLinks());
		}

		if ($pagination->getCurrentPage() == $pagination->getTotalPages())
		{
			Assert::eq($pagination->getLastVisiblePage(), $pagination->getCurrentPage());
		}
	}

	/**
	 * @dataProvider provider
	 */
	public function testOutput($pagination)
	{
		$crawler = new Crawler($pagination->render());

		Assert::count(1, $crawler->filter('ul'));
		Assert::count(1, $crawler->filter('.active'));
	}

	public function testGetCurrentPage()
	{
		$currentPage = 1;
		$pagination = new Pagination(1, 50, 6, '/');
		Assert::eq($currentPage, $pagination->getCurrentPage());
	}

	public function testTotalItems()
	{
		$totalItems = 50;
		$pagination = new Pagination(1, 50, 6, '/');
		Assert::eq($totalItems, $pagination->getTotalItems());
	}

	public function testItemsPerPage()
	{
		$itemsPerPage = 6;
		$pagination = new Pagination(1, 50, 6, '/');
		Assert::eq($itemsPerPage, $pagination->getItemsPerPage());
	}

}
