<?php namespace Creitive\Asset;

use Creitive\Asset\Asset;
use Symfony\Component\DomCrawler\Crawler;
use TestCase;
use Way\Tests\Assert;

class AssetTest extends TestCase {

	public function providerCss()
	{
		return array(
			array(
				array(
					'jquery-ui',
					'//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css'
				),
			),
		);
	}

	public function providerJs()
	{
		return array(
			array(
				array(
					'plugins-front',
					'/js/plugins/front/compiled.js',
				),
			),
		);
	}

	/**
	 * @dataProvider providerCss
	 */
	public function testCssAsset($resource)
	{
		$asset = new Asset;

		$asset->container('head')->add($resource[0], $resource[1]);
		$crawler = new Crawler($asset->container('head')->render());

		Assert::count(1, $crawler->filter('link'));
	}

	/**
	 * @dataProvider providerJs
	 */
	public function testJsAsset($resource)
	{
		$asset = new Asset;

		$asset->container('head')->add($resource[0], $resource[1]);
		$crawler = new Crawler($asset->container('head')->render());

		Assert::count(1, $crawler->filter('script'));
	}

}
