<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Admin\AbstractBaseController;

class HomeController extends AbstractBaseController {

	public function __construct()
	{
		parent::__construct();

		$this->viewData->bodyDataPage = 'admin-home';
		$this->viewData->pageTitle->setPage(Lang::get('common.adminPanel'));
	}

	/**
	 * Gets the admin panel homepage.
	 *
	 * @return void
	 */
	public function index()
	{
		$this->loadContent('admin.home');
	}

}
