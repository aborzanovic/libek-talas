<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use AssetManager;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\AbstractBaseController as RootAbstractBaseController;
use URL;
use View;

abstract class AbstractBaseController extends RootAbstractBaseController {

	/**
	 * {@inheritDoc}
	 */
	protected $layout = 'layouts.admin';

	public function __construct()
	{
		parent::__construct();

		$this->viewData->bodyDataPage = 'admin-home';
		$this->viewData->pageTitle->setPage('Admin panel');

		$this->buildNavigation();
	}

	/**
	 * {@inheritDoc}
	 */
	protected function loadContent($partial, $data = [])
	{
		View::inject('content', View::make($partial, $data)->render());
	}

	/**
	 * {@inheritDoc}
	 */
	public function setBaseAssets()
	{
		AssetManager::loadAssetsForAdmin();
	}

	/**
	 * Builds the admin panel navigation.
	 *
	 * @return void
	 */
	protected function buildNavigation()
	{
		$navigation = $this->navigation->get('admin.main');

		$navigation->addLast(
			'home',
			[
				'href' => URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\HomeController@index'),
				'icon' => 'home',
				'label' => Lang::get('admin/navigation.home'),
			]
		);

		if ($this->currentUser->hasAccess('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index'))
		{
			$navigation->addLast(
				'news-articles',
				[
					'href' => URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index'),
					'icon' => 'file-text-o',
					'label' => Lang::get('admin/navigation.newsArticles'),
				]
			);
		}
		
		if ($this->currentUser->hasAccess('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index'))
		{
			$navigation->addLast(
				'users',
				[
					'href' => URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index'),
					'icon' => 'users',
					'label' => Lang::get('admin/navigation.users'),
				]
			);
		}

		$navigation->addLast(
			'change-password',
			[
				'href' => URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@index'),
				'icon' => 'lock',
				'label' => Lang::get('admin/navigation.changePassword'),
			]
		);

		$navigation->addLast(
			'back-to-website',
			[
				'href' => URL::action('Libek\LibekOrgRs\Http\Controllers\Front\LanguageRedirectController@getRedirect'),
				'icon' => 'globe',
				'label' => Lang::get('admin/navigation.backToWebsite'),
			]
		);

		$navigation->setActive('home');
	}

}
