<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;
use Libek\LibekOrgRs\Auth\User;
use Libek\LibekOrgRs\Auth\User\Manager as UserManager;
use Libek\LibekOrgRs\Http\Controllers\Admin\AbstractBaseController;
use Redirect;

class UserController extends AbstractBaseController {

	/**
	 * The current request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A Translator instance.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $translator;

	/**
	 * A UserManager instance.
	 *
	 * @var \Libek\LibekOrgRs\Auth\User\Manager
	 */
	protected $userManager;

	public function __construct(Request $request, Translator $translator, UserManager $userManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->translator = $translator;
		$this->userManager = $userManager;

		$this->viewData->bodyDataPage = 'admin-users';
		$this->viewData->pageTitle->setPage($this->translator->get('admin/users.module'));
		$this->navigation->get('admin.main')->setActive('users');
	}

	/**
	 * Shows all users.
	 *
	 * @return void
	 */
	public function index()
	{
		if ($this->request->has('roles'))
		{
			$users = $this->userManager->getUsersWithAnyRoles($this->request->get('roles'));
		}
		else
		{
			$users = $this->userManager->getAllUsers();
		}

		$data = [
			'users' => $users,
		];

		$this->loadContent('admin.users.index', $data);
	}

	/**
	 * Displays the user create form.
	 *
	 * @return void
	 */
	public function create()
	{
		$data = [
			'roleOptions' => $this->userManager->getRoleOptions(),
			'defaultRoleOption' => 'client',
		];

		$this->loadContent('admin.users.create', $data);
	}

	/**
	 * Saves a new user.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		if ($user = $this->userManager->createUser($this->request->all()))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit', ['user' => $user->id])
				->with('successMessages', new MessageBag([$this->translator->get('admin/users.successMessages.create')]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@create')
			->withErrors($this->userManager->getErrors())
			->withInput();
	}

	/**
	 * Shows the specified user.
	 *
	 * @param \Libek\LibekOrgRs\Models\User $user
	 * @return void
	 */
	public function edit(User $user)
	{
		$data = [
			'roleOptions' => $this->userManager->getRoleOptions(),
			'user' => $user,
		];

		$this->loadContent('admin.users.edit', $data);
	}

	/**
	 * Updates the specified user.
	 *
	 * @param \Libek\LibekOrgRs\Models\User $user
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(User $user)
	{
		if ($this->userManager->updateUser($user, $this->request->all()))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit', ['user' => $user->id])
				->with('successMessages', new MessageBag([$this->translator->get('admin/users.successMessages.edit')]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit', ['user' => $user->id])
			->withErrors($this->userManager->getErrors())
			->withInput();
	}

	/**
	 * Displays the user deletion confirmation form.
	 *
	 * @param \Libek\LibekOrgRs\Models\User $user
	 * @return void
	 */
	public function confirmDelete(User $user)
	{
		$data = [
			'user' => $user,
		];

		$this->loadContent('admin.users.delete', $data);
	}

	/**
	 * Deletes a user.
	 *
	 * @param \Libek\LibekOrgRs\Models\User $user
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(User $user)
	{
		if ($this->userManager->deleteUser($user, $this->request->all()))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index')
				->with('successMessages', new MessageBag([$this->translator->get('admin/users.successMessages.delete', ['fullName' => $user->full_name])]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index');
	}

}
