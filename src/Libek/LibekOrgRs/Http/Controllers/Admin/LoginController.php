<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use AssetManager;
use Illuminate\Http\Request;
use Libek\LibekOrgRs\Auth\Login\Manager as LoginManager;
use Libek\LibekOrgRs\Http\Controllers\AbstractBaseController;
use Redirect;
use URL;

class LoginController extends AbstractBaseController {

	/**
	 * The current request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A LoginManager instance.
	 *
	 * @var \Libek\LibekOrgRs\Auth\Login\Manager
	 */
	protected $loginManager;

	public function __construct(Request $request, LoginManager $loginManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->loginManager = $loginManager;

		$this->viewData->bodyDataPage = 'admin-login';
		$this->viewData->pageTitle->setPage('Admin login');
	}

	/**
	 * Gets the login form.
	 *
	 * @return void
	 */
	public function getLogin()
	{
		$this->loadContent('admin.login');
	}

	/**
	 * Attempts to log the user in, and redirects them to the admin panel home
	 * page if successful, or back to the login form otherwise.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postLogin()
	{
		if ($this->loginManager->login($this->request->all()))
		{
			$default = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\HomeController@index');

			return Redirect::intended($default);
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@getLogin')
			->withErrors($this->loginManager->getErrors())
			->withInput();
	}

	/**
	 * Logs the user out, and redirects them back to the login form.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postLogout()
	{
		$this->loginManager->logout();

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@getLogin')->with('loggedOut', true);
	}

	/**
	 * Sets the base assets (scripts and styles).
	 *
	 * @return void
	 */
	public function setBaseAssets()
	{
		AssetManager::loadAssetsForAdminLogin();
	}

}
