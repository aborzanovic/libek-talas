<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;
use Libek\LibekOrgRs\Auth\Password\Manager as PasswordManager;
use Libek\LibekOrgRs\Http\Controllers\Admin\AbstractBaseController;
use Redirect;

class PasswordController extends AbstractBaseController {

	/**
	 * The current request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A Translator instance.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $translator;

	/**
	 * A PasswordManager instance.
	 *
	 * @var \Libek\LibekOrgRs\Auth\Password\Manager
	 */
	protected $passwordManager;

	public function __construct(Request $request, Translator $translator, PasswordManager $passwordManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->translator = $translator;
		$this->passwordManager = $passwordManager;

		$this->viewData->bodyDataPage = 'admin-password';
		$this->viewData->pageTitle->setPage($this->translator->get('admin/changePassword.module'));
		$this->navigation->get('admin.main')->setActive('change-password');
	}

	/**
	 * Gets the password update form for the current user.
	 *
	 * @return void
	 */
	public function index()
	{
		$this->loadContent('admin.password.edit');
	}

	/**
	 * Updates the current user's password.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postUpdate()
	{
		if ( ! $this->passwordManager->update($this->request->all(), $this->currentUser))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@index')
				->withErrors($this->passwordManager->getErrors())
				->withInput();
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@index')
			->with('successMessages', new MessageBag([$this->translator->get('admin/changePassword.successMessages.update')]));
	}

}
