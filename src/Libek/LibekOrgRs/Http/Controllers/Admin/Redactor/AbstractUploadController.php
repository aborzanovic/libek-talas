<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin\Redactor;

use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;
use Libek\LibekOrgRs\Http\Controllers\Admin\AbstractBaseController;
use Psr\Log\LoggerInterface;

abstract class AbstractUploadController extends AbstractBaseController {

	/**
	 * The current request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A logger instance.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger;

	/**
	 * A Validation factory instance.
	 *
	 * @var \Illuminate\Validation\Factory
	 */
	protected $validator;

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $rules = [
		'upload' => [],
	];

	public function __construct(Request $request, LoggerInterface $logger, Validator $validator)
	{
		parent::__construct();

		$this->request = $request;
		$this->logger = $logger;
		$this->validator = $validator;
	}

	/**
	 * Uploads a file to the server, and returns its location.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	abstract public function store();

	/**
	 * Gets the upload validator.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getUploadValidator($inputData)
	{
		return $this->validator->make($inputData, $this->rules['upload']);
	}

}
