<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin\Redactor;

use Exception;
use Libek\LibekOrgRs\Http\Controllers\Admin\Redactor\AbstractUploadController;
use Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileController extends AbstractUploadController {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'upload' => [
			'file' => ['required', 'mimes:pdf,doc,docx,xls,xlsx,odt,ods,txt,csv,zip,rar,tar,gz,tgz', 'max:10240'],
		],
	];

	/**
	 * Uploads a file to the server, and returns its location.
	 *
	 * Returns a JSON response containing the file's URL in a `filelink`
	 * attribute, and its name in a `filename` attribute, which is what Redactor
	 * expects.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store()
	{
		$validator = $this->getUploadValidator($this->request->all());

		if ($validator->fails())
		{
			return Response::json(['error' => 'Bad input data'], 400);
		}

		try
		{
			$file = $this->request->file('file');

			$parameters = $this->getDestination($file);

			$file->move($parameters['destination'], $parameters['name']);

			$data = [
				'filelink' => $parameters['fileHref'],
				'filename' => $file->getClientOriginalName(),
			];

			return Response::json($data);
		}
		catch (FileException $e)
		{
			$this->logger->error($e);

			return Response::json(['error' => 'Unable to move the uploaded file'], 500);
		}
		catch (Exception $e)
		{
			$this->logger->error($e);

			return Response::json(['error' => 'Bad input data'], 400);
		}
	}

	/**
	 * Generates a new filename for uploading files.
	 *
	 * Returns an array with the following keys:
	 *
	 * - `destination`: the path where the file should be moved
	 * - `name`: the name that should be given to the file
	 * - `fileHref`: the URL of the file on the server (relative to the domain
	 *    root)
	 *
	 * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
	 * @return array
	 */
	protected function getDestination(UploadedFile $file)
	{
		$path = '/uploads/files/';

		$randomFileName = time() . '.' . str_random(16) . '.' . $file->guessExtension();

		return [
			'destination' => public_path() . $path,
			'name' => $randomFileName,
			'fileHref' => $path . $randomFileName,
		];
	}

}
