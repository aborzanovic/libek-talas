<?php namespace Libek\LibekOrgRs\Http\Controllers\Admin;

use Creitive\Routing\LocaleResolver;
use Illuminate\Config\Repository as Config;
use Illuminate\Http\Request;
use Illuminate\Pagination\Factory as Paginator;
use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;
use Libek\LibekOrgRs\News\Article;
use Libek\LibekOrgRs\News\Article\Manager as ArticleManager;
use Libek\LibekOrgRs\Http\Controllers\Admin\AbstractBaseController;
use Redirect;
use DB;

class NewsArticleController extends AbstractBaseController {

	/**
	 * The current request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A Config repository instance.
	 *
	 * @var \Illuminate\Config\Repository
	 */
	protected $config;

	/**
	 * A Translator instance.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $translator;

	/**
	 * A Pagination factory instance.
	 *
	 * @var \Illuminate\Pagination\Factory
	 */
	protected $paginator;

	/**
	 * A LocaleResolver instance.
	 *
	 * @var \Creitive\Routing\LocaleResolver
	 */
	protected $localeResolver;

	/**
	 * An ArticleManager instance.
	 *
	 * @var \Libek\LibekOrgRs\News\Article\Manager
	 */
	protected $articleManager;

	public function __construct(Request $request, Config $config, Translator $translator, Paginator $paginator, LocaleResolver $localeResolver, ArticleManager $articleManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->config = $config;
		$this->translator = $translator;
		$this->paginator = $paginator;
		$this->localeResolver = $localeResolver;
		$this->articleManager = $articleManager;

		$this->viewData->bodyDataPage = 'admin-news-articles';
		$this->viewData->pageTitle->setPage($this->translator->get('admin/newsArticles.module'));
		$this->navigation->get('admin.main')->setActive('news-articles');
	}

	/**
	 * Shows all articles.
	 *
	 * Optionally filters by city and/or category, depending on the passed
	 * input.
	 *
	 * @return void
	 */
	public function index()
	{
		$perPage = $this->request->get('perPage', $this->config->get('admin.perPage'));

		$articles = $this->articleManager->getArticles(array_merge($this->request->all(), ['perPage' => $perPage]));
		$total = $this->articleManager->getLastTotalRowCount();
		$pagination = $this->paginator->make($articles->toArray(), $total, $perPage);

		$data = [
			'articles' => $articles,
			'pagination' => $pagination,
		];

		$this->loadContent('admin.news-articles.index', $data);
	}

	/**
	 * Displays the article create form.
	 *
	 * @return void
	 */
	public function create()
	{
		$data = [
			'languageOptions' => $this->getLanguageOptions(),
			'tagOptions' => $this->getAvailableTags(),
		];

		$this->loadContent('admin.news-articles.create', $data);
	}

	/**
	 * Saves a new article.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		$articleData = $this->request->all();
		$articleData['user_id'] = $this->currentUser->id;

		if ($article = $this->articleManager->createArticle($articleData))
		{
			if (! isset($articleData['tags'])) {
				$articleData['tags'] = [];
			}
			$this->resyncTags($articleData['tags'], $article);

			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit', ['article' => $article->id])
				->with('successMessages', new MessageBag([$this->translator->get('admin/newsArticles.successMessages.create')]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@create')
			->withErrors($this->articleManager->getErrors())
			->withInput();
	}

	/**
	 * Shows the specified article.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return void
	 */
	public function edit(Article $article)
	{
		$data = [
			'article' => $article,
			'languageOptions' => $this->getLanguageOptions(),
			'tagOptions' => $this->getAvailableTags(),
			'tagSelected' => $this->getSelectedTags($article),
		];

		$this->loadContent('admin.news-articles.edit', $data);
	}

	/**
	 * Updates the specified article.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Article $article)
	{
		$articleData = $this->request->all();
		$articleData['user_id'] = $this->currentUser->id;
		if ($this->articleManager->updateArticle($article, $articleData))
		{
			if (! isset($articleData['tags'])) {
				$articleData['tags'] = [];
			}
			$this->resyncTags($articleData['tags'], $article);

			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit', ['article' => $article->id])
				->with('successMessages', new MessageBag([$this->translator->get('admin/newsArticles.successMessages.edit')]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit', ['article' => $article->id])
			->withErrors($this->articleManager->getErrors())
			->withInput();
	}

	/**
	 * Displays the article deletion confirmation form.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return void
	 */
	public function confirmDelete(Article $article)
	{
		$data = [
			'article' => $article,
		];

		$this->loadContent('admin.news-articles.delete', $data);
	}

	/**
	 * Deletes an article.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(Article $article)
	{
		if ($this->articleManager->deleteArticle($article, $this->request->all()))
		{
			$this->resyncTags([], $article);

			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index')
				->with('successMessages', new MessageBag([$this->translator->get('admin/newsArticles.successMessages.delete', ['title' => $article->title])]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index');
	}

	/**
	 * Returns the available language options.
	 *
	 * @return array
	 */
	protected function getLanguageOptions()
	{
		$languageOptions = [];

		foreach ($this->localeResolver->getAvailableLocales() as $properties)
		{
			$languageOptions[$properties['language']] = $properties['name'];
		}

		return $languageOptions;
	}

	protected function getAvailableTags()
	{
		$options = [];
		foreach (DB::table('tags')->get() as $val) {
			$options[$val->name] = $val->name;
		}

		return $options;
	}

	protected function getSelectedTags($article) {
		$options = [];
		foreach (DB::table('tags as t')
			->join('news_article_tag as nt', 'nt.tag_id', '=', 't.id')
			->where('nt.news_article_id', $article->id)->get() as $val) {
			$options[] = $val->name;
		}

		return $options;
	}

	protected function resyncTags($tags, $article) {
		DB::table('news_article_tag')->where('news_article_id', $article->id)->delete();

		foreach ($tags as $tag) {
			$existingTag = DB::table('tags')->where('name', 'like', "$tag")->first();
			if (! $existingTag) {
				$existingTag = DB::table('tags')->insertGetId(['name' => $tag]);
			}
			else {
				$existingTag = $existingTag->id;
			}
			DB::table('news_article_tag')->insert([
				'tag_id' => $existingTag,
				'news_article_id' => $article->id
			]);
		}
	}
}
