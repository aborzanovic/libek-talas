<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;

class PPController extends AbstractBaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'individual-policy';
        $this->viewData->pageTitle->setPage(Lang::get('navigation.individual-policy'));
    }

    /**
     * Gets the about page.
     *
     * @return void
     */
    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('individual-policy');
    }

}