<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 2/22/2016
 * Time: 9:15 PM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use \DrewM\MailChimp\MailChimp;
use Illuminate\Http\Request;
use Validator;

class NewsletterController extends AbstractBaseController
{

    protected $request;

    public function __construct(Request $request)
	{
		parent::__construct();
		$this->request = $request;
	}


    public function subscribe()
    {
        $validator = Validator::make(
            $this->request->all(),
            array('email' => 'required|email')
        );
        if ($validator->fails()) {
            return "Morate uneti email adresu!";
        }
        else {
            $MailChimp = new MailChimp(getenv('MAILCHIMP_API_KEY'));

            $list_id = getenv('MAILCHIMP_LIST_TALAS');
            $result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => $this->request->input('email'),
                'status'        => 'subscribed',
            ]);
            return $result['status'] === 'subscribed' ? "SUCC" : "Došlo je do greške!";
        }
    }


}