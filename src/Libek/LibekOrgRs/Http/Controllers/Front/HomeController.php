<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\News\Article;
use Libek\LibekOrgRs\Auth\User;
use Libek\LibekOrgRs\News\Article\Repository as NewsArticleRepository;
use Libek\LibekOrgRs\Slideshow\Image\Repository as SlideshowImagesRepository;
use Pagination;
use Response;
use URL;
use Validator;
use DB;

class HomeController extends AbstractBaseController {

	protected $slideshowImagesRepository;
	protected $newsArticleRepository;
	protected $newsNum = 12;

	public function __construct(NewsArticleRepository $newsArticleRepository, SlideshowImagesRepository $slideshowImagesRepository)
	{
		parent::__construct();

		$this->newsArticleRepository = $newsArticleRepository;
		$this->slideshowImagesRepository = $slideshowImagesRepository;

		$this->viewData->bodyDataPage = 'home';
		$this->viewData->pageTitle->setTagline(Lang::get('common.homeTagline'));
		$this->viewData->headerContainerHasBackground = false;
	}

	/**
	 * Gets the website homepage.
	 *
	 * @return void
	 */
	public function getIndex($offset = 0)
	{
		$slideshowArticles = $this->getSlideshowArticles();
		$mostPopularArticles = $this->getMostPopularArticles();
		$mostPopularAuthors = [];
		$mostPopularTags = [];
		$searchTags = [];

		foreach($slideshowArticles as $art) {
			$mostPopularAuthors[$art->id] = User::find($art->user_id);
			$mostPopularTags[$art->id] = $this->getSelectedTags($art);
		}

		$searchTags = $this->getAvailableTags();
		$tagOptions = $this->getAvailableTags();

		$data = [
			'articles' => $this->getHomeNews(),
			'slideshowArticles' => $slideshowArticles,
			'mostPopularArticles' => $mostPopularArticles->chunk(3),
			'mostPopularArticle' => $mostPopularArticles->first(),
			'listingTitle' => ucfirst(Lang::get("home.news")),
			'urlGenerator' => $this->getUrlGenerator(),
			'mostPopularAuthors' => $mostPopularAuthors,
			'mostPopularTags' => $mostPopularTags,
			'searchTags' => $searchTags,
			'tagOptions' => $tagOptions
		];
		$this->loadContent('home', $data);
	}

	protected function getHomeNews()
	{
		return $this->newsArticleRepository->getPage(0, null, null, $this->newsNum, $this->currentLanguage);
	}

	protected function getSlideshowArticles() {
		return $this->newsArticleRepository->getSlideshowArticles();
	}

	protected function getMostPopularArticles()
	{
		return $this->newsArticleRepository->getMostPopularArticles(6, $this->currentLanguage);
	}

	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getArticle',
				[
					'year' => $article->published_at->format('Y'),
					'month' => $article->published_at->format('m'),
					'day' => $article->published_at->format('d'),
					'slug' => $article->slug,
				]
			);
		};
	}

	protected function getSelectedTags($article) {
		$options = [];
		foreach (DB::table('tags as t')
			->join('news_article_tag as nt', 'nt.tag_id', '=', 't.id')
			->select(
				't.name as name',
				't.id as id'
			)
			->where('nt.news_article_id', $article->id)->get() as $val) {
			$options[] = (object) ['name' => $val->name, 'id' => $val->id];
		}

		return $options;
	}

	protected function getAvailableTags()
	{
		$options = [];
		foreach (DB::table('tags')->get() as $val) {
			$options[$val->id] = $val->name;
		}

		return $options;
	}
}
