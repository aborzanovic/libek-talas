<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 2/29/2016
 * Time: 9:02 PM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;
use Libek\LibekOrgRs\TeamMembers\Person;
use Libek\LibekOrgRs\TeamMembers\Person\Repository as teamMembersRepository;


class TeamController extends AbstractBaseController
{
    /*
     *  A team memebers repository instance
     *
     */

    protected $teamMembersRepository;
    protected $perPage = 12;

    public function __construct(teamMembersRepository $teamMembersRepository)
    {
        parent::__construct();

        $this->teamMembersRepository = $teamMembersRepository;

        $this->viewData->bodyDataPage = 'team';
        $this->viewData->pageTitle->setPage(Lang::get('navigation.team'));
    }


    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $persons = $this->teamMembersRepository->getPersons(0, $this->perPage);

        $data = [
                'persons' => $persons
        ];

        $this->loadContent('team', $data);
    }


}