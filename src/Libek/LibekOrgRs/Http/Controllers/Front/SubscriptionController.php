<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\Managers\SubscriptionManager;
use Redirect;
use Session;

class SubscriptionController extends AbstractBaseController {

	/**
	 * An instance of the current request.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * An instance of the subscription manager.
	 *
	 * @var \Libek\LibekOrgRs\Managers\SubscriptionManager
	 */
	protected $subscriptionManager;

	public function __construct(Request $request, SubscriptionManager $subscriptionManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->subscriptionManager = $subscriptionManager;

		$this->viewData->bodyDataPage = 'subscriptions';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.subscriptions'));
		$this->viewData->headerContainerHasBackground = false;

		$this->viewData->successMessages = Session::get('successMessages', new MessageBag);
	}

	/**
	 * Gets the subscription form for volunteers for the 2014 Taxpayers' March.
	 *
	 * @return void
	 */
	public function getVolunteerForm()
	{
		$this->loadContent('subscriptions.volunteers.index');
	}

	/**
	 * Posts the contact form.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postVolunteerForm()
	{
		if ($this->subscriptionManager->subscribeVolunteer($this->request->all()))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\SubscriptionController@getVolunteerForm')
				->with('successMessages', new MessageBag([Lang::get('subscriptions.success')]));
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\SubscriptionController@getVolunteerForm')
			->withErrors($this->subscriptionManager->getValidator())
			->withInput();
	}

}
