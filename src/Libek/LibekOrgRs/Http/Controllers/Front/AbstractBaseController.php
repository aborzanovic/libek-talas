<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use AssetManager;
use Libek\LibekOrgRs\Http\Controllers\AbstractBaseController as RootAbstractBaseController;
use View;

abstract class AbstractBaseController extends RootAbstractBaseController {

	/**
	 * {@inheritDoc}
	 */
	protected $layout = 'layouts.front';

	/**
	 * {@inheritDoc}
	 */
	protected function loadContent($partial, $data = [])
	{
		View::inject('content', View::make($partial, $data)->render());
	}

	/**
	 * {@inheritDoc}
	 */
	protected function setBaseAssets()
	{
		AssetManager::loadAssetsForFront();
	}

}
