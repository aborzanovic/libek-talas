<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\Projects\Article;
use Libek\LibekOrgRs\Projects\Article\Repository as ProjectArticleRepository;
use URL;

class ProjectArticleController extends AbstractBaseController {

	/**
	 * A Project article repository instance.
	 *
	 * @var \Libek\LibekOrgRs\Projects\Article\Repository
	 */
	protected $projectArticleRepository;

	protected $recommendedNum = 4;

	public function __construct(ProjectArticleRepository $projectArticleRepository)
	{
		parent::__construct();

		$this->projectArticleRepository = $projectArticleRepository;

		$this->viewData->bodyDataPage = 'projects';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.projects'));
	}

	/**
	 * Gets all project articles.
	 *
	 * @return void
	 */
	public function getIndex()
	{
		$articles = $this->projectArticleRepository->getAll($this->currentLanguage);

		$data = [
			'articles' => $articles,
			'listingTitle' => ucfirst(Lang::get("articles.projects")),
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'urlGenerator' => $this->getUrlGenerator(),
		];

		$this->loadContent('project-articles/listing', $data);
	}

	/**
	 * Gets a specific project article.
	 *
	 * @param string $slug
	 * @return void
	 */
	public function getArticle($slug)
	{
		$article = $this->projectArticleRepository->getArticle($slug, $this->currentLanguage);
		$articles = $this->projectArticleRepository->getAll(0, $this->recommendedNum, $this->currentLanguage)->slice(0,$this->recommendedNum, true);

		$data = [
			'article' => $article,
			'left_recommended' => $articles->slice(0,1, true),
			'bottom_recommended' => $articles->slice(1,3, true),
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'urlGenerator' => $this->getUrlGenerator(),
		];

		$this->setArticleMeta($article);

		$this->loadContent('project-articles/single', $data);
	}

	/**
	 * Gets a closure for generating social plugin containers.
	 *
	 * @return \Closure
	 */
	protected function getSocialPluginsGenerator()
	{
		return function($url)
		{
			$socialPlugins = new \Creitive\SocialPlugins\SocialPlugins($url);

			$socialPlugins->addPlugins(
				new \Creitive\SocialPlugins\Twitter\Tweet,
				new \Creitive\SocialPlugins\Facebook\Like
			);

			return $socialPlugins;
		};
	}

	/**
	 * Gets a closure for generating project article URLs.
	 *
	 * @return \Closure
	 */
	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\ProjectArticleController@getArticle',
				[
					'slug' => $article->slug,
				]
			);
		};
	}

	/**
	 * Sets the various meta properties for an article.
	 *
	 * @param \Creitive\Database\Eloquent\Model $article
	 */
	protected function setArticleMeta($article)
	{
		if ($article->meta_description)
		{
			$this->viewData->meta->description = $article->meta_description;
			$this->viewData->meta->og['og:description'] = $article->meta_description;
		}
		else
		{
			$this->viewData->meta->description = strip_tags($article->contents);
			$this->viewData->meta->og['og:description'] = strip_tags($article->contents);
		}

		if ($article->meta_title)
		{
			$this->viewData->pageTitle->setPage($article->meta_title);
			$this->viewData->meta->og['og:title'] = $article->meta_title;
		}
		else
		{
			$this->viewData->pageTitle->setPage($article->title);
			$this->viewData->meta->og['og:title'] = $article->title;
		}

		$this->viewData->meta->og['og:image'] = $article->getImage('main', 'og');
		$this->viewData->meta->og['og:type'] = 'article';
	}

}
