<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Creitive\Routing\LocaleResolver;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Redirect;

class LanguageRedirectController extends AbstractBaseController {

	/**
	 * A LocaleResolver instance.
	 *
	 * @var \Creitive\Routing\LocaleResolver
	 */
	protected $localeResolver;

	public function __construct(LocaleResolver $localeResolver)
	{
		$this->localeResolver = $localeResolver;
	}

	/**
	 * Redirects the user to the default locale version of the application.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function getRedirect()
	{
		return Redirect::to('/'.$this->localeResolver->getLanguage());
	}

}
