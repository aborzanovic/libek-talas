<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/11/2016
 * Time: 2:01 AM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lang;
use Libek\LibekOrgRs\JohnGalt\Article;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;
use Libek\LibekOrgRs\JohnGalt\Article\Repository as JohnGaltRepository;
use Libek\LibekOrgRs\Slideshow\Image\Repository as SlideshowImagesRepository;
use Pagination;
use Response;
use URL;
use Validator;
use Zend\Feed\Writer\Feed;

class JohnGaltController extends AbstractBaseController
{
    protected $galtArticleRepository;

    protected $perPage = 6;

    protected $subdomain = 'johngalt';

    public function __construct(SlideshowImagesRepository $slideshowImagesRepository, JohnGaltRepository $galtArticleRepository )
    {
        parent::__construct();

        $this->slideshowImagesRepository = $slideshowImagesRepository;
        $this->galtArticleRepository = $galtArticleRepository;

        $this->viewData->bodyDataPage = 'john-galt';
        $this->viewData->pageTitle->setPage('John Galt');
    }

    /**
     * Gets the about page.
     *
     * @return void
     */
    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $data = [
            'backgroundImages' => $this->slideshowImagesRepository->getImages('johngalt')
        ];
        $this->loadContent('john-galt', $data);
    }

    public function getQA()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('john-galt.Q&A.q&a');
    }

    public function getWorks()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('john-galt.works.works');
    }

    public function getBlog()
    {
        return $this->getPage(0);


       // $this->loadContent('john-galt.blog.blog',$data);
    }

    public function getPage($page)
    {
        $page = (int) $page;

        $articles = $this->galtArticleRepository->getPage($page, $this->perPage, $this->currentLanguage);
        $pagination = Pagination::get(
            $page,
            $this->galtArticleRepository->getLastTotalRowCount(),
            $this->perPage,
            URL::action('Libek\LibekOrgRs\Http\Controllers\Front\JohnGaltController@getBlog')
        );

        $data = [
            'articles' => $articles,
            'listingTitle' => ucfirst(Lang::get("articles.blog")),
            'pagination' => $pagination,
            'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
            'urlGenerator' => $this->getUrlGenerator(),
        ];


        $this->loadContent('john-galt.blog.blog',$data);
        //$this->loadContent('news-articles/listing', $data);
    }

    public function getArticle($year, $month, $day, $slug)
    {
        $validator = $this->getDateValidator($year, $month, $day);

        if ($validator->fails())
        {
            throw new ModelNotFoundException;
        }

        $article = $this->galtArticleRepository->getArticle($year, $month, $day, $slug, $this->currentLanguage);
        //$articles = $this->galtArticleRepository->getPage(0, $this->recommendedNum, $this->currentLanguage);

        $data = [
            'article' => $article,
            'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
            'urlGenerator' => $this->getUrlGenerator(),
        ];

        $this->setArticleMeta($article);

        $this->loadContent('news-articles/single', $data);
    }

    protected function getSocialPluginsGenerator()
    {
        return function($url)
        {
            $socialPlugins = new \Creitive\SocialPlugins\SocialPlugins($url);

            $socialPlugins->addPlugins(
                new \Creitive\SocialPlugins\Twitter\Tweet,
                new \Creitive\SocialPlugins\Facebook\Like
            );

            return $socialPlugins;
        };
    }

    protected function getUrlGenerator()
    {
        return function(Article $article)
        {
            return URL::action(
                'Libek\LibekOrgRs\Http\Controllers\Front\JohnGaltController@getArticle',
                [
                    'year' => $article->published_at->format('Y'),
                    'month' => $article->published_at->format('m'),
                    'day' => $article->published_at->format('d'),
                    'slug' => $article->slug,
                ]
            );
        };
    }

    /**
     * Gets a validator for the date.
     *
     * @param string $year
     * @param string $month
     * @param string $day
     * @return \Libek\LibekOrgRs\Validation\Validator
     */
    protected function getDateValidator($year, $month, $day)
    {
        return Validator::make(
            [
                'date' => "{$year}-{$month}-{$day}",
            ],
            [
                'date' => 'custom_date_format',
            ]
        );
    }

}