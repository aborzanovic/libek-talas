<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\News\Article\Repository as NewsArticleRepository;
use Libek\LibekOrgRs\News\Article;
use View;
use DB;
use URL;

class AboutUsController extends AbstractBaseController {

	protected $newsArticleRepository;

	public function __construct(NewsArticleRepository $newsArticleRepository)
	{
		parent::__construct();

		$this->newsArticleRepository = $newsArticleRepository;

		$this->viewData->bodyDataPage = 'about-us';
		$this->viewData->pageTitle->setPage("O Talasu");
	}

	/**
	 * Gets the about page.
	 *
	 * @return void
	 */
	public function getIndex()
	{
		$this->viewData->headerContainerHasBackground = false;

		$mostPopularArticles = $this->getMostPopularArticles();

		$tagOptions = $this->getAvailableTags();

		$data = [
			'mostPopularArticles' => $mostPopularArticles->chunk(3),
			'mostPopularArticle' => $mostPopularArticles->first(),
			'urlGenerator' => $this->getUrlGenerator(),
			'tagOptions' => $tagOptions
		];

		$this->loadContent('about-us', $data);
	}


	protected function getAvailableTags()
	{
		$options = [];
		foreach (DB::table('tags')->get() as $val) {
			$options[$val->id] = $val->name;
		}

		return $options;
	}

	protected function getMostPopularArticles()
	{
		return $this->newsArticleRepository->getMostPopularArticles(6, $this->currentLanguage);
	}

	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getArticle',
				[
					'year' => $article->published_at->format('Y'),
					'month' => $article->published_at->format('m'),
					'day' => $article->published_at->format('d'),
					'slug' => $article->slug,
				]
			);
		};
	}



	/**
	 * Gets the Executive Board page.
	 *
	 * @return void
	 */
	public function getExecutiveBoard()
	{
		$this->getSubpage('executive-board');
	}

	/**
	 * Gets the Communication and Protocol page.
	 *
	 * @return void
	 */
	public function getCommunicationAndProtocol()
	{
		$this->getSubpage('communication-and-protocol');
	}

	/**
	 * Gets the Research Department page.
	 *
	 * @return void
	 */
	public function getResearchDepartment()
	{
		$this->getSubpage('research-department');
	}

	/**
	 * Gets a specific about us subpage.
	 *
	 * @param string $page
	 * @return void
	 */
	protected function getSubpage($page)
	{
		$this->viewData->bodyDataPage = $page;

		$data = [
			'subpageContent' => View::make("about-us/{$page}.{$this->currentLocale}")->render(),
		];

		$this->loadContent('about-us/subpage', $data);
	}

}
