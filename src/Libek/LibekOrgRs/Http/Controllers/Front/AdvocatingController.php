<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/13/2016
 * Time: 7:15 PM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;

class AdvocatingController extends AbstractBaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'advocating';
        $this->viewData->pageTitle->setPage(Lang::get('navigation.advocating'));
    }

    /**
     * Gets the about page.
     *
     * @return void
     */
    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('advocating.advocating');
    }
}