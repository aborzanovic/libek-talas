<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Lang;
use Libek\LibekOrgRs\Applications\ApplicationForm\Manager as ApplicationFormManager;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Redirect;
use Session;

class ApplicationFormController extends AbstractBaseController {

	/**
	 * An instance of the current request.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * An instance of the application form manager.
	 *
	 * @var \Libek\LibekOrgRs\Managers\ApplicationFormManager
	 */
	protected $applicationFormManager;

	public function __construct(Request $request, ApplicationFormManager $applicationFormManager)
	{
		parent::__construct();

		$this->request = $request;
		$this->applicationFormManager = $applicationFormManager;

		$this->viewData->bodyDataPage = 'application-form';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.applicationForms'));
		$this->viewData->headerContainerHasBackground = false;

		$this->viewData->successMessages = Session::get('successMessages', new MessageBag);
	}

	/**
	 * Gets the requested application form.
	 *
	 * @param string $slug
	 * @return void
	 */
	public function getApplicationForm($slug)
	{
		$applicationForm = $this->applicationFormManager->getApplicationForm($this->currentLanguage, $slug);

		$data = [
			'applicationForm' => $applicationForm,
		];

		if ($applicationForm->isActive())
		{
			$this->loadContent('application-forms.'.$applicationForm->type.'.index', $data);
		}
		else
		{
			$this->loadContent('application-forms.'.$applicationForm->type.'.expired', $data);
		}
	}

	/**
	 * Posts the application form.
	 *
	 * @param string $slug
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postApplicationForm($slug)
	{
		$applicationForm = $this->applicationFormManager->getApplicationForm($this->currentLanguage, $slug);

		if ( ! $applicationForm->isActive())
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@getApplicationForm', ['slug' => $slug]);
		}

		if ($this->applicationFormManager->saveSubmittedApplication($applicationForm, $this->request->all()))
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@getApplicationForm', ['slug' => $slug])
				->with('successMessages', $this->applicationFormManager->getSuccessMessages());
		}

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@getApplicationForm', ['slug' => $slug])
			->withErrors($this->applicationFormManager->getValidator())
			->withInput();
	}

}
