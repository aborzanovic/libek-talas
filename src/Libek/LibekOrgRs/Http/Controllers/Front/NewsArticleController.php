<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use View;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\News\Article;
use Libek\LibekOrgRs\News\Article\Repository as NewsArticleRepository;
use Libek\LibekOrgRs\Auth\User;
use Pagination;
use Response;
use URL;
use DB;
use Validator;
use Zend\Feed\Writer\Feed;

class NewsArticleController extends AbstractBaseController {

	/**
	 * A News article repository instance.
	 *
	 * @var \Libek\LibekOrgRs\Repository\NewsArticleRepository
	 */
	protected $newsArticleRepository;
	protected $request;

	/**
	 * How many articles to list per page.
	 *
	 * @var integer
	 */
	protected $perPage = 12;
	protected $recommendedNum = 4;

	public function __construct(Request $request, NewsArticleRepository $newsArticleRepository)
	{
		parent::__construct();

		$this->newsArticleRepository = $newsArticleRepository;

		$this->viewData->bodyDataPage = 'news';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.news'));
		$this->viewData->feedUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getRssFeed');
		$this->request = $request;
	}

	/**
	 * Gets the first page of news articles.
	 *
	 * @return void
	 */
	public function getIndex()
	{
		return $this->getPage(0);
	}

	/**
	 * Gets a specific page of news articles.
	 *
	 * @param string $page
	 * @return void
	 */
	protected function pageResponse($articles, $page, $author = null, $tag = null, $search = null)
	{
		$pagination = Pagination::get(
			$page,
			$this->newsArticleRepository->getLastTotalRowCount(),
			$this->perPage,
			URL::action('Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getIndex')
		);

		if (! is_null($author)) {
			$author = User::find($author);
			$author = (object) ['name' => $author->first_name . ' ' . $author->last_name, 'id' => $author->id];
		}

		if (! is_null($tag)) {
			$tag = DB::table('tags')->where('id', $tag)->first();
		}

		$searchTags = $this->getAvailableTags();
		$tagOptions = $this->getAvailableTags();

		$mostPopularArticles = $this->newsArticleRepository->getMostPopularArticles(1, $this->currentLanguage);

		$data = [
			'articles' => $articles,
			'listingTitle' => ucfirst(Lang::get("articles.news")),
			'pagination' => $pagination,
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'urlGenerator' => $this->getUrlGenerator(),
			'page' => $pagination->getCurrentPage(),
			'totalPages' => $pagination->getTotalPages(),
			'listingType' => is_null($author) ? (is_null($tag) ? (is_null($search) ? 'none' : 'search') : 'tag') : 'author',
			'listingAuthor' => $author,
			'listingTag' => $tag,
			'searchTags' => $searchTags,
			'tagOptions' => $tagOptions,
			'mostPopularArticle' => $mostPopularArticles->first()
		];
		/*
		if ($page === 0 && is_null($author) && is_null($tag) && is_null($search)) {
			$mostPopularArticles = $this->newsArticleRepository->getMostPopularArticles(6, $this->currentLanguage);
			$data['mostPopularArticles'] = $mostPopularArticles->chunk(3);
			$data['mostPopularArticle'] = $mostPopularArticles->slice(0, 1, true);
		}
		else {
			$data['mostPopularArticle'] = $this->newsArticleRepository->getMostPopularArticles(1, $this->currentLanguage);
		}*/

		
		return $data;
	}
	public function page($page, $author = null, $tag = null)
	{
		$page = (int) $page;

		$articles = $this->newsArticleRepository->getPage($page, $author, $tag, $this->perPage, $this->currentLanguage);
		
		return $this->pageResponse($articles, $page, $author, $tag);
	}

	public function getPage($page, $author = null, $tag = null)
	{
		$this->loadContent('news-articles/listing', $this->page($page, $author, $tag));
	}

	public function getPageAjax($page, $author = null, $tag = null)
	{
		return View::make('article-listing-ajax', $this->page($page, $author, $tag));
	}

	public function getByTag($tag)
	{
		return $this->getPage(0, null, $tag);
	}

	public function getByTagAjax($tag, $page)
	{
		return $this->getPageAjax($page, null, $tag);
	}

	public function getByAuthor($author)
	{
		return $this->getPage(0, $author, null);
	}

	public function getByAuthorAjax($author, $page)
	{
		return $this->getPageAjax($page, $author, null);
	}

	public function getBySearch()
	{
		$articles = $this->newsArticleRepository->searchArticlesByTags($this->request->input('tags'), 0, $this->perPage, $this->currentLanguage);
		
		$this->loadContent('news-articles/listing', $this->pageResponse($articles, 0, null, null, ''));
	}

	public function getBySearchAjax($page)
	{
		$page = (int) $page;
		
		$articles = $this->newsArticleRepository->searchArticlesByTags($this->request->input('tags'), $page, $this->perPage, $this->currentLanguage);
		return View::make('article-listing-ajax', $this->pageResponse($articles, $page, null, null, ''));
	}

	/**
	 * Gets a specific news article.
	 *
	 * @param string $year
	 * @param string $month
	 * @param string $day
	 * @param string $slug
	 * @return void
	 */
	public function getArticle($year, $month, $day, $slug)
	{
		$validator = $this->getDateValidator($year, $month, $day);

		if ($validator->fails())
		{
			throw new ModelNotFoundException;
		}

		$article = $this->newsArticleRepository->getArticle($year, $month, $day, $slug, $this->currentLanguage);
		$articles = $this->newsArticleRepository->getPage(0, null, null, $this->recommendedNum, $this->currentLanguage);
		$author = User::find($article->user_id);
		$a = (object) ['name' => $author->first_name . ' ' . $author->last_name, 'id' => $author->id];
		$tags = $this->getSelectedTags($article);
		$tagsIds = array_map(function($val) { return $val->id; }, $tags);
		$tagOptions = $this->getAvailableTags();


		$mostPopularArticles = $this->newsArticleRepository->getMostPopularArticles(1, $this->currentLanguage);


		$data = [
			'article' => $article,
			'articles' => $articles,
			'author' => $a,
			'nextArticle' => $this->newsArticleRepository->getNextArticle($article->id, $this->currentLanguage),
			'prevArticle' => $this->newsArticleRepository->getPrevArticle($article->id, $this->currentLanguage),
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'recommended' => $this->newsArticleRepository->searchArticlesByTags($tagsIds, 0, 5, $this->currentLanguage, [ $article->id ]),
			'urlGenerator' => $this->getUrlGenerator(),
			'tags' => $tags,
			'mostPopularArticle' => $mostPopularArticles->first(),
			'tagOptions' => $tagOptions
		];

		DB::table('news_articles')->where('id', $article->id)->increment('view_count');

		$this->setArticleMeta($article);

		$this->loadContent('news-articles/single', $data);
	}

	/**
	 * Returns an RSS 2.0 feed of the last 20 news articles.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRssFeed()
	{
		$articles = $this->newsArticleRepository->getPage(0, 20, $this->currentLanguage);
		$urlGenerator = $this->getUrlGenerator();

		$feed = new Feed;

		$feed->setTitle(Lang::get('common.siteName'));
		$feed->setLink(URL::action('Libek\LibekOrgRs\Http\Controllers\Front\HomeController@getIndex'));
		$feed->setFeedLink(URL::action('Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getRssFeed'), 'rss');
		$feed->setDescription(Lang::get('common.metaDescription'));

		foreach ($articles as $article)
		{
			$entry = $feed->createEntry();

			$entry->setTitle($article->title);
			$entry->setLink($urlGenerator($article));
			$entry->setDateCreated($article->published_at);

			$entry->setDescription('<img src="'.URL::to($article->getImage('main', 'full')).'">'.$article->lead);

			$entry->setContent($article->contents);
			$entry->addCategories(
				[
					[
						'term' => Lang::get('routes.news'),
						'label' => Lang::get('navigation.news'),
					],
				]
			);

			$feed->addEntry($entry);
		}

		return Response::make($feed->export('rss'))
			->header('Content-Type', 'application/rss+xml;charset=UTF-8');
	}

	/**
	 * Gets a closure for generating social plugin containers.
	 *
	 * @return \Closure
	 */
	protected function getSocialPluginsGenerator()
	{
		return function($url)
		{
			$socialPlugins = new \Creitive\SocialPlugins\SocialPlugins($url);

			$socialPlugins->addPlugins(
				new \Creitive\SocialPlugins\Twitter\Tweet,
				new \Creitive\SocialPlugins\Facebook\Like
			);

			return $socialPlugins;
		};
	}

	/**
	 * Gets a closure for generating news article URLs.
	 *
	 * @return \Closure
	 */
	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getArticle',
				[
					'year' => $article->published_at->format('Y'),
					'month' => $article->published_at->format('m'),
					'day' => $article->published_at->format('d'),
					'slug' => $article->slug,
				]
			);
		};
	}

	/**
	 * Gets a validator for the date.
	 *
	 * @param string $year
	 * @param string $month
	 * @param string $day
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getDateValidator($year, $month, $day)
	{
		return Validator::make(
			[
				'date' => "{$year}-{$month}-{$day}",
			],
			[
				'date' => 'custom_date_format',
			]
		);
	}

	protected function getSelectedTags($article) {
		$options = [];
		foreach (DB::table('tags as t')
			->join('news_article_tag as nt', 'nt.tag_id', '=', 't.id')
			->select(
				't.name as name',
				't.id as id'
			)
			->where('nt.news_article_id', $article->id)->get() as $val) {
			$options[] = (object) ['name' => $val->name, 'id' => $val->id];
		}

		return $options;
	}

	protected function getAvailableTags()
	{
		$options = [];
		foreach (DB::table('tags')->get() as $val) {
			$options[$val->id] = $val->name;
		}

		return $options;
	}


}
