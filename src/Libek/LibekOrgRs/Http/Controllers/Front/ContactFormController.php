<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Creitive\Mandrill\Mailer;
use Creitive\Mandrill\Message;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Redirect;
use Session;
use Str;
use Validator;

class ContactFormController extends AbstractBaseController {

	/**
	 * An instance of the current request.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * A Mandrill Mailer instance.
	 *
	 * @var \Creitive\Mandrill\Mailer
	 */
	protected $mandrillMailer;

	public function __construct(Request $request, Mailer $mandrillMailer)
	{
		parent::__construct();

		$this->request = $request;
		$this->mandrillMailer = $mandrillMailer;

		$this->viewData->bodyDataPage = 'contact';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.contact'));
		$this->viewData->headerContainerHasBackground = false;

		$this->viewData->successMessages = Session::get('successMessages', new MessageBag);
	}

	/**
	 * Gets the website contact page.
	 *
	 * For now, this is just a static page with a contact email address listed,
	 * but it will later be expanded to handle an actual contact form.
	 *
	 * @return void
	 */
	public function getForm()
	{
		$this->loadContent('contact');
	}

	/**
	 * Posts the contact form.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postForm()
	{
		$validator = $this->getContactFormValidator($this->request);

		if ($validator->fails())
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\ContactFormController@getForm')->withErrors($validator)->withInput();
		}

		$this->sendMessage($this->request);

		return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Front\ContactFormController@getForm')->with('successMessages', new MessageBag([Lang::get('contact.success')]));
	}

	/**
	 * Gets a validator for the contact form.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getContactFormValidator(Request $request)
	{
		return Validator::make(
			[
				'email' => $request->get('email'),
				'full_name' => $request->get('full_name'),
				'message' => $request->get('message'),
			],
			[
				'email' => 'required|email',
				'full_name' => 'required',
				'message' => 'required',
			]
		);
	}

	/**
	 * Sends the message via Mandrill.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return void
	 */
	protected function sendMessage(Request $request)
	{
		$message = $this->getMessage($request);

		return $this->mandrillMailer->send($message);
	}

	/**
	 * Gets a Mandrill message instance, composed from the appropriate input
	 * data.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Creitive\Mandrill\Message
	 */
	protected function getMessage(Request $request)
	{
		$subject = Lang::get('contact.subject').' ['.Str::random(8).']';

		return $this->mandrillMailer->newMessage()
			->to('kontakt@libek.org.rs', 'Libertarijanski Klub Libek')
			->replyTo($request->get('email'), $request->get('full_name'))
			->subject($subject)
			->body(htmlspecialchars($request->get('message')));
	}

}
