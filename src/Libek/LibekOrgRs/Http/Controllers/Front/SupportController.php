<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 8/9/2016
 * Time: 6:01 PM
 */

namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;
use Libek\LibekOrgRs\TeamMembers\Person;
use Libek\LibekOrgRs\TeamMembers\Person\Repository as teamMembersRepository;

class SupportController extends AbstractBaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'support';
        $this->viewData->pageTitle->setPage(Lang::get('navigation.support'));
    }


    /*
     * gets the Support page
     */

    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('support');
    }

}