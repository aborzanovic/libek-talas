<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 2/22/2016
 * Time: 9:15 PM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use View;

class EducationController extends AbstractBaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'education';
        $this->viewData->pageTitle->setPage(Lang::get('navigation.education'));
    }


    public function getIndex()
    {
        $this->viewData->headerContainerHasBackground = false;

        $this->loadContent('education');
    }

    public function getJohnGalt ()
    {

        $this->loadContent('john-galt');
    }


}