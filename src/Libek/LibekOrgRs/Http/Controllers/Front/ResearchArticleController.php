<?php namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\Research\Article;
use Libek\LibekOrgRs\Research\Article\Repository as ResearchArticleRepository;
use Pagination;
use URL;
use Validator;

class ResearchArticleController extends AbstractBaseController {

	/**
	 * A Research article repository instance.
	 *
	 * @var \Libek\LibekOrgRs\Repository\ResearchArticleRepository
	 */
	protected $researchArticleRepository;

	/**
	 * How many articles to list per page.
	 *
	 * @var integer
	 */
	protected $perPage = 6;
	protected $recommendedNum = 4;

	public function __construct(ResearchArticleRepository $researchArticleRepository)
	{
		parent::__construct();

		$this->researchArticleRepository = $researchArticleRepository;

		$this->viewData->bodyDataPage = 'research';
		$this->viewData->pageTitle->setPage(Lang::get('navigation.research'));
	}

	/**
	 * Gets the first page of research articles.
	 *
	 * @return void
	 */
	public function getIndex()
	{
		return $this->getPage(0);
	}

	/**
	 * Gets a specific page of research articles.
	 *
	 * @param string $page
	 * @return void
	 */
	public function getPage($page)
	{
		$page = (int) $page;

		$articles = $this->researchArticleRepository->getPage($page, $this->perPage, $this->currentLanguage);
		$pagination = Pagination::get(
			$page,
			$this->researchArticleRepository->getLastTotalRowCount(),
			$this->perPage,
			URL::action('Libek\LibekOrgRs\Http\Controllers\Front\ResearchArticleController@getIndex')
		);

		$data = [
			'articles' => $articles,
			'listingTitle' => ucfirst(Lang::get("articles.research")),
			'pagination' => $pagination,
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'urlGenerator' => $this->getUrlGenerator(),
		];

		$this->loadContent('research-articles/listing', $data);
	}

	/**
	 * Gets a specific research article.
	 *
	 * @param string $year
	 * @param string $month
	 * @param string $day
	 * @param string $slug
	 * @return void
	 */
	public function getArticle($year, $month, $day, $slug)
	{
		$validator = $this->getDateValidator($year, $month, $day);

		if ($validator->fails())
		{
			throw new ModelNotFoundException;
		}

		$article = $this->researchArticleRepository->getArticle($year, $month, $day, $slug, $this->currentLanguage);
		$articles = $this->researchArticleRepository->getPage(0, $this->recommendedNum, $this->currentLanguage);

		$data = [
			'article' => $article,
			'left_recommended' => $articles->slice(0,1, true)->first(),
			'bottom_recommended' => $articles->slice(1,3, true),
			'socialPluginsGenerator' => $this->getSocialPluginsGenerator(),
			'urlGenerator' => $this->getUrlGenerator(),
		];

		$this->setArticleMeta($article);

		$this->loadContent('research-articles/single', $data);
	}

	/**
	 * Gets a closure for generating social plugin containers.
	 *
	 * @return \Closure
	 */
	protected function getSocialPluginsGenerator()
	{
		return function($url)
		{
			$socialPlugins = new \Creitive\SocialPlugins\SocialPlugins($url);

			$socialPlugins->addPlugins(
				new \Creitive\SocialPlugins\Twitter\Tweet,
				new \Creitive\SocialPlugins\Facebook\Like
			);

			return $socialPlugins;
		};
	}

	/**
	 * Gets a closure for generating research article URLs.
	 *
	 * @return \Closure
	 */
	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\ResearchArticleController@getArticle',
				[
					'year' => $article->published_at->format('Y'),
					'month' => $article->published_at->format('m'),
					'day' => $article->published_at->format('d'),
					'slug' => $article->slug,
				]
			);
		};
	}

	/**
	 * Gets a validator for the date.
	 *
	 * @param string $year
	 * @param string $month
	 * @param string $day
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getDateValidator($year, $month, $day)
	{
		return Validator::make(
			[
				'date' => "{$year}-{$month}-{$day}",
			],
			[
				'date' => 'custom_date_format',
			]
		);
	}

}
