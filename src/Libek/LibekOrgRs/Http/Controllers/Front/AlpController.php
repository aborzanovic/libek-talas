<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/11/2016
 * Time: 2:01 AM
 */
namespace Libek\LibekOrgRs\Http\Controllers\Front;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lang;
use Libek\LibekOrgRs\Http\Controllers\Front\AbstractBaseController;
use Libek\LibekOrgRs\Generations\Article;
use Libek\LibekOrgRs\Generations\Article\Repository as GenerationsArticleRepository;
use Libek\LibekOrgRs\Mentors\Person as MentorsPerson;
use Libek\LibekOrgRs\Mentors\Person\Repository as MentorRepository;
use Libek\LibekOrgRs\Lecturers\Person as LecturersPerson;
use Libek\LibekOrgRs\Lecturers\Person\Repository as LecturersRepository;
use Libek\LibekOrgRs\Alumni\Person as AlumniPerson;
use Libek\LibekOrgRs\Alumni\Person\Repository as AlumniRepository;
use Libek\LibekOrgRs\Slideshow\Image\Repository as SlideshowImagesRepository;
use View;
use Pagination;
use Response;
use URL;
use Validator;

class AlpController extends AbstractBaseController
{
    protected $generationsArticleRepository;
	protected $mentorsPersonRepository;
	protected $lecturersPersonRepository;
	protected $alumniPersonRepository;

    protected $perPagePersons = 300;
	protected $perPageGenerations = 6;

	protected $subdomain = 'alp';

    public function __construct(SlideshowImagesRepository $slideshowImagesRepository, GenerationsArticleRepository $generationsArticleRepository, MentorRepository $mentorsPersonRepository,
	LecturersRepository $lecturersPersonRepository, AlumniRepository $alumniPersonRepository)
    {
        parent::__construct();

	    $this->slideshowImagesRepository = $slideshowImagesRepository;
        $this->generationsArticleRepository = $generationsArticleRepository;
	    $this->mentorsPersonRepository = $mentorsPersonRepository;
	    $this->lecturersPersonRepository = $lecturersPersonRepository;
	    $this->alumniPersonRepository = $alumniPersonRepository;

        $this->viewData->bodyDataPage = 'alp-home';
        $this->viewData->pageTitle->setPage('Akademija Liberalne Politike');
    }

    public function getIndex()
	{
		$data = [
			'backgroundImages' => $this->slideshowImagesRepository->getImages('alp')
		];
		$this->loadContent('alp-home', $data);
	}

	public function getAlumni()
	{
		return $this->getGenerationsPage(0);
	}

    public function getGenerationsPage($page)
	{
		$page = (int) $page;

		$articles = $this->generationsArticleRepository->getPage($page, $this->perPageGenerations, $this->currentLanguage);
		$pagination = Pagination::get(
			$page,
			$this->generationsArticleRepository->getLastTotalRowCount(),
			$this->perPageGenerations,
			URL::action('Libek\LibekOrgRs\Http\Controllers\Front\AlpController@getIndex')
		);

        $this->viewData->headerContainerHasBackground = false;

        $data = [
			'articles' => $articles,
			'listingTitle' => ucfirst(Lang::get("alp.generations")),
			'pagination' => $pagination,
			'urlGenerator' => $this->getUrlGenerator(),
		];

        $this->loadContent('alp/alumni', $data);
    }

	public function getMentors()
	{
		return $this->getMentorsPage(0);
	}

	public function getMentorsPage($page)
	{
		$page = (int) $page;

		$persons = $this->mentorsPersonRepository->getPersons($page, $this->perPagePersons);
		$pagination = Pagination::get(
			$page,
			$this->mentorsPersonRepository->getLastTotalRowCount(),
			$this->perPagePersons,
			URL::action('Libek\LibekOrgRs\Http\Controllers\Front\AlpController@getIndex')
		);

        $this->viewData->headerContainerHasBackground = false;

        $data = [
			'persons' => $persons,
			'listingTitle' => ucfirst(Lang::get("alp.mentors")),
			'pagination' => $pagination,
			'urlGenerator' => $this->getUrlGenerator(),
		];

        $this->loadContent('alp/mentors', $data);
    }

	public function getLecturers()
	{
		return $this->getLecturersPage(0);
	}

	public function getLecturersPage($page)
	{
		$page = (int) $page;

		$persons = $this->lecturersPersonRepository->getPersons($page, $this->perPagePersons);
		$pagination = Pagination::get(
			$page,
			$this->lecturersPersonRepository->getLastTotalRowCount(),
			$this->perPagePersons,
			URL::action('Libek\LibekOrgRs\Http\Controllers\Front\AlpController@getIndex')
		);

        $this->viewData->headerContainerHasBackground = false;

        $data = [
			'persons' => $persons,
			'listingTitle' => ucfirst(Lang::get("alp.lecturers")),
			'pagination' => $pagination,
			'urlGenerator' => $this->getUrlGenerator(),
		];

        $this->loadContent('alp/lecturers', $data);
    }

	protected function getUrlGenerator()
	{
		return function(Article $article)
		{
			return URL::action(
				'Libek\LibekOrgRs\Http\Controllers\Front\AlpController@getArticle',
				[
					'id' => $article->id
				]
			);
		};
	}

	protected function getDateValidator($year, $month, $day)
	{
		return Validator::make(
			[
				'date' => "{$year}-{$month}-{$day}",
			],
			[
				'date' => 'custom_date_format',
			]
		);
	}

	public function getArticle($id)
	{

		$article = $this->generationsArticleRepository->getArticleById($id, $this->currentLanguage);

		$data = [
			'article' => $article,
			'urlGenerator' => $this->getUrlGenerator(),
		];

		$this->setArticleMeta($article);

		$this->loadContent('alp/single', $data);
	}
}