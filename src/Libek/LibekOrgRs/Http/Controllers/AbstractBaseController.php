<?php namespace Libek\LibekOrgRs\Http\Controllers;

use App;
use Config;
use Controller;
use Illuminate\Support\MessageBag;
use Lang;
use LocaleResolver;
use Sentinel;
use Session;
use URL;
use View;

abstract class AbstractBaseController extends Controller {

	/**
	 * {@inheritDoc}
	 */
	protected $layout = 'layouts.master';

	protected $subdomain = '';

	/**
	 * Whether to use the CSRF protection filter.
	 *
	 * May be overridden in extended classes, to make it possible to disable the
	 * CSRF filter for certain controllers.
	 *
	 * The original use-case that prompted the need for this was receiving
	 * payment notifications from a payment processor - the notifications are
	 * `POST` requests, but we obviously don't want a CSRF check there, since
	 * we'll use other means to determine whether the request is authentic or
	 * not).
	 *
	 * @var boolean
	 */
	protected $csrfProtection = true;

	/**
	 * The currently logged-in user, if available.
	 *
	 * @var \Libek\LibekOrgRs\Auth\User|null
	 */
	protected $currentUser;

	public function __construct()
	{
		if ($this->csrfProtection)
		{
			$this->beforeFilter('csrf', ['on' => ['post', 'put', 'delete']]);
		}

		$this->setBaseProperties();
		$this->setBaseViewData();
		$this->setBaseAssets();

		$this->viewData->subdomain = $this->subdomain;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if (!is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Loads the content partial from the appropriate view.
	 *
	 * @param string $partial
	 * @param array $data
	 * @return void
	 */
	protected function loadContent($partial, $data = [])
	{
		View::inject('layoutContent', View::make($partial, $data)->render());
	}

	/**
	 * Sets some base controller properties which are needed by the child
	 * classes.
	 *
	 * @return void
	 */
	protected function setBaseProperties()
	{
		$this->currentUser = Sentinel::getUser();

		$this->currentLanguage = LocaleResolver::getLanguage();
		$this->currentLocale = App::getLocale();

		$this->navigation = App::make('Creitive\Navigation\Factory');
	}

	/**
	 * Sets base view data.
	 *
	 * @return void
	 */
	protected function setBaseViewData()
	{
		$this->viewData = App::make('Creitive\ViewData\ViewData');

		$this->viewData->currentLanguage = $this->currentLanguage;
		$this->viewData->currentLocale = $this->currentLocale;
		$this->viewData->currentUser = $this->currentUser;

		$this->viewData->navigation = $this->navigation;

		$this->viewData->pageTitle->setSiteName(Lang::get('common.siteName'));

		$this->viewData->meta->description = Lang::get('common.metaDescription');
		$this->viewData->meta->og = $this->getMetaOg();

		$this->viewData->bodyDataPage = '';
		$this->viewData->successMessages = Session::get('successMessages', new MessageBag);
		$this->viewData->headerContainerHasBackground = true;
	}

	/**
	 * Gets an array of OpenGraph tags which can be output as `<meta>` tags.
	 *
	 * @return array
	 */
	protected function getMetaOg()
	{
		return [
			'og:site_name' => Lang::get('common.siteName'),
			'og:title' => Lang::get('common.siteName'),
			'og:locale' => App::getLocale(),
			'og:type' => 'website',
			'og:description' => Lang::get('common.metaDescription'),
			'og:image' => URL::to('/images/logo-1200x630.png'),
			'fb:app_id' => Config::get('facebook.appId'),
		];
	}

	/**
	 * Sets the various meta properties for an article.
	 *
	 * @param \Creitive\Database\Eloquent\Model $article
	 */
	protected function setArticleMeta($article)
	{
		if ($article->meta_description)
		{
			$this->viewData->meta->description = $article->meta_description;
			$this->viewData->meta->og['og:description'] = $article->meta_description;
		}
		else
		{
			$this->viewData->meta->description = strip_tags($article->lead);
			$this->viewData->meta->og['og:description'] = strip_tags($article->lead);
		}

		if ($article->meta_title)
		{
			$this->viewData->pageTitle->setPage($article->meta_title);
			$this->viewData->meta->og['og:title'] = $article->meta_title;
		}
		else
		{
			$this->viewData->pageTitle->setPage($article->title);
			$this->viewData->meta->og['og:title'] = $article->title;
		}

		$this->viewData->meta->og['og:image'] = URL::to($article->getImage('main', 'og'));
		$this->viewData->meta->og['og:type'] = 'article';
	}

	/**
	 * Sets the base assets (scripts and styles).
	 *
	 * @return void
	 */
	abstract protected function setBaseAssets();

}
