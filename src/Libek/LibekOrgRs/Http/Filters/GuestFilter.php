<?php namespace Libek\LibekOrgRs\Http\Filters;

use Sentinel;
use Redirect;
use URL;

class GuestFilter {

	/**
	 * Redirects the user to the home page, in case the user is already
	 * logged-in.
	 *
	 * This may be used to prevent logged-in users from accessing pages that
	 * should only be accessible by guests, most notably the login and
	 * registration pages.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function filter()
	{
		if (Sentinel::check())
		{
			return Redirect::action('Libek\LibekOrgRs\Http\Controllers\Admin\HomeController@index');
		}
	}

}
