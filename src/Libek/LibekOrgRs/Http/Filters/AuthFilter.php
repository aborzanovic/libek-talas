<?php namespace Libek\LibekOrgRs\Http\Filters;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Redirect;
use Sentinel;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use URL;

class AuthFilter {

	/**
	 * Checks if the user is authenticated and if they aren't, throws an
	 * `AccessDeniedHttpException` in case of an AJAX request, or redirects the
	 * user to the login page.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|void
	 * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
	 */
	public function filter(Route $route, Request $request)
	{
		if (Sentinel::guest())
		{
			if ($request->ajax())
			{
				throw new AccessDeniedHttpException;
			}
			else
			{
				return Redirect::guest(URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@getLogin'));
			}
		}
	}

}
