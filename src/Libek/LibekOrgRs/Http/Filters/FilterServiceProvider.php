<?php namespace Libek\LibekOrgRs\Http\Filters;

use Creitive\Http\Filters\FilterServiceProvider as CreitiveFilterServiceProvider;

class FilterServiceProvider extends CreitiveFilterServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	protected $filters = [
		'ajax' => 'Creitive\Http\Filters\AjaxFilter',
		'auth' => 'Libek\LibekOrgRs\Http\Filters\AuthFilter',
		'auth.basic' => 'Libek\LibekOrgRs\Http\Filters\BasicAuthFilter',
		'auth.user' => 'Libek\LibekOrgRs\Http\Filters\UserAuthFilter',
		'csrf' => 'Creitive\Http\Filters\CsrfFilter',
		'forceSsl' => 'Creitive\Http\Filters\ForceSslFilter',
		'guest' => 'Libek\LibekOrgRs\Http\Filters\GuestFilter',
		'permissions' => 'Libek\LibekOrgRs\Http\Filters\PermissionsFilter',
	];

}
