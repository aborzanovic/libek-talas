<?php namespace Libek\LibekOrgRs\Http\Filters;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Sentinel;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PermissionsFilter {

	/**
	 * Checks if the user has permissions to access the current action, and
	 * throws an `AccessDeniedHttpException` if they don't.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|void
	 * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
	 */
	public function filter(Route $route, Request $request)
	{
		if ( ! Sentinel::hasAccess($route->getActionName()))
		{
			throw new AccessDeniedHttpException;
		}
	}

}
