<?php namespace Libek\LibekOrgRs\Http\Filters;

use Sentinel;
use Symfony\Component\HttpFoundation\Response;

class BasicAuthFilter {

	/**
	 * Performs basic HTTP authentication.
	 *
	 * @return \Symfony\Component\HttpFoundation\Response|void
	 */
	public function filter()
	{
		Sentinel::creatingBasicResponse(function()
		{
			$headers = ['WWW-Authenticate' => 'Basic'];

			return new Response('Invalid credentials.', 401, $headers);
		});

		return Sentinel::basic();
	}

}
