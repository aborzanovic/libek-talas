<?php namespace Libek\LibekOrgRs\Applications;

use Creitive\Database\Eloquent\Model;
use Creitive\Models\Traits\PublishableTrait;
use Creitive\Models\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Libek\LibekOrgRs\Applications\SubmittedApplication;

class ApplicationForm extends Model {

	use PublishableTrait;
	use SoftDeletingTrait;
	use SortableTrait;

	/**
	 * {@inheritDoc}
	 */
	public function getDates()
	{
		return array_merge(parent::getDates(), ['deadline', 'published_at']);
	}

	/**
	 * Eloquent relationship: an application form has many submitted
	 * applications.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function submittedApplications()
	{
		return $this->hasMany(SubmittedApplication::class);
	}

	/**
	 * Checks whether this application form is active at the moment (ie. the
	 * deadline for applications hasn't been reached yet).
	 *
	 * @return boolean
	 */
	public function isActive()
	{
		return $this->deadline->isFuture();
	}

}
