<?php namespace Libek\LibekOrgRs\Applications\ApplicationForm;

use Carbon\Carbon;
use Creitive\Mandrill\Mailer;
use Exception;
use Illuminate\Support\MessageBag;
use Lang;
use Libek\LibekOrgRs\Applications\ApplicationForm;
use Libek\LibekOrgRs\Applications\ApplicationForm\Repository as ApplicationFormRepository;
use Libek\LibekOrgRs\Applications\SubmittedApplication;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\Factory as CompilerFactory;
use Str;
use View;

class Manager {

	/**
	 * A Compiler factory.
	 *
	 * @var \Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\Factory
	 */
	protected $compilerFactory;

	/**
	 * An ApplicationFormRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Applications\ApplicationForm\Repository
	 */
	protected $applicationFormRepository;

	/**
	 * A Mandrill mailer instance.
	 *
	 * @var \Creitive\Mandrill\Mailer
	 */
	protected $mandrillMailer;

	/**
	 * The success messages to be passed to the controller.
	 *
	 * @var \Illuminate\Support\MessageBag
	 */
	protected $successMessages;

	/**
	 * An instance of the Validator class, for returning errors to the
	 * controller.
	 *
	 * @var \Libek\LibekOrgRs\Validation\Validator|null
	 */
	protected $validator = null;

	public function __construct(
		CompilerFactory $compilerFactory,
		ApplicationFormRepository $applicationFormRepository,
		Mailer $mandrillMailer)
	{
		$this->compilerFactory = $compilerFactory;
		$this->applicationFormRepository = $applicationFormRepository;
		$this->mandrillMailer = $mandrillMailer;

		$this->successMessages = new MessageBag;
	}

	/**
	 * Gets the application form by the passed language and slug.
	 *
	 * Throws a `ModelNotFoundException` if the form doesn't exist.
	 *
	 * @param string $language
	 * @param string $slug
	 * @return \Libek\LibekOrgRs\Applications\ApplicationForm
	 * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
	 */
	public function getApplicationForm($language, $slug)
	{
		return $this->applicationFormRepository->getByLanguageAndSlug($language, $slug);
	}

	/**
	 * Saves the submitted application form to the database.
	 *
	 * Returns `true` on success, or `false` on failure.
	 *
	 * @param \Libek\LibekOrgRs\Applications\ApplicationForm $applicationForm
	 * @param array $inputData
	 * @return boolean
	 */
	public function saveSubmittedApplication(ApplicationForm $applicationForm, array $inputData)
	{
		$compiler = $this->compilerFactory->getCompiler($applicationForm);

		$validator = $compiler->getValidator($inputData);

		if ($validator->fails())
		{
			$this->validator = $validator;

			return false;
		}

		$submittedApplication = $compiler->compile($inputData);
		$submittedApplication = $applicationForm->submittedApplications()->save($submittedApplication);

		$this->sendSubmittedApplicationNotification($applicationForm, $submittedApplication);

		$this->successMessages->add(
			'successfulApplication',
			Lang::get('applicationForms.'.Str::camel($applicationForm->type).'.success', ['title' => $applicationForm->title])
		);

		return true;
	}

	/**
	 * Returns the validator instance.
	 *
	 * @return \Libek\LibekOrgRs\Validation\Validator|null
	 */
	public function getValidator()
	{
		return $this->validator;
	}

	/**
	 * Returns the success messages.
	 *
	 * @return \Illuminate\Support\MessageBag
	 */
	public function getSuccessMessages()
	{
		return $this->successMessages;
	}

	/**
	 * Sends an email with the submitted application notification.
	 *
	 * @param \Libek\LibekOrgRs\Applications\ApplicationForm $applicationForm
	 * @param \Libek\LibekOrgRs\Applications\SubmittedApplication $submittedApplication
	 * @return array|boolean
	 */
	public function sendSubmittedApplicationNotification(ApplicationForm $applicationForm, SubmittedApplication $submittedApplication)
	{
		$message = $this->getSubmittedApplicationMessage($applicationForm, $submittedApplication);

		return $this->mandrillMailer->send($message);
	}

	/**
	 * Gets a Mandrill message instance, composed from the appropriate submitted
	 * application.
	 *
	 * @param \Libek\LibekOrgRs\Applications\ApplicationForm $applicationForm
	 * @param \Libek\LibekOrgRs\Applications\SubmittedApplication $submittedApplication
	 * @return \Creitive\Mandrill\Message
	 */
	protected function getSubmittedApplicationMessage(ApplicationForm $applicationForm, SubmittedApplication $submittedApplication)
	{
		/**
		 * Compile all needed data.
		 */

		$toAddress = Lang::get('applicationForms.'.Str::camel($applicationForm->type).'.email.to.address');
		$toName = Lang::get('applicationForms.'.Str::camel($applicationForm->type).'.email.to.name');

		$replyToAddress = $submittedApplication->data['email'];
		$replyToName = $submittedApplication->data['full_name'];

		$random = Str::random(8);

		$subject = Lang::get(
			'applicationForms.'.Str::camel($applicationForm->type).'.email.subject',
			[
				'title' => $applicationForm->title,
				'random' => $random,
			]
		);

		$body = View::make(
			'application-forms.'.$applicationForm->type.'.email',
			[
				'subject' => $subject,
				'applicationForm' => $applicationForm,
				'submittedApplication' => $submittedApplication,
			]
		)->render();

		/**
		 * Create the message.
		 */

		$message = $this->mandrillMailer->newMessage()
			->to($toAddress, $toName)
			->replyTo($replyToAddress, $replyToName)
			->subject($subject)
			->body($body);

		foreach ($submittedApplication->data['attachments'] as $attachment)
		{
			$message->attach(basename($attachment['path']), $attachment['path']);
		}

		return $message;
	}

}
