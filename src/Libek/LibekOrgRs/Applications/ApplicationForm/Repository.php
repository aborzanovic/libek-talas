<?php namespace Libek\LibekOrgRs\Applications\ApplicationForm;

use Libek\LibekOrgRs\Applications\ApplicationForm;

class Repository {

	/**
	 * An ApplicationForm model instance.
	 *
	 * @var \Libek\LibekOrgRs\Applications\ApplicationForm
	 */
	protected $applicationFormModel;

	public function __construct(ApplicationForm $applicationFormModel)
	{
		$this->applicationFormModel = $applicationFormModel;
	}

	/**
	 * Gets the requested application form, if it exists.
	 *
	 * @param string $language
	 * @param string $slug
	 * @return \Libek\LibekOrgRs\Applications\ApplicationForm
	 */
	public function getByLanguageAndSlug($language, $slug)
	{
		return $this->applicationFormModel
			->whereLanguage($language)
			->whereSlug($slug)
			->published()
			->firstOrFail();
	}

}
