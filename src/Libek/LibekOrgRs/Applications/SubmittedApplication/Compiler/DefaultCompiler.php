<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler;

use Illuminate\Validation\Factory as Validator;
use Libek\LibekOrgRs\Applications\ApplicationForm;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Repository as SubmittedApplicationRepository;

abstract class DefaultCompiler implements Compiler {

	/**
	 * A Validator factory.
	 *
	 * @var \Illuminate\Validation\Factory
	 */
	protected $validator;

	/**
	 * A SubmittedApplicationRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Applications\SubmittedApplication\Repository
	 */
	protected $submittedApplicationRepository;

	/**
	 * The application form for which the submitted application is being
	 * compiled.
	 *
	 * @var \Libek\LibekOrgRs\Applications\ApplicationForm
	 */
	protected $applicationForm;

	public function __construct(Validator $validator, SubmittedApplicationRepository $submittedApplicationRepository, ApplicationForm $applicationForm)
	{
		$this->validator = $validator;
		$this->submittedApplicationRepository = $submittedApplicationRepository;
		$this->applicationForm = $applicationForm;
	}

	/**
	 * Gets a validator for the submitted application.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	public function getValidator(array $inputData)
	{
		return $this->validator->make($inputData, $this->getValidationRules());
	}

	/**
	 * Gets the validation rules for validating this application.
	 *
	 * @return array
	 */
	abstract protected function getValidationRules();

	/**
	 * Compiles a submitted application for the given input data.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Applications\SubmittedApplication
	 */
	abstract public function compile(array $inputData);

}
