<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler;

use Exception;
use Illuminate\Validation\Factory as Validator;
use Libek\LibekOrgRs\Applications\ApplicationForm;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\AcademyCompiler;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\EsflcBerlinCompiler;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\JohnGaltCompiler;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\SeminarCompiler;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Repository as SubmittedApplicationRepository;

class Factory {

	/**
	 * A Validator factory.
	 *
	 * @var \Illuminate\Validation\Factory
	 */
	protected $validator;

	/**
	 * A SubmittedApplicationRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Applications\SubmittedApplication\Repository
	 */
	protected $submittedApplicationRepository;

	public function __construct(Validator $validator, SubmittedApplicationRepository $submittedApplicationRepository)
	{
		$this->validator = $validator;
		$this->submittedApplicationRepository = $submittedApplicationRepository;
	}

	/**
	 * Gets a compiler for the specified application form.
	 *
	 * @param \Libek\LibekOrgRs\Applications\ApplicationForm $applicationForm
	 * @return \Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler
	 */
	public function getCompiler(ApplicationForm $applicationForm)
	{
		switch ($applicationForm->type)
		{
			case 'academy':
				$compilerClass = AcademyCompiler::class;
				break;

			case 'seminar':
				$compilerClass = SeminarCompiler::class;
				break;

			case 'esflc-berlin':
				$compilerClass = EsflcBerlinCompiler::class;
				break;

			case 'john-galt':
				$compilerClass = JohnGaltCompiler::class;
				break;

			default:
				throw new Exception('Unknown application type: '.$applicationForm->type.', ID: '.$applicationForm->id);
		}

		return new $compilerClass($this->validator, $this->submittedApplicationRepository, $applicationForm);
	}

}
