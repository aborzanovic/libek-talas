<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler;

use Carbon\Carbon;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\DefaultCompiler;
use Str;

class JohnGaltCompiler extends DefaultCompiler {

	/**
	 * {@inheritDoc}
	 */
	protected function getValidationRules()
	{
		return [
			'email' => 'required|email',
			'full_name' => 'required',
			'cv' => 'required|mimes:pdf,doc,docx,odt|max:10240',
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function compile(array $inputData)
	{
		$submittedApplication = $this->submittedApplicationRepository->newInstance();

		$newData = [];

		$inputFields = [
			'full_name',
			'birthdate',
			'sex',
			'cv',
			'email',
			'phone',
			'faculty',
			'reference',
			'motivation',
			'expectations',
			'future',
			'ego',
			'quote',
			'needs',
		];

		foreach ($inputFields as $field)
		{
			$newData[$field] = isset($inputData[$field]) ? $inputData[$field] : '';
		}

		$cv = $inputData['cv'];

		$now = Carbon::now();

		$targetPath = storage_path()."/submitted-applications/{$this->applicationForm->type}/{$this->applicationForm->id}";
		$targetName = $now->format('U').'-'.Str::random(8).'.'.$cv->guessExtension();

		$uploadedCv = $cv->move($targetPath, $targetName);

		$newData['attachments'] = [
			[
				'original_name' => $cv->getClientOriginalName(),
				'path' => $uploadedCv->getPathname(),
			],
		];

		$submittedApplication->data = $newData;
		$submittedApplication->submitted_at = $now;

		return $submittedApplication;
	}

}
