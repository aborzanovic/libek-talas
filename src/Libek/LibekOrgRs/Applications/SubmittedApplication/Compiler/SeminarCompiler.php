<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler;

use Carbon\Carbon;
use Libek\LibekOrgRs\Applications\SubmittedApplication\Compiler\DefaultCompiler;

class SeminarCompiler extends DefaultCompiler {

	/**
	 * {@inheritDoc}
	 */
	protected function getValidationRules()
	{
		return [
			'email' => 'required|email',
			'full_name' => 'required',
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function compile(array $inputData)
	{
		$submittedApplication = $this->submittedApplicationRepository->newInstance();

		$newData = [];

		$inputFields = [
			'full_name',
			'birthdate',
			'sex',
			'email',
			'phone',
			'school',
			'history',
			'memberships',
			'faculty',
			'problems',
			'solutions',
			'ego',
			'expectations',
			'reference',
			'needs',
		];

		foreach ($inputFields as $field)
		{
			$newData[$field] = isset($inputData[$field]) ? $inputData[$field] : '';
		}

		$newData['attachments'] = [];

		$submittedApplication->data = $newData;
		$submittedApplication->submitted_at = Carbon::now();

		return $submittedApplication;
	}

}
