<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication;

use Libek\LibekOrgRs\Applications\SubmittedApplication;

class Repository {

	/**
	 * A SubmittedApplication model instance.
	 *
	 * @var \Libek\LibekOrgRs\Applications\SubmittedApplication
	 */
	protected $submittedApplicationModel;

	public function __construct(SubmittedApplication $submittedApplicationModel)
	{
		$this->submittedApplicationModel = $submittedApplicationModel;
	}

	/**
	 * Creates a new SubmittedApplication instance.
	 *
	 * @return \Libek\LibekOrgRs\Applications\SubmittedApplication
	 */
	public function newInstance()
	{
		return $this->submittedApplicationModel->newInstance();
	}

}
