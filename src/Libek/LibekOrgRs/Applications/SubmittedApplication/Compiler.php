<?php namespace Libek\LibekOrgRs\Applications\SubmittedApplication;

interface Compiler {

	public function getValidator(array $inputData);

	public function compile(array $inputData);

}
