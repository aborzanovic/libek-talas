<?php namespace Libek\LibekOrgRs\Applications;

use Creitive\Database\Eloquent\Model;
use Hampel\Json\Json;
use Hampel\Json\JsonException;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Libek\LibekOrgRs\Applications\ApplicationForm;
use Log;

class SubmittedApplication extends Model {

	use SoftDeletingTrait;

	/**
	 * {@inheritDoc}
	 */
	public function getDates()
	{
		return array_merge(parent::getDates(), ['submitted_at']);
	}

	/**
	 * Eloquent relationship: a submitted application belongs to one application
	 * form.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function applicationForm()
	{
		return $this->belongsTo(ApplicationForm::class);
	}

	/**
	 * The `data` attribute accessor.
	 *
	 * Converts stored JSON to an array.
	 *
	 * @param string $data
	 * @return array
	 */
	public function getDataAttribute($data)
	{
		try
		{
			return Json::decode($data, true);
		}
		catch (JsonException $e)
		{
			Log::error('Failed to decode data in the database, defaulting to an empty array.');

			return [];
		}
	}

	/**
	 * The `data` attribute mutator.
	 *
	 * Converts the passed array into a JSON string for database storage.
	 *
	 * @param array $data
	 * @return void
	 */
	public function setDataAttribute(array $data)
	{
		try
		{
			$this->attributes['data'] = Json::encode($data);
		}
		catch (JsonException $e)
		{
			$this->attributes['data'] = '[]';

			Log::error("Failed to encode the provided data to json, defaulting to an empty array. The provided data was:\n".print_r($data, true));
		}
	}

}
