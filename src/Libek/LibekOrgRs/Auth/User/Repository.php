<?php namespace Libek\LibekOrgRs\Auth\User;

use Cartalyst\Sentinel\Sentinel;
use Illuminate\Database\Eloquent\Builder;
use Libek\LibekOrgRs\Auth\User;

class Repository {

	/**
	 * A User model instance.
	 *
	 * @var \Libek\LibekOrgRs\Auth\User
	 */
	protected $userModel;

	/**
	 * A Sentinel instance.
	 *
	 * @var \Cartalyst\Sentinel\Sentinel
	 */
	protected $sentinel;

	public function __construct(User $userModel, Sentinel $sentinel)
	{
		$this->userModel = $userModel;
		$this->sentinel = $sentinel;
	}

	/**
	 * Gets all users.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAll()
	{
		return $this->userModel->all();
	}

	/**
	 * Gets all users with any of the specified roles.
	 *
	 * @param array $roles
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getUsersWithAnyRoles(array $roles = [])
	{
		if (empty($roles))
		{
			return $this->getUsersWithoutRoles();
		}

		return $this->userModel->whereHas('roles', function(Builder $query) use ($roles)
		{
			return $query->where(function(Builder $query) use ($roles)
			{
				foreach ($roles as $role)
				{
					$query->orWhere('slug', $role);
				}

				return $query;
			});
		})->get();
	}

	/**
	 * Returns all users that have no roles.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getUsersWithoutRoles()
	{
		return $this->userModel->withoutRoles()->get();
	}

	/**
	 * Gets all admins and employees.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAdminsAndEmployees()
	{
		return $this->userModel->whereHas('roles', function(Builder $query)
		{
			return $query->where(function(Builder $query)
			{
				$query->orWhere('slug', 'admin');
				$query->orWhere('slug', 'employee');

				return $query;
			});
		})->get();
	}

	/**
	 * Creates a new user and returns it.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Auth\User
	 */
	public function create(array $inputData)
	{
		$userConfig = [
			'email' => $inputData['email'],
			'password' => $inputData['password'],
			'first_name' => $inputData['first_name'],
			'last_name' => $inputData['last_name'],
		];

		$user = $this->sentinel->registerAndActivate($userConfig);

		$role = $this->sentinel->findRoleBySlug($inputData['role']);
		$role->users()->attach($user);

		return $user;
	}

	/**
	 * Updates the passed user and returns it.
	 *
	 * @param \Libek\LibekOrgRs\Auth\User $user
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Auth\User
	 */
	public function update(User $user, array $inputData)
	{
		$userConfig = [
			'email' => $inputData['email'],
			'first_name' => $inputData['first_name'],
			'last_name' => $inputData['last_name'],
		];

		if ( ! empty($inputData['password']))
		{
			$userConfig['password'] = $inputData['password'];
		}

		$this->sentinel->update($user, $userConfig);

		$role = $this->sentinel->findRoleBySlug($inputData['role']);
		$user->roles()->sync([$role->id]);

		return $user;
	}

}
