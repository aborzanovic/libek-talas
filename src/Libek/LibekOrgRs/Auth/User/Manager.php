<?php namespace Libek\LibekOrgRs\Auth\User;

use Creitive\Managers\AbstractBaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory as Validator;
use Libek\LibekOrgRs\Auth\User;
use Libek\LibekOrgRs\Auth\User\Repository as UserRepository;

class Manager extends AbstractBaseManager {

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = [
		'create' => [
			'email' => ['required', 'email', 'unique:users'],
			'first_name' => ['required'],
			'last_name' => ['required'],
			'password' => ['required'],
			'role' => ['required'],
		],
		'update' => [
			'email' => ['required', 'email'],
			'first_name' => ['required'],
			'last_name' => ['required'],
			'role' => ['required'],
		],
	];

	/**
	 * A Translator instance.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $translator;

	/**
	 * A Validation factory instance.
	 *
	 * @var \Illuminate\Validation\Factory
	 */
	protected $validator;

	/**
	 * A User repository instance.
	 *
	 * @var \Libek\LibekOrgRs\Auth\User\Repository
	 */
	protected $userRepository;

	public function __construct(Translator $translator, Validator $validator, UserRepository $userRepository)
	{
		parent::__construct();

		$this->translator = $translator;
		$this->validator = $validator;
		$this->userRepository = $userRepository;
	}

	/**
	 * Returns all users.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAllUsers()
	{
		return $this->userRepository->getAll();
	}

	/**
	 * Gets all users with the specified roles.
	 *
	 * @param array $roles
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getUsersWithAnyRoles(array $roles = [])
	{
		return $this->userRepository->getUsersWithAnyRoles($roles);
	}

	/**
	 * Attempts to create a new user, and returns it if successful. Returns
	 * `false` on failure.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Auth\User|boolean
	 */
	public function createUser(array $inputData)
	{
		$validator = $this->getCreateValidator($inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->userRepository->create($inputData);
	}

	/**
	 * Attempts to update the specified user, and returns it if successful.
	 * Returns `false` on failure.
	 *
	 * @param \Libek\LibekOrgRs\Auth\User $user
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Auth\User|boolean
	 */
	public function updateUser(User $user, $inputData)
	{
		$validator = $this->getUpdateValidator($user, $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->userRepository->update($user, $inputData);
	}

	/**
	 * Attempts to delete the specified user.
	 *
	 * The input data must contain an `action` key, with the `confirm` value, in
	 * order for the user to be deleted - otherwise, the user will not be
	 * deleted, and `false` will be returned. If the user has been deleted
	 * successfully, `true` will be returned.
	 *
	 * @param \Libek\LibekOrgRs\Auth\User $user
	 * @return boolean
	 */
	public function deleteUser(User $user, array $inputData)
	{
		$action = array_get($inputData, 'action', 'cancel');

		if ($action === 'confirm')
		{
			$user->delete();

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Returns the role options.
	 *
	 * @return array
	 */
	public function getRoleOptions()
	{
		return $this->translator->get('roles');
	}

	/**
	 * Returns the validator for creating a user.
	 *
	 * @param array $input
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	public function getCreateValidator(array $input)
	{
		$rules = $this->validationRules['create'];

		$roles = implode(',', array_keys($this->getRoleOptions()));
		$rules['role'][] = "in:{$roles}";

		return $this->validator->make($input, $rules);
	}

	/**
	 * Returns the validator for updating a user.
	 *
	 * @param \Libek\LibekOrgRs\Auth\User $user
	 * @param array $input
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	public function getUpdateValidator(User $user, array $input)
	{
		$rules = $this->validationRules['update'];

		$roles = implode(',', array_keys($this->getRoleOptions()));
		$rules['role'][] = "in:{$roles}";

		$rules['email'][] = "unique:users,email,{$user->id}";

		return $this->validator->make($input, $rules);
	}

}
