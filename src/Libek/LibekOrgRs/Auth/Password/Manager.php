<?php namespace Libek\LibekOrgRs\Auth\Password;

use Cartalyst\Sentinel\Sentinel;
use Creitive\Managers\AbstractBaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory as Validator;
use Libek\LibekOrgRs\Auth\User;

class Manager extends AbstractBaseManager {

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = [
		'update' => [
			'current_password' => 'required|current_password',
			'new_password' => 'required|confirmed',
		],
	];

	/**
	 * A Translator instance.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $translator;

	/**
	 * A Validation factory instance.
	 *
	 * @var \Illuminate\Validation\Factory
	 */
	protected $validator;

	/**
	 * The Sentinel instance.
	 *
	 * @var \Cartalyst\Sentinel\Sentinel
	 */
	protected $sentinel;

	public function __construct(Translator $translator, Validator $validator, Sentinel $sentinel)
	{
		parent::__construct();

		$this->translator = $translator;
		$this->validator = $validator;
		$this->sentinel = $sentinel;
	}

	/**
	 * Attempts to update the user's password based on the passed input.
	 *
	 * The input should contain the current password, the new password, and the
	 * new password confirmation.
	 *
	 * Returns `false` on validation failure.
	 * Returns `true` on success.
	 *
	 * @param array $input
	 * @param \Libek\LibekOrgRs\Auth\User $user
	 * @return boolean
	 */
	public function update(array $input = [], User $user)
	{
		$input['current_user'] = $user;

		$validator = $this->getUpdateValidator($input);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		$credentials = [
			'password' => $input['new_password'],
		];

		$this->sentinel->update($user, $credentials);

		return true;
	}

	/**
	 * Gets the password update validator.
	 *
	 * @param array $input
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getUpdateValidator(array $input = [])
	{
		return $this->validator->make($input, $this->validationRules['update'], $this->getErrorMessages('update'));
	}

	/**
	 * Compiles the error messages for the update validation.
	 *
	 * @param string $action
	 * @return array
	 */
	protected function getErrorMessages($action)
	{
		return array_dot($this->translator->get("admin/changePassword.errorMessages.{$action}"));
	}

}
