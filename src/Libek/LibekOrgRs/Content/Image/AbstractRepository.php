<?php namespace Libek\LibekOrgRs\Content\Image;

use DB;
use Carbon\Carbon;
use Creitive\Database\Eloquent\Model;

abstract class AbstractRepository {

	/**
	 * A model instance.
	 *
	 * @var \Creitive\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * The last total row count for paginated results.
	 *
	 * @var integer
	 */
	protected $lastTotalRowCount = 0;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Gets the last total row count for paginated results.
	 *
	 * @return integer
	 */
	public function getLastTotalRowCount()
	{
		return $this->lastTotalRowCount;
	}

	/**
	 * Gets a specified page of articles, in the specified language.
	 *
	 * @param integer $page
	 * @param integer $perPage
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getPage($page, $perPage)
	{
		$images = $this->model
			->sqlCalcFoundRows()
			->forPage($page + 1, $perPage)
			->get();

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $images;
	}

	public function getImages($section)
	{
		$images = $this->model
			->sqlCalcFoundRows()
			->where('section','=',$section)
			->get();

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $images;
	}
}
