<?php namespace Libek\LibekOrgRs\Content;

use Creitive\Database\Eloquent\Model;
use Creitive\Models\Traits\DateableTrait;
use Creitive\Models\Traits\ImageableTrait;
use Creitive\Models\Traits\PublishableTrait;
use Creitive\Models\Traits\StickableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

abstract class AbstractPerson extends Model {

	use DateableTrait;
	use ImageableTrait;
	use PublishableTrait;
	use SoftDeletingTrait;
	use StickableTrait;

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		return [
			'versions' => [
				'main' => [
					'full' => [
						'method' => 'cropThumbnailImage',
						'width' => 600,
						'height' => 600,
					],
					'thumbnail' => [
						'method' => 'cropThumbnailImage',
						'width' => 175,
						'height' => 175,
					],
					'og' => [
						'method' => 'cropThumbnailImage',
						'width' => 600,
						'height' => 600,
					],
				],
			],
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function getDates()
	{
		return array_merge(parent::getDates());
	}

}
