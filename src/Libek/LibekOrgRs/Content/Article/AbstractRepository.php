<?php namespace Libek\LibekOrgRs\Content\Article;

use DB;
use URL;
use Carbon\Carbon;
use Creitive\Database\Eloquent\Model;

abstract class AbstractRepository {

	/**
	 * A model instance.
	 *
	 * @var \Creitive\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * The last total row count for paginated results.
	 *
	 * @var integer
	 */
	protected $lastTotalRowCount = 0;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Gets the last total row count for paginated results.
	 *
	 * @return integer
	 */
	public function getLastTotalRowCount()
	{
		return $this->lastTotalRowCount;
	}

	/**
	 * Gets a specified page of articles, in the specified language.
	 *
	 * @param integer $page
	 * @param integer $perPage
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getPage($page, $author, $tag, $perPage, $language)
	{
		$articles = $this->model
			->sqlCalcFoundRows();

		if (! is_null($tag)) {
			$articles = $articles->whereIn(
				'id',
				array_map(
					function ($val) {
						return $val->news_article_id;
					},
					DB::table('news_article_tag')->where('tag_id', $tag)->get()
				)
			);
		}

		if (! is_null($author)) {
			$articles = $articles->where('user_id', $author);
		}

		$articles = $articles->whereLanguage($language)
			->published()
			->orderBySticky()
			->orderBy('created_at','desc')
			->forPage($page + 1, $perPage)
			/*->toSql();//*/->get();
			// dd($articles);

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $articles;
	}

	/**
	 * Gets a specified article.
	 *
	 * Throws a \Illuminate\Database\Eloquent\ModelNotFoundException if the
	 * specified article doesn't exist.
	 *
	 * @param string $year
	 * @param string $month
	 * @param string $day
	 * @param string $slug
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Model
	 */
	public function getArticle($year, $month, $day, $slug, $language)
	{
		$date = Carbon::createFromDate((int) $year, (int) $month, (int) $day);

		return $this->model
			->published()
			->whereDate('published_at', $date)
			->whereSlug($slug)
			->whereLanguage($language)
			->limit(1)
			->firstOrFail();
	}

	public function getPrevArticle($id, $language)
	{
		$article = $this->model
			->published()
			->select('title', 'published_at', 'slug')
			->whereLanguage($language)
			->limit(1)
			->where('id', '>', $id)
			->orderBy('id', 'asc')
			->first();
		
		if ($article) {		
			return (object) [
				'url' => URL::action(
					'Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getArticle',
					[
						'year' => $article->published_at->format('Y'),
						'month' => $article->published_at->format('m'),
						'day' => $article->published_at->format('d'),
						'slug' => $article->slug,
					]),
				'title' => $article->title
			];
		}
		return null;
	}

	public function getNextArticle($id, $language)
	{
		$article = $this->model
			->published()
			->select('title', 'published_at', 'slug')
			->whereLanguage($language)
			->limit(1)
			->where('id', '<', $id)
			->orderBy('id', 'desc')
			->first();
		
		if ($article) {		
			return (object) [
				'url' => URL::action(
					'Libek\LibekOrgRs\Http\Controllers\Front\NewsArticleController@getArticle',
					[
						'year' => $article->published_at->format('Y'),
						'month' => $article->published_at->format('m'),
						'day' => $article->published_at->format('d'),
						'slug' => $article->slug,
					]),
				'title' => $article->title
			];
		}
		return null;
	}

	public function getArticleById($id, $language)
	{
		$id = (int) $id;
		return $this->model
			->published()
			->where('id','=',$id)
			->whereLanguage($language)
			->limit(1)
			->firstOrFail();
	}

}
