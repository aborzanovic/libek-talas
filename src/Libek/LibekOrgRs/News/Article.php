<?php namespace Libek\LibekOrgRs\News;

use Libek\LibekOrgRs\Content\AbstractArticle;

class Article extends AbstractArticle {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'news_articles';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'NewsArticle';

		return $imageConfiguration;
	}

}
