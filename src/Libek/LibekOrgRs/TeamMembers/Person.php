<?php namespace Libek\LibekOrgRs\TeamMembers;

use Libek\LibekOrgRs\Content\AbstractPerson;

class Person extends AbstractPerson {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'team_members';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'TeamMember';

		return $imageConfiguration;
	}

}
