<?php namespace Libek\LibekOrgRs\TeamMembers\Person;

use Creitive\Managers\AbstractBaseManager;
use Libek\LibekOrgRs\TeamMembers\Person;
use Libek\LibekOrgRs\TeamMembers\Person\Repository as PersonRepository;

class Manager extends AbstractBaseManager {

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = [
		'getPaginated' => [
			'page' => ['digits_only', 'min:1'],
			'perPage' => ['digits_only', 'min:1', 'max:100'],
		],
		'create' => [
			'name' => ['required'],
			'surname' => ['required'],
			'title_sr' => ['required'],
			'title_en' => ['required'],
			'image' => ['image', 'max:10240']
		],
		'update' => [
			'name' => ['required'],
			'surname' => ['required'],
			'title_sr' => ['required'],
			'title_en' => ['required'],
			'image' => ['image', 'max:10240']
		],
	];

	/**
	 * An Article repository instance.
	 *
	 * @var \Libek\LibekOrgRs\News\Article\Repository
	 */
	protected $personRepository;

	/**
	 * An ImageRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Content\Image\Repository
	 */
	protected $imageRepository;

	/**
	 * The last total row count for paginated results.
	 *
	 * @var integer
	 */
	protected $lastTotalRowCount;

	public function __construct(PersonRepository $personRepository)
	{
		parent::__construct();

		$this->personRepository = $personRepository;
	}

	/**
	 * Gets the last total row count for paginated results.
	 *
	 * @return integer
	 */
	public function getLastTotalRowCount()
	{
		return $this->lastTotalRowCount;
	}

	/**
	 * Gets articles based on the params passed via the input data.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article\Collection
	 */
	public function getPersons(array $inputData = []){

		$validator = $this->getValidator('getPaginated', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		$page = (int) array_get($inputData, 'page', 1);
		$perPage = (int) array_get($inputData, 'perPage', 10);

		$persons = $this->personRepository->getPersons($page, $perPage);

		$this->lastTotalRowCount = $this->personRepository->getLastTotalRowCount();

		return $persons;
	}

	/**
	 * Attempts to create a new article, and returns it if successful. Returns
	 * `false` on failure.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article|boolean
	 */
	public function createPerson(array $inputData)
	{
		$validator = $this->getValidator('create', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->personRepository->create($inputData);
	}

	/**
	 * Attempts to update the specified article, and returns it if successful.
	 * Returns `false` on failure.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article|boolean
	 */
	public function updatePerson(Person $person, $inputData)
	{
		$validator = $this->getValidator('update', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->personRepository->update($person, $inputData);
	}

	/**
	 * Attempts to delete the specified article.
	 *
	 * The input data must contain an `action` key, with the `confirm` value, in
	 * order for the article to be deleted - otherwise, the article will not be
	 * deleted, and `false` will be returned. If the article has been deleted
	 * successfully, `true` will be returned.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return boolean
	 */
	public function deletePerson(Person $person, array $inputData)
	{
		$action = array_get($inputData, 'action', 'cancel');

		if ($action === 'confirm')
		{
			$person->delete();

			return true;
		}
		else
		{
			return false;
		}
	}

}
