<?php namespace Libek\LibekOrgRs\Validation;

/*
 * Some might consider this a particularly awful class. We can neither confirm
 * nor deny that claim.
 */

use Creitive\FilesArray\File as CreitiveFile;
use DateTime;
use Exception;
use Illuminate\Validation\Validator as BaseValidator;
use Neutron\ReCaptcha\Response as ReCaptchaResponse;
use Sentinel;
use Str;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyFile;

class Validator extends BaseValidator {

	/**
	 * Validates whether a slug is valid.
	 *
	 * The constraints are mostly related to the allowed characters - a slug may
	 * only have lowercase ASCII letters, numbers, and dashes, must not start or
	 * end with a dash, and must not have two consecutive dashes.
	 *
	 * @param string $attribute
	 * @param string $slug
	 * @return bool
	 */
	public function validateSlug($attribute, $slug)
	{
		return $slug === Str::slug($slug);
	}

	/**
	 * Tests whether a given string is a valid integer, meaning it only contains
	 * actual digits, and nothing else.
	 *
	 * @param  string  $string
	 * @return boolean
	 */
	public function validateDigitsOnly($attribute, $input)
	{
		/**
		 * This pattern only allows digits in the string, and nothing else, and
		 * requires at least one digit present.
		 *
		 * @var string
		 */
		$pattern = '/^\d+$/D';

		return (bool) preg_match($pattern, $input);
	}

	/**
	 * Validates whether a passed file is an image.
	 *
	 * Accepts Creitive files, Symfony files, or a path to a file. For more
	 * info, inspect the relevant classes.
	 *
	 * @param string $attribute
	 * @param mixed $file
	 * @return boolean
	 */
	public function validateImage($attribute, $file)
	{
		if ($file instanceof CreitiveFile)
		{
			if ($file->isNotUploaded())
			{
				return true;
			}

			if ($file->isUploadedBadly())
			{
				return false;
			}

			return $file->isImage();
		}

		if ($file instanceof SymfonyFile)
		{
			return parent::validateImage($attribute, $file);
		}

		if (!is_string($file))
		{
			throw new Exception('Invalid type passed to Libek\LibekOrgRs\Validation\Validator::validateImage()');
		}

		if (empty($file))
		{
			return true;
		}

		if (is_readable($file) && is_file($file))
		{
			$file = new CreitiveFile(array(
				'tmp_name' => $file,
			));

			return $file->isImage();
		}

		return false;
	}

	/**
	 * Validates that a date is correct according to the provided format.
	 *
	 * @param string $attribute
	 * @param string $date
	 * @param array  $parameters
	 * @return boolean
	 */
	public function validateCustomDateFormat($attribute, $date, array $parameters = array())
	{
		$format = isset($parameters[0]) ? $parameters[0] : 'Y-m-d';

		return (
			($datetime = DateTime::createFromFormat($format, $date))
			&& ($datetime->format($format) === $date)
		);
	}

	/**
	 * Validates that the provided password matches the one belonging to the
	 * current user.
	 *
	 * Expects to have 'current_user' in the input data.
	 *
	 * @param string $attribute
	 * @param string $password
	 * @return boolean
	 */
	public function validateCurrentPassword($attribute, $password)
	{
		$currentUser = $this->data['current_user'];

		$credentials = ['password' => $password];

		return Sentinel::validateCredentials($currentUser, $credentials);
	}

	/**
	 * Validates that the ReCaptcha response is correct.
	 *
	 * @param string $attribute
	 * @param \Neutron\ReCaptcha\ReCaptcha
	 * @return boolean
	 */
	public function validateValidRecaptchaResponse($attribute, ReCaptchaResponse $response)
	{
		return $response->isValid();
	}

}
