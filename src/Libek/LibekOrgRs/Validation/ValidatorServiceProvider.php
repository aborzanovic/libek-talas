<?php namespace Libek\LibekOrgRs\Validation;

use Illuminate\Support\ServiceProvider;
use Libek\LibekOrgRs\Validation\Validator;

class ValidatorServiceProvider extends ServiceProvider {

	public function register() {}

	public function boot()
	{
		$this->app['validator']->resolver(function($translator, $data, $rules, $messages)
		{
			return new Validator($translator, $data, $rules, $messages);
		});
	}

}
