<?php namespace Libek\LibekOrgRs\Managers;

use Config;
use Creitive\Mandrill\Mailer;
use Lang;
use Mailchimp;
use Mailchimp_List_AlreadySubscribed;
use Mailchimp_lists;
use Str;
use Validator;
use View;

class SubscriptionManager {

	/**
	 * A Mandrill mailer instance.
	 *
	 * @var \Creitive\Mandrill\Mailer
	 */
	protected $mandrillMailer;

	/**
	 * An instance of the Validator class, for returning errors to the
	 * controller.
	 *
	 * @var \Libek\LibekOrgRs\Validation\Validator|null
	 */
	protected $validator = null;

	public function __construct(Mailer $mandrillMailer)
	{
		$this->mandrillMailer = $mandrillMailer;
	}

	/**
	 * Attempts to subscribe the volunteer.
	 *
	 * Returns `true` on success or `false` on failure.
	 *
	 * @param array $inputData
	 * @return boolean
	 */
	public function subscribeVolunteer(array $inputData)
	{
		$validator = $this->getVolunteerSubscriptionValidator($inputData);

		if ($validator->fails())
		{
			$this->validator = $validator;

			return false;
		}

		$this->notifyAdministrators($inputData);
		$this->performVolunteerSubscription($inputData);

		return true;
	}

	/**
	 * Returns the validator instance.
	 *
	 * @return \Libek\LibekOrgRs\Validation\Validator|null
	 */
	public function getValidator()
	{
		return $this->validator;
	}

	/**
	 * Gets a validator for the contact form.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Validation\Validator
	 */
	protected function getVolunteerSubscriptionValidator(array $inputData)
	{
		return Validator::make(
			[
				'email' => $inputData['email'],
				'full_name' => $inputData['full_name'],
			],
			[
				'email' => 'required|email',
				'full_name' => 'required',
			]
		);
	}

	/**
	 * Notifies the website administrators about the new volunteer subscription.
	 *
	 * @param array $inputData
	 * @return array|boolean
	 */
	protected function notifyAdministrators(array $inputData)
	{
		$message = $this->getNotificationMessage($inputData);

		return $this->mandrillMailer->send($message);
	}

	/**
	 * Gets a Mandrill message instance, composed from the appropriate submitted
	 * application.
	 *
	 * @param array $inputData
	 * @return \Creitive\Mandrill\Message
	 */
	protected function getNotificationMessage(array $inputData)
	{
		/**
		 * Compile all needed data.
		 */

		$toAddress = Lang::get('subscriptions.email.to.address');
		$toName = Lang::get('subscriptions.email.to.name');

		$replyToAddress = $inputData['email'];
		$replyToName = $inputData['full_name'];

		$random = Str::random(8);

		$subject = Lang::get(
			'subscriptions.email.subject',
			[
				'random' => $random,
			]
		);

		$body = View::make(
			'subscriptions.volunteers.email',
			[
				'subject' => $subject,
				'inputData' => $inputData,
			]
		)->render();

		$message = $this->mandrillMailer->newMessage()
			->to($toAddress, $toName)
			->replyTo($replyToAddress, $replyToName)
			->subject($subject)
			->body($body);

		return $message;
	}

	/**
	 * Subscribes the volunteer to the predefined list.
	 *
	 * @param array $inputData
	 * @return array
	 */
	protected function performVolunteerSubscription(array $inputData)
	{
		$mailchimp = new Mailchimp(
			Config::get('mailchimp.apiKey'),
			[
				'CURLOPT_FOLLOWLOCATION' => true,
			]
		);

		try
		{
			return $mailchimp->lists->subscribe(
				Config::get('mailchimp.listIds.volunteers'),
				[
					'email' => $inputData['email'],
				],
				[
					'FULL_NAME' => $inputData['full_name'],
				]
			);
		}
		catch (Mailchimp_List_AlreadySubscribed $e)
		{
			return true;
		}
	}

}
