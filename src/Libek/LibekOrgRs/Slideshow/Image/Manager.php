<?php namespace Libek\LibekOrgRs\Slideshow\Image;

use Creitive\Managers\AbstractBaseManager;
use Libek\LibekOrgRs\Slideshow\Image;
use Libek\LibekOrgRs\Slideshow\Image\Repository as ImageRepository;

class Manager extends AbstractBaseManager {

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = [
		'getPaginated' => [
			'page' => ['digits_only', 'min:1'],
			'perPage' => ['digits_only', 'min:1', 'max:100'],
		],
		'create' => [
			'section' => ['required'],
			'image' => ['image', 'max:10240']
		],
		'update' => [
			'section' => ['required'],
			'image' => ['image', 'max:10240']
		],
	];

	/**
	 * An ImageRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Content\Image\Repository
	 */
	protected $imageRepository;

	/**
	 * The last total row count for paginated results.
	 *
	 * @var integer
	 */
	protected $lastTotalRowCount;

	public function __construct(ImageRepository $imageRepository)
	{
		parent::__construct();

		$this->imageRepository = $imageRepository;
	}

	/**
	 * Gets the last total row count for paginated results.
	 *
	 * @return integer
	 */
	public function getLastTotalRowCount()
	{
		return $this->lastTotalRowCount;
	}

	/**
	 * Gets articles based on the params passed via the input data.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article\Collection
	 */
	public function getPage(array $inputData = []){

		$validator = $this->getValidator('getPaginated', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		$page = (int) array_get($inputData, 'page', 1);
		$perPage = (int) array_get($inputData, 'perPage', 10);

		$images = $this->imageRepository->getPage($page, $perPage);

		$this->lastTotalRowCount = $this->imageRepository->getLastTotalRowCount();

		return $images;
	}

	public function getImages(array $inputData = []){

		$section = array_get($inputData, 'section', 'libek');

		$images = $this->imageRepository->getImages($section);

		$this->lastTotalRowCount = $this->imageRepository->getLastTotalRowCount();

		return $images;
	}

	/**
	 * Attempts to create a new article, and returns it if successful. Returns
	 * `false` on failure.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article|boolean
	 */
	public function createImage(array $inputData)
	{
		$validator = $this->getValidator('create', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->imageRepository->create($inputData);
	}

	/**
	 * Attempts to update the specified article, and returns it if successful.
	 * Returns `false` on failure.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article|boolean
	 */
	public function updateImage(Image $image, $inputData)
	{
		$validator = $this->getValidator('update', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->imageRepository->update($image, $inputData);
	}

	/**
	 * Attempts to delete the specified article.
	 *
	 * The input data must contain an `action` key, with the `confirm` value, in
	 * order for the article to be deleted - otherwise, the article will not be
	 * deleted, and `false` will be returned. If the article has been deleted
	 * successfully, `true` will be returned.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @return boolean
	 */
	public function deleteImage(Image $image, array $inputData)
	{
		$action = array_get($inputData, 'action', 'cancel');

		if ($action === 'confirm')
		{
			$image->delete();

			return true;
		}
		else
		{
			return false;
		}
	}

}
