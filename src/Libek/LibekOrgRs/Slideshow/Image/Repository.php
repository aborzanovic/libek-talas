<?php namespace Libek\LibekOrgRs\Slideshow\Image;

use Carbon\Carbon;
use Lang;
use Libek\LibekOrgRs\Content\Image\AbstractRepository;
use Libek\LibekOrgRs\Slideshow\Image;

class Repository extends AbstractRepository {

	public function __construct(Image $model)
	{
		parent::__construct($model);
	}

	/**
	 * Gets paginated articles.
	 *
	 * @param integer $page
	 * @param integer $perPage
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getPage($page, $perPage)
	{
		$query = $this->model
			->sqlCalcFoundRows()
			->orderBy('created_at', 'asc');

		$query = $query->forPage($page, $perPage);

		$images = $query->get();

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $images;
	}

	/**
	 * Creates a new article and returns it.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function create(array $inputData)
	{
		$image = $this->model->newInstance();

		return $this->populateAndSave($image, $inputData);
	}

	/**
	 * Updates the passed article and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function update(Image $image, array $inputData)
	{
		return $this->populateAndSave($image, $inputData);
	}

	/**
	 * Populates the passed Article instance with the input data.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populate(Image $image, array $inputData)
	{
		$image->section = array_get($inputData, 'section', 'libek');
		$image->uploadImage($inputData['image'], 'main');
		return $image;
	}

	/**
	 * Populates the passed instance with the input data, saves it and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populateAndSave(Image $image, array $inputData)
	{
		$image = $this->populate($image, $inputData);

		$image->save();

		return $image;
	}

}
