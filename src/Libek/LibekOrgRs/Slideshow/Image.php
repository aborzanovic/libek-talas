<?php namespace Libek\LibekOrgRs\Slideshow;

use Libek\LibekOrgRs\Content\AbstractImage;

class Image extends AbstractImage {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'slideshow_images';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'SlideshowImages';

		return $imageConfiguration;
	}

}
