<?php namespace Libek\LibekOrgRs\AssetManager;

use Illuminate\Support\ServiceProvider;
use Libek\LibekOrgRs\AssetManager\AssetManager;

class AssetManagerServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['Libek\LibekOrgRs\AssetManager\AssetManager'] = $this->app->share(function($app)
		{
			return new AssetManager(
				$app->environment(),
				$app['creitive.asset'],
				$app['creitive.scripts']
			);
		});
	}

}
