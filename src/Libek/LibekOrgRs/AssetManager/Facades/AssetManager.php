<?php namespace Libek\LibekOrgRs\AssetManager\Facades;

use Illuminate\Support\Facades\Facade;

class AssetManager extends Facade {

	/**
	 * {@inheritDoc}
	 */
	protected static function getFacadeAccessor()
	{
		return 'Libek\LibekOrgRs\AssetManager\AssetManager';
	}

}
