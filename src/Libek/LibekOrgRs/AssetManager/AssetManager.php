<?php namespace Libek\LibekOrgRs\AssetManager;

/**
 * This class is horrible. I'm so sorry.
 */

use Creitive\Asset\Factory as AssetFactory;
use Creitive\ScriptHandler\ScriptHandler;

class AssetManager {

	/**
	 * The current application environment.
	 *
	 * @var string
	 */
	protected $environment;

	/**
	 * An Asset Factory instance.
	 *
	 * @var \Creitive\Asset\Factory
	 */
	protected $asset;

	/**
	 * A ScriptHandler instance.
	 *
	 * @var \Creitive\ScriptHandler\ScriptHandler
	 */
	protected $scriptHandler;

	public function __construct($environment, AssetFactory $asset, ScriptHandler $scriptHandler)
	{
		$this->environment = $environment;
		$this->asset = $asset;
		$this->scriptHandler = $scriptHandler;
	}

	/**
	 * Loads base assets used by the application front-end.
	 *
	 * @return void
	 */
	public function loadAssetsForFront()
	{
		$this->scriptHandler->loadJQuery();
		$this->scriptHandler->loadPolyfillIo();
		$this->scriptHandler->loadFacebookScripts();
		$this->scriptHandler->loadTwitterScripts();

		$this->asset->container('head')->add(
			'system',
			elixir('css/app.css'),
			[
				'polyfill',
				'jquery-ui',
			]
		);
		
		// $this->addScript(
		// 	'bodyEnd',
		// 	'plugins.front',
		// 	[
		// 		'jquery',
		// 		'jquery-ui',
		// 	]
		// );
		
		// $this->addScript(
		// 	'bodyEnd',
		// 	'front',
		// 	[
		// 		'plugins.front',
		// 	]
		// );

        $this->addScript(
            'bodyEnd',
            'bigfoot',
            [
                'jquery'
            ]
		);
		
		$this->addScript(
            'bodyEnd',
            'front_extra',
            [
				'bootstrap',
				'notify'
            ]
		);
		
		$this->addScript(
            'bodyEnd',
            'bootstrap',
            [
				'tether',
				'jquery'
            ]
        );

		$this->addScript(
            'bodyEnd',
            'tether',
            [
                'jquery'
            ]
		);
		
		$this->addScript(
			'bodyEnd',
			'selectize',
			[
				'jquery',
				'front_extra'
			]
		);

		$this->addScript(
			'bodyEnd',
			'notify',
			[
				'jquery'
			]
		);
	}

	/**
	 * Loads base assets used by the admin panel login screen.
	 *
	 * @return void
	 */
	public function loadAssetsForAdminLogin()
	{
		$this->scriptHandler->loadJQuery();
		$this->scriptHandler->loadPolyfillIo();

		$this->asset->container('head')->add(
			'system',
			'/styles/login.css',
			[
				'polyfill',
				'jquery-ui',
			]
		);

		$this->addScript(
			'bodyEnd',
			'plugins.login',
			[
				'jquery',
				'jquery-ui',
			]
		);

		$this->addScript(
			'bodyEnd',
			'login',
			[
				'plugins.login',
			]
		);
	}

	/**
	 * Loads base assets used by the admin panel.
	 *
	 * @return void
	 */
	public function loadAssetsForAdmin()
	{
		$this->scriptHandler->loadJQuery();
		$this->scriptHandler->loadPolyfillIo();

		$this->asset->container('head')->add(
			'system',
			'/styles/admin.css',
			[
				'polyfill',
				'jquery-ui',
			]
		);

		$this->addScript(
			'bodyEnd',
			'plugins.admin',
			[
				'jquery',
				'jquery-ui',
			]
		);

		$this->addScript(
			'bodyEnd',
			'admin_extra',
			[
				'jquery',
				'jquery-ui',
			]
		);

		$this->addScript(
			'bodyEnd',
			'selectize',
			[
				'jquery',
				'jquery-ui',
				'admin_extra'
			]
		);

		$this->addScript(
			'bodyEnd',
			'admin',
			[
				'plugins.admin',
			]
		);
	}

	/**
	 * Adds a script to the requested container, respecting the environment.
	 *
	 * If the current environment is "local", the script will be loaded as
	 * entered - Otherwise, ".min" will be appended to its name, so as to load
	 * the minified versions in other environments (specifically, the
	 * "production" environment).
	 *
	 * @param string $container
	 * @param string $name
	 * @param array $dependencies
	 * @return void
	 */
	public function addScript($container, $name, array $dependencies)
	{
		if ($this->environment === 'local')
		{
			$path = "/scripts/{$name}.js";
		}
		else
		{
			$path = "/scripts/{$name}.min.js";
		}

		$this->asset->container($container)->add(
			$name,
			$path,
			$dependencies
		);
	}

}
