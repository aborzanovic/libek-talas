<?php namespace Libek\LibekOrgRs\Lecturers\Person;

use Carbon\Carbon;
use Lang;
use Libek\LibekOrgRs\Content\Person\AbstractRepository;
use Libek\LibekOrgRs\Lecturers\Person;

class Repository extends AbstractRepository {

	public function __construct(Person $model)
	{
		parent::__construct($model);
	}

	/**
	 * Gets paginated articles.
	 *
	 * @param integer $page
	 * @param integer $perPage
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getPersons($page, $perPage)
	{
		$query = $this->model
			->sqlCalcFoundRows()
			->orderBy('created_at', 'desc');

		$query = $query->forPage($page, $perPage);

		$persons = $query->get();

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $persons;
	}

	/**
	 * Creates a new article and returns it.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function create(array $inputData)
	{
		$person = $this->model->newInstance();

		return $this->populateAndSave($person, $inputData);
	}

	/**
	 * Updates the passed article and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function update(Person $person, array $inputData)
	{
		return $this->populateAndSave($person, $inputData);
	}

	/**
	 * Populates the passed Article instance with the input data.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populate(Person $person, array $inputData)
	{
		$person->name = array_get($inputData, 'name', null);
		$person->surname = array_get($inputData, 'surname', null);
		$person->title_en = array_get($inputData, 'title_en', null);
		$person->title_sr = array_get($inputData, 'title_sr', null);

		if (isset($inputData['image']))
		{
			$person->uploadImage($inputData['image'], 'main');
		}

		return $person;
	}

	/**
	 * Populates the passed instance with the input data, saves it and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populateAndSave(Person $person, array $inputData)
	{
		$person = $this->populate($person, $inputData);

		$person->save();

		return $person;
	}

}
