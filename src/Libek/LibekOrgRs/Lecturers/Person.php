<?php namespace Libek\LibekOrgRs\Lecturers;

use Libek\LibekOrgRs\Content\AbstractPerson;

class Person extends AbstractPerson {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'lecturers';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'Lecturers';

		return $imageConfiguration;
	}

}
