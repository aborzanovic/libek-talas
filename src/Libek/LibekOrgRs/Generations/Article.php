<?php namespace Libek\LibekOrgRs\Generations;

use Libek\LibekOrgRs\Content\AbstractArticle;

class Article extends AbstractArticle {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'generations_articles';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'GenerationsArticle';

		return $imageConfiguration;
	}

}
