<?php namespace Libek\LibekOrgRs\Alumni;

use Libek\LibekOrgRs\Content\AbstractPerson;

class Person extends AbstractPerson {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'alumni';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'Alumni';

		return $imageConfiguration;
	}

}
