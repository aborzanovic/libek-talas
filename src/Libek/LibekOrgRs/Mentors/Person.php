<?php namespace Libek\LibekOrgRs\Mentors;

use Libek\LibekOrgRs\Content\AbstractPerson;

class Person extends AbstractPerson {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'mentors';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'Mentor';

		return $imageConfiguration;
	}

}
