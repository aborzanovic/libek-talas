<?php namespace Libek\LibekOrgRs\Projects;

use Creitive\Database\Eloquent\Model;
use Creitive\Models\Traits\DateableTrait;
use Creitive\Models\Traits\ImageableTrait;
use Creitive\Models\Traits\PublishableTrait;
use Creitive\Models\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Article extends Model {

	use DateableTrait;
	use ImageableTrait;
	use PublishableTrait;
	use SoftDeletingTrait;
	use SortableTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'project_articles';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		return [
			'versions' => [
				'main' => [
					'full' => [],
					'cover' => [
						'method' => 'cropThumbnailImage',
						'width' => 2000,
						'height' => 985,
					],
					'thumbnail' => [
						'method' => 'cropThumbnailImage',
						'width' => 640,
						'height' => 480,
					],
					'og' => [
						'method' => 'cropThumbnailImage',
						'width' => 600,
						'height' => 600,
					],
				],
			],
			'prefix' => 'ProjectArticle',
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function getDates()
	{
		return array_merge(parent::getDates(), ['published_at']);
	}

}
