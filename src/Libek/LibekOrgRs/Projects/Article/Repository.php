<?php namespace Libek\LibekOrgRs\Projects\Article;

use Carbon\Carbon;
use Libek\LibekOrgRs\Projects\Article;

class Repository {

	/**
	 * A ProjectArticle model instance.
	 *
	 * @var \Libek\LibekOrgRs\Projects\Article
	 */
	protected $projectArticleModel;

	public function __construct(Article $projectArticleModel)
	{
		$this->projectArticleModel = $projectArticleModel;
	}

	/**
	 * Gets all articles, in the specified language.
	 *
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getAll($language)
	{
		$projectArticles = $this->projectArticleModel
			->whereLanguage($language)
			->published()
			->orderByNewestPublished()
			->get();

		return $projectArticles;
	}

	/**
	 * Gets a specified article.
	 *
	 * Throws a \Illuminate\Database\Eloquent\ModelNotFoundException if the
	 * specified article doesn't exist.
	 *
	 * @param string $slug
	 * @param string $language
	 * @return \Libek\LibekOrgRs\Projects\Article
	 */
	public function getArticle($slug, $language)
	{
		return $this->projectArticleModel
			->published()
			->whereSlug($slug)
			->whereLanguage($language)
			->limit(1)
			->firstOrFail();
	}

}
