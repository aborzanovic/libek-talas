<?php namespace Libek\LibekOrgRs\JohnGalt;

use Libek\LibekOrgRs\Content\AbstractArticle;

class Article extends AbstractArticle {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'john_galt_news_articles';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'JohnGaltArticle';

		return $imageConfiguration;
	}

}
