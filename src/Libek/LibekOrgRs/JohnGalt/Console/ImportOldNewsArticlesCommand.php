<?php namespace Libek\LibekOrgRs\JohnGalt\Console;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Libek\LibekOrgRs\JohnGalt\Article;

class ImportOldJohnGaltArticlesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:johngalt';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Imports news articles from the old Libek database';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$oldArticles = DB::connection('libek_old')->select('SELECT * FROM `johngalt`');

		foreach ($oldArticles as $oldArticle)
		{
			$article = $this->convertOldArticle($oldArticle);

			//$article->save();
		}
	}

	/**
	 * Converts a row from the old database to a model from the new system.
	 *
	 * @param \stdClass $oldArticle
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function convertOldArticle($oldArticle)
	{
		$newArticle = new Article;

		$newArticle->language = $oldArticle->language;
		$newArticle->title = $oldArticle->title;
		$newArticle->slug = $oldArticle->short_link;
		$newArticle->lead = $oldArticle->text_intro;
		$newArticle->contents = $oldArticle->text_intro . $oldArticle->text;
		$newArticle->meta_title = $oldArticle->title;
		$newArticle->meta_description = '';
		$newArticle->image_main_full = null;
		$newArticle->image_main_thumbnail = null;
		$newArticle->image_main_og = null;
		$newArticle->published_at = null;
		$newArticle->created_at = new Carbon($oldArticle->datetime_added);
		$newArticle->updated_at = new Carbon($oldArticle->datetime_added);
		$newArticle->deleted_at = null;

		return $newArticle;
	}

}
