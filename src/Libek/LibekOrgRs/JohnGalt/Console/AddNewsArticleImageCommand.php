<?php namespace Libek\LibekOrgRs\News\Console;

use Illuminate\Console\Command;
use Libek\LibekOrgRs\JohnGalt\Article;
use Symfony\Component\Console\Input\InputArgument;

class AddJohnGaltArticleImageCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'addimage:johngalt';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Uploads a news article image from a local path.';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$id = $this->argument('id');
		$filename = $this->argument('filename');

		$article = Article::findOrFail($id);

		$article->uploadImage($filename, 'main');
		$article->save();

		$this->info("Successfully uploaded image \"{$filename}\" to article with ID {$id}: \"{$article->title}\"");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('id', InputArgument::REQUIRED, 'The ID of the article whose image is being uploaded.'),
			array('filename', InputArgument::REQUIRED, 'The path to the image being uploaded.'),
		);
	}

}
