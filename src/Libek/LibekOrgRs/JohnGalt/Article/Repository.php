<?php namespace Libek\LibekOrgRs\JohnGalt\Article;

use Carbon\Carbon;
use Lang;
use Libek\LibekOrgRs\Content\Article\AbstractRepository;
use Libek\LibekOrgRs\JohnGalt\Article;

class Repository extends AbstractRepository {

	public function __construct(Article $model)
	{
		parent::__construct($model);
	}

	/**
	 * Gets paginated articles.
	 *
	 * @param integer $page
	 * @param integer $perPage
	 * @param string $language
	 * @return \Creitive\Database\Eloquent\Collection
	 */
	public function getArticles($page, $perPage, $language = null)
	{
		$query = $this->model
			->sqlCalcFoundRows()
			->orderBy('created_at', 'desc');

		if ( ! is_null($language))
		{
			$query = $query->whereLanguage($language);
		}

		$query = $query->forPage($page, $perPage);

		$articles = $query->get();

		$this->lastTotalRowCount = $this->model->getLastTotalRowCount();

		return $articles;
	}

	/**
	 * Creates a new article and returns it.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function create(array $inputData)
	{
		$article = $this->model->newInstance();

		return $this->populateAndSave($article, $inputData);
	}

	/**
	 * Updates the passed article and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	public function update(Article $article, array $inputData)
	{
		return $this->populateAndSave($article, $inputData);
	}

	/**
	 * Populates the passed Article instance with the input data.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populate(Article $article, array $inputData)
	{
		$article->language = array_get($inputData, 'language', null);
		$article->title = array_get($inputData, 'title', null);
		$article->slug = array_get($inputData, 'slug', null);
		$article->lead = array_get($inputData, 'lead', null);
		$article->contents = array_get($inputData, 'contents', null);
		$article->meta_title = array_get($inputData, 'meta_title', null);
		$article->meta_description = array_get($inputData, 'meta_description', null);
		$article->sticky = (int) array_get($inputData, 'sticky', 0);

		$publishedAt = array_get($inputData, 'published_at', '');

		if ($publishedAt)
		{
			$article->published_at = Carbon::createFromFormat(Lang::get('common.dateTimeFormat'), $publishedAt);
		}
		else
		{
			$article->published_at = null;
		}

		if (isset($inputData['image_main']))
		{
			$article->uploadImage($inputData['image_main'], 'main');
		}

		return $article;
	}

	/**
	 * Populates the passed instance with the input data, saves it and returns it.
	 *
	 * @param \Libek\LibekOrgRs\News\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\News\Article
	 */
	protected function populateAndSave(Article $article, array $inputData)
	{
		$article = $this->populate($article, $inputData);

		$article->save();

		return $article;
	}

}
