<?php namespace Libek\LibekOrgRs\Research;

use Libek\LibekOrgRs\Content\AbstractArticle;

class Article extends AbstractArticle {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'research_articles';

	/**
	 * {@inheritDoc}
	 */
	public function getImageConfiguration()
	{
		$imageConfiguration = parent::getImageConfiguration();

		$imageConfiguration['prefix'] = 'ResearchArticle';

		return $imageConfiguration;
	}

}
