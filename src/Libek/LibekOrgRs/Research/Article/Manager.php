<?php namespace Libek\LibekOrgRs\Research\Article;

use Creitive\Managers\AbstractBaseManager;
use Libek\LibekOrgRs\Research\Article;
use Libek\LibekOrgRs\Research\Article\Repository as ArticleRepository;

class Manager extends AbstractBaseManager {

	/**
	 * Validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = [
		'getPaginated' => [
			'page' => ['digits_only', 'min:1'],
			'perPage' => ['digits_only', 'min:1', 'max:100'],
		],
		'create' => [
			'language' => ['required'],
			'title' => ['required'],
			'slug' => ['slug'],
			'lead' => ['required'],
			'contents' => ['required'],
			'sticky' => ['required', 'digits_only', 'min:0'],
			'image_main' => ['image', 'max:10240'],
		],
		'update' => [
			'language' => ['required'],
			'title' => ['required'],
			'slug' => ['slug'],
			'lead' => ['required'],
			'contents' => ['required'],
			'sticky' => ['required', 'digits_only', 'min:0'],
			'image_main' => ['image', 'max:10240'],
		],
	];

	/**
	 * An Article repository instance.
	 *
	 * @var \Libek\LibekOrgRs\Research\Article\Repository
	 */
	protected $articleRepository;

	/**
	 * An ImageRepository instance.
	 *
	 * @var \Libek\LibekOrgRs\Content\Image\Repository
	 */
	protected $imageRepository;

	/**
	 * The last total row count for paginated results.
	 *
	 * @var integer
	 */
	protected $lastTotalRowCount;

	public function __construct(ArticleRepository $articleRepository)
	{
		parent::__construct();

		$this->articleRepository = $articleRepository;
	}

	/**
	 * Gets the last total row count for paginated results.
	 *
	 * @return integer
	 */
	public function getLastTotalRowCount()
	{
		return $this->lastTotalRowCount;
	}

	/**
	 * Gets articles based on the params passed via the input data.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Research\Article\Collection
	 */
	public function getArticles(array $inputData = [])
	{
		$validator = $this->getValidator('getPaginated', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		$page = (int) array_get($inputData, 'page', 1);
		$perPage = (int) array_get($inputData, 'perPage', 10);

		$articles = $this->articleRepository->getArticles($page, $perPage);

		$this->lastTotalRowCount = $this->articleRepository->getLastTotalRowCount();

		return $articles;
	}

	/**
	 * Attempts to create a new article, and returns it if successful. Returns
	 * `false` on failure.
	 *
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Research\Article|boolean
	 */
	public function createArticle(array $inputData)
	{
		$validator = $this->getValidator('create', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->articleRepository->create($inputData);
	}

	/**
	 * Attempts to update the specified article, and returns it if successful.
	 * Returns `false` on failure.
	 *
	 * @param \Libek\LibekOrgRs\Research\Article $article
	 * @param array $inputData
	 * @return \Libek\LibekOrgRs\Research\Article|boolean
	 */
	public function updateArticle(Article $article, $inputData)
	{
		$validator = $this->getValidator('update', $inputData);

		if ($validator->fails())
		{
			$this->errors = $validator->errors();

			return false;
		}

		return $this->articleRepository->update($article, $inputData);
	}

	/**
	 * Attempts to delete the specified article.
	 *
	 * The input data must contain an `action` key, with the `confirm` value, in
	 * order for the article to be deleted - otherwise, the article will not be
	 * deleted, and `false` will be returned. If the article has been deleted
	 * successfully, `true` will be returned.
	 *
	 * @param \Libek\LibekOrgRs\Research\Article $article
	 * @return boolean
	 */
	public function deleteArticle(Article $article, array $inputData)
	{
		$action = array_get($inputData, 'action', 'cancel');

		if ($action === 'confirm')
		{
			$article->delete();

			return true;
		}
		else
		{
			return false;
		}
	}

}
