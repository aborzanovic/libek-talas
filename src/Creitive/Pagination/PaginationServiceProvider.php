<?php namespace Creitive\Pagination;

use Creitive\Pagination\Factory as PaginationFactory;
use Illuminate\Support\ServiceProvider;

class PaginationServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['pagination'] = $this->app->share(function($app)
		{
			return new PaginationFactory($app['config'], $app['translator']);
		});
	}

}
