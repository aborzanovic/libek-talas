<?php namespace Creitive\Pagination;

class Pagination {

	/**
	 * These constants define whether the generated links will have pagination
	 * embedded as an URL segment, or as a query string parameter.
	 */
	const LINKS_SEGMENT = 0;
	const LINKS_QUERY = 1;

	/**
	 * The current page.
	 *
	 * @var integer
	 */
	protected $currentPage = 0;

	/**
	 * The total number of items.
	 *
	 * @var integer
	 */
	protected $totalItems = 0;

	/**
	 * How many items are on each page.
	 *
	 * @var integer
	 */
	protected $itemsPerPage = 10;

	/**
	 * The total number of pages.
	 *
	 * @var integer
	 */
	protected $totalPages = 0;

	/**
	 * How many links before and after the current page should be displayed.
	 *
	 * @var integer
	 */
	protected $visiblePageLinks = 2;

	/**
	 * The base URL upon which the pagination URL segments will be appended.
	 *
	 * @var string
	 */
	protected $baseUrl = '/';

	/**
	 * Whether the URLs will be appended to the `$baseUrl` as a regular segment,
	 * or as a `$_GET` query string parameter. Should be either
	 * `Pagination::LINKS_SEGMENT` or `Pagination::LINKS_QUERY`. The default is
	 * to use the segment strategy.
	 *
	 * @var integer
	 */
	protected $urlStrategy = 0;

	/**
	 * This variable holds all the query parameters that will automatically be
	 * appended to links. The array of these parameters should be passed to an
	 * instance of this class via the `setQueryParameters()` method. The class
	 * will automatically detect the current parameters, and add them to all
	 * links - if this isn't desired, just call `setQueryParameters(array())`,
	 * and no parameters will be appended to links.
	 *
	 * @var array
	 */
	protected $queryParameters = array();

	/**
	 * The default definitions of all the words used for generating the actual
	 * HTML in English. This is used as a fallback, in case the instantiated
	 * object isn't supplied with custom language definitions.
	 *
	 * @var array
	 */
	protected static $defaultLanguageDefinitions = array(
		'first' => 'First',
		'previous' => 'Previous',
		'next' => 'Next',
		'last' => 'Last',
		'urlSegment' => 'page',
	);

	/**
	 * The language currently in use by a Pagination object.
	 *
	 * @var array
	 */
	public $languageDefinitions = array();

	/**
	 * Classes applied to the main `<div>` element container for the complete
	 * pagination.
	 *
	 * @var array
	 */
	protected $paginationCssClasses = array();

	/**
	 * The constructor.
	 *
	 * @param integer $currentPage
	 * @param integer $totalItems
	 * @param integer $itemsPerPage
	 * @param string $baseUrl
	 */
	public function __construct(
		$currentPage,
		$totalItems,
		$itemsPerPage,
		$baseUrl,
		array $languageDefinitions = array(),
		$paginationCssClasses = 'pagination'
	)
	{
		$this->setCurrentPage($currentPage);
		$this->setTotalItems($totalItems);
		$this->setItemsPerPage($itemsPerPage);
		$this->setBaseUrl($baseUrl);
		$this->setLanguageDefinitions($languageDefinitions);
		$this->setPaginationCssClasses($paginationCssClasses);

		if (!empty($_GET))
		{
			$this->setQueryParameters($_GET);
		}
	}

	/**
	 * Sets the language definitions in use.
	 *
	 * Uses only the keys it needs, validates the input data, and uses the
	 * defaults if anything is missing or invalid. The defaults are stored in
	 * the `static::$defaultLanguageDefinitions` array, and are in English.
	 *
	 * @param array $languageDefinitions
	 */
	public function setLanguageDefinitions(array $languageDefinitions)
	{
		foreach (static::$defaultLanguageDefinitions as $key => $default)
		{
			if (isset($languageDefinitions[$key]) && is_string($languageDefinitions[$key]))
			{
				$this->languageDefinitions[$key] = $languageDefinitions[$key];
			}
			else
			{
				$this->languageDefinitions[$key] = $default;
			}
		}
	}

	/**
	 * Sets the current page.
	 *
	 * @param integer $currentPage
	 */
	public function setCurrentPage($currentPage)
	{
		if (!is_int($currentPage))
		{
			throw new \InvalidArgumentException(
				'Pagination::setCurrentPage() only accepts integers, but '
				. (is_object($currentPage) ? get_class($currentPage) : gettype($currentPage))
				. ' given: ' . print_r($currentPage, true)
			);
		}

		if ($currentPage < 0)
		{
			throw new \DomainException(
				'Pagination::setCurrentPage() only accepts integers >= 0, but '
				. $currentPage . ' given.'
			);
		}

		$this->currentPage = (int) $currentPage;
	}

	/**
	 * Gets the current page.
	 *
	 * @return integer
	 */
	public function getCurrentPage()
	{
		return $this->currentPage;
	}

	/**
	 * Sets the total number of items.
	 *
	 * @param integer $totalItems
	 */
	public function setTotalItems($totalItems)
	{
		if (!is_int($totalItems))
		{
			throw new \InvalidArgumentException(
				'Pagination::setTotalItems() only accepts integers, but '
				. (is_object($totalItems) ? get_class($totalItems) : gettype($totalItems))
				. ' given: ' . print_r($totalItems, true)
			);
		}

		if ($totalItems < 0)
		{
			throw new \DomainException(
				'Pagination::setTotalItems() only accepts integers >= 0, but '
				. $totalItems . ' given.'
			);
		}

		$this->totalItems = (int) $totalItems;
		$this->updateTotalPages();
	}

	/**
	 * Gets the total items.
	 *
	 * @return integer
	 */
	public function getTotalItems()
	{
		return $this->totalItems;
	}

	/**
	 * Gets the index of the first visible item.
	 *
	 * @return integer
	 */
	public function getFirstVisibleItem()
	{
		return $this->currentPage * $this->itemsPerPage + 1;
	}

	/**
	 * Gets the index of the last visible item.
	 *
	 * @return integer
	 */
	public function getLastVisibleItem()
	{
		$lastVisibleItem = ($this->currentPage + 1) * $this->itemsPerPage;
		$totalItems = $this->getTotalItems();

		return ($lastVisibleItem < $totalItems) ? $lastVisibleItem : $totalItems;
	}

	/**
	 * Sets the number of items on each page.
	 *
	 * @param integer $itemsPerPage
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		if (!is_int($itemsPerPage))
		{
			throw new \InvalidArgumentException(
				'Pagination::setItemsPerPage() only accepts integers, but '
				. (is_object($itemsPerPage) ? get_class($itemsPerPage) : gettype($itemsPerPage))
				. ' given: ' . print_r($itemsPerPage, true)
			);
		}

		if ($itemsPerPage <= 0)
		{
			throw new \DomainException(
				'Pagination::setItemsPerPage() only accepts integers > 0, but '
				. $itemsPerPage . ' given.'
			);
		}

		$this->itemsPerPage = (int) $itemsPerPage;
		$this->updateTotalPages();
	}

	/**
	 * Gets the number of items on each page.
	 *
	 * @return integer
	 */
	public function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}

	/**
	 * Calculates the total number of pages depending on the total items, and
	 * the items per page, and updates the protected property, so we wouldn't
	 * have to calculate this every time we need the total number of pages, we
	 * will just recalculate it when it actually changes.
	 *
	 * @return integer
	 */
	public function updateTotalPages()
	{
		$this->totalPages = ceil($this->totalItems / $this->itemsPerPage);
	}

	/**
	 * Gets the total number of pages.
	 *
	 * @return integer
	 */
	public function getTotalPages()
	{
		return $this->totalPages;
	}

	/**
	 * Sets the maximum number of direct page links visible before or after the
	 * current page link.
	 *
	 * @param integer $visiblePageLinks
	 */
	public function setVisiblePageLinks($visiblePageLinks)
	{
		if (!is_int($visiblePageLinks))
		{
			throw new \InvalidArgumentException(
				'Pagination::setVisiblePageLinks() only accepts integers, but '
				. (is_object($visiblePageLinks) ? get_class($visiblePageLinks) : gettype($visiblePageLinks))
				. ' given: ' . print_r($visiblePageLinks, true)
			);
		}

		if ($visiblePageLinks <= 0)
		{
			throw new \DomainException(
				'Pagination::setVisiblePageLinks() only accepts integers > 0, but '
				. $visiblePageLinks . ' given.'
			);
		}

		$this->visiblePageLinks = (int) $visiblePageLinks;
	}

	/**
	 * Gets the maximum number of direct page links visible before or after the
	 * current page link.
	 *
	 * @return integer
	 */
	public function getVisiblePageLinks()
	{
		return $this->visiblePageLinks;
	}

	/**
	 * Sets the base url, making sure to suffix it with a forward slash, and to
	 * make it just a slash if it was called with an empty string.
	 *
	 * @param string $baseUrl
	 */
	public function setBaseUrl($baseUrl)
	{
		if (!is_string($baseUrl))
		{
			throw new \InvalidArgumentException(
				'Pagination::setBaseUrl() only accepts strings, but '
				. (is_object($baseUrl) ? get_class($baseUrl) : gettype($baseUrl))
				. ' given: ' . print_r($baseUrl, true)
			);
		}

		if (!$baseUrl)
		{
			$baseUrl = '/';
		}

		if (substr($baseUrl, -1) !== '/')
		{
			$baseUrl .= '/';
		}

		$this->baseUrl = $baseUrl;
	}

	/**
	 * Sets the CSS classes to be applied to the containing `<div>` element. Can
	 * be passed a string or an array. If passed a string, separate CSS classes
	 * should be separated by spaces.
	 *
	 * @param string|array $paginationCssClasses
	 */
	public function setPaginationCssClasses($paginationCssClasses)
	{
		if (is_string($paginationCssClasses))
		{
			$paginationCssClasses = explode(' ', $paginationCssClasses);
		}

		if (!is_array($paginationCssClasses))
		{
			throw new \InvalidArgumentException(
				'Pagination::setPaginationCssClasses() only accepts strings or arrays, but '
				. (is_object($paginationCssClasses) ? get_class($paginationCssClasses) : gettype($paginationCssClasses))
				. ' given: ' . print_r($paginationCssClasses, true)
			);
		}

		foreach ($paginationCssClasses as $key => $cssClass)
		{
			if (!is_string($cssClass))
			{
				throw new \InvalidArgumentException(
					'Pagination::setPaginationCssClasses() was passed an array, but at least one of the values was not a string: '
					. '$paginationCssClasses[' . $key . '] = ' . print_r($cssClass, true)
				);
			}
		}

		$this->paginationCssClasses = $paginationCssClasses;
	}

	/**
	 * Sets the strategy that will be used when page links are generated. Can be
	 * either `Pagination::LINKS_SEGMENT` or `Pagination::LINKS_QUERY`. The
	 * `Pagination::LINKS_SEGMENT` strategy renders pagination links as URL
	 * segments, e.g. `website.com/items/page-1`. The `Pagination::LINKS_QUERY`
	 * strategy renders them as URL query parameters, e.g.
	 * `website.com/items?page=1`. Both versions will also always include the
	 * query parameters forwarded to this class with the `setQueryParameters()`
	 * method.
	 *
	 * @param integer $urlStrategy
	 */
	public function setUrlStrategy($urlStrategy)
	{
		if (!is_int($urlStrategy))
		{
			throw new \InvalidArgumentException(
				'Pagination::setUrlStrategy() only accepts integers, but '
				. (is_object($urlStrategy) ? get_class($urlStrategy) : gettype($urlStrategy))
				. ' given: ' . print_r($urlStrategy, true)
			);
		}

		$allowedStrategies = array(
			static::LINKS_SEGMENT,
			static::LINKS_QUERY,
		);

		if (!in_array($urlStrategy, $allowedStrategies))
		{
			throw new \DomainException(
				'Pagination::setUrlStrategy() only accepts certain predefined constants, but '
				. $urlStrategy . ' given.'
			);
		}

		$this->urlStrategy = $urlStrategy;
	}

	/**
	 * Sets the query parameters that will automatically be appended to all
	 * links. This is automatically called upon creating an instance of this
	 * class, and populated with the current `$_GET` contents.
	 *
	 * Calling this method with an empty `array()` will stop the class from
	 * appending any query parameters to links (except, potentially, the `page`
	 * parameter itself, if the according URL generating strategy is
	 * configured).
	 *
	 * @param array $queryParameters
	 */
	public function setQueryParameters(array $queryParameters)
	{
		$this->queryParameters = $queryParameters;
	}

	/**
	 * Gets the first page link that should be visible (constrained to the
	 * available range of pages).
	 *
	 * @return integer
	 */
	public function getFirstVisiblePage()
	{
		$firstVisiblePage = $this->currentPage - $this->visiblePageLinks;

		return ($firstVisiblePage >= 0) ? $firstVisiblePage : 0;
	}

	/**
	 * Gets the last page link that should be visible (constrained to the
	 * available range of pages).
	 *
	 * @return integer
	 */
	public function getLastVisiblePage()
	{
		$lastVisiblePage = $this->currentPage + $this->visiblePageLinks;

		return ($lastVisiblePage >= $this->totalPages) ? $this->totalPages - 1 : $lastVisiblePage;
	}

	/**
	 * Gets the compiled URL for a page.
	 *
	 * @param  integer $page
	 * @return string
	 */
	protected function getPageUrl($page)
	{
		$segment = $this->languageDefinitions['urlSegment'];

		switch ($this->urlStrategy)
		{
			case static::LINKS_SEGMENT:
				$parameters = $this->queryParameters;

				if ($page)
				{
					$baseUrl = "{$this->baseUrl}{$segment}-{$page}";
				}
				else
				{
					$baseUrl = $this->baseUrl;
				}

				break;

			case static::LINKS_QUERY:
				$parameters = $this->queryParameters;

				if ($page)
				{
					$parameters[$this->languageDefinitions['urlSegment']] = $page;
				}
				else if (isset($parameters[$this->languageDefinitions['urlSegment']]))
				{
					unset($parameters[$this->languageDefinitions['urlSegment']]);
				}

				$baseUrl = $this->baseUrl;

				break;

			default:
				throw new Exception(
					'Pagination::$urlStrategy is corrupted: '
					. print_r($this->urlStrategy, true)
				);
		}

		if ($parameters)
		{
			return $baseUrl . '?' . http_build_query($parameters, '', '&amp;');
		}
		else
		{
			return $baseUrl;
		}
	}

	/**
	 * Returns a `<li>` element with the specified classes and contents.
	 *
	 * @param  string $classes  Multiple classes should be space-separated
	 * @param  string $contents
	 * @return string
	 */
	protected function getLi($classes, $contents)
	{
		return '<li class="' . $classes . '">' . $contents . '</li>';
	}

	/**
	 * Returns an `<a>` element with the specified href and contents.
	 *
	 * @param  string $href
	 * @param  string $contents
	 * @return string
	 */
	protected function getA($href, $contents)
	{
		if ($href)
		{
			return '<a href="' . $href . '">' . $contents . '</a>';
		}
		else
		{
			return '<a>' . $contents . '</a>';
		}
	}

	/**
	 * Gets a compiled link to a page within a list item.
	 *
	 * @param  string $liClasses Multiple classes should be space-separated
	 * @param  string $pageUrl
	 * @param  string $contents
	 * @return string
	 */
	protected function getLiA($liClasses, $pageUrl, $contents)
	{
		return $this->getLi(
			$liClasses,
			$this->getA(
				$pageUrl,
				$contents
			)
		);
	}

	/**
	 * Gets the link for the first page.
	 *
	 * @return string
	 */
	protected function getFirstLink()
	{
		if ($this->currentPage)
		{
			$liClasses = 'first';
			$pageUrl = $this->getPageUrl(0);
		}
		else
		{
			$liClasses = 'first disabled';
			$pageUrl = '';
		}

		$contents = $this->languageDefinitions['first'];

		return $this->getLiA($liClasses, $pageUrl, $contents);
	}

	/**
	 * Gets the link for the previous page.
	 *
	 * @return string
	 */
	protected function getPreviousLink()
	{
		if ($this->currentPage)
		{
			$liClasses = 'previous';
			$pageUrl = $this->getPageUrl($this->currentPage - 1);
		}
		else
		{
			$liClasses = 'previous disabled';
			$pageUrl = '';
		}

		$contents = $this->languageDefinitions['previous'];

		return $this->getLiA($liClasses, $pageUrl, $contents);
	}

	/**
	 * Gets the middle links in the pagination.
	 *
	 * `$this->currentPage ± $thisvisiblePageLinks`
	 *
	 * @return string
	 */
	protected function getMiddleLinks()
	{
		$firstVisiblePage = $this->getFirstVisiblePage();
		$lastVisiblePage = $this->getLastVisiblePage();

		$middleLinks = '';

		if ($firstVisiblePage)
		{
			$middleLinks .= $this->getLiA('disabled', '', '...');
		}

		for ($page = $firstVisiblePage; $page <= $lastVisiblePage; $page++)
		{
			if ($page === $this->currentPage)
			{
				$liClasses = 'active';
				$pageUrl = '';
			}
			else
			{
				$liClasses = '';
				$pageUrl = $this->getPageUrl($page);
			}

			$contents = (string) ($page + 1);

			$middleLinks .= $this->getLiA($liClasses, $pageUrl, $contents);
		}

		if ($lastVisiblePage + 1 < $this->totalPages)
		{
			$middleLinks .= $this->getLiA('disabled', '', '...');
		}

		return $middleLinks;
	}

	/**
	 * Gets the link for the next page.
	 *
	 * @return string
	 */
	protected function getNextLink()
	{
		if ($this->currentPage + 1 < $this->totalPages)
		{
			$liClasses = 'next';
			$pageUrl = $this->getPageUrl($this->currentPage + 1);
		}
		else
		{
			$liClasses = 'next disabled';
			$pageUrl = '';
		}

		$contents = $this->languageDefinitions['next'];

		return $this->getLiA($liClasses, $pageUrl, $contents);
	}

	/**
	 * Gets the link for the last page.
	 *
	 * @return string
	 */
	protected function getLastLink()
	{
		if ($this->currentPage + 1 < $this->totalPages)
		{
			$liClasses = 'last';
			$pageUrl = $this->getPageUrl($this->totalPages - 1);
		}
		else
		{
			$liClasses = 'last disabled';
			$pageUrl = '';
		}

		$contents = $this->languageDefinitions['last'];

		return $this->getLiA($liClasses, $pageUrl, $contents);
	}

	/**
	 * Renders the complete HTML for the pagination and returns it as a string.
	 *
	 * @return string
	 */
	public function render()
	{
		$totalPages = $this->getTotalPages();

		if (!$totalPages)
		{
			return '';
		}

		$links = array(
			$this->getFirstLink(),
			$this->getPreviousLink(),
			$this->getMiddleLinks(),
			$this->getNextLink(),
			$this->getLastLink(),
		);

		$cssClasses = implode(' ', $this->paginationCssClasses);

		return '<ul class="' . $cssClasses . '">' . implode('', $links) . '</ul>';
	}

	/**
	 * `__toString` magic method.
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->render();
	}

}
