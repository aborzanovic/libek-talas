<?php namespace Creitive\Pagination;

use Creitive\Pagination\Pagination;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Translation\Translator;

class Factory {

	/**
	 * An instance of the config repository.
	 *
	 * @var \Illuminate\Config\Repository
	 */
	protected $config;

	/**
	 * An instance of the translator.
	 *
	 * @var \Illuminate\Translation\Translator
	 */
	protected $lang;

	public function __construct(ConfigRepository $config, Translator $lang)
	{
		$this->config = $config;
		$this->lang = $lang;
	}

	/**
	 * Gets a new `Pagination` object, instantiated with the correct parameters,
	 * and the current website language (unless explicitly specified).
	 *
	 * @param integer $currentPage
	 * @param integer $totalItems
	 * @param integer $itemsPerPage
	 * @param string $baseUrl
	 * @param string $language
	 * @return \Creitive\Pagination\Pagination
	 */
	public function get($currentPage, $totalItems, $itemsPerPage, $baseUrl, $language = '')
	{
		return new Pagination(
			$currentPage,
			$totalItems,
			$itemsPerPage,
			$baseUrl,
			$this->getPaginationLanguageDefinitions($language)
		);
	}

	/**
	 * Loads language definitions for the `Pagination` helper class from the
	 * relevant language file - uses the current website language if one is not
	 * provided as an argument.
	 *
	 * @param string $language
	 * @return array
	 */
	public function getPaginationLanguageDefinitions($language = '')
	{
		$language = $language ?: $this->config->get('app.locale');

		return $this->lang->get('pagination', array(), $language);
	}

}
