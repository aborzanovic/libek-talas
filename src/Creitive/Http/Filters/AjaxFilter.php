<?php namespace Creitive\Http\Filters;

use Creitive\Http\Exception\NonAjaxRequestException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class AjaxFilter {

	/**
	 * AJAX route filter.
	 *
	 * This filter only allows AJAX calls, and throws an exception if needed.
	 *
	 * AJAX calls are identified by the `X-Requested-With` header, whose value
	 * must be `XMLHttpRequest`. Obviously, the headers can easily be spoofed,
	 * so this is not meant to be a security filter, but rather a convenience
	 * filter, to make it easier to test and handle AJAX requests.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return void
	 * @throws \Creitive\Http\Exception\NonAjaxRequestException
	 */
	public function filter(Route $route, Request $request)
	{
		if ( ! $request->ajax())
		{
			throw new NonAjaxRequestException;
		}
	}

}
