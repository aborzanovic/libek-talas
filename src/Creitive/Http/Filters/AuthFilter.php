<?php namespace Creitive\Http\Filters;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Redirect;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AuthFilter {

	/**
	 * Checks if the user is authenticated and if they aren't, throws an
	 * `AccessDeniedHttpException` in case of an AJAX request, or redirects the
	 * user to the login page.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|void
	 * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
	 */
	public function filter(Route $route, Request $request)
	{
		if (Auth::guest())
		{
			if ($request->ajax())
			{
				throw new AccessDeniedHttpException;
			}
			else
			{
				return Redirect::guest('/admin/login');
			}
		}
	}

}
