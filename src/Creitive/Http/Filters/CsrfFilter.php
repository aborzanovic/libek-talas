<?php namespace Creitive\Http\Filters;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Session\Store as Session;
use Illuminate\Session\TokenMismatchException;

class CsrfFilter {

	/**
	 * A session store instance.
	 *
	 * @var \Illuminate\Session\Store
	 */
	protected $session;

	public function __construct(Session $session)
	{
		$this->session = $session;
	}

	/**
	 * Checks whether the submitted CSRF token matches the one stored in the
	 * current session.
	 *
	 * If it doesn't, a `TokenMismatchException` will be thrown.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return void
	 * @throws \Illuminate\Session\TokenMismatchException
	 */
	public function filter(Route $route, Request $request)
	{
		if ($this->session->token() !== $request->get('_token'))
		{
			throw new TokenMismatchException;
		}
	}

}
