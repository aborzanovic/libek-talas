<?php namespace Creitive\Http\Filters;

use Auth;

class BasicAuthFilter {

	/**
	 * Performs basic HTTP authentication.
	 *
	 * @return \Symfony\Component\HttpFoundation\Response|void
	 */
	public function filter()
	{
		return Auth::basic();
	}

}
