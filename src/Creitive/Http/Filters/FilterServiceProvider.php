<?php namespace Creitive\Http\Filters;

use Illuminate\Support\ServiceProvider;

class FilterServiceProvider extends ServiceProvider {

	/**
	 * An array of all available filters.
	 *
	 * @var array
	 */
	protected $filters = [
		'ajax' => 'Creitive\Http\Filters\AjaxFilter',
		'auth' => 'Creitive\Http\Filters\AuthFilter',
		'auth.basic' => 'Creitive\Http\Filters\BasicAuthFilter',
		'csrf' => 'Creitive\Http\Filters\CsrfFilter',
		'forceSsl' => 'Creitive\Http\Filters\ForceSslFilter',
		'guest' => 'Creitive\Http\Filters\GuestFilter',
	];

	/**
	 * Boots the service provider.
	 *
	 * @return void
	 */
	public function boot()
	{
		$router = $this->app['router'];

		foreach ($this->filters as $name => $filter)
		{
			$router->filter($name, $filter);
		}
	}

	/**
	 * Registers the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		/**
		 * This provider doesn't register anything.
		 */
	}

}
