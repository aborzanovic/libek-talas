<?php namespace Creitive\Http\Filters;

use Illuminate\Config\Repository as Config;;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Route;

class ForceSslFilter {

	/**
	 * A Config repository instance..
	 *
	 * We need this because we should only apply the filter if the appropriate
	 * config parameter is set. Ideally, we should have a service provider for
	 * this filter, that passes that option to this class, but for now, this
	 * works as a temporary solution.
	 *
	 * @var \Illuminate\Config\Repository
	 */
	protected $config;

	/**
	 * A Redirector instance.
	 *
	 * @var \Illuminate\Routing\Redirector
	 */
	protected $redirect;

	public function __construct(Config $config, Redirector $redirect)
	{
		$this->config = $config;
		$this->redirect = $redirect;
	}

	/**
	 * Redirects the user to the SSL version of the current URI, if they the
	 * current request is not made via SSL.
	 *
	 * @param \Illuminate\Routing\Route $route
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|void
	 */
	public function filter(Route $route, Request $request)
	{
		if ( ! $this->config->get('app.forceSsl'))
		{
			return;
		}

		if ( ! $request->secure())
		{
			return $this->redirect->secure($request->getRequestUri());
		}
	}

}
