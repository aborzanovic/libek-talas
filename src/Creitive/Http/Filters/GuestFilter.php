<?php namespace Creitive\Http\Filters;

use Auth;
use Redirect;

class GuestFilter {

	/**
	 * Redirects the user to the website home page, in case the user is already
	 * logged-in.
	 *
	 * This may be used to prevent logged-in users from accessing pages that
	 * should only be accessible by guests, most notably the login page.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function filter()
	{
		if (Auth::check())
		{
			return Redirect::to('/admin');
		}
	}

}
