<?php namespace Creitive\Http\Exception;

use Exception;

class NonAjaxRequestException extends Exception {}
