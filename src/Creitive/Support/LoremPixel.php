<?php namespace Creitive\Support;

use Config;
use Exception;
use Str;

class LoremPixel {

	const DOMAIN = 'lorempixel.com';

	public static $secure = false;

	/**
	 * Gets an URL to a placeholder image according to the passed arguments.
	 *
	 * @param integer $width
	 * @param integer $height
	 * @param array $parameters
	 * @return string
	 */
	public static function getPlaceholder($width = 100, $height = 200, array $parameters = array())
	{
		$outputParts = array();

		if ( ! empty($parameters['gray']))
		{
			$outputParts[] = 'g';
		}

		$outputParts[] = (string) $width;
		$outputParts[] = (string) $height;

		if ( ! empty($parameters['category']))
		{
			$outputParts[] = $parameters['category'];
		}

		if ( ! empty($parameters['image']))
		{
			$outputParts[] = (string) $parameters['image'];
		}

		if ( ! empty($parameters['text']))
		{
			$outputParts[] = rawurlencode($parameters['text']);
		}

		$protocol = static::$secure ? 'https' : 'http';
		$domain = static::DOMAIN;
		$path = implode('/', $outputParts);
		$queryString = empty($parameters['random']) ? '' : Str::random(16);

		return "{$protocol}://{$domain}/{$path}?{$queryString}";
	}

	/**
	 * Gets a predefined placeholder image.
	 *
	 * @param string $placeholder
	 * @return string
	 */
	public static function get($placeholder)
	{
		$parameters = Config::get("lorempixel.{$placeholder}");

		if ( ! $parameters)
		{
			throw new Exception("Placeholder {$placeholder} not defined yet!");
		}

		$width = $parameters['width'];
		$height = $parameters['height'];

		unset($parameters['width']);
		unset($parameters['height']);

		return static::getPlaceholder($width, $height, $parameters);
	}

}
