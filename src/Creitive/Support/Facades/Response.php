<?php namespace Creitive\Support\Facades;

use Illuminate\Support\Facades\Response as BaseResponse;

class Response extends BaseResponse {

	/**
	 * Return a new JSON error response from the application.
	 *
	 * @param string $message
	 * @param integer $status
	 * @return \Illuminate\Http\JsonResponse
	 */
	public static function jsonError($message, $status = 500)
	{
		$data = [
			'error' => [
				'message' => $message,
			],
		];

		return static::json($data, $status);
	}

}
