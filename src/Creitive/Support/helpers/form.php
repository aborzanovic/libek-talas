<?php

	function post_field($field_name = '', $default = '')
	{
		if ( ! $field_name )
		{
			return null;
		}

		if ( mb_strpos( $field_name, '[' ) === false )
		{
			return _post_field_string( $field_name, $default );
		}
		else
		{
			return _post_field_array( $field_name, $default );
		}

	}

	function _post_field_string($field_name = '', $default = '')
	{
		if ( isset( $_POST[$field_name] ) )
		{
			return $_POST[$field_name];
		}
		else
		{
			return $default;
		}
	}

	function _post_field_array($field_name = '', $default = '')
	{
		$path = parse_array_path( $field_name );

		if ( ! isset( $_POST[$path->root] ) )
		{
			return $default;
		}

		$current = $_POST[$path->root];

		foreach ( $path->indexes as $index )
		{
			if (
				( is_string( $current ) )
				|| ( ! isset( $current[$index] ) )
			)
			{
				return $default;
			}

			$current = $current[$index];
		}

		return $current;
	}

	function parse_array_path($name)
	{
		$name = str_replace( '][', '--->', $name );
		$name = str_replace( ']', '', $name );
		$name = str_replace( '[', '--->', $name );

		$name_parts = explode( '--->', $name );

		$path = new stdClass();

		$path->root = array_shift( $name_parts );
		$path->indexes = $name_parts;

		return $path;
	}

	function post_checked($field_name = '', $default = false)
	{
		if ( ! $field_name )
		{
			return false;
		}

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
		{
			return ( post_field( $field_name ) === $field_name );
		}
		else
		{
			return $default;
		}
	}

	function render_notes($notes, $class)
	{
		if ( ! is_array( $notes ) )
		{
			return "<div class=\"alert-{$class}\">{$notes}</div>";
		}

		$output = '';

		foreach ($notes as $note)
		{
			$output .= "<div class=\"alert-{$class}\">{$note}</div>";
		}

		return $output;
	}

	function render_errors($errors, $name)
	{
		if ( ! isset( $errors[$name] ) )
		{
			return '';
		}

		return render_notes($errors[$name], 'error');
	}

	function render_info($info)
	{
		if ( ! $info )
		{
			return '';
		}

		return render_notes($info, 'info');
	}

	function render_warning($warning)
	{
		if ( ! $warning )
		{
			return '';
		}

		return render_notes($warning, 'warning');
	}

	function render_text_input_with_checkbox($name = '', $label = '', $default = '', $checked = false, $errors = array(), $classes = '', $info = '', $attributes = array(),  $container_classes = '')
	{
		if ( ! $name )
		{
			return false;
		}

		if ( is_array( $classes ) )
		{
			$classes = implode(' ', $classes);
		}

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		$value = htmlspecialchars(post_field($name, $default));

		$name_public = "{$name}_public";
		$checked = post_checked( $name_public, $checked );
		$checked_attr = $checked ? 'checked="checked"' : '';

		return <<<FORM
		<div class="item-container {$container_classes}">
			<label for="form-{$name}">{$label}:</label>
			<input
				class="{$classes}"
				type="text"
				id="form-{$name}"
				name="{$name}"
				value="{$value}"
				{$attrs}
			>
			<label class="checkbox-field">
				<input
					id="form-{$name_public}"
					type="checkbox"
					name="{$name_public}"
					{$checked_attr}
					value="{$name_public}"
				>
			</label>

			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_text_input($name = '', $label = '', $default = '', $errors = array(), $classes = '', $info = '', $attributes = array(), $container_classes = '')
	{
		if ( ! $name )
		{
			return false;
		}

		if ( is_array( $classes ) )
		{
			$classes = implode(' ', $classes);
		}

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		if ( isset( $errors[$name] ) )
		{
			$classes .= ' error';
		}

		$value = htmlspecialchars(post_field($name, $default));

		return <<<FORM
		<div class="item-container {$container_classes}">
			<label for="form-{$name}">{$label}:</label>
			<input
				class="{$classes}"
				type="text"
				id="form-{$name}"
				name="{$name}"
				value="{$value}"
				{$attrs}
			>

			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_text_input_with_placeholder($name = '', $placeholder = '', $default = '', $errors = array(), $classes = '', $info = '', $attributes = array(),  $container_classes = '')
	{
		if ( ! $name )
		{
			return false;
		}

		if ( is_array( $classes ) )
		{
			$classes = implode(' ', $classes);
		}

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		if ( isset( $errors[$name] ) )
		{
			$classes .= ' error';
		}

		$value = htmlspecialchars(post_field($name, $default));

		return <<<FORM
		<div class="item-container {$container_classes}">
			<input
				class="input-placeholder-replacement {$classes}"
				type="text"
				id="form-{$name}"
				name="{$name}"
				value="{$value}"
				title="{$placeholder}"
				placeholder="{$placeholder}"
				{$attrs}
			>

			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_password_input($name = '', $label = '', $default = '', $errors = array(), $classes = '', $info = '', $attributes = array(),  $container_classes = '')
	{
		if ( ! $name )
		{
			return false;
		}

		if ( is_array( $classes ) )
		{
			$classes = implode(' ', $classes);
		}

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		if ( isset( $errors[$name] ) )
		{
			$classes .= ' error';
		}

		$value = htmlspecialchars(post_field($name, $default));

		return <<<FORM
		<div class="item-container {$container_classes}">
			<label for="form-{$name}">{$label}:</label>
			<input
				class="{$classes}"
				type="password"
				id="form-{$name}"
				name="{$name}"
				value="{$value}"
				{$attrs}
			>

			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_textarea($name = '', $label = '', $default = '', $classes = 'redactor', $errors = array(), $info = '', $rows = '', $cols = '', $container_classes = '')
	{
		if ( ! $name )
		{
			return false;
		}

		if ( is_array( $classes ) )
		{
			$classes = implode(' ', $classes);
		}

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$info = render_info($info);
		$errors = render_errors($errors, $name);

		if ( isset( $errors[$name] ) )
		{
			$classes .= ' error';
		}

		$value = htmlspecialchars(post_field($name, $default));

		$rows = $rows ? "rows=\"{$rows}\"" : '';
		$cols = $cols ? "cols=\"{$cols}\"" : '';

		return <<<FORM
		<div class="item-container {$container_classes}">
			<label for="form-{$name}" data-status="closed" >{$label}:</label>
			<div>
				<textarea
					{$rows}
					{$cols}
					class="{$classes}"
					id="form-{$name}"
					name="{$name}">{$value}</textarea>

				{$info}
				{$errors}
			</div>
		</div>
FORM;
	}

	function render_image($name, $label, $default = '', $errors = array(), $info='', $width = 160, $height = 120, $deletable = false, $title = '', $container_classes = '')
	{
		$title			= htmlspecialchars( $title, ENT_QUOTES, 'UTF-8' );
		$errors			= render_errors( $errors, $name );
		$info			= render_info( $info );

		$delete			= '';
		$image_preview	= '';

		if ( $width && $height )
		{
			$width_height = "width=\"{$width}\" height=\"{$height}\"";
		}
		else
		{
			$width_height = '';
		}

		if ( $default != '' )
		{
			if ( $deletable )
			{
				$delete = <<<DEL
				<div>
					<input id="form-delete_{$name}" name="delete_{$name}" value="delete_{$name}" type="checkbox"> <label for="form-delete_{$name}" class="label-for-checkbox">Obrišite sliku</label>
				</div>
DEL;
			}

			$image_preview = <<<PREV
				<div>
					<img src="{$default}" {$width_height} alt="{$title}" title="{$title}">
				</div>
PREV;
		}

		return <<<FORM
		<div class="item-container {$container_classes}">
			<label for="form-{$name}">{$label}:</label>

			{$image_preview}

			<input id="form-{$name}" type="file" name="{$name}">
			{$info}

			{$delete}

			{$errors}
		</div>
FORM;
	}

	function render_select($name, $label, $options = array(), $selected = null, $errors = array(), $info = '', $container_classes = '', $attributes = array())
	{
		// other stuff
		$selected = post_field($name, $selected);

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$output_options = '';

		foreach ($options as $value => $text)
		{
			if ( $value == $selected )
			{
				$output_options .= '<option value="' . $value . '" selected>' . $text . '</option>';
			}
			else
			{
				$output_options .= '<option value="' . $value . '">' . $text . '</option>';
			}
		}

		if ( $label )
		{
			$label = '<label for="form-'.$name.'" data-status="closed">'.$label.':</label>';
		}

		return <<<FORM
		<div class="item-container {$container_classes}">
			{$label}
				<select id="form-{$name}" name="{$name}" {$attrs}>
					{$output_options}
				</select>

				{$info}
				{$errors}
		</div>
FORM;
	}

	function render_select_with_checkbox($name, $label, $options = array(), $selected = null, $checked = false, $errors = array(), $info = '', $container_classes = '', $attributes = array())
	{
		// other stuff
		$selected = post_field($name, $selected);

		$info		= render_info($info);
		$errors		= render_errors($errors, $name);
		$attrs		= render_attributes($attributes);

		if ( is_array( $container_classes ) )
		{
			$container_classes = implode(' ', $container_classes);
		}

		$output_options = '';

		foreach ($options as $value => $text)
		{
			if ( $value == $selected )
			{
				$output_options .= '<option value="' . $value . '" selected>' . $text . '</option>';
			}
			else
			{
				$output_options .= '<option value="' . $value . '">' . $text . '</option>';
			}
		}

		if ( $label )
		{
			$label = '<label for="form-'.$name.'" data-status="closed">'.$label.':</label>';
		}

		$name_public = "{$name}_public";
		$checked = post_checked( $name_public, $checked );
		$checked_attr = $checked ? 'checked="checked"' : '';

		return <<<FORM
		<div class="item-container {$container_classes}">
			{$label}
				<select id="form-{$name}" name="{$name}" {$attrs}>
					<option value="0"></option>
					{$output_options}
				</select>

				<label class="checkbox-field">
					<input
						id="form-{$name_public}"
						type="checkbox"
						name="{$name_public}"
						{$checked_attr}
						value="{$name_public}"
					>
				</label>

				{$info}
				{$errors}
		</div>
FORM;
	}

	function render_checkbox($name, $label = '', $default = false, $info = '', $errors = '', $attributes = array() )
	{
		if ( ! $name )
		{
			return false;
		}

		$info = render_info( $info );
		$errors = render_errors( $errors, $name );
		$attrs = render_attributes( $attributes );

		if ( isset( $_POST[$name] ) && ( $_POST[$name] === $name ) )
		{
			$checked = true;
		}
		else if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
		{
			$checked = false;
		}
		else
		{
			$checked = $default;
		}

		$label_element = '';

		if ( ! empty( $label ) )
		{
			$label_element = '<label for="form-' . $name . '" class="label-for-checkbox">' . $label . '</label>';
		}

		$checked = $checked ? 'checked="checked"' : '';

		return <<<FORM
		<div class="item-container" {$attrs}>
			<input
				id="form-{$name}"
				type="checkbox"
				name="{$name}"
				{$checked}
				value="{$name}"
			>
			$label_element
			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_checkbox_multiple($name, $value, $default = false)
	{
		if ( ! $name )
		{
			return false;
		}

		if ( isset( $_POST[$name] ) && ( $_POST[$name] === $name ) )
		{
			$checked = true;
		}
		else if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
		{
			$checked = false;
		}
		else
		{
			$checked = $default;
		}

		$checked = $checked ? 'checked="checked"' : '';

		return <<<FORM
		<div class="item-container">
			<input
				type="checkbox"
				name="{$name}"
				{$checked}
				value="{$value}"
			>
		</div>
FORM;
	}

	function render_radio($name, $label = '', $value = '', $default = false, $info = '', $errors = '')
	{
		if ( ! $name )
		{
			return false;
		}

		$info = render_info( $info );
		$errors = render_errors($errors, $name);

		if ( isset( $_POST[$name] ) && ( $_POST[$name] === $name ) )
		{
			$checked = true;
		}
		else if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
		{
			$checked = false;
		}
		else
		{
			$checked = $default;
		}

		$checked = $checked ? 'checked="checked"' : '';

		$checked = $checked ? 'checked="checked"' : '';

		$input_element = <<<INPUT
			<input
				type="radio"
				name="{$name}"
				{$checked}
				value="{$value}"
			>
INPUT;

		if ( ! empty( $label ) )
		{
			$label_element = "<label>{$input_element} {$label}</label>";
		}
		else
		{
			$label_element = $input_element;
		}

		return <<<FORM
		<div class="item-container">
			{$label_element}
			{$info}
			{$errors}
		</div>
FORM;
	}

	function render_category_options( $categories = null, $level = 0, $parent_id = 0, $current_id = -1 )
	{
		$options = '';

		$indentation = str_repeat('&nbsp;', $level * 5);

		foreach ( $categories->list as $category )
		{
			$cat_id = $category->get_id();

			$disabled	= ( $cat_id == $current_id ) ? 'disabled' : '';
			$selected	= ( $cat_id == $parent_id ) ? 'selected' : '';

			$options	.= "<option value=\"{$cat_id}\" {$selected} {$disabled}>{$indentation}{$category->name_sr}</option>";

			$options .= render_category_options($category->subcategories, $level + 1, $parent_id, $current_id);
		}

		return $options;
	}

	function render_page_options($pages = null, $level = 0, $parent_id = 0, $current_id = -1)
	{
		$options = '';

		$indentation = str_repeat('&nbsp;', $level * 5);

		foreach ($pages->list as $page)
		{
			$cat_id = $page->get_id();

			$disabled	= ( $cat_id == $current_id ) ? 'disabled' : '';
			$selected	= ( $cat_id == $parent_id ) ? 'selected' : '';

			$options	.= "<option value=\"{$cat_id}\" {$selected} {$disabled}>{$indentation}{$page->title}</option>";

			$options .= render_page_options($page->subpages, $level + 1, $parent_id, $current_id);
		}

		return $options;
	}

	function render_hidden($name = '', $value = '')
	{
		if ( ! $name )
		{
			return false;
		}

		return <<<FORM
		<input type="hidden" name="{$name}" value="{$value}">
FORM;
	}

	function render_submit($value = 'Submit', $name = 'submit', $disabled = false)
	{
		$value = htmlspecialchars( $value );
		$disabled = $disabled ? 'disabled' : '';

		return <<<FORM
		<div class="button-container">
			<input type="submit" name="{$name}" value="{$value}" {$disabled}>
		</div>
FORM;
	}

	/**
	 * Returns the original filename supplied by the client during the upload,
	 * or a default value provided as the second argument.
	 * If the client didn't supply the value, and a default value wasn't set,
	 * the function will automatically generate a random string.
	 * The random generator isn't meant to be secure in any way, so it shouldn't
	 * be relied upon for dealing with sensitive data.
	 * This function is multibyte-safe.
	 * @param	string	$files_index	The index of the file info in $_FILES
	 * @param	string	$default		The default value to return if $_FILES
	 *									fails to supply a name for the file
	 * @return	string					The resultant filename
	 */
	function get_original_filename($files_array, $default = null)
	{
		if ( isset( $files_array['name'] ) && $files_array['name'] )
		{
			return mb_basename( $files_array['name'] );
		}
		else if ( $default )
		{
			return $default;
		}
		else
		{
			return generate_random_string();
		}
	}

	/**
	 * Extracts a decimal number (maybe a version number) from a string
	 *
	 * Meant to be used for cleaning some extra garbled characters when
	 * pasting stuff like latitudes/longitutes. Also able to grab numbers
	 * with multiple decimals, like 3.4.7 (hence the version number hint)
	 * @param    string   $string   Input string
	 * @return   string             Cleaned string
	 */
	function extract_decimal_number($string)
	{
		if (preg_match('/\-?\d+(?:\.\d+)+/', $string, $matches))
		{
			return $matches[0];
		}

		return '';
	}

	/**
	 * Formats a length by first formatting it to two decimal places, with a
	 * dot as a decimal separator, and then strips all zeroes from the right
	 * side, and, if necessary, the decimal separator.
	 *
	 * The result is lengths like "6", "8", "8.5", "11", etc. - never something
	 * like "6.00", or "8.50".
	 *
	 * @param  integer $length
	 * @return string
	 */
	function format_length( $length = 0 )
	{
		$length = number_format( $length / 100, 2, '.', '' );
		$length = rtrim( $length, '0' );
		$length = rtrim( $length, '.' );

		return $length;
	}

if ( ! function_exists( 'is_valid_date' ) )
{
	/**
	 * Check whether a date is valid
	 *
	 * Checks an input date against a specifiable format, to see
	 * whether the input matches, and whether it results in the
	 * expected DateTime object. For a guide to available formats
	 * see http://php.net/manual/en/function.date.php
	 *
	 * @param  string  $date   The date to be checked
	 * @param  string  $format A valid date format accepted by
	 *                         DateTime::createFromFormat
	 * @return boolean
	 */
	function is_valid_date( $date, $format = 'd.m.Y.' )
	{
		return (
			( $datetime = DateTime::createFromFormat( $format, $date ) )
			&& ( $datetime->format( $format ) === $date )
		);
	}
}
else
{
	throw new LogicException( 'Duplicate function definition: is_valid_date' );
}

if ( ! function_exists( 'is_valid_time' ) )
{
	/**
	 * Checks whether a time is valid
	 *
	 * Checks whether an entered time is valid. Accepts only times in the
	 * "HH:MM" 24-hour format
	 *
	 * @param  string  $time The time to be checked
	 * @return boolean
	 */
	function is_valid_time( $time )
	{
		$valid_format = '/^\d\d:\d\d$/D';

		if ( ! preg_match( $valid_format, $time ) )
		{
			return false;
		}

		list( $h, $m ) = explode( ':', $time );

		$h = (int) $h;
		$m = (int) $m;

		if (
				( $h < 0 )
				|| ( $h > 23 )
				|| ( $m < 0 )
				|| ( $m > 59 )
			)
		{
			return false;
		}

		return true;
	}
}
else
{
	throw new LogicException( 'Duplicate function definition: is_valid_time' );
}

if ( ! function_exists( 'isImageFile' ) )
{
	/**
	 * Checks whether a file is an image.
	 *
	 * Performs a series of checks to determine if the file is really an image,
	 * including actually opening the file using `Imagick` and `GD`.
	 *
	 * @param  string  $filePath
	 * @return boolean
	 */
	function isImageFile($filePath)
	{
		if (!file_exists($filePath))
		{
			return false;
		}

		try
		{
			$image = new \Imagick($filePath);
		}
		catch (\ImagickException $exception)
		{
			return false;
		}
		catch (\Exception $exception)
		{
			\Log::error($exception);
			return false;
		}

		if (!$image->valid())
		{
			return false;
		}

		$image->destroy();

		$info = getimagesize($filePath);

		if (!$info)
		{
			return false;
		}

		return ($info[0] && $info[1]);
	}
}
else
{
	throw new LogicException( 'Duplicate function definition: isImageFile' );
}

if ( ! function_exists( 'is_valid_image_upload' ) )
{
	/**
	 * Checks whether a $_FILES array entry is a valid uploaded image file
	 *
	 * @param  array   $files_array
	 * @return boolean
	 */
	function is_valid_image_upload( array $files_array )
	{
		return isImageFile( $files_array['tmp_name'] );
	}
}
else
{
	throw new LogicException( 'Duplicate function definition: is_valid_image_upload' );
}

if ( ! function_exists( 'is_valid_select_option' ) )
{
	/**
	 * Checks whether a select option (submitted through a form and assumed to
	 * be a string) is valid, according to an array of valid select options.
	 * @param  string  $option
	 * @param  array   $select_options
	 * @return boolean
	 */
	function is_valid_select_option( $option, array $select_options )
	{
		$select_options = array_map( 'strval', array_keys( $select_options ) );
		return in_array( $option, $select_options, true );
	}

}
