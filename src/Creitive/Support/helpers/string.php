<?php

	/**
	 * Performs a multi-byte str_split with an optional chunk length.
	 *
	 * @param  string $string
	 * @param  int    $split_length = 1
	 * @param  string $encoding = 'UTF-8'
	 * @return array
	 */
	function mb_str_split($string, $split_length = 1, $encoding = 'UTF-8')
	{
		$length = mb_strlen($string, $encoding);
		$result = array();
		$chunk = '';
		$chunk_length = 0;

		for ($i = 0; $i < $length; $i++)
		{
			$chunk .= mb_substr($string, $i, 1, $encoding);
			$chunk_length++;

			if ($chunk_length === $split_length)
			{
				$result[] = $chunk;
				$chunk = '';
				$chunk_length = 0;
			}
		}

		if ($chunk_length)
		{
			$result[] = $chunk;
		}

		return $result;
	}
