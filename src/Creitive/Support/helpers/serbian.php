<?php

	/**
	 * Returns the name of the day in Serbian, lowercase.
	 *
	 * @param  int|DateTime $datetime
	 * @param  string       $script = 'sr-lat' Can be 'sr-lat' or 'sr-cyr'
	 * @return string
	 */
	function get_serbian_day_name( $datetime, $script = 'sr-lat' )
	{
		$day_names['sr-lat'][0]			= 'nedelja';
		$day_names['sr-lat'][1]			= 'ponedeljak';
		$day_names['sr-lat'][2]			= 'utorak';
		$day_names['sr-lat'][3]			= 'sreda';
		$day_names['sr-lat'][4]			= 'četvrtak';
		$day_names['sr-lat'][5]			= 'petak';
		$day_names['sr-lat'][6]			= 'subota';
		$day_names['sr-lat']['default']	= 'greška';

		$day_names['sr-cyr'][0]			= 'недеља';
		$day_names['sr-cyr'][1]			= 'понедељак';
		$day_names['sr-cyr'][2]			= 'уторак';
		$day_names['sr-cyr'][3]			= 'среда';
		$day_names['sr-cyr'][4]			= 'четвртак';
		$day_names['sr-cyr'][5]			= 'петак';
		$day_names['sr-cyr'][6]			= 'субота';
		$day_names['sr-cyr']['default']	= 'грешка';

		if (!isset($day_names[$script]))
		{
			return 'Nepostojeći jezik.';
		}

		if ( $datetime instanceof DateTime )
		{
			$day_index = (int) $datetime->format('w');
		}
		else
		{
			$day_index = (int) $datetime;
		}

		if ( $day_index === 7 )
		{
			$day_index = 0;
		}

		return isset($day_names[$script][$day_index]) ? $day_names[$script][$day_index] : $day_names[$script]['default'];
	}

	/**
	 * Returns the name of the month in Serbian, lowercase.
	 *
	 * @param  int|DateTime $datetime          Either a datetime or a numeric
	 *                                         month index
	 * @param  string       $script = 'sr-lat' Can be 'sr-lat' or 'sr-cyr'
	 * @return string
	 */
	function get_serbian_month_name($datetime, $script = 'sr-lat')
	{
		$month_names['sr-lat'][1]			= 'januar';
		$month_names['sr-lat'][2]			= 'februar';
		$month_names['sr-lat'][3]			= 'mart';
		$month_names['sr-lat'][4]			= 'april';
		$month_names['sr-lat'][5]			= 'maj';
		$month_names['sr-lat'][6]			= 'jun';
		$month_names['sr-lat'][7]			= 'jul';
		$month_names['sr-lat'][8]			= 'avgust';
		$month_names['sr-lat'][9]			= 'septembar';
		$month_names['sr-lat'][10]			= 'oktobar';
		$month_names['sr-lat'][11]			= 'novembar';
		$month_names['sr-lat'][12]			= 'decembar';
		$month_names['sr-lat']['default']	= 'greška';

		$month_names['sr-cyr'][1]			= 'јануар';
		$month_names['sr-cyr'][2]			= 'фебруар';
		$month_names['sr-cyr'][3]			= 'март';
		$month_names['sr-cyr'][4]			= 'април';
		$month_names['sr-cyr'][5]			= 'мај';
		$month_names['sr-cyr'][6]			= 'јун';
		$month_names['sr-cyr'][7]			= 'јул';
		$month_names['sr-cyr'][8]			= 'август';
		$month_names['sr-cyr'][9]			= 'септембар';
		$month_names['sr-cyr'][10]			= 'октобар';
		$month_names['sr-cyr'][11]			= 'новембар';
		$month_names['sr-cyr'][12]			= 'децембар';
		$month_names['sr-cyr']['default']	= 'грешка';

		if (!isset($month_names[$script]))
		{
			return 'Nepostojeći jezik.';
		}

		if ( $datetime instanceof DateTime )
		{
			$month_index = (int) $datetime->format('m');
		}
		else
		{
			$month_index = (int) $datetime;
		}

		return (isset($month_names[$script][$month_index])) ? $month_names[$script][$month_index] : $month_names[$script]['default'];
	}

	/**
	 * Returns the abbreviation of the current month in title case.
	 *
	 * @param  int|DateTime $month
	 * @return string
	 */
	function get_serbian_month_abbr( $month )
	{
		$name = get_serbian_month_name( $month );
		$name = mb_substr( $name, 0, 3 );
		$name = mb_convert_case( $name, MB_CASE_TITLE );

		return $name;
	}

	/**
	 * Returns the file upload error message in Serbian.
	 *
	 * @param  int    $error_code
	 * @param  string $script = 'sr-lat' Can be 'sr-lat' or 'sr-cyr'
	 * @return string
	 */
	function file_upload_error_message($error_code, $script = 'sr-lat')
	{
		$messages['sr-lat'][UPLOAD_ERR_INI_SIZE]	= 'Veličina fajla premašuje dozvoljenu vrednost podešenu na serveru.';
		$messages['sr-lat'][UPLOAD_ERR_FORM_SIZE]	= 'Veličina fajla je veća od dozvoljene.';
		$messages['sr-lat'][UPLOAD_ERR_PARTIAL]		= 'Fajl nije kompletno poslat.';
		$messages['sr-lat'][UPLOAD_ERR_NO_FILE]		= 'Nije izabran nijedan fajl za kačenje.';
		$messages['sr-lat'][UPLOAD_ERR_NO_TMP_DIR]	= 'Na serveru fali privremeni direktorijum.';
		$messages['sr-lat'][UPLOAD_ERR_CANT_WRITE]	= 'Fajl nije uspešno zapisan na disk.';
		$messages['sr-lat'][UPLOAD_ERR_EXTENSION]	= 'Kačenje fajla prekinuto od strane neke PHP ekstenzije.';
		$messages['sr-lat']['default']				= 'Nepoznata greška prilikom kačenja fajla.';

		$messages['sr-cyr'][UPLOAD_ERR_INI_SIZE]	= 'Величина фајла премашује дозвољену вредност подешену на серверу.';
		$messages['sr-cyr'][UPLOAD_ERR_FORM_SIZE]	= 'Величина фајла је већа од дозвољене.';
		$messages['sr-cyr'][UPLOAD_ERR_PARTIAL]		= 'Фајл није комплетно послат.';
		$messages['sr-cyr'][UPLOAD_ERR_NO_FILE]		= 'Није изабран ниједан фајл за качење.';
		$messages['sr-cyr'][UPLOAD_ERR_NO_TMP_DIR]	= 'На серверу фали привремени директоријум.';
		$messages['sr-cyr'][UPLOAD_ERR_CANT_WRITE]	= 'Фајл није успешно записан на диск.';
		$messages['sr-cyr'][UPLOAD_ERR_EXTENSION]	= 'Качење фајла прекинуто од стране неке PHP екстензије.';
		$messages['sr-cyr']['default']				= 'Непозната грешка приликом качења фајла.';

		if (!isset($messages[$script]))
		{
			return 'Nepostojeći jezik.';
		}

		return (isset($messages[$script][$error_code])) ? $messages[$script][$error_code] : $messages[$script]['default'];
	}

	/**
	 * Changes word form depending on the given number.
	 *
	 * Expects an array with values '1', '2-4', and '5+' set.
	 *
	 * @param  integer $number The input number
	 * @param  array   $words  The array of different word forms
	 * @return string          The appropriate word form
	 */
	function get_serbian_word_form_by_number($number = 0, $words = array())
	{
		if ( is_string( $words ) )
		{
			return $words;
		}

		if ( ! is_array( $words ) )
		{
			return '';
		}

		if ( ! isset( $words['1'], $words['2-4'], $words['5+'] ) )
		{
			$words = array(
				'1'		=> 'nova',
				'2-4'	=> 'nove',
				'5+'	=> 'novih',
			);
		}

		// Extract the last two digits
		$digit_tens = (int) ( ( $number % 100 ) / 10 );
		$digit_ones = $number % 10;

		if ( $digit_tens === 1)
		{
			return $words['5+'];
		}
		else if ( $digit_ones === 1 )
		{
			return $words['1'];
		}
		else if ( ( 2 <= $digit_ones ) && ( $digit_ones <= 4 ) )
		{
			return $words['2-4'];
		}
		else
		{
			return $words['5+'];
		}
	}

