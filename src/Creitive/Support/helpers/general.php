<?php

	/**
	 * Extracts only the digits from a multibyte string.
	 *
	 * @param  string $string Input string
	 * @return string
	 */
	function isolate_digits_from_string($string)
	{
		$digits = '';
		$string_length = mb_strlen($string);
		for ($i = 0; $i < $string_length; $i++)
		{
			$c = mb_substr($string, $i, 1);
			if (
					( strlen($c) === 1 )
					&& ( $c >= '0' )
					&& ( $c <= '9' )
				)
			{
				$digits .= (string) $c;
			}
		}

		return $digits;
	}

	/**
	 * Returns an array with links to categories as keys, and the category names as values.
	 *
	 * Useful for generating breadcrumbs.
	 *
	 * @param  Category $category
	 * @return array
	 */
	function get_category_hierarchy_array( $category , $lang = 'sr' )
	{
		$names = $category->get_name_hierarchy_array( $lang );
		$links = $category->get_permalink_hierarchy_array( $lang );

		for ($i = 1; $i < count( $links ); $i++)
		{
			$links[$i] = $links[$i - 1] . '/' . $links[$i];
		}

		foreach ($links as $i => $link)
		{
			$links[$i] = $link;
		}

		return array_combine( $links, $names );
	}

	function render_attributes($attributes = array())
	{
		if ( ! $attributes )
		{
			return '';
		}

		$attrs = array();
		foreach ($attributes as $attr => $val)
		{
			$attrs[] = $attr . '="' . htmlspecialchars($val, ENT_QUOTES, 'UTF-8', false) . '"';
		}

		$attrs = implode(' ', $attrs);

		return $attrs;
	}

	function render_link($url, $contents = '', $class = '', $id = '', $attributes = array())
	{
		$attrs = render_attributes( $attributes );

		return "<a href=\"{$url}\" class=\"{$class}\" id=\"{$id}\" {$attrs}>{$contents}</a>";
	}

	function intize(&$arg)
	{
		$arg = (int) $arg;
	}

	function human_filesize($bytes, $decimals = 2)
	{
		$sizes = 'BKMGTP';
		$factor = min( floor((strlen($bytes) - 1) / 3), 5 );
		$decimals = ( $factor <= 1 ) ? 0 : $decimals;

		return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . $sizes[$factor] . ($factor ? 'B' : '');
	}

	function make_google_maps_link($lat, $lon)
	{
		return sprintf("http://maps.google.com/maps?iwloc=A&amp;z=18&amp;t=h&amp;q=%s,%s", $lat, $lon);
	}

	/**
	 * This function generates a v4 compatible UUID.
	 *
	 * @link http://www.php.net/manual/en/function.uniqid.php#94959
	 * @return string The generated UUID
	 */
	function generate_v4_uuid()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

	if ( ! function_exists( 'apache_request_headers' ) )
	{
		/**
		 * This function replaces the apache_request_headers() when running PHP
		 * on other servers, such as nginx. It tries to parse the headers from
		 * the $_SERVER superglobal, as close as possible.
		 *
		 * @return array
		 */
		function apache_request_headers()
		{
			$headers = array();

			foreach ($_SERVER as $key => $value)
			{
				if ( substr( $key, 0, 5) === 'HTTP_' )
				{
					/**
					 * First we're gonna strip out the first 5 characters,
					 * namely, the "HTTP_" prefix that is added to all headers
					 * when storing them in the $_SERVER superglobal.
					 *
					 * Next, we'll replace all underscores with spaces, so we
					 * would be able to easily use ucwords() to capitalize the
					 * header names into something resembling the original
					 * header naming conventions.
					 */
					$key = substr( $key, 5 );
					$key = str_replace( '_', ' ', $key );
					$key = strtolower( $key );
					$key = ucwords( $key );
					$key = str_replace( ' ', '-', $key );

					$headers[$key] = $value;
				}
				else
				{
					$headers[$key] = $value;
				}
			}

			return $headers;
		}

	}

	/**
	 * Returns a `realpath()` but using a different path as a starting point for
	 * resolving it, since `realpath()` starts from the current working
	 * directory, which might not be ideal.
	 * @param  string $path
	 * @param  string $original_path
	 * @return string
	 */
	function realpath_relative_to( $path, $original_path )
	{
		if (
			( substr( $path, 0, 1 ) === '/' )
			|| ( substr( $path, 1, 2 ) === ':\\' )
		)
		{
			return realpath( $path );
		}
		else
		{
			return realpath( $original_path . DIRECTORY_SEPARATOR . $path );
		}
	}
