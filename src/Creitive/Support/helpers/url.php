<?php

if ( ! function_exists('parse_current_server_name') )
{
	/**
	 * Parses the $_SERVER['SERVER_NAME'] and returns an array with whichever of
	 * the following keys could be set: 'TLD', 'ccSLD', 'SLD', 'SUB1', 'SUB2'.
	 *
	 * If the $_SERVER['SERVER_NAME'] variable isn't set (like, say, when the
	 * app is run via the CLI), it will return `null`.
	 *
	 * @return mixed
	 */

	function parse_current_server_name()
	{
		if (empty($_SERVER['SERVER_NAME']))
		{
			return null;
		}

		return parse_server_name($_SERVER['SERVER_NAME']);
	}

}

if ( ! function_exists('parse_server_name') )
{
	/**
	 * Parses a server name and returns an array with whichever of the following
	 * keys could be set: 'TLD', 'ccSLD', 'SLD', 'SUB1', 'SUB2'.
	 *
	 * @return array
	 */
	function parse_server_name($serverName)
	{
		if ( ! is_string( $serverName ) )
		{
			throw new InvalidArgumentException( 'The passed argument is not a string, but a [' . gettype( $serverName ) . ']' );
		}

		$serverName = strtolower( $serverName );

		/**
		 * If there is not period in the passed server name, just return it as a
		 * SLD; this is useful for cases like "localhost".
		 */
		if ( ! strpos( $serverName, '.' ) )
		{
			return array(
				'SLD' => $serverName,
			);
		}

		$output_parts = array();

		/**
		 * Here, we'll keep a list of the most common ccSLDs, for ad-hoc
		 * attempts at figuring out which part is which.
		 */
		$common_cc_slds = array(
			'co', 'org', 'edu', 'gov'
		);

		$parts = explode( '.', $serverName );

		/**
		 * First, we'll extract the TLD, since we know it's there, and it's
		 * always last.
		 */
		$current_index = count( $parts ) - 1;

		$output_parts['TLD'] = $parts[$current_index];

		/**
		 * Now, we'll fetch the next part, and check to see if it's a ccSLD. If
		 * it is, we'll also grab the next part (SLD), and if it's not, then it
		 * must be a SLD, so we'll fetch it immediately.
		 */
		$current_index--;

		if ( in_array( $parts[$current_index], $common_cc_slds ) )
		{
			$output_parts['ccSLD'] = $parts[$current_index];

			$current_index--;

			if ( $current_index >= 0 )
			{
				$output_parts['SLD'] = $parts[$current_index];
			}
		}
		else
		{
			$output_parts['SLD'] = $parts[$current_index];
		}

		/**
		 * Take the rest of the parts and pick them up as SUB1, SUB2, ... in
		 * reversed order.
		 */
		$current_index--;
		$i = 1;

		while ( $current_index >= 0 )
		{
			$output_parts['SUB'.$i] = $parts[$current_index];
			$i++;
			$current_index--;
		}

		return $output_parts;
	}

}

if ( ! function_exists('full_site_url') )
{
	/**
	 * Gets the correct site url (should be used instead of CI's built-in
	 * `site_url()`). Currently does not work when called from a CLI-invoked
	 * script.
	 *
	 * @param  string $uri
	 * @return string
	 */
	function full_site_url( $uri = null, $force_same_level = false )
	{
		/**
		 * If the URL is already complete, just return it.
		 */

		if ( preg_match( '#^https?://#i', $uri ) )
		{
			return $uri;
		}

		/**
		 * Figure out the protocol.
		 */

		if (
			( ! empty( $_SERVER['HTTPS'] ) )
			&& ( $_SERVER['HTTPS'] !== 'off' )
		)
		{
			$protocol = 'https';
		}
		else
		{
			$protocol = 'http';
		}

		/**
		 * Get the actual server name.
		 */

		$site = $_SERVER['SERVER_NAME'];

		$clean_request = $_SERVER['REQUEST_URI'];

		if ( strpos( $clean_request, '?' ) !== false )
		{
			$clean_request = substr( $clean_request, 0, strpos( $clean_request, '?' ) );
		}

		/**
		 * By default, return the address of the current page.
		 */

		if ( $uri === null )
		{
			return "{$protocol}://{$site}{$clean_request}";
		}

		/**
		 * If we have an relative redirect, we need to figure out the path.
		 */

		if ( substr( $uri, 0, 1 ) !== '/' )
		{
			/**
			 * Now we need to figure out the actual path to redirect to.
			 */

			if ( ! $force_same_level )
			{
				$last_char = substr( $clean_request, -1, 1 );

				if ( $last_char === '/' )
				{
					$uri = "{$clean_request}{$uri}";
				}
				else
				{
					$uri = "{$clean_request}/{$uri}";
				}
			}
			else
			{
				$last_slash = strripos( $clean_request, '/' );
				$uri = substr( $clean_request, 0, $last_slash ) . "/{$uri}";
			}
		}

		/**
		 * Clean out the weird "#_=_" hash that Facebook randomly appends to
		 * URLs (as of 20.12.2012.). This is an ad-hoc solution, so, whatever,
		 * don't pay too much attention to it. This should be removed once
		 * Facebook changes the behavior. For more info, google "facebook #_=_",
		 * or check the linked page.
		 *
		 * @see http://stackoverflow.com/questions/7131909/facebook-callback-appends-to-return-url
		 */
		if (
			( strlen( $uri ) > 4 )
			&& ( substr( $uri, -4 ) === '#_=_' )
		)
		{
			$uri = substr( $uri, 0, -4 );
		}

		return "{$protocol}://{$site}{$uri}";
	}

}

if ( ! function_exists('fix_url_protocol') )
{
	/**
	 * "Fixes" the protocol in an url string, by checking if it starts with
	 * `http` or `https`, and, if not, prepending it with `http`.
	 *
	 * This is a quick and dirty solution, and it would be better to do it with
	 * `parse_url` and `http_build_query`, but we'll fix that when we find time.
	 *
	 * @param  string $url
	 * @return string
	 */
	function fix_url_protocol( $url )
	{
		if (
			( preg_match( '#^https?://.*#', $url ) )
			|| ( empty( $url ) )
		)
		{
			return $url;
		}
		else
		{
			return 'http://'.$url;
		}
	}

}
