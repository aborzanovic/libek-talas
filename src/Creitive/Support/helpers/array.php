<?php

	/**
	 * Returns the key of the first element of an array.
	 *
	 * @param  array $arr The array in question
	 * @return mixed      The requested key
	 */
	function get_first_key(array $arr)
	{
		reset($arr);
		return key($arr);
	}

	/**
	 * Removes all items with a specified value from an array.
	 *
	 * @param  array $array
	 * @param  mixed $value
	 * @return array
	 */
	function array_remove( array $array, $value )
	{
		$function = function( $item ) use ( $value )
		{
			return ( $item !== $value );
		};

		return array_filter( $array, $function );
	}

	/**
	 * Appends an item onto the beginning of an array.
	 *
	 * @param  array $arr
	 * @param  mixed $key
	 * @param  mixed $val
	 * @return array
	 */
	function array_unshift_assoc( array &$arr, $key, $val )
	{
		$arr = array_reverse($arr, true);
		$arr[$key] = $val;
		$arr = array_reverse($arr, true);
		return $arr;
	}

	/**
	 * Formats a line (passed as a fields array) as CSV and returns the CSV as a
	 * string.
	 *
	 * @link   http://stackoverflow.com/questions/3933668/convert-array-into-csv/3933816#3933816
	 * @link   http://us3.php.net/manual/en/function.fputcsv.php#87120
	 * @param  array   $fields
	 * @param  string  $delimiter
	 * @param  string  $enclosure
	 * @param  boolean $enclose_all
	 * @param  boolean $null_to_mysql_null
	 * @return void
	 */
	function array_to_csv(
		array $fields,
		$delimiter = ',',
		$enclosure = '"',
		$enclose_all = true,
		$null_to_mysql_null = false
	)
	{
		$delimiter_esc = preg_quote( $delimiter, '/' );
		$enclosure_esc = preg_quote( $enclosure, '/' );

		$has_special_character = "/(?:{$delimiter_esc}|{$enclosure_esc}|\s)/";

		$output = array();

		foreach ( $fields as $field )
		{
			if ( $field === null && $null_to_mysql_null )
			{
				$output[] = 'NULL';
				continue;
			}

			/**
			 * Enclose fields containing $delimiter, $enclosure or whitespace
			 */
			if ( $enclose_all || preg_match( $has_special_character, $field ) )
			{
				$output[] = $enclosure . str_replace( $enclosure, $enclosure . $enclosure, $field ) . $enclosure;
			}
			else
			{
				$output[] = $field;
			}
		}

		return implode( $delimiter, $output );
	}

	/**
	 * Builds a tree structure from a flat array.
	 *
	 * Expects an array of elements, each of which is an array, and each must
	 * have the keys `id` and `parent_id`, which are used to build the tree.
	 *
	 * Returns an array of elements, each of which has a `children` key, that is
	 * an array of that element's children.
	 *
	 * @param  array   $elements
	 * @param  integer $parentId
	 * @return array
	 */
	function buildTree($elements, $parentId = 0)
	{
		$branch = array();

		foreach ($elements as $element)
		{
			if ($element['parent_id'] == $parentId)
			{
				$element['children'] = buildTree($elements, $element['id']);

				$branch[] = $element;
			}
		}

		return $branch;
	}

	/**
	 * Compiles options for a `<select>` form element.
	 *
	 * The input is expected to be an array built via `buildTree()`.
	 *
	 * @param array   $categories
	 * @param integer $level
	 * @return array
	 */
	function compileSelectOptions($categories = array(), $level = 0)
	{
		$options = array();

		$indentation = str_repeat('&nbsp;', $level * 5);

		foreach ($categories as $category)
		{
			$options[$category->id] = $indentation . $category->name;

			$options += compileSelectOptions($category['children'], $level + 1);
		}

		return $options;
	}
