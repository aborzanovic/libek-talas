<?php namespace Creitive\Asset;

use Creitive\Asset\Container;

class Factory {

	/**
	 * All of the instantiated asset containers.
	 *
	 * @var array
	 */
	public $containers = [];

	/**
	 * The root URL from which the assets are loaded.
	 *
	 * May be `null`, in which case the assets are loaded using the current
	 * domain as the root URL. Allows, for example, serving assets from a
	 * different domain.
	 *
	 * @var string|null
	 */
	protected $assetUrl;

	public function __construct($assetUrl = null)
	{
		$this->assetUrl = $assetUrl;
	}

	/**
	 * Get an asset container instance.
	 *
	 * <code>
	 *     // Get the default asset container
	 *     $container = Asset::container();
	 *
	 *     // Get a named asset container
	 *     $container = Asset::container('footer');
	 * </code>
	 *
	 * @param string $container
	 * @return \Creitive\Asset\Container
	 */
	public function container($container = 'default')
	{
		if ( ! isset($this->containers[$container]))
		{
			$this->containers[$container] = new Container($container, $this->assetUrl);
		}

		return $this->containers[$container];
	}

	/**
	 * Magic Method for calling methods on the default container.
	 *
	 * <code>
	 *     // Call the "styles" method on the default container
	 *     echo Asset::styles();
	 *
	 *     // Call the "add" method on the default container
	 *     Asset::add('jquery', 'js/jquery.js');
	 * </code>
	 *
	 * @param string $method
	 * @param array $parameters
	 * @return mixed
	 */
	public function __call($method, $parameters)
	{
		return call_user_func_array([$this->container(), $method], $parameters);
	}

}
