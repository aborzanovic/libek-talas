<?php namespace Creitive\Asset;

use Creitive\Asset\Factory;
use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app['creitive.asset'] = $this->app->share(function($app)
		{
			$assetUrl = $app['config']->get('app.assetUrl', null);

			return new Factory($assetUrl);
		});

		$this->app->alias('creitive.asset', 'Creitive\Asset\Factory');
	}

}
