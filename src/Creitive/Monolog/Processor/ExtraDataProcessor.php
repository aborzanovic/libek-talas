<?php

namespace Creitive\Monolog\Processor;

/**
 * Injects arbitrary extra data in all records.
 *
 * @author Miloš Levačić <milos@levacic.net>
 */
class ExtraDataProcessor
{
    /**
     * @var array
     */
    protected $extraData = array();

    /**
     * @param array $extraData Extra data to be added
     */
    public function __construct(array $extraData = array())
    {
        $this->extraData = $extraData;
    }

    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['extra'] = $this->appendExtraFields($record['extra']);

        return $record;
    }

    /**
     * @param  array $extra
     * @return array
     */
    private function appendExtraFields(array $extra)
    {
        foreach ($this->extraData as $key => $value) {
            $extra[$key] = $value;
        }

        return $extra;
    }
}
