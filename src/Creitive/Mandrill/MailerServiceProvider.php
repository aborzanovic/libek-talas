<?php namespace Creitive\Mandrill;

use Creitive\Mandrill\Mailer;
use Illuminate\Support\ServiceProvider;
use Mandrill;

class MailerServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app->bindShared('mandrill', function($app)
		{
			return new Mandrill($app['config']->get('mandrill.apiKey'));
		});

		$this->app->bindShared('mandrill.mailer', function($app)
		{
			$config = $app['config']->get('mandrill');

			return new Mailer(
				$app['mandrill'],
				$app['log'],
				$config['fromEmail'],
				$config['fromName'],
				$config['replyToEmail'],
				$config['replyToName'],
				$config['async'],
				$config['ipPool']
			);
		});

		$this->app->alias('mandrill', 'Mandrill');
		$this->app->alias('mandrill.mailer', 'Creitive\Mandrill\Mailer');
	}

}
