<?php namespace Creitive\Robots;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Response;

class ServiceProvider extends BaseServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->package('creitive/robots', 'creitive/robots');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$env = $this->app->environment();

		$this->app['view']->addNamespace('creitive/robots', __DIR__.'/views');

		/*
		 * Check if we have an environment-specific `robots.txt` view.
		 */

		if ($this->app['view']->exists("creitive/robots::{$env}/robots"))
		{
			$viewPath = "creitive/robots::{$env}/robots";
		}
		else
		{
			$viewPath = "creitive/robots::robots";
		}

		$contents = $this->app['view']->make($viewPath)->render();

		/*
		 * Register the route.
		 */

		$this->app['router']->get('robots.txt', function() use ($contents)
		{
			$response = Response::make($contents, 200, ['Content-Type' => 'text/plain']);

			/*
			 * @todo Figure out how to configure proper caching.
			 */

			$response->setCache([
				'public' => true,
			]);

			return $response;
		});
	}

}
