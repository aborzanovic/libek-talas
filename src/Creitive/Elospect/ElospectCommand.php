<?php namespace Creitive\Elospect;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Input\InputArgument;

class ElospectCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'elospect';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Inspects Eloquent models and lists their database columns.';

	/**
	 * Execute the console command.
	 *
	 * Check the provided links for more ways to do this.
	 *
	 * @link https://stackoverflow.com/questions/19951787/laravel-4-get-column-names
	 * @link http://laravelsnippets.com/snippets/get-all-columns-names-from-a-eloquent-model-2
	 * @return void
	 */
	public function fire()
	{
		$model = $this->argument('model');

		if (!class_exists($model))
		{
			$this->error("Class {$model} not found - pay attention to case-sensitivity");
			return;
		}

		$instance = new $model;

		if (!($instance instanceof Model))
		{
			$this->error("Class {$model} must extend \\Illuminate\\Database\\Eloquent\\Model");
			return;
		}

		$table = $instance->getTable();

		$this->info("Columns for class <comment>{$model}</comment> in table <comment>{$table}</comment>:");

		$connection = $instance->getConnection();
		$schemaManager = $connection->getDoctrineSchemaManager();

		foreach ($schemaManager->listTableColumns($table) as $name => $details)
		{
			$this->output->writeln($name.' (<comment>'.$details->getType().'</comment>)');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('model', InputArgument::REQUIRED, 'The Eloquent model to inspect.'),
		);
	}

}
