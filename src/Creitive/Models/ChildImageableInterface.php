<?php namespace Creitive\Models;

interface ChildImageableInterface {

	public function images();

}
