<?php namespace Creitive\Models;

interface NestableSluggableInterface {

	public function getFullSlugAttribute();

	public function getSlugPathAttribute();

}
