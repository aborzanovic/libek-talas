<?php namespace Creitive\Models;

interface SluggableInterface {

	public function sluggify();

	public function validateSlug($slug);

}
