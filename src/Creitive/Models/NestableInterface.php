<?php namespace Creitive\Models;

interface NestableInterface {

	public function setParent(NestableInterface $item);

	public function addChild(NestableInterface $item);

	public function hasParent();

	public function hasChildren();

	public function root();

}
