<?php namespace Creitive\Models\Traits;

use LogicException;

trait NestableSluggableTrait {

	/**
	 * Must return a string which is the name of an Eloquent relationship
	 * defined on the model, that resolves the "parent" `BelongsTo`
	 * relationship.
	 *
	 * @return string
	 */
	abstract protected function getNestableSluggableParentAttribute();

	/**
	 * The `full_slug` attribute getter.
	 *
	 * @return string
	 */
	public function getFullSlugAttribute()
	{
		return implode('/', $this->slug_path);
	}

	/**
	 * Gets the slug path for this item.
	 *
	 * @return array
	 */
	public function getSlugPathAttribute()
	{
		$parentAttribute = $this->getNestableSluggableParentAttribute();

		$parent = $this->{$parentAttribute};

		if ( is_null($parent))
		{
			return [$this->slug];
		}

		$path = $parent->slug_path;

		array_push($path, $this->slug);

		return $path;
	}

}
