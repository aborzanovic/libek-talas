<?php namespace Creitive\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait StickableTrait {

	/**
	 * Orders sticky items first.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeOrderBySticky(Builder $query)
	{
		return $query->orderBy('sticky', 'desc');
	}

}
