<?php namespace Creitive\Models\Traits;

use Creitive\Models\NestableInterface;

trait NestableTrait {

	/**
	 * Sets this item's parent.
	 *
	 * @param \Creitive\Models\NestableInterface|null $item
	 * @return void
	 */
	public function setParent(NestableInterface $item = null)
	{
		$this->setRelation('parent', $item);
	}

	/**
	 * Initializes this item's children.
	 *
	 * @return void
	 */
	public function initializeChildren()
	{
		$this->setRelation('children', $this->newCollection());
	}

	/**
	 * Adds a child of this item.
	 *
	 * @param \Creitive\Models\NestableInterface $item
	 * @return void
	 */
	public function addChild(NestableInterface $item)
	{
		$this->children->push($item);
	}

	/**
	 * Eloquent relationship: a page may belong to a parent page.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent()
	{
		return $this->belongsTo(get_called_class(), 'parent_id');
	}

	/**
	 * Eloquent relationship: a page may have many subpages.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children()
	{
		return $this->hasMany(get_called_class(), 'parent_id');
	}

	/**
	 * Checks whether this item has a parent.
	 *
	 * @return boolean
	 */
	public function hasParent()
	{
		return $this->parent_id !== null;
	}

	/**
	 * Checks whether this item has children.
	 *
	 * @return boolean
	 */
	public function hasChildren()
	{
		return ! $this->children->isEmpty();
	}

	/**
	 * Gets the root node from the current node.
	 *
	 * @return \Creitive\Models\NestableInterface
	 */
	public function root()
	{
		if ( ! $this->hasParent())
		{
			return $this;
		}

		return $this->parent->root();
	}

}
