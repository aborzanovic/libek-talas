<?php namespace Creitive\Models\Traits;

use Creitive\Models\ChildImageableInterface;
use Exception;
use ImageUploader;

trait ImageableTrait {

	/**
	 * Gets the configuration for this model's images.
	 *
	 * This should be overriden in the appropriate model where this trait is
	 * used, and it should return an array with the image configuration.
	 *
	 * In general, this array should contain a 'versions' key, with the
	 * appropriate version configuration listed within.
	 *
	 * Sorry for the lack of documentation on how this works - no time to
	 * explain right now. To be honest? We're on a deadline, and it's likely we
	 * won't ever find the time to document this here, so to see how it's
	 * supposed to work, check some of the models that use this, and try to
	 * figure it out. Good luck!
	 *
	 * @todo Document how this works.
	 * @var array
	 */
	abstract public function getImageConfiguration();

	/**
	 * Gets an image of a specified version and size, or, if not found, a
	 * default image - the convention is to put the default images in the public
	 * folder, under the path `public/images/default/ClassName/version/size.png`.
	 *
	 * @param string $version
	 * @param string $size
	 * @param boolean $checkChildren
	 * @return string
	 */
	public function getImage($version, $size, $checkChildren = true)
	{
		$imageVersionSize = "image_{$version}_{$size}";

		if (isset($this->$imageVersionSize) && $this->$imageVersionSize)
		{
			return $this->{$imageVersionSize};
		}

		if ($this->hasChildImages() && $checkChildren)
		{
			return $this->getChildImage($version, $size);
		}

		return $this->getDefaultImage($version, $size);
	}

	/**
	 * Checks whether the model has an image of the specified version and size.
	 *
	 * This checks not only whether it is configured, but also whether there is
	 * an uploaded image. It also checks child images, unless the third argument
	 * is set to `false`;
	 *
	 * @param string $version
	 * @param string $size
	 * @param boolean $checkChildren
	 * @return boolean
	 */
	public function hasImage($version, $size, $checkChildren = true)
	{
		$imageVersionSize = "image_{$version}_{$size}";

		if (isset($this->$imageVersionSize) && $this->$imageVersionSize)
		{
			return true;
		}

		return $checkChildren ? $this->hasChildImage($version, $size) : false;
	}

	/**
	 * Checks whether the model has a child image of the specified version and
	 * size.
	 *
	 * @param string $version
	 * @param string $size
	 * @return boolean
	 */
	public function hasChildImage($version, $size)
	{
		if ( ! $this->hasChildImages())
		{
			return false;
		}

		if ($this->images->isEmpty())
		{
			return false;
		}

		$firstImage = $this->images->first();

		return $firstImage ? $firstImage->hasImage($version, $size) : false;
	}

	/**
	 * Gets the default image for the requested version and size.
	 *
	 * @param string $version
	 * @param string $size
	 * @return string
	 */
	public function getDefaultImage($version, $size)
	{
		$prefix = $this->getImagePrefix();

		if ($this->language)
		{
			$language = ".{$this->language}";
		}
		else
		{
			$language = '';
		}

		return "/images/default/{$prefix}/{$version}/{$size}{$language}.png";
	}

	/**
	 * Gets the first image of the specified version and size from the related
	 * images of the model.
	 *
	 * @param string $version
	 * @param string $size
	 * @return string
	 */
	public function getChildImage($version, $size)
	{
		/**
		 * First we'll try to fetch the related images and return the first one.
		 */
		$firstImage = $this->images->first();

		if ($firstImage)
		{
			return $firstImage->getImage($version, $size);
		}

		/**
		 * If that didn't work, we have to find the model of the related images,
		 * and return its default image of the specified version and size.
		 */
		$imagesRelation = $this->images();

		$imagesModel = $imagesRelation->getRelated();

		return $imagesModel->getDefaultImage($version, $size);
	}

	/**
	 * Checks whether this model has child images, by testing whether the
	 * appropriate interface is implemented.
	 *
	 * @return boolean
	 */
	public function hasChildImages()
	{
		return $this instanceof ChildImageableInterface;
	}

	/**
	 * Uploads an image, and sets the relevant properties on the instance.
	 *
	 * @param mixed $file
	 * @param string $version
	 * @return boolean
	 */
	public function uploadImage($file, $version)
	{
		if ( ! $this->imageVersionConfigured($version))
		{
			throw new Exception('Image version '.$version.' not configured in model: '.get_called_class());
		}

		$prefix = $this->getImagePrefix();
		$versionConfiguration = $this->getImageVersionConfiguration($version);

		$paths = ImageUploader::upload($file, $version, $versionConfiguration, $prefix);

		if ( ! $paths)
		{
			return false;
		}

		foreach ($versionConfiguration as $size => $params)
		{
			$imageProperty = "image_{$version}_{$size}";
			$this->{$imageProperty} = $paths[$size];
		}

		return true;
	}

	/**
	 * Deletes this model's images of a specified version.
	 *
	 * Doesn't actually delete the images on the filesystem, just unlinks them
	 * within the database.
	 *
	 * @param string $version
	 * @return void
	 */
	public function deleteImage($version)
	{
		if ( ! $this->imageVersionConfigured($version))
		{
			throw new Exception('Image version '.$version.' not configured in model: '.get_called_class());
		}

		$versionConfiguration = $this->getImageVersionConfiguration($version);

		foreach ($imageVersions as $version => $params)
		{
			$imageProperty = "image_{$version}";
			$this->{$imageProperty} = null;
		}
	}

	/**
	 * Gets the configured image prefix.
	 *
	 * If there is a `prefix` key in the image configuration array, its value
	 * will be used as the image prefix. Otherwise the class name (without its
	 * namespace path) will be used instead.
	 *
	 * @return string
	 */
	public function getImagePrefix()
	{
		$imageConfiguration = $this->getImageConfiguration();

		if (isset($imageConfiguration['prefix']))
		{
			return $imageConfiguration['prefix'];
		}

		$classTree = explode('\\', get_called_class());

		return end($classTree);
	}

	/**
	 * Gets the image configuration for the requested version.
	 *
	 * @param string $version
	 * @return array
	 */
	public function getImageVersionConfiguration($version)
	{
		$imageConfiguration = $this->getImageConfiguration();

		return $imageConfiguration['versions'][$version];
	}

	/**
	 * Checks whether this model contains configuration for the passed image
	 * version.
	 *
	 * @param string $version
	 * @return boolean
	 */
	public function imageVersionConfigured($version)
	{
		$imageConfiguration = $this->getImageConfiguration();

		return isset($imageConfiguration['versions'][$version]);
	}

	/**
	 * Checks whether a version and size of an image have been configured for
	 * this model.
	 *
	 * @param string $version
	 * @param string $size
	 * @return boolean
	 */
	public function imageVersionSizeConfigured($version, $size)
	{
		$imageConfiguration = $this->getImageConfiguration();

		return (
			(isset($imageConfiguration['versions'][$version]))
			&& (isset($imageConfiguration['versions'][$version][$size]))
		);
	}

}
