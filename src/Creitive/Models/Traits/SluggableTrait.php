<?php namespace Creitive\Models\Traits;

use BadMethodCallException;

trait SluggableTrait {

	/**
	 * The configuration for generating slugs.
	 *
	 * @var array
	 */
	protected $sluggable = [
		/**
		 * The source from which the slug is formed.
		 *
		 * Must be one of the following:
		 *
		 * 1. `string` - the slug is created from that column's value
		 *
		 * 2. `array` of `string`s - the given columns are concatenated with a
		 * space character, and the slug is created from that value.
		 *
		 * 3. A `Closure` - the instance is passed to the closure, which should
		 * return a source string from which the slug will be created. Note that
		 * if you're using a Closure, you'll have to override the constructor
		 * method in your model, and assign the Closure to
		 * `$this->sluggable['source']` within the constructor, as it's
		 * impossible to define a class property to be a Closure.
		 *
		 * @var string|array|\Closure
		 */
		'source' => 'title',

		/**
		 * The target column into which the slug will be saved. This column is
		 * also checked to see whether the slug should be generated at all:
		 *
		 * 1. If the column is empty, the slug will be generated from the source
		 * value.
		 *
		 * 2. If the column has a value, which is a valid slug, it will be left
		 * alone.
		 *
		 * 3. If the column has a value, which is an invalid slug, the model
		 * will be prevented from being saved.
		 *
		 * @var string
		 */
		'target' => 'slug',

		/**
		 * The character used for separating words.
		 *
		 * It's not recommended to change this from the default dash character.
		 *
		 * @var string
		 */
		'separator' => '-',

		/**
		 * The method used for sluggification. Must be something callable, or
		 * `null`. If `null`, `\Str::slug()` will be called.
		 *
		 * The method will be passed a single argument, the input string, and is
		 * expected to produce a sluggified string.
		 *
		 * @var callable|null
		 */
		'sluggifyMethod' => null,
	];

	/**
	 * Boots the sluggable trait.
	 *
	 * Hooks into the model's "saving" event, to automatically set the slug when
	 * needed.
	 *
	 * @return void
	 */
	public static function bootSluggableTrait()
	{
		static::saving(function($model)
		{
			return $model->sluggify();
		});
	}

	/**
	 * Automatically performs the sluggification.
	 *
	 * Returns `true` on success, or `false` on failure. For a description on
	 * what constitutes success and failure, see the documentation for the
	 * `target` configuration parameter.
	 *
	 * @return boolean
	 */
	public function sluggify()
	{
		$source = $this->getSourceSlugString();
		$slug = $this->sluggable['target'];

		if (empty($this->{$slug}))
		{
			$this->{$slug} = $this->callSluggifyMethod($source);

			return true;
		}

		return $this->validateSlug($this->{$slug});
	}

	/**
	 * Gets the source slug string, based on the configuration.
	 *
	 * @return string
	 */
	public function getSourceSlugString()
	{
		$source = $this->sluggable['source'];

		if (is_string($source))
		{
			return $this->{$source};
		}

		if (is_array($source))
		{
			$parts = [];

			foreach ($source as $sourceAttribute)
			{
				$parts[] = $this->{$sourceAttribute};
			}

			return implode(' ', $parts);
		}

		if (is_callable($source))
		{
			return $source($this);
		}

		throw new BadMethodCallException('The sluggable source is misconfigured.');
	}

	/**
	 * Validates whether the passed string is a valid slug.
	 *
	 * @param string $string
	 * @return string
	 */
	public function validateSlug($string)
	{
		return $string === $this->callSluggifyMethod($string);
	}

	/**
	 * Calls the configured sluggify method, and returns the generated slug.
	 *
	 * @param string $string
	 * @return string
	 */
	public function callSluggifyMethod($string)
	{
		$method = $this->sluggable['sluggifyMethod'] ?: ['\Str', 'slug'];

		if ( ! is_callable($method))
		{
			throw new BadMethodCallException('The configured sluggifyMethod is not callable.');
		}

		return $method($string, $this->sluggable['separator']);
	}

}
