<?php namespace Creitive\SocialPlugins\Twitter;

use Creitive\SocialPlugins\BaseSocialPlugin;
use Creitive\SocialPlugins\SocialPluginInterface;

class Tweet extends BaseSocialPlugin implements SocialPluginInterface {

	protected $containerClass = 'twitter-tweet';

	/**
	 * The text that will be displayed as the link contents.
	 *
	 * @var string
	 */
	protected $linkText = 'Tweet';

	/**
	 * The attribute documentation can be found at Twitter's website.
	 *
	 * @link https://dev.twitter.com/docs/tweet-button
	 * @var array
	 */
	protected $attributes = array(
		/**
		 * The class used by Twitter's JS to initialize the plugin.
		 */
		'class' => 'twitter-share-button',

		/**
		 * The fallback URL for the link, in case the Twitter JS doesn't load.
		 */
		'href' => 'https://twitter.com/share',

		/**
		 * The URL to share.
		 *
		 * If empty, it will default to the current page.
		 */
		'data-url' => '',

		/**
		 * The URL to which the shared URL resolves.
		 *
		 * If empty, it will default to the shared URL. However, this is useful
		 * sometimes when sharing, for example, shortened URLs - in this case,
		 * Twitter would update the Tweet count for the shortened URL, while the
		 * actual page where the shortened URL redirects won't get any shares
		 * counted. In such situations, it is recommended to provide the
		 * canonical page URL here, to ensure correct counts.
		 */
		'data-counturl' => '',

		/**
		 * The text to share.
		 *
		 * If empty, it will default to the current page's `<title>` tag.
		 */
		'data-text' => '',

		/**
		 * Screen name of the user to which the Tweet will be attributed to.
		 *
		 * If empty, the Tweet won't be attributed to anyone.
		 */
		'data-via' => '',

		/**
		 * Language used for the plugin.
		 *
		 * If empty, it will default to English, but it's okay to explicitly set
		 * it here, as well.
		 */
		'data-lang' => 'en',

		/**
		 * The size of the Tweet button.
		 *
		 * If not supplied, it will default to 'medium'.
		 *
		 * Options: 'medium', 'large'
		 */
		'data-size' => 'medium',

		/**
		 * Suggested accounts for the user to follow.
		 *
		 * The documentation for this option is a bit too long to add here, but
		 * it's available within the official Twitter documentation.
		 *
		 * This is rarely used, though, so it should be left empty most of the
		 * time.
		 */
		'data-related' => '',

		/**
		 * Count box position.
		 *
		 * If empty, it will default to 'horizontal'.
		 *
		 * Options: 'none', 'horizontal', 'vertical'
		 */
		'data-count' => 'horizontal',

		/**
		 * Comma separated hashtags appended to tweet text.
		 */
		'data-hashtags' => '',

		/**
		 * Opt-out of tailoring Twitter.
		 *
		 * Options: 'false', 'true'
		 */
		'data-dnt' => 'false',
	);

	/**
	 * Sets the link text.
	 *
	 * @param string $linkText
	 */
	public function setLinkText($linkText)
	{
		$this->linkText = $linkText;
	}

	/**
	 * Gets the link text.
	 *
	 * @return string
	 */
	public function getLinkText()
	{
		return $this->linkText;
	}

	public function render()
	{
		$this->attributes['data-url'] = $this->getUrl();
		return '<a ' . $this->renderAttributes() . '>' . htmlspecialchars($this->linkText) . '</a>';
	}

}
