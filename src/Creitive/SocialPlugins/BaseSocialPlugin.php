<?php namespace Creitive\SocialPlugins;

abstract class BaseSocialPlugin {

	/**
	 * The URL to be used for the social plugins.
	 *
	 * This will usually be the URL of the page where the plugin is injected.
	 *
	 * @var string
	 */
	protected $url;

	/**
	 * The container class for this plugin.
	 *
	 * This will be prefixed automatically by the `SocialPlugins` class, in
	 * order to facilitate some CSS namespacing functionality.
	 *
	 * @var string
	 */
	protected $containerClass = 'social-plugin';

	/**
	 * An array of all the attributes that the plugin's HTML element needs to
	 * have.
	 *
	 * This will be overriden by each individual plugin, and it's used this way
	 * because *all* of the popular social plugins have mostly the same system
	 * of customizing their look and behavior - through HTML5 data attributes.
	 *
	 * @var array
	 */
	protected $attributes = array();

	public function __construct($containerClass = null, $url = '')
	{
		if (!is_null($containerClass))
		{
			$this->setContainerClass($containerClass);
		}

		$this->setUrl($url);
	}

	/**
	 * Sets the URL for this plugin.
	 *
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * Gets the URL for this plugin.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Sets the container class for this plugin.
	 *
	 * @param string $containerClass
	 */
	public function setContainerClass($containerClass)
	{
		$this->containerClass = $containerClass;
	}

	/**
	 * Gets the container class for this plugin.
	 *
	 * @return string
	 */
	public function getContainerClass()
	{
		return $this->containerClass;
	}

	/**
	 * Sets an attribute for this plugin.
	 *
	 * @param string $attribute
	 * @param string $value
	 */
	public function setAttribute($attribute, $value)
	{
		$this->attributes[$attribute] = $value;
	}

	/**
	 * Gets an attribute for this plugin.
	 *
	 * Returns `null` if the attribute is not found.
	 *
	 * @param  string      $attribute
	 * @return string|null
	 */
	public function getAttribute($attribute)
	{
		return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : null;
	}

	/**
	 * Adds/updates multiple attributes for this plugin at once.
	 *
	 * @param array $attributes
	 */
	public function addAttributes(array $attributes)
	{
		foreach ($attributes as $attribute => $value)
		{
			$this->setAttribute($attribute, $value);
		}
	}

	/**
	 * Sets all of this plugin's attributes at once, deleting any old attributes
	 * that may have been configured.
	 *
	 * @param array $attributes
	 */
	public function setAttributes(array $attributes)
	{
		$this->attributes = $attributes;
	}

	/**
	 * Gets all of this plugin's attributes.
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * Renders all attributes as a string which can be injected into HTML as an
	 * element's attribute list.
	 *
	 * @return string
	 */
	public function renderAttributes()
	{
		$attributes = array();

		foreach ($this->attributes as $attribute => $value)
		{
			if ($value !== '')
			{
				$attributes[] = "{$attribute}=\"{$value}\"";
			}
		}

		return implode(' ', $attributes);
	}

	/**
	 * Renders the complete plugin.
	 *
	 * @return string
	 */
	public function render() {}

	/**
	 * Magic method.
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->render();
	}

}
