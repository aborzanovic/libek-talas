<?php namespace Creitive\SocialPlugins\Pinterest;

use Creitive\SocialPlugins\BaseSocialPlugin;
use Creitive\SocialPlugins\SocialPluginInterface;

class PinIt extends BaseSocialPlugin implements SocialPluginInterface {

	protected $containerClass = 'pinterest-pinit';

	protected $linkContents = '<img src="//assets.pinterest.com/images/pidgets/pin_it_button.png">';

	protected $queryData = array(
		'url' => '',
		'media' => '',
		'description' => '',
	);

	/**
	 * The attribute documentation can be found at Pinterest's website.
	 *
	 * @link http://developers.pinterest.com/pin_it/
	 * @var array
	 */
	protected $attributes = array(
		/**
		 * The sharing URL for Pinterest, which will automatically be updated
		 * later to add the complete query string.
		 */
		'href' => '//pinterest.com/pin/create/button/',

		/**
		 * Plugin type.
		 *
		 * This has a few other options, but they're not likely to be used, and
		 * they won't be listed here, since Pinterest doesn't offer a good
		 * documentation for this - they basically require you to use their own
		 * Widget Builder and then read the generated code to figure out all the
		 * options.
		 */
		'data-pin-do' => 'buttonPin',

		/**
		 * The position of the pin count.
		 *
		 * Options: 'above', 'beside', 'none'
		 */
		'data-pin-config' => 'beside',
	);

	public function setCountPosition($countPosition)
	{
		if (!in_array($countPosition, array('above', 'beside', 'none')))
		{
			throw new DomainException('Invalid count position!');
		}

		$this->attributes['data-pin-config'] = $countPosition;
	}

	public function getCountPosition()
	{
		return $this->attributes['data-pin-config'];
	}

	public function setMedia($media)
	{
		$this->queryData['media'] = $media;
	}

	public function getMedia()
	{
		return $this->queryData['media'];
	}

	public function setDescription($description)
	{
		$this->queryData['description'] = $description;
	}

	public function getDescription()
	{
		return $this->queryData['description'];
	}

	public function buildHref()
	{
		$queryData = array_filter($this->queryData);
		$queryString = http_build_query($queryData);
		return "{$this->attributes['href']}?{$queryString}";
	}

	public function render()
	{
		$this->queryData['url'] = $this->getUrl();
		$this->attributes['href'] = $this->buildHref();
		return '<a ' . $this->renderAttributes() . '>' . $this->linkContents . '</a>';
	}

}
