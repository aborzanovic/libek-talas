<?php namespace Creitive\SocialPlugins\Facebook;

use Creitive\SocialPlugins\BaseSocialPlugin;
use Creitive\SocialPlugins\SocialPluginInterface;

class Like extends BaseSocialPlugin implements SocialPluginInterface {

	protected $containerClass = 'facebook-like';

	/**
	 * The attribute documentation can be found at Facebook's website.
	 *
	 * @link https://developers.facebook.com/docs/reference/plugins/like/
	 * @var array
	 */
	protected $attributes = array(
		/**
		 * The class used by Facebook's JS to initialize the plugin.
		 */
		'class' => 'fb-like',

		/**
		 * The URL to like. The XFBML version defaults to the current page.
		 *
		 * Note: After July 2013 migration, href should be an absolute URL.
		 */
		'data-href' => '',

		/**
		 * Specifies whether to include a Send button with the Like button. This
		 * only works with the XFBML version.
		 *
		 * Options: 'true', 'false'
		 */
		'data-send' => 'false',

		/**
		 * There are three options:
		 *
		 * - 'standard': displays social text to the right of the button and
		 * friends' profile photos below. Minimum width: 225 pixels. Minimum
		 * increases by 40px if action is 'recommend' by and increases by 60px
		 * if send is 'true'. Default width: 450 pixels. Height: 35 pixels
		 * (without photos) or 80 pixels (with photos).
		 *
		 * - 'button_count': displays the total number of likes to the right of
		 * the button. Minimum width: 90 pixels. Default width: 90 pixels.
		 * Height: 20 pixels.
		 *
		 * - 'box_count': displays the total number of likes above the button.
		 * Minimum width: 55 pixels. Default width: 55 pixels. Height: 65
		 * pixels.
		 *
		 * Options: 'standard', 'button_count', 'box_count'
		 */
		'data-layout' => 'button_count',

		/**
		 * Specifies whether to display profile photos below the button
		 * (standard layout only). You must not enable this on child-directed
		 * sites.
		 *
		 * Options: 'true', 'false'
		 */
		'data-show-faces' => 'true',

		/**
		 * The width of the Like button.
		 */
		'data-width' => '450',

		/**
		 * The verb to display on the button.
		 *
		 * Options: 'like', 'recommend'
		 */
		'data-action' => 'like',

		/**
		 * The font to display in the button.
		 *
		 * Options: 'arial', 'lucida grande', 'segoe ui', 'tahoma',
		 * 'trebuchet ms', 'verdana'
		 */
		'data-font' => 'arial',

		/**
		 * The color scheme for the like button.
		 *
 		 * Options: 'light', 'dark'
		 */
		'data-colorscheme' => 'light',
	);

	public function render()
	{
		$this->attributes['data-href'] = $this->getUrl();
		return '<div ' . $this->renderAttributes() . '></div>';
	}

}
