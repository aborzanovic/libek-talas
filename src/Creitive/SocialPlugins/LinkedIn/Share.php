<?php namespace Creitive\SocialPlugins\LinkedIn;

use Creitive\SocialPlugins\BaseSocialPlugin;
use Creitive\SocialPlugins\SocialPluginInterface;

class Share extends BaseSocialPlugin implements SocialPluginInterface {

	protected $containerClass = 'linkedin-share';

	/**
	 * The attribute documentation can be found at LinkedIn's website.
	 *
	 * @link http://developer.linkedin.com/share-plugin-reference
	 * @var array
	 */
	protected $attributes = array(
		/**
		 * The script type used by LinkedIn's JS to initialize the plugin.
		 */
		'type' => 'IN/Share',

		/**
		 * The URL to share.
		 *
		 * If left empty, the current page will be shared by default.
		 */
		'data-url' => '',

		/**
		 * The position of the counter.
		 *
		 * Options: 'right', 'top', or leave empty to hide the counter
		 */
		'data-counter' => 'right',

		/**
		 * Whether to show a zero or a placeholder in the counter if there are
		 * no shares. Shows a 0 digit if counter="right". Shows a LinkedIn Icon
		 * if counter="top".
		 *
		 * It's useful to set this to `true`, so that the plugin will have
		 * predictable dimensions.
		 *
		 * Options: 'false', 'true'
		 */
		'data-showzero' => 'true',

		/**
		 * The name of a javascript function to fire if the URL is successfully
		 * shared. It fires after the link is shared and does not wait on the
		 * window to close. This call will also pass a String argument that
		 * contains the URL that was shared.
		 */
		'data-onsuccess' => '',

		/**
		 * The name of a javascript function to fire if there is an error
		 * sharing the URL. It fires after the link is shared and does not wait
		 * on the window to close. This call will also pass a String argument
		 * that contains the URL that had an error.
		 */
		'data-onerror' => '',
	);

	public function render()
	{
		$this->attributes['data-url'] = $this->getUrl();
		return '<script ' . $this->renderAttributes() . '></script>';
	}

}
