<?php namespace Creitive\SocialPlugins;

/**
 * An interface for social plugins.
 *
 * All social plugins must implement this interface.
 *
 * When creating a plugin, it's recommended to extend it from
 * `Creitive\SocialPlugins\BaseSocialPlugin` (which already implements most of
 * this interface's methods), and add the relevant `__toString()` representation
 * of the new plugin. Most likely, the `$attributes` property will also require
 * some basic configuration, to set the default attributes used by the plugin.
 */
interface SocialPluginInterface {

	public function setUrl($url);

	public function getUrl();

	public function setContainerClass($containerClass);

	public function getContainerClass();

	public function render();

	public function __toString();

}
