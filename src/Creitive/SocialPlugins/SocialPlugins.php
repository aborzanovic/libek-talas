<?php namespace Creitive\SocialPlugins;

use Creitive\SocialPlugins\SocialPluginInterface;

class SocialPlugins {

	/**
	 * The prefix which will be added to all the container elements for each of
	 * the plugins.
	 *
	 * Together with each of the plugins' own container classes, this can be
	 * used to style the plugin containers in any way desired - in practice,
	 * this is used most of the time in order to position the plugins correctly,
	 * relative to each other, so that they look nicely when shown together.
	 *
	 * @var string
	 */
	protected $containerClassPrefix;

	/**
	 * The URL which will automatically be injected into all of the plugins
	 * added to this collection.
	 *
	 * @var string
	 */
	protected $url;

	/**
	 * An array of all the plugins added to this collection.
	 *
	 * @var array
	 */
	public $plugins = array();

	public function __construct($url = '', $containerClassPrefix = 'social-plugins')
	{
		$this->setUrl($url);
		$this->setContainerClassPrefix($containerClassPrefix);
	}

	/**
	 * Sets the URL for this collection.
	 *
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		foreach ($this->plugins as $plugin)
		{
			$plugin->setUrl($this->url);
		}
	}

	/**
	 * Gets the URL for this collection.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Sets the container class prefix for this collection.
	 *
	 * @param string $containerClassPrefix
	 */
	public function setContainerClassPrefix($containerClassPrefix)
	{
		$this->containerClassPrefix = $containerClassPrefix;
	}

	/**
	 * Gets the container class prefix for this collection.
	 *
	 * @return string
	 */
	public function getContainerClassPrefix()
	{
		return $this->containerClassPrefix;
	}

	/**
	 * Adds multiple plugins to this collection.
	 *
	 * All of the arguments passed to this method must implement
	 * `Creitive\SocialPlugins\SocialPluginInterface`, otherwise a
	 * `DomainException` will be thrown.
	 */
	public function addPlugins()
	{
		$plugins = func_get_args();

		foreach ($plugins as $plugin)
		{
			if (!($plugin instanceof SocialPluginInterface))
			{
				throw new DomainException('All arguments must be objects of a class that implements Creitive\\SocialPlugins\\SocialPluginInterface');
			}
		}

		foreach ($plugins as $plugin)
		{
			$plugin->setUrl($this->url);
			$this->plugins[] = $plugin;
		}
	}

	public function getPlugins()
	{
		return $this->plugins;
	}

	/**
	 * Renders the output for all of the plugins added to this collection,
	 * automatically wrapping each plugin in a `<div>` element with a container
	 * class added (based on this collection's container class prefix, and each
	 * of the plugins' own container classes).
	 *
	 * @return string
	 */
	public function render()
	{
		$containerClassPrefix = $this->getContainerClassPrefix();

		$output = '<div class="' . $containerClassPrefix . '">';

		foreach ($this->plugins as $plugin)
		{
			$output .= '<div class="' . $containerClassPrefix . '-' . $plugin->getContainerClass() . '">' . $plugin . '</div>';
		}

		$output .= '</div>';

		return $output;
	}

	public function __toString()
	{
		return $this->render();
	}

}
