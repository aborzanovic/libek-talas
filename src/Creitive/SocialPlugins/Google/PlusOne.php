<?php namespace Creitive\SocialPlugins\Google;

use Creitive\SocialPlugins\BaseSocialPlugin;
use Creitive\SocialPlugins\SocialPluginInterface;

class PlusOne extends BaseSocialPlugin implements SocialPluginInterface {

	protected $containerClass = 'google-plusone';

	protected $attributes = array(

		/**
		 * Class used by Googles plusone.js to init a plugin.
		 */
		'class' => 'g-plusone',

		/**
		 * Sets the URL to +1. Set this attribute when you have a +1 button next
		 * to an item description for another page and want the button to +1
		 * the referenced page and not the current page.
		 */
		'data-href' => '',

		/**
		 * Sets the +1 button size to render.
		 *
		 * Options: small, medium, standard, tall
		 */
		'data-size' => 'medium',

		/**
		 * Sets the +1 annotation.
		 *
		 * Options: none, bubble, inline
		 */
		'data-annotation' => 'bubble',

		/**
		 * If annotation is set to "inline", this parameter sets the width in
		 * pixels to use for the button and its inline annotation. If the width
		 * is omitted, a button and its inline annotation use 450px.
		 */
		'data-width' => '250px',

		/**
		 * Sets alignment of the element whithin its parent.
		 *
		 * Options: left, right
		 */
		'data-align' => 'left',

		/**
		 * Sets the preferred positions to display hover and confirmation
		 * bubbles, which are relative to the button. Set this parameter when
		 * your page contains certain elements, such as Flash objects, that
		 * might interfere with rendering the bubbles.
		 *
		 * If empty, the rendering logic will guess the best position, usually
		 * defaulting to below the button by using the bottom value.
		 *
		 * Options: top, right, bottom, left
		 */
		'data-expandTo' => 'right',

		/**
		 * If specified, this function is called after the user clicks the +1
		 * button. The callback function must be in the global namespace and
		 * accept a single parameter, which is a JSON object with the following
		 * structure:
		 *
		 *     {
		 *         "href": target URL,
		 *         "state": "on"|"off"
		 *     }
		 *
		 * Where state is set "on" for a +1, and off for the removal of +1
		 */
		'data-callback' => '',

		/**
		 * If specified, this function is called either when a hover bubble
		 * displays, which is caused by the user hovering the mouse over the +1
		 * button, or when a confirmation bubble displays, which is caused by
		 * the user +1'ing the page. You can use this callback function to
		 * modify your page, such as pausing a video when the bubble appears.
		 *
		 * This function must be in the global namespace and accept a single
		 * parameter, which is a JSON object with the following structure:
		 *
		 *     {
		 *          "id" : targetURL,
		 *          "type" : hover|confirm
		 *     }
		 */
		'data-onstartinteraction' => '',

		/**
		 * If specified, this function is called when either a hover or
		 * confirmation bubble disappears. You can use this callback function to
		 * modify your page, such as resuming a video when the bubble closes.
		 *
		 * This function accepts a single parameter, which is identical in
		 * structure to the parameter passed to onstartinteraction.
		 */
		'data-onendinteraction' => '',

		/**
		 * To disable showing recommendations within the +1 hover bubble.
		 *
		 * Options: true, false
		 */
		'data-recommendations' => 'false',
	);

	public function render()
	{
		$this->attributes['href'] = $this->getUrl();
		return '<div ' . $this->renderAttributes() . '></div>';
	}

}
