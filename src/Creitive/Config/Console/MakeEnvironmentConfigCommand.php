<?php namespace Creitive\Config\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class MakeEnvironmentConfigCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'make:env';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Environment configuration generator';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$sourceFile = $this->option('source');
		$targetFile = $this->option('target');

		$template = require $sourceFile;
		$target = file_exists($targetFile) ? (require $targetFile) : [];

		$this->info("Configure the new environment variable values. Current values are shown in square brackets. Just press ENTER to leave the default. This will overwrite {$targetFile}");

		foreach ($template as $key => $value)
		{
			$current = isset($target[$key]) ? $target[$key] : $value;

			$target[$key] = $this->ask("{$key} [{$current}]:", $current);
		}

		file_put_contents($targetFile, $this->compileFile($target));

		return 0;
	}

	/**
	 * Compiles the configuration array into a file that can later be required
	 * within PHP.
	 *
	 * @param array $configuration
	 * @return string
	 */
	public function compileFile(array $configuration)
	{
		$values = var_export($configuration, true);

		return "<?php return {$values};";
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['source', null, InputOption::VALUE_REQUIRED, 'The source file', '.environment.template.php'],
			['target', null, InputOption::VALUE_REQUIRED, 'The source file', '.environment.php'],
		];
	}

}
