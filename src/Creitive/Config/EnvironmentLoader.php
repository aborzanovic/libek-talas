<?php namespace Creitive\Config;

/**
 * PHP $_ENV loader for protecting sensitive configuration options.
 *
 * Inspired by the wonderful "Dotenv" library by Vance Lucas.
 */
class EnvironmentLoader {

	/**
	 * The path to the configuration files.
	 *
	 * @var string
	 */
	protected $path;

	/**
	 * The server environment instance.
	 *
	 * @param string $path
	 * @return void
	 */
	public function __construct($path)
	{
		$this->path = $path;
	}

	/**
	 * Load the server variables for a given environment.
	 *
	 * @return void
	 */
	public function load()
	{
		foreach ($this->loadFromFile() as $key => $value)
		{
			$_ENV[$key] = $value;

			$_SERVER[$key] = $value;

			putenv("{$key}={$value}");

			/**
			 * In cases where an environment variable is set by Apache, e.g. via
			 * .htaccess or the VirtualHost block, none of the above approaches
			 * seem to work; ie. the variable is overriden by whatever is
			 * already configured with Apache. This is the correct solution for
			 * that problem, but `apache_setenv()` is only available if PHP is
			 * actually running on Apache, so we'll just call it when we can.
			 */
			if (function_exists('apache_setenv'))
			{
				apache_setenv($key, $value);
			}
		}
	}

	/**
	 * Load the environment variables.
	 *
	 * @return array
	 */
	public function loadFromFile()
	{
		$path = $this->path.'/.environment.php';

		if ( ! file_exists($path))
		{
			return [];
		}

		return require $path;
	}

}
