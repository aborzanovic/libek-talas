<?php namespace Creitive\ScriptHandler\Facades;

use Illuminate\Support\Facades\Facade;

class Scripts extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'creitive.scripts';
	}

}
