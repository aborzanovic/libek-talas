<?php namespace Creitive\ScriptHandler;

use Creitive\ScriptHandler\ScriptHandler;
use Illuminate\Support\ServiceProvider;

class ScriptHandlerServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app['creitive.scripts'] = $this->app->share(function($app)
		{
			$app['view']->addNamespace('creitive/scripts', __DIR__.'/views');

			return new ScriptHandler(
				$app->environment(),
				$app['creitive.asset'],
				$app['view'],
				$app['config']
			);
		});

		$this->app->alias('creitive.scripts', 'Creitive\ScriptHandler\ScriptHandler');
	}

}
