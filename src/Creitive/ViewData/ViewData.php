<?php namespace Creitive\ViewData;

use Creitive\Html\CssClassHandler;
use Creitive\Html\PageTitle;
use stdClass;

class ViewData {

	/**
	 * The title of the current page.
	 *
	 * @var \Creitive\Html\PageTitle
	 */
	public $pageTitle;

	/**
	 * The `[data-page]` attribute embedded into the `<body>` element.
	 *
	 * @var string
	 */
	public $bodyDataPage;

	/**
	 * The CSS classes to be applied to the `<body>` element.
	 *
	 * @var string
	 */
	public $bodyClasses;

	public function __construct()
	{
		$this->pageTitle = new PageTitle;
		$this->bodyClasses = new CssClassHandler();
		$this->bodyDataPage = '';

		$this->meta = new stdClass;
		$this->meta->description = '';
		$this->meta->og = [];
	}

}
