<?php namespace Creitive\ViewData;

use Creitive\ViewData\ViewData;
use Illuminate\Support\ServiceProvider;

class ViewDataServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app->singleton('Creitive\ViewData\ViewData', function()
		{
			return new ViewData;
		});

		$this->app['view']->composer('*', 'Creitive\ViewData\Composer');
	}

}
