<?php namespace Creitive\Dropzone;

use Illuminate\Support\ServiceProvider;

class DropzoneServiceProvider extends ServiceProvider {

	public function boot()
	{
		$this->package('creitive/dropzone');
	}

	public function register()
	{
		$this->app['dropzone'] = $this->app->share(function($app)
		{
			return new Dropzone;
		});
	}

}
