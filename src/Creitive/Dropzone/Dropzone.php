<?php namespace Creitive\Dropzone;

use Closure;
use Hampel\Json\Json;
use Traversable;

class Dropzone {

	/**
	 * Creates some HTML for the Dropzone.js script.
	 *
	 * This only makes sense if used with the accompanying Javascript
	 * implementation.
	 *
	 * @param array $configuration
	 * @return string
	 */
	public function create(array $configuration)
	{
		$previewsId = $configuration['entityName'].'-previews';
		$uploaderId = $configuration['entityName'].'-uploader';
		$uploadUrl = $configuration['uploadUrl'];
		$deleteUrl = $configuration['deleteUrl'];
		$sortUrl = array_get($configuration, 'sortUrl');
		$sortAttribute = $sortUrl ? "data-sort-url=\"{$sortUrl}\"" : '';
		$sortable = $sortUrl ? 'dropzone-sortable' : '';
		$existingImagesIndex = $configuration['entityName'].'s';
		$images = $this->getImagesArray($configuration['images'], $configuration['imageCallback']);
		$jsonImages = Json::encode($images);
		$dropImageText = isset($configuration['dropImageText']) ? $configuration['dropImageText'] : 'Drop images or click here to upload them.';

		return <<<DROPZONE
			<div id="{$previewsId}" class="dropzone-previews {$previewsId} {$sortable}"></div>
			<div
				id="{$uploaderId}"
				class="col-lg-12 {$uploaderId}"
				data-upload-url="{$uploadUrl}"
				data-delete-url="{$deleteUrl}"
				{$sortAttribute}
				data-dropzone
				data-existing-images="{$existingImagesIndex}"
				data-previews-container="{$previewsId}"
			>
				{$dropImageText}
			</div>
			<script>
				if ( typeof dropzoneImages === "undefined" ) {
					var dropzoneImages = {};
				}

				dropzoneImages["{$existingImagesIndex}"] = {$jsonImages};
			</script>
DROPZONE;
	}

	/**
	 * Applies the provided callback to each of the images, storing the result
	 * into an array which is then returned. Basically an `array_map`, but we
	 * can't use that because the first argument might not be an actual array.
	 *
	 * @param \Traversable|array $images
	 * @param \Closure $callback
	 * @return array
	 */
	protected function getImagesArray(Traversable $images, Closure $callback)
	{
		$newImages = array();

		foreach ($images as $image)
		{
			$newImages[] = $callback($image);
		}

		return $newImages;
	}

}
