<?php namespace Creitive\Database\Query\Grammars;

use Creitive\Database\Query\Grammars\MySqlGrammar;
use Eloquent;
use Illuminate\Support\ServiceProvider;

class MySqlGrammarServiceProvider extends ServiceProvider {

	public function register() {}

	public function boot()
	{
		/**
		 * Use our own `MySqlGrammar` class, so as to be able to use the
		 * `SQL_CALC_FOUND_ROWS` to count results.
		 */

		$connection = $this->app['db']->connection();
		$grammar = new MySqlGrammar;
		$connection->setQueryGrammar($grammar);
	}

}
