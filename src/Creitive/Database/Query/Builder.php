<?php namespace Creitive\Database\Query;

use Illuminate\Database\Query\Builder as BaseBuilder;

class Builder extends BaseBuilder {

	public $sqlCalcFoundRows = false;

	public function sqlCalcFoundRows($on = true)
	{
		$this->sqlCalcFoundRows = $on;

		return $this;
	}

}
