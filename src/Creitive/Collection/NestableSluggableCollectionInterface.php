<?php namespace Creitive\Collection;

interface NestableSluggableCollectionInterface {

	public function findBySlugPath(array $path);

}
