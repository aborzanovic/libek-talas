<?php namespace Creitive\Collection;

interface SluggableCollectionInterface {

	public function findBySlug($slug);

}
