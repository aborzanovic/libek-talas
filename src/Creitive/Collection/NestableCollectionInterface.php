<?php namespace Creitive\Collection;

interface NestableCollectionInterface {

	public function nest();

}
