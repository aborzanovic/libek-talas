<?php namespace Creitive\Collection\Traits;

use Creitive\Models\SluggableInterface;

trait SluggableCollectionTrait {

	/**
	 * Finds an item by its slug.
	 *
	 * Only searches the root-level items in this collection.
	 *
	 * Returns `null` if the item isn't found.
	 *
	 * @param string $slug
	 * @return \Creitive\Models\SluggableInterface
	 */
	public function findBySlug($slug)
	{
		return $this->first(function($key, SluggableInterface $item) use ($slug)
		{
			return $item->slug === $slug;
		});
	}

}
