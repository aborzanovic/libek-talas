<?php namespace Creitive\Collection\Traits;

trait NestableCollectionTrait {

	/**
	 * Returns a new collection with all the items nested.
	 *
	 * @return \Creitive\Collection\NestableCollectionInterface
	 */
	public function nest()
	{
		$rootCollection = new static;

		foreach ($this as $item)
		{
			$item->initializeChildren();

			if (!$item->hasParent())
			{
				$item->setParent(null);

				$rootCollection->push($item);

				continue;
			}

			$parent = $this->find($item->parent_id);

			$item->setParent($parent);
			$parent->addChild($item);
		}

		return $rootCollection;
	}

	/**
	 * Recursively finds a model in the nestable collection by key.
	 *
	 * @param mixed $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function find($key, $default = null)
	{
		$item = parent::find($key, null);

		if ( ! is_null($item))
		{
			return $item;
		}

		foreach ($this as $item)
		{
			$child = $item->children->find($key);

			if ( ! is_null($child))
			{
				return $child;
			}
		}

		return $default;
	}

	/**
	 * Returns a flattened array of primary keys.
	 *
	 * @return array
	 */
	public function modelKeys()
	{
		$keys = [];

		foreach ($this as $item)
		{
			$keys[] = $item->getKey();

			if ($item->hasChildren())
			{
				$keys = array_merge($keys, $item->children->modelKeys());
			}
		}

		return $keys;
	}

}
