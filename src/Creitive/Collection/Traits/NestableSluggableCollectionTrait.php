<?php namespace Creitive\Collection\Traits;

use LengthException;

trait NestableSluggableCollectionTrait {

	/**
	 * Finds an item by the slug-path to it, starting from the root-level in
	 * this collection.
	 *
	 * Throws a `LengthException` if an empty array is passed.
	 *
	 * Returns `null` if the item isn't found.
	 *
	 * @param array $path
	 * @return \Creitive\Models\NestableSluggableInterface
	 * @throws \LengthException
	 */
	public function findBySlugPath(array $path)
	{
		if (empty($path))
		{
			throw new LengthException;
		}

		$slug = array_shift($path);

		$item = $this->findBySlug($slug);

		if (empty($path) || is_null($item))
		{
			return $item;
		}

		return $item->children->findBySlugPath($path);
	}

}
