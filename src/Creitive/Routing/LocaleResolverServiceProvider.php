<?php namespace Creitive\Routing;

use Creitive\Routing\LocaleResolver;
use Illuminate\Support\ServiceProvider;

class LocaleResolverServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app->singleton('Creitive\Routing\LocaleResolver', function($app)
		{
			$availableLocales = $app['config']->get('app.locales');
			$defaultLocale = $app['config']->get('app.locale');

			return new LocaleResolver($availableLocales, $defaultLocale);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->setLocaleByUrl();
	}

	/**
	 * Sets the application locale according to the first URL segment.
	 *
	 * @return void
	 */
	protected function setLocaleByUrl()
	{
		$localeResolver = $this->app->make('Creitive\Routing\LocaleResolver');

		$firstSegment = $this->app['request']->segment(1);

		$locale = $localeResolver->getLocaleByLanguage($firstSegment);

		if ( ! is_null($locale))
		{
			$this->app->setLocale($locale);
			$localeResolver->setLocale($locale);
		}
	}

}
