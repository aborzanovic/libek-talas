<?php namespace Creitive\Routing\Facades;

use Illuminate\Support\Facades\Facade;

class LocaleResolver extends Facade {

	/**
	 * {@inheritDoc}
	 */
	protected static function getFacadeAccessor()
	{
		return 'Creitive\Routing\LocaleResolver';
	}

}
