<?php namespace Creitive\Image\Facades;

use Illuminate\Support\Facades\Facade;

class ImageUploader extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'creitive.image.uploader';
	}

}
