<?php namespace Creitive\Image;

use Creitive\Image\Uploader\FileArgumentParser;
use Creitive\Image\Uploader\FilenameManager;
use Creitive\Image\Uploader\ImageProcessor;
use Creitive\Support\FileSystem;
use Exception;
use Imagick;

class Uploader {

	/**
	 * The parser for the file argument.
	 *
	 * @todo Add getter and setter
	 * @var \Creitive\Image\Uploader\FileArgumentParser
	 */
	protected $fileArgumentParser;

	/**
	 * The filename manager.
	 *
	 * @todo Add getter and setter
	 * @var \Creitive\Image\Uploader\FilenameManager
	 */
	protected $filenameManager;

	/**
	 * The image processor.
	 *
	 * @todo Add getter and setter
	 * @var \Creitive\Image\Uploader\ImageProcessor
	 */
	protected $imageProcessor;

	/**
	 * The compression quality used for `JPEG` files.
	 *
	 * @todo Add getter and setter
	 * @var integer
	 */
	protected $imageCompressionQuality = 80;

	public function __construct(
		FileArgumentParser $fileArgumentParser,
		FilenameManager $filenameManager,
		ImageProcessor $imageProcessor
	)
	{
		$this->fileArgumentParser = $fileArgumentParser;
		$this->filenameManager = $filenameManager;
		$this->imageProcessor = $imageProcessor;
	}

	/**
	 * Uploads an image and returns the paths of all the versions in an array.
	 *
	 * @param mixed $file
	 * @param string $version
	 * @param array $versions
	 * @param string $prefix
	 * @param string $path
	 * @return array
	 */
	public function upload($file, $version, array $versions, $prefix = '', $path = null)
	{
		if (is_null($path))
		{
			$path = $this->filenameManager->getDefaultPath();
		}

		return $this->generateAllImages(
			$this->fileArgumentParser->parse($file),
			$version,
			$versions,
			$prefix,
			$path
		);
	}

	/**
	 * Performs all the actions needed to generate the required image versions.
	 *
	 * Returns an array in which each index is one of the required versions, and
	 * the corresponding value is the path that should be added to the database
	 * as the image location relative to the document root (though it's actually
	 * an absolute URL, suitable for outputting directly as the `src` attribute
	 * of an `<img>` element).
	 *
	 * @param array  $file
	 * @param string $version
	 * @param array  $versions
	 * @param string $prefix
	 * @param string $path
	 * @return array
	 */
	protected function generateAllImages(array $file, $version, array $versions, $prefix, $path)
	{
		/**
		 * Get an Imagick instance if we can, otherwise return false
		 */
		$image = $this->uploadAndGetImagick($file);

		if ( ! $image instanceof Imagick)
		{
			return false;
		}

		$extension = $this->filenameManager->getExtension($file);
		$filenames = $this->filenameManager->generateFilenames($path, $prefix, $extension, $version, $versions);

		$this->imageProcessor->processImages($image, $versions, $filenames);

		/**
		 * Generate the paths for the database.
		 */
		$return = [];

		foreach ($versions as $version => $parameters)
		{
			$return[$version] = $filenames[$version]['path']['db'];
		}

		return $return;
	}

	/**
	 * Uploads a file and returns an `Imagick` instance of it.
	 *
	 * @param array $file
	 * @return \Imagick
	 */
	protected function uploadAndGetImagick(array $file)
	{
		$extension = $this->filenameManager->getExtension($file);
		$root = $this->filenameManager->getTemporaryPath();

		/**
		 * Generate the filename for the original file, as well as its full
		 * path.
		 */
		$filenameRandom = $this->filenameManager->generateRandomFilename();

		$tmpUploadedFilePath = "{$root}/{$filenameRandom}";

		$targetPath = dirname($tmpUploadedFilePath);

		/**
		 * Make sure the path is writable, and create it if it doesn't exist
		 * yet.
		 */
		if ( ! FileSystem::ensureWritablePath($targetPath, true))
		{
			throw new Exception("The specified path cannot be created or written to: {$targetPath}");
		}

		/**
		 * Sometimes (when uploading an image from a path), the file is not
		 * uploaded via a web server, it already exists in a local location, but
		 * in those cases, the handling code should also add an `uploaded` key
		 * to the $file array and set it to false, because then we'll just copy
		 * it to the new location.
		 */
		if (isset($file['uploaded']) && ! $file['uploaded'])
		{
			if ( ! copy($file['tmp_name'], $tmpUploadedFilePath))
			{
				throw new Exception("The file could not be copied from its source to the specified location: {$file['tmp_name']} -> {$tmpUploadedFilePath}");
			}
		}
		else
		{
			/**
			 * Move the uploaded file to our own location. This is done so as to
			 * free up PHP's native temporary uploads path, and let us work with our
			 * own.
			 */
			if ( ! move_uploaded_file($file['tmp_name'], $tmpUploadedFilePath))
			{
				throw new Exception("The uploaded file cannot be moved to the specified location: {$tmpUploadedFilePath}");
			}
		}

		/**
		 * We'll immediately load the file into an `Imagick` instance.
		 */
		$image = new Imagick($tmpUploadedFilePath);

		/**
		 * We don't need the uploaded file anymore, so we're deleting it. There
		 * is a slight gotcha with this - if the upload script breaks for some
		 * reason during the processing happening later (after this method is
		 * finished), the uploaded file will be lost, as after this point, it is
		 * only kept in the memory of the current process, within the previously
		 * created `Imagick` object. However, in that case, we need to inform
		 * the user that an error has occured anyway, so it's not a problem.
		 */
		unlink($tmpUploadedFilePath);

		/**
		 * If we have a `JPEG` file, we'll set the compression and the quality
		 * here. This is just an ad-hoc test, as it checks the user-supplied
		 * filename extension, but it's not important, so whatever.
		 */
		if (($extension === 'jpg') || ($extension === 'jpeg'))
		{
			$image->setImageCompression(Imagick::COMPRESSION_JPEG);
			$image->setImageCompressionQuality($this->imageCompressionQuality);
		}

		/**
		 * Strip image metadata, because we don't want it, and for security
		 * purposes, because we don't want to accidentally leak any information
		 * we weren't meant to.
		 */
		$image->stripImage();

		return $image;
	}

}
