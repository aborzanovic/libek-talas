<?php namespace Creitive\Image\Uploader;

use Closure;
use Creitive\Support\FileSystem;
use Exception;
use Imagick;

class ImageProcessor {

	/**
	 * Processes all of the image versions.
	 *
	 * @param \Imagick $image
	 * @param array $versions
	 * @param array $filenames
	 * @return void
	 */
	public function processImages(Imagick $image, array $versions, $filenames)
	{
		foreach ($versions as $version => $parameters)
		{
			$this->processImage(
				$image,
				$parameters,
				$filenames[$version]['path']['filesystem']
			);
		}
	}

	/**
	 * Processes a single image based on the parameters passed.
	 *
	 * @param \Imagick $original
	 * @param array $parameters
	 * @param string $targetFilename
	 * @return void
	 */
	protected function processImage(Imagick $original, array $parameters, $targetFilename)
	{
		$image = $this->applyImageTransformations($original, $parameters);

		$targetDir = dirname($targetFilename);

		if ( ! FileSystem::ensureWritablePath($targetDir, true))
		{
			throw new Exception("The specified path cannot be created or written to: {$targetDir}");
		}

		$image->writeImages($targetFilename, true);
		$image->destroy();
	}

	/**
	 * Applies all image transformation described in the parameters array.
	 *
	 * @param \Imagick $original
	 * @param array $parameters
	 * @return \Imagick
	 */
	protected function applyImageTransformations(Imagick $original, array $parameters)
	{
		$image = clone $original;

		if (isset($parameters['method']))
		{
			$method = $parameters['method'];

			if ($method instanceof Closure)
			{
				$method($image);
			}
			else if (method_exists($this, $method))
			{
				$image = $this->$method($image, $parameters);
			}
			else if (method_exists($image, $method))
			{
				$image->$method($parameters['width'], $parameters['height']);
			}
			else
			{
				throw new Exception('Unknown method provided: '.$method);
			}
		}

		if (isset($parameters['sharpen']) && $parameters['sharpen'])
		{
			$image->sharpenImage(2, 1);
		}

		return $image;
	}

	/**
	 * Creates a thumbnail of an image, and fills the remaining pixels with a
	 * color supplied through the second argument (which defaults to white, if
	 * no color is supplied).
	 *
	 * The `$params` array is expected to contain two keys: `width` and
	 * `height`, representing the desired thumbnail dimensions in pixels. An
	 * optional third key, `color`, is accepted, which should either be a
	 * `string`, representing a color accepted by `Imagick::newImage()`, or an
	 * `ImagickPixel` with the specified color. See this link for details:
	 * http://php.net/imagick.newimage For a transparent background, set this
	 * parameter to the string `none`.
	 *
	 * @param \Imagick $image
	 * @param array $params
	 * @return \Imagick
	 */
	protected function thumbnailAndFillBackground(Imagick $image, array $params)
	{
		$tmpImage = clone $image;

		$tmpImage->thumbnailImage($params['width'], $params['height'], true);
		$geometry = $tmpImage->getImageGeometry();

		$background = isset($params['color']) ? $params['color'] : '#fff';

		$x = ($params['width'] - $geometry['width']) / 2;
		$y = ($params['height'] - $geometry['height']) / 2;

		$canvas = new Imagick;
		$canvas->newImage($params['width'], $params['height'], $background, 'png');
		$canvas->compositeImage($tmpImage, Imagick::COMPOSITE_OVER, $x, $y);

		$tmpImage->destroy();

		return $canvas;
	}

	/**
	 * Crops the largest possible square from the passed image.
	 *
	 * The square will be centered both horizontally and vertically within the
	 * image.
	 *
	 * @param \Imagick $image
	 * @return \Imagick
	 */
	protected function cropLargestSquare(Imagick $image)
	{
		$width = $image->getImageWidth();
		$height = $image->getImageHeight();

		$squareSideLength = min($width, $height);

		if ($squareSideLength === $width)
		{
			$x = 0;
			$y = (int) ($height / 2 - $squareSideLength / 2);
		}
		else
		{
			$x = (int) ($width / 2 - $squareSideLength / 2);
			$y = 0;
		}

		return $image->crop($squareSideLength, $squareSideLength, $x, $y);
	}

}
