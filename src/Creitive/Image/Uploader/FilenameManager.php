<?php namespace Creitive\Image\Uploader;

use InvalidArgumentException;

class FilenameManager {

	/**
	 * The default path for storing uploaded images.
	 *
	 * See `getDefaultPath()` for more info.
	 *
	 * @var string
	 */
	protected $defaultPath;

	public function __construct($defaultPath = 'images/upload')
	{
		$this->defaultPath = $defaultPath;
	}

	/**
	 * Returns the default path for storing uploaded images.
	 *
	 * This path should be in the public folder, and it should be ignored in
	 * your VCS - the recommended solution is to just symlink it to somewhere
	 * outside of your project root (in production), or just create the folder
	 * for development purposes.
	 *
	 * @return string
	 */
	public function getDefaultPath()
	{
		return $this->defaultPath;
	}

	/**
	 * Gets the default path for uploaded images.
	 *
	 * @param string $path
	 * @return string
	 */
	public function getPublicPath($path = null)
	{
		if (is_null($path))
		{
			$path = $this->getDefaultPath();
		}

		return public_path($path);
	}

	/**
	 * Returns a path for storing temporary files.
	 *
	 * @return string
	 */
	public function getTemporaryPath()
	{
		return storage_path('tmp');
	}

	/**
	 * Generates a random filename (useful for unique filenames). It includes
	 * both the current timestamp, and a random part.
	 *
	 * @param integer $maxLength
	 * @return string
	 */
	public function generateRandomFilename($maxLength = 16)
	{
		$filename = time().'.'.md5(time().mt_rand());

		return substr($filename, 0, $maxLength);
	}

	/**
	 * Compiles a filename based on the passed parts.
	 *
	 * @param string $prefix
	 * @param string $name
	 * @param string $suffix
	 * @param string $extension
	 * @return string
	 */
	public function compileNewFilename($prefix = 'image', $name = 'no-name', $version = '', $size = '', $extension = 'png')
	{
		$parts = [];

		$parts[] = $prefix;
		$parts[] = $name;
		$parts[] = $version;

		if ($size)
		{
			$parts[] = $size;
		}

		return implode('/', $parts).'.'.$extension;
	}

	/**
	 * Returns the extension of the uploaded file.
	 *
	 * This shouldn't be considered a safe method, as it just uses whatever the
	 * client has provided. If you need additional validation, do it yourself.
	 *
	 * @param array $file
	 * @return string
	 */
	public function getExtension(array $file)
	{
		if ( ! isset($file['name']))
		{
			throw new InvalidArgumentException('Creitive\\Image\\Uploader\\FilenameManager::getExtension() only accepts an array with a \'name\' index.');
		}

		return pathinfo($file['name'], PATHINFO_EXTENSION);
	}

	/**
	 * Generates an array of filenames for the various image sizes.
	 *
	 * @param string $path
	 * @param string $prefix
	 * @param string $extension
	 * @param array $versions
	 * @return array
	 */
	public function generateFilenames($path = null, $prefix = '', $extension = '', $version = '', $sizes = [])
	{
		if (is_null($path))
		{
			$path = $this->getDefaultPath();
		}

		$fullPath = $this->getPublicPath($path);

		$filenameRandom = $this->generateRandomFilename();

		$filenames = [];

		foreach ($sizes as $size => $parameters)
		{
			$filename = $this->compileNewFilename($prefix, $filenameRandom, $version, $size, $extension);

			$filenames[$size] = [
				'filename' => $filename,
				'path' => [
					'filesystem' => "{$fullPath}/{$filename}",
					'db' => "/{$path}/{$filename}",
				],
			];
		}

		return $filenames;
	}

}
