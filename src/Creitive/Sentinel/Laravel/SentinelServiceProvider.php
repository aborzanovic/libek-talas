<?php namespace Creitive\Sentinel\Laravel;

use Creitive\Sentinel\Cookies\IlluminateCookie;
use Illuminate\Support\ServiceProvider;

class SentinelServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->registerCookie();
	}

	/**
	 * Registers the cookie.
	 *
	 * @return void
	 */
	protected function registerCookie()
	{
		$this->app['sentinel.cookie'] = $this->app->share(function($app)
		{
			$key = $app['config']['cartalyst/sentinel::cookie'];
			$secure = $app['config']['session.secure'];

			return new IlluminateCookie($app['request'], $app['cookie'], $key, $secure);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function provides()
	{
		return [
			'sentinel.cookie',
		];
	}

}
