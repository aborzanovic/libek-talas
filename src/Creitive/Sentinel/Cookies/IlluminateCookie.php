<?php namespace Creitive\Sentinel\Cookies;

use Cartalyst\Sentinel\Cookies\IlluminateCookie as BaseIlluminateCookie;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;

class IlluminateCookie extends BaseIlluminateCookie {

	/**
	 * Whether to use secure cookies.
	 *
	 * @var boolean
	 */
	protected $secure;

	/**
	 * Create a new Illuminate cookie driver.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Illuminate\Cookie\CookieJar $jar
	 * @param string $key
	 * @param boolean $secure
	 * @return void
	 */
	public function __construct(Request $request, CookieJar $jar, $key = null, $secure = false)
	{
		$this->request = $request;

		$this->jar = $jar;

		if (isset($key))
		{
			$this->key = $key;
		}

		$this->secure = $secure;
	}

	/**
	 * {@inheritDoc}
	 */
	public function put($value)
	{
		$cookie = $this->jar->forever($this->key, $value, null, null, $this->secure);

		$this->jar->queue($cookie);
	}

}
