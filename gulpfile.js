var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;
elixir.config.assetsPath = 'assets/';
elixir.config.bowerDir  = 'vendor/bower_components';

elixir(function(mix) {
    mix.sass("app.scss")
    	.version("css/app.css");
});