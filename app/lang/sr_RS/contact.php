<?php

return [
	'labels' => [
		'fullName' => 'Ime i prezime',
		'email' => 'Email adresa',
		'message' => 'Poruka',
		'send' => [
			'default' => 'Pošalji',
			'loading' => 'Slanje u toku...',
		],
	],
	'errors' => [
		'fullName' => 'Morate uneti ime i prezime.',
		'email' => 'Morate uneti ispravnu email adresu.',
		'message' => 'Morate uneti poruku.',
	],
	'privacy' => '<strong>Privatnost:</strong> Radi zaštite privatnosti, Vaši podaci neće nikada biti prosleđivani trećim licima, niti korišćeni u bilo koju svrhu osim komunikacije sa Vama.',
	'success' => 'Uspešno ste poslali poruku. Hvala Vam što nas kontaktirate, potrudićemo se da Vam odgovorimo što je pre moguće.',
	'subject' => 'Kontakt',
];
