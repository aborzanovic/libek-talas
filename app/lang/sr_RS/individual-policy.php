<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/14/2016
 * Time: 2:56 AM
 */

return [
  'content1' => 'Seminar Politika pojedinca ima za cilj upoznavanje učesnika sa osnovama političke tradicije liberalizma sa akcentom na praktičnoj primeni ideja u rešavanju raznih društvenih problema. Teme obuhvaćene seminarom se kreću od prava pojedinaca, problema jednakosti i pravde, poimanja građanskog društva i države, kao i osnovnih postulata tržišne ekonomije.',
    'content2' => 'Seminar se tradicionalno odvija tokom dva dana tokom kojih se polaznici upoznaju sa političkom filozofijom libertarijanizma i učestvuju u interaktivnim radionicama. Prvi seminar „Politka pojedinca“ je održan u Beogradu, 2010. godine.',
    'content3' => 'Seminar je do sada održan četrnaest puta u različitim gradovima Srbije. Osim u Beogradu, bili smo i u Nišu, Kragujevcu, Novom Sadu i Pančevu. Nakon seminara saradnja sa srednjoškolcima se često nastavlja i kroz <a href="alp"> Akademiju liberalne politike.</a> '
];