<?php

return array(

	'aboutUs' => 'o-nama',
	'news' => 'vesti',
    'team' => 'tim',
    'education' => 'obrazovanje',
	'research' => 'istrazivanja',
	'projects' => 'projekti',
	'contact' => 'kontakt',
    'blog' => 'blog',
    'Q&A' =>  'Q&A',
    'works' => 'dela',
    'advocating' => 'zagovaranje',
    'johnGalt' => 'john-galt',
	'alp' => 'alp',
    'support' => 'podrska',
    'individual-policy' => 'politika-pojedinca',
	'lecturers' => 'predavaci',
	'mentors' => 'mentori',
	'alumni' => 'alumnisti',
	'executiveBoard' => 'izvrsni-odbor',
	'communicationAndProtocol' => 'komunikacija-i-protokol',
	'researchDepartment' => 'istrazivacka-jedinica',
	'subscriptions' => 'prijave',
	'applicationForms' => 'aplikacije',
    'newsletter' => 'prijavite-se',
    'thanks' => 'hvala-sto-nas-pratite'

);
