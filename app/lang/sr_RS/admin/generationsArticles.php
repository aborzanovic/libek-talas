<?php

return [

	'module' => 'Generacije',

	'titles' => [
		'index' => 'Generacije',
		'create' => 'Dodavanje nove generacije',
		'edit' => 'Izmena',
		'delete' => 'Brisanje',
	],

	'index' => [
		'links' => [
			'create' => 'Dodaj novu generaciju',
			'edit' => 'Izmeni',
			'delete' => 'Obriši',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Osnovna podešavanja',
		'content' => 'Sadržaj',
		'seo' => 'Optimizacija za pretraživače',
	],

	'labels' => [
		'language' => 'Jezik',
		'title' => 'Naslov',
		'slug' => 'URL segment',
		'createdAt' => 'Dodato',
		'publishedAt' => 'Objavljeno',
		'sticky' => 'Lepljivi index',
		'lead' => 'Uvod',
		'contents' => 'Sadržaj',
		'imageMain' => 'Slika',
		'metaTitle' => 'Meta naslov',
		'metaDescription' => 'Meta opis',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Sačuvaj',
			'loading' => 'Čuvanje uz toku...',
		],
	],

	'help' => [
		'slug' => 'Preporučujemo Vam da ostavite ovo polje prazno - u tom slučaju, sistem će automatski generisati URL segment na osnovu naslova.',
		'publishedAt' => 'Datum/vreme mora biti uneto u sledećem formatu: <code>DD.MM.YYYY. hh:mm</code>. Ukoliko za sada ne želite da objavite ovaj članak, ostavite ovo polje prazno.',
		'imageMain' => 'Molimo Vas, koristite sliku minimalnih dimenzija <code>960×543px</code>.',
		'sticky' => 'Broj kojim se članak može učiniti "lepljivim". Što je veći ovaj broj, to će članak biti "lepljiviji", tj. prikazivaće se ispred ostalih stranica na prednjem delu sajta. Ostavite vrednost <code>0</code> ukoliko ne želite da ovaj članak bude lepljiv.',
		'metaTitle' => 'Preporučena dužina do 60 karaktera. Možete da ostavite ovo polje prazno, u kom slučaju će se automatski koristiti naslov članka.',
		'metaDescription' => 'Preporučena dužina do 160 karaktera. Možete da ostavite ovo polje prazno, u kom slučaju će se automatski koristiti isečak iz sadržaja članka.',
	],

	'confirmDelete' => [
		'message' => 'Da li ste sigurni da želite da obrišete ovu generaciju?',
		'confirm' => [
			'default' => 'Potvrdi',
			'loading' => 'Brisanje u toku...',
		],
		'cancel' => [
			'default' => 'Odustani',
			'loading' => 'Odustajanje u toku...',
		],
	],

	'successMessages' => [
		'create' => 'Uspešno ste dodali novu generaciju.',
		'edit' => 'Uspešno ste izmenili ovu generaciju.',
		'delete' => 'Uspešno ste obrisali generaciju ":title".',
	],

];
