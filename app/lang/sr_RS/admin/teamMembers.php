<?php

return [

	'module' => 'Članovi tima',

	'titles' => [
		'index' => 'Članovi tima',
		'create' => 'Dodavanje novog člana',
		'edit' => 'Izmena',
		'delete' => 'Brisanje',
	],

	'index' => [
		'links' => [
			'create' => 'Dodaj novog člana',
			'edit' => 'Izmeni',
			'delete' => 'Obriši',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Osnovna podešavanja',
		'content' => 'Sadržaj',
		'seo' => 'Optimizacija za pretraživače',
	],

	'labels' => [
		'name' => 'Ime',
		'surname' => 'Prezme',
		'title_sr' => 'Titula (srpski)',
		'title_en' => 'Titula (engleski)',
		'createdAt' => 'Dodato',
		'image' => 'Slika',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Sačuvaj',
			'loading' => 'Čuvanje uz toku...',
		],
	],

	'help' => [
		'image' => ''
	],

	'confirmDelete' => [
		'message' => 'Da li ste sigurni da želite da obrišete ovog člana?',
		'confirm' => [
			'default' => 'Potvrdi',
			'loading' => 'Brisanje u toku...',
		],
		'cancel' => [
			'default' => 'Odustani',
			'loading' => 'Odustajanje u toku...',
		],
	],

	'successMessages' => [
		'create' => 'Uspešno ste dodali novog člana.',
		'edit' => 'Uspešno ste izmenili ovog člana.',
		'delete' => 'Uspešno ste obrisali člana.',
	],

];
