<?php

return [

	'module' => 'Promena lozinke',

	'titles' => [
		'update' => 'Promena lozinke',
	],

	'labels' => [
		'currentPassword' => 'Trenutna lozinka',
		'newPassword' => 'Nova lozinka',
		'newPasswordConfirmation' => 'Ponovite novu lozinku',
		'update' => [
			'default' => 'Izmeni',
			'loading' => 'Izmena u toku...',
		],
	],

	'successMessages' => [
		'update' => 'Uspešno ste izmenili svoju lozinku.',
	],

	'errorMessages' => [
		'update' => [
			'current_password' => [
				'required' => 'Morate uneti trenutnu lozinku.',
				'current_password' => 'Lozinka koju ste uneli nije tačna.',
			],
			'new_password' => [
				'required' => 'Morate uneti novu lozinku.',
				'confirmed' => 'Morate tačno ponoviti novu lozinku.',
			],
		],
	],

];
