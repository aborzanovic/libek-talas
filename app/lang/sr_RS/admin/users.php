<?php

return [

	'module' => 'Korisnici',

	'titles' => [
		'index' => 'Korisnici',
		'create' => 'Dodavanje novog korisnika',
		'edit' => 'Izmena',
		'delete' => 'Brisanje',
	],

	'index' => [
		'links' => [
			'create' => 'Dodajte novog korisnika',
			'edit' => 'Izmena',
			'delete' => 'Brisanje',
			'roles' => [
				'all' => 'Svi korisnici',
				'admins' => 'Administratori',
				'guests' => 'Gosti',
			],
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Osnovna podešavanja',
	],

	'labels' => [
		'id' => 'ID',
		'fullName' => 'Ime i prezime',
		'firstName' => 'Ime',
		'lastName' => 'Prezime',
		'email' => 'Email',
		'password' => 'Lozinka',
		'role' => 'Uloga',
		'roles' => 'Uloge',
		'save' => [
			'default' => 'Sačuvaj',
			'loading' => 'Čuvanje u toku...',
		],
	],

	'help' => [
		'password' => 'Ukoliko ne želite da promenite lozinku ovog korisnika, samo ostavite ovo polje prazno.',
	],

	'confirmDelete' => [
		'message' => 'Da li ste sigurni da želite da obrišete ovog korisnika?',
		'confirm' => [
			'default' => 'Potvrdi',
			'loading' => 'Brisanje u toku...',
		],
		'cancel' => [
			'default' => 'Odustani',
			'loading' => 'Odustajanje u toku...',
		],
	],

	'successMessages' => [
		'create' => 'Uspešno ste dodali novog korisnika.',
		'edit' => 'Uspešno ste izmenili ovog korisnika.',
		'delete' => 'Uspešno ste obrisali korisnika ":fullName".',
	],

];
