<?php

return [

	'hello' => 'Ulogovani ste kao :name',

	'logout' => [
		'default' => 'Odjavite se',
		'loading' => 'Odjava u toku...',
	],

];
