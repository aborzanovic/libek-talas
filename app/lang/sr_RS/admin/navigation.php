<?php

return [

	'home' => 'Početna',
	'slideshowImages' => 'Slideshow slike',
	'newsArticles' => 'Vesti',
	'generationsArticles' => 'Generacije',
	'johnGaltArticles' => 'John Galt vesti',
	'researchArticles' => 'Istraživanja',
	'teamMembers' => 'Članovi tima',
	'mentors' => 'Mentori',
	'lecturers' => 'Predavači',
	'alumni' => 'Alumnisti',
	'users' => 'Korisnici',
	'changePassword' => 'Promena lozinke',
	'backToWebsite' => 'Povratak na sajt',

];
