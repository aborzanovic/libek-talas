<?php

return [

	'module' => 'Alumnisti',

	'titles' => [
		'index' => 'Alumnisti',
		'create' => 'Dodavanje novog alumniste',
		'edit' => 'Izmena',
		'delete' => 'Brisanje',
	],

	'index' => [
		'links' => [
			'create' => 'Dodaj novog alumnistu',
			'edit' => 'Izmeni',
			'delete' => 'Obriši',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Osnovna podešavanja',
		'content' => 'Sadržaj',
		'seo' => 'Optimizacija za pretraživače',
	],

	'labels' => [
		'name' => 'Ime',
		'surname' => 'Prezme',
		'title_sr' => 'Obrazovanje (srpski)',
		'title_en' => 'Obrazovanje (engleski)',
		'generation' => 'Generacija (godina)',
		'createdAt' => 'Dodato',
		'image' => 'Slika',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Sačuvaj',
			'loading' => 'Čuvanje uz toku...',
		],
	],

	'help' => [
		'image' => ''
	],

	'confirmDelete' => [
		'message' => 'Da li ste sigurni da želite da obrišete ovog alumnistu?',
		'confirm' => [
			'default' => 'Potvrdi',
			'loading' => 'Brisanje u toku...',
		],
		'cancel' => [
			'default' => 'Odustani',
			'loading' => 'Odustajanje u toku...',
		],
	],

	'successMessages' => [
		'create' => 'Uspešno ste dodali novog alumnistu.',
		'edit' => 'Uspešno ste izmenili ovog alumnistu.',
		'delete' => 'Uspešno ste obrisali alumnistu.',
	],

];
