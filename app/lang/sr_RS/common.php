<?php

return array(

	'siteName' => 'Talas',
	'homeTagline' => '',
	'metaDescription' => 'Talas',
	'noscriptWarning' => 'Izgleda da Vaš browser blokira izvršavanje skripti na ovom sajtu! Većina stvari bi trebala da radi kako treba i bez skripti, ali Vam preporučujemo da ih uključite, kako biste imali kompletan doživljaj.',
	'dateFormat' => 'd.m.Y.',
	'dateTimeFormat' => 'd.m.Y. H:i',
	'noArticles' => 'Žao nam je ali trenutno nemamo članaka u ovoj sekciji. Pratite nas, obećavamo da ćemo ih dodati uskoro!',
	'adminPanel' => 'Admin panel',
	'decimalSeparator' => ',',
	'thousandsSeparator' => '.',

);
