<?php

return array(

	'first' => '« Prva',
	'previous' => '‹ Prethodna',
	'next' => 'Sledeća ›',
	'last' => 'Poslednja »',
	'urlSegment' => 'strana',

);
