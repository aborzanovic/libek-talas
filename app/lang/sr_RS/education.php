<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/2/2016
 * Time: 1:45 PM
 */

return [
    'tiny' =>    'Obrazovanje',
    'large' => [
        '1' => 'Prvi strateški stub Libekovog delovanja predstavlja obrazovanje. Libek se
                    od svog osnivanja bavi obrazovanjem studenata, srednjoškolaca i društvenih aktivista.',
        '2' =>  'Libek je danas vodeći obrazovno-istraživački centar u našem regionu koji se bavi političkom
                    filozofijom slobode, proučavanjem tržišne ekonomije i srodnih relevantnih fenomena.'
    ],
    'medium' => [
        '1' => 'Tokom osam godina rada na ovom polju, postajemo prepoznati kao neko ko uporno i posvećeno okuplja
                    najkvalitetnije mlade ljude u Srbiji.',
        '2' => 'Naš najvažniji obrazovni program je Akademija liberalne politike (ALP) koju u 2017. godini pohađa
                    šesta generacija polaznika.',
        '3' => ' Misiju na polju obrazovanja počeli smo sa programom «Politika pojedinca». Do sada smo održali 15
                    seminara ovog programa u više gradova u Srbiji.',
        '4' => 'Osim ovoga radimo na širenju obrazovnih aktivnosti kako bismo ponudili veći izbor na polju
                    unapređenja praktičnih veština važnih za savremeno društvo.'
    ],
    'titles' => [
        'alp' => 'AKADEMIJA LIBERALNE POLITIKE',
        'john_galt' => 'John Galt',
        'pop' => 'POP EKONOMIJA',
        'firm_policy' => 'POLITIKA POJEDINCA'
    ],
    'content' =>[
        'alp' => 'Obrazovanje za slobodu za studente',
        'john_galt' => 'Uvod u filozofiju Objektivizma',
        'pop' => 'Serijal klipova za bolje razumevanje ekonomije',
        'firm_policy' => 'Program za srednjoškolce'
    ]
];