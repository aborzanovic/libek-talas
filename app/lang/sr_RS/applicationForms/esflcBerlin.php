<?php

return [

	'applicationTitle' => 'Prijava :title',

	'email' => [
		'subject' => 'Prijava: :title [:random]',
		'to' => [
			'address' => 'rnikolic@studentsforliberty.org',
			'name' => 'Ratko Nikolić',
		],
	],

	'titles' => [
		'personalInfo' => 'Lični podaci',
		'contactInfo' => 'Kontakt informacije',
		'education' => '(Ne)formalno obrazovanje',
		'other' => 'Ostalo',
	],

	'labels' => [
		'fullName' => 'Ime i prezime',
		'birthdate' => 'Datum rođenja',

		'email' => 'Email adresa',
		'phone' => 'Broj telefona',

		'faculty' => 'Koji fakultet studirate?',
		'memberships' => 'Članstvo u studentskom parlamentu, nekoj organizaciji ili političkoj stranci?',
		'libekHistory' => 'Da li ste do sada učestvovali na nekoj od naših aktivnosti?',

		'reference' => 'Kako ste saznali za ESFL?',
		'motivation' => 'Šta Vas je motivisalo da se prijavite za putovanje na ovogodišnju ESFL konferenciju u Berlinu?',
		'future' => 'Kako ćete iskoristiti znanje i kontakte stečene na istoj?',

		'send' => [
			'default' => 'Pošalji',
			'loading' => 'Slanje u toku...',
		]
	],

	'errors' => [
		'fullName' => 'Morate uneti svoje ime i prezime',
		'email' => 'Morate uneti svoju email adresu',
	],

	'success' => 'Uspešno ste se prijavili za program ":title". Rezultate selekcije ćete dobiti nakon isteka roka za prijave na email adresu koju ste ostavili u prijavi. Za sva dodatna pitanja kontaktirajte nas na <a href="mailto:rnikolic@studentsforliberty.org">rnikolic@studentsforliberty.org</a>. Hvala Vam na interesovanju!',

	'notes' => [
		'problems' => '<strong>NAPOMENA:</strong> Ukoliko imate bilo kakvih problema sa slanjem prijave, slobodno nam pišite na <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>, a mi ćemo Vam u najkraćem mogućem roku odgovoriti, kako bismo rešili problem.</h3>',
		'privacy' => '<strong>PRIVATNOST:</strong> Radi zaštite privatnosti kandidata, prikupljeni podaci neće nikada biti prosleđivani trećim licima, niti korišćeni u bilo koju svrhu osim izbora učesnika za seminar. Kontakt informacije će biti korišćene isključivo za komunikaciju sa učesnicima.</h3>',
	],

	'expiredNote' => 'Nažalost, rok za prijavu za ovaj konkurs je istekao. Ukoliko imate dodatnih pitanja, javite nam se na <a href="mailto:rnikolic@studentsforliberty.org">rnikolic@studentsforliberty.org</a>. Hvala Vam na razumevanju.',

];
