<?php

return [

	'applicationTitle' => 'Prijava :title',

	'email' => [
		'subject' => 'Prijava: :title [:random]',
		'to' => [
			'address' => 'prijava.john.galt@libek.org.rs',
			'name' => 'Libertarijanski Klub Libek',
		],
	],

	'titles' => [
		'personalInfo' => 'Lični podaci',
		'contactInfo' => 'Kontakt informacije',
		'education' => 'Obrazovanje',
		'personalAttitude' => 'Lični stav',
		'other' => 'Ostalo',
	],

	'labels' => [
		'fullName' => 'Ime i prezime',
		'birthdate' => 'Datum rođenja',
		'sex' => 'Pol',
		'cv' => 'Tvoj CV',

		'email' => 'Email adresa',
		'phone' => 'Broj telefona',

		'faculty' => 'Škola/Fakultet i smer',

		'motivation' => 'Koja je tvoja glavna motivacija za prijavu za seminar? (max 250 karaktera)',
		'future' => 'Kako misliš da ćeš primeniti iskustvo sa seminara u svom daljem usavršavanju? (max 500 karaktera)',
		'ego' => 'Koje osobine misliš da su baš specifično tvoje, koje mogu da te razlikuju od ostalih kandidata?',
		'quote' => 'Daj nam komentar na ovaj citat: „Kada se ne slažem sa racionalnim čovekom, puštam realnost da bude krajnji sudija; ako sam u pravu, on će naučiti; ako ja nisam u pravu, ja ću naučiti; neko od nas će pobediti u raspravi, ali oboje ćemo profitirati.“',

		'reference' => 'Kako si saznao/la za seminar?',
		'expectations' => 'Tri stvari koje očekuješ da dobiješ pohađanjem ovog seminara?',
		'needs' => 'Imaš li neke specijalne potrebe? (ishrana, invaliditet...)',

		'send' => [
			'default' => 'Pošalji',
			'loading' => 'Slanje u toku...',
		]
	],

	'cvInAttachment' => 'CV se nalazi u attachment-u.',

	'sexes' => [
		'm' => 'M',
		'f' => 'Ž',
	],

	'errors' => [
		'fullName' => 'Moraš uneti svoje ime i prezime',
		'email' => 'Moraš uneti svoju email adresu',
		'cv' => 'Moraš dodati validan fajl. Dozvoljene vrste fajlova su: <code>pdf</code>, <code>doc</code>, <code>docx</code>, i <code>odt</code>. Maksimalna dozvoljena veličina fajla je <code>10MB</code>.',
	],

	'success' => 'Uspešno si se prijavio/la za program ":title". Rezultate selekcije ćeš dobiti na email adresu nakon isteka roka za prijave. Za sva dodatna pitanja kontaktiraj nas na <a href="mailto:prijava.john.galt@libek.org.rs">prijava.john.galt@libek.org.rs</a>. Hvala ti na interesovanju!',

	'notes' => [
		'problems' => '<strong>NAPOMENA:</strong> Ukoliko imaš bilo kakvih problema sa slanjem prijave, slobodno nam piši na <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>, a mi ćemo ti u najkraćem mogućem roku odgovoriti, kako bismo rešili problem.</h3>',
		'privacy' => '<strong>PRIVATNOST:</strong> Radi zaštite privatnosti kandidata, prikupljeni podaci neće nikada biti prosleđivani trećim licima, niti korišćeni u bilo koju svrhu osim izbora učesnika za seminar. Kontakt informacije će biti korišćene isključivo za komunikaciju sa učesnicima.</h3>',
	],

	'expiredNote' => 'Nažalost, rok za prijavu za ovaj konkurs je istekao. Ukoliko imaš dodatnih pitanja, javi nam se na <a href="mailto:prijava.john.galt@libek.org.rs">prijava.john.galt@libek.org.rs</a>. Hvala na razumevanju.',

];
