<?php

return [

	'applicationTitle' => 'Prijava :title',

	'email' => [
		'subject' => 'Prijava: :title [:random]',
		'to' => [
			'address' => 'prijava@libek.org.rs',
			'name' => 'Talas',
		],
	],

	'titles' => [
		'personalInfo' => 'Lični podaci',
		'contactInfo' => 'Kontakt informacije',
		'education' => '(Ne)formalno obrazovanje',
		'personalAttitude' => 'Lični stav',
		'other' => 'Ostalo',
	],

	'labels' => [
		'fullName' => 'Ime i prezime',
		'birthdate' => 'Datum rođenja',
		'sex' => 'Pol',

		'email' => 'Email adresa',
		'phone' => 'Broj telefona',

		'school' => 'Srednja škola i smer',
		'history' => 'Da li ste do sada pohađali neke kurseve, seminare, radionice, konferencije i ako jeste, koje?',
		'memberships' => 'Članstvo u đačkom parlamentu, nekoj organizaciji ili političkoj stranci?',
		'faculty' => 'Koji fakultet studirate ili želite da upišete? Zašto?',

		'problems' => 'Koji su po Vama glavni problemi sa kojima se suočava naše društvo?',
		'solutions' => 'Na koji način mislite da Vi možete da doprinesete rešavanju tih problema?',
		'ego' => 'Za koje svoje osobine smatrate da Vas čine dobrim kandidatom?',

		'expectations' => 'Koja su Vaša očekivanja od seminara "Politika pojedinca"?',
		'reference' => 'Kako ste saznali za ovaj seminar?',
		'needs' => 'Imate li neke specijalne potrebe? (ishrana, invaliditet...)',

		'send' => [
			'default' => 'Pošalji',
			'loading' => 'Slanje u toku...',
		]
	],

	'sexes' => [
		'm' => 'M',
		'f' => 'Ž',
	],

	'errors' => [
		'fullName' => 'Morate uneti svoje ime i prezime',
		'email' => 'Morate uneti svoju email adresu',
	],

	'success' => 'Uspešno ste se prijavili za program ":title". Rezultate selekcije ćete dobiti nakon isteka roka za prijave na email adresu koju ste ostavili u prijavi. Za sva dodatna pitanja kontaktirajte nas na <a href="mailto:kontakt@libek.org.rs">kontakt@libek.org.rs</a>. Hvala Vam na interesovanju!',

	'notes' => [
		'problems' => '<strong>NAPOMENA:</strong> Ukoliko imate bilo kakvih problema sa slanjem prijave, slobodno nam pišite na <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>, a mi ćemo Vam u najkraćem mogućem roku odgovoriti, kako bismo rešili problem.</h3>',
		'privacy' => '<strong>PRIVATNOST:</strong> Radi zaštite privatnosti kandidata, prikupljeni podaci neće nikada biti prosleđivani trećim licima, niti korišćeni u bilo koju svrhu osim izbora učesnika za seminar. Kontakt informacije će biti korišćene isključivo za komunikaciju sa učesnicima.</h3>',
	],

	'expiredNote' => 'Nažalost, rok za prijavu za ovaj konkurs je istekao. Ukoliko imate dodatnih pitanja, javite nam se na <a href="mailto:kontakt@libek.org.rs">kontakt@libek.org.rs</a>. Hvala Vam na razumevanju.',

];
