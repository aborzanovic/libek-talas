<?php

return [

	'email' => [
		'subject' => 'Prijava volontera - Odbrani novčanik 2014 [:random]',
		'to' => [
			'address' => 'kontakt@libek.org.rs',
			'name' => 'Talas',
		],
	],

	'labels' => [
		'fullName' => 'Ime i prezime',
		'email' => 'Email adresa',
		'message' => 'Na koji način želiš da se uključiš (ulične akcije, kreativni tim, logistika, nešto drugo..)?',
		'send' => [
			'default' => 'Pošalji',
			'loading' => 'Slanje u toku...',
		],
	],

	'errors' => [
		'fullName' => 'Morate uneti ime i prezime.',
		'email' => 'Morate uneti ispravnu email adresu.',
	],

	'motivational' => 'Misliš da država ne troši odgovorno novac građana? Veruješ da nisi dužan da porezom plaćaš njihovo rasipanje? Nećeš da ćutiš o tome? Priključi se organizacionom timu Marša poreskih obveznika i pomozi da se poruka "ODBRANI NOVČANIK" čuje dalje i snažnije!',

	'privacy' => '<strong>Privatnost:</strong> Radi zaštite privatnosti, Vaši podaci neće nikada biti prosleđivani trećim licima (osim <a href="http://mailchimp.com/" target="_blank">MailChimp</a> platforme koju koristimo za slanje newslettera), niti korišćeni u bilo koju svrhu osim komunikacije sa Vama.',

	'success' => 'Uspešno ste se prijavili kao volonter. Uskoro će Vam stići email u kojem treba da potvrdite svoju prijavu. Hvala Vam za interesovanje, kontaktiraćemo Vas uskoro sa više informacija.',

];
