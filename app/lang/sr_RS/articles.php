<?php
return [
	'news' => 'vesti',
	'research' => 'istraživanja',
	'projects' => 'projekti',
    'blog' => 'blog',
    'highlights' => 'POPULARNO',
    'recommended' => 'IZDVAJAMO',
    'read_more' => 'ČITAJ JOŠ'
];
?>