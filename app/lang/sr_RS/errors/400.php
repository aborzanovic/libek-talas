<?php

return array(
	'title' => '400 - Talas',
	'h1' => 'Talas',
	'h2' => 'Greška 400: Loš Zahtev',
	'text' => array(
		'Žao nam je, ali zahtev koji ste poslali nije validan.',
		'Molimo Vas, koristite samo linkove u okviru sajta kako biste našli željeni sadržaj. Ako Vas je ovde doveo upravo link sa našeg sajta, bili bismo Vam veoma zahvalni ako obavestite administratora o ovome, slanjem emaila na adresu <a href="mailto:admin@talas.rs">admin@talas.rs</a>.',
		'Ako kliknete <a href="/sr">ovde</a>, vratićete se na početnu stranicu sajta.',
	),
);
