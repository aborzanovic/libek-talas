<?php

return array(
	'title' => '500 - Talas',
	'h1' => 'Talas',
	'h2' => 'Greška 500: Interna Serverska Greška',
	'text' => array(
		'Žao nam je, ali nešto je pošlo naopako i administrator je obavešten o ovome.',
		'Molimo Vas, koristite samo linkove u okviru sajta kako biste našli željeni sadržaj. Ako Vas je ovde doveo upravo link sa našeg sajta, bili bismo Vam veoma zahvalni ako obavestite administratora o ovome, slanjem emaila na adresu <a href="mailto:admin@talas.rs">admin@talas.rs</a>.',
		'Ako kliknete <a href="/sr">ovde</a>, vratićete se na početnu stranicu sajta.',
	),
);
