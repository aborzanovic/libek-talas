<?php

$applicationForms = [];

foreach (File::allFiles(__DIR__.'/applicationForms') as $file)
{
	$applicationForms[basename($file, '.php')] = File::getRequire($file);
}

return $applicationForms;
