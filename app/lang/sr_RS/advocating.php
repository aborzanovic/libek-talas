<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/13/2016
 * Time: 7:34 PM
 */

return [
    'content1' => 'Libek se od svog osnivanja zalaže za principe zdrave tržišne privrede, doslednu primenu vladavine prava i strogo ograničenu državu koja se ne meša u privredni život građana. ',
    'content2' => 'Organizujući veliki broj konferencija i tribina podstičemo razgovor o suštinskim izazovima trošenja našeg zajedničkog novca i odgovornosti države prema građanima. ',
    'content3' => 'Zbog toga je moto našeg javnog zagovaranja – Odbrani novčanik! Zbog toga svake godine organizujemo Marš poreskih obveznika koji smo pokrenuli 2013. godine.  ',
    'content4' => 'Sa sve većim brojem partnera, udruženja i pojedinaca stvaramo novu preduzetničku kulturu u Srbiji. Zajedno vršimo pritisak na donosioce političkih odluka da snize poreze, da ukinu bespotrebnu regulaciju i da obuzdaju rast javnog duga Srbije. '

];