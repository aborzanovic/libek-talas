<?php

return [

	'title' => 'Admin panel',

	'labels' => [
		'email' => 'Email',
		'password' => 'Lozinka',
		'rememberMe' => 'Zapamti me',
		'send' => [
			'default' => 'Prijava',
			'loading' => 'Prijavljivanje u toku...',
		],
	],

	'placeholders' => [
		'email' => 'admin@example.com',
	],

	'errors' => [
		'email' => [
			'notActivated' => 'Ovaj nalog još nije aktiviran.',
			'suspended' => 'Pristup nalogu Vam je privremeno suspendovan, zbog previše uzastopnih neuspešnih pokušaja prijave. Molimo Vas pokušajte ponovo malo kasnije.',
			'unregistered' => 'Ovaj nalog ne postoji, ili je lozinka pogrešna.',
		],
	],

];
