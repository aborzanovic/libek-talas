<?php

return array(

	'home' => 'POČETNA',
    'education'=> 'Obrazovanje',
	'team' => 'TIM',
	'news' => 'VESTI',
    'advocating' => 'Zagovaranje',
	'research' => 'Istraživanja',
	'projects' => 'Projekti',
	'gallery' => 'Galerija',
	'lecturers' => 'PREDAVAČI',
	'mentors' => 'MENTORI',
	'alumni' => 'ALUMNISTI',
    'alp' => 'ALP',
    'john-galt' => "JOHN GALT",
	'contact' => 'KONTAKT',
    'support' => 'PODRŠKA',
    'application' => 'PRIJAVA',
    'blog' => 'BLOG',
    'Q&A' =>  'Q&A',
    'works' => 'DELA',
    'individual-policy' => 'Politika pojedinca',
	'subscriptions' => 'Prijave',
	'applicationForms' => 'Aplikacije',
    'backToLibek' => '&laquo; Natrag na Libek',
    'newsletter' => 'Prijavite se'

);
















