<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/11/2016
 * Time: 5:21 PM
 */

return [
    'generation' => 'generation',
    'generations' => 'generations',
	'lecturers' => 'lecturers',
	'mentors' => 'mentors'
];