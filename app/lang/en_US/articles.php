<?php
return [
	'news' => 'news',
	'research' => 'research',
	'projects' => 'project',
    'blog' => 'blog',
    'highlights' => 'HIGHLIGHTS',
    'recommended' => 'RECOMMENDED',
    'read_more' => 'READ MORE'
];
?>