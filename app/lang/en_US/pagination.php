<?php

return array(

	'first' => '« First',
	'previous' => '‹ Previous',
	'next' => 'Next ›',
	'last' => 'Last »',
	'urlSegment' => 'page',

);
