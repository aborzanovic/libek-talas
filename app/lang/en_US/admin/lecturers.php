<?php

return [

	'module' => 'Lecturers',

	'titles' => [
		'index' => 'Lecturers',
		'create' => 'Create a new lecturer',
		'edit' => 'Edit',
		'delete' => 'Delete',
	],

	'index' => [
		'links' => [
			'create' => 'Create a new lecturer',
			'edit' => 'Edit',
			'delete' => 'Delete',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Basic Configuration',
		'content' => 'Content',
		'seo' => 'Search Engine Optimization',
	],

	'labels' => [
		'name' => 'First name',
		'surname' => 'Last name',
		'title_sr' => 'Title (serbian)',
		'title_en' => 'Title (english)',
		'createdAt' => 'Created',
		'image' => 'Image',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Save',
			'loading' => 'Saving...',
		],
	],

	'help' => [
		'image' => ''
	],

	'confirmDelete' => [
		'message' => 'Are you sure you want to delete this lecturer?',
		'confirm' => [
			'default' => 'Confirm',
			'loading' => 'Deleting...',
		],
		'cancel' => [
			'default' => 'Cancel',
			'loading' => 'Cancelling...',
		],
	],

	'successMessages' => [
		'create' => 'You have successfully created a new lecturer.',
		'edit' => 'You have successfully updated this lecturer.',
		'delete' => 'You have successfully deleted the lecturer.',
	],

];
