<?php

return [

	'home' => 'Home',
	'slideshowImages' => 'Slideshow images',
	'newsArticles' => 'News articles',
	'generationsArticles' => 'Generations',
	'johnGaltArticles' => 'John Galt articles',
	'researchArticles' => 'Research articles',
	'teamMembers' => 'Team members',
	'mentors' => 'Mentors',
	'lecturers' => 'Lecturers',
	'alumni' => 'Alumni',
	'users' => 'Users',
	'changePassword' => 'Change password',
	'backToWebsite' => 'Back to website',

];
