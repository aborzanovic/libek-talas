<?php

return [

	'module' => 'Alumni',

	'titles' => [
		'index' => 'Alumni',
		'create' => 'Create a new alumnus',
		'edit' => 'Edit',
		'delete' => 'Delete',
	],

	'index' => [
		'links' => [
			'create' => 'Create a new alumnus',
			'edit' => 'Edit',
			'delete' => 'Delete',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Basic Configuration',
		'content' => 'Content',
		'seo' => 'Search Engine Optimization',
	],

	'labels' => [
		'name' => 'First name',
		'surname' => 'Last name',
		'title_sr' => 'Education (serbian)',
		'title_en' => 'Education (english)',
		'generation' => 'Generation (year)',
		'createdAt' => 'Created',
		'image' => 'Image',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Save',
			'loading' => 'Saving...',
		],
	],

	'help' => [
		'image' => ''
	],

	'confirmDelete' => [
		'message' => 'Are you sure you want to delete this alumnus?',
		'confirm' => [
			'default' => 'Confirm',
			'loading' => 'Deleting...',
		],
		'cancel' => [
			'default' => 'Cancel',
			'loading' => 'Cancelling...',
		],
	],

	'successMessages' => [
		'create' => 'You have successfully created a new alumnus.',
		'edit' => 'You have successfully updated this alumnus.',
		'delete' => 'You have successfully deleted the alumnus.',
	],

];
