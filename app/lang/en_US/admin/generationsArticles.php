<?php

return [

	'module' => 'Generations',

	'titles' => [
		'index' => 'Generations',
		'create' => 'Create a new generation',
		'edit' => 'Edit',
		'delete' => 'Delete',
	],

	'index' => [
		'links' => [
			'create' => 'Create a new generation',
			'edit' => 'Edit',
			'delete' => 'Delete',
		],
	],

	'panelTitles' => [
		'basicConfiguration' => 'Basic Configuration',
		'content' => 'Content',
		'seo' => 'Search Engine Optimization',
	],

	'labels' => [
		'language' => 'Language',
		'title' => 'Title',
		'slug' => 'Slug',
		'createdAt' => 'Created',
		'publishedAt' => 'Published',
		'sticky' => 'Sticky index',
		'lead' => 'Lead',
		'contents' => 'Contents',
		'imageMain' => 'Image',
		'metaTitle' => 'Meta title',
		'metaDescription' => 'Meta description',
		'notAvailable' => 'N/A',
		'save' => [
			'default' => 'Save',
			'loading' => 'Saving...',
		],
	],

	'help' => [
		'slug' => 'We recommend leaving this field blank - in that case, the system will automatically generate the slug based on the title.',
		'publishedAt' => 'The date/time must be entered in the following format: <code>MM/DD/YYYY hh:mm</code>. If you do not wish to publish the article yet, leave this field empty.',
		'sticky' => 'A number which overrides the default article sorting by date/time on the website front-end. Articles with a larger sticky index are displayed first. Leave at <code>0</code> if you do not wish to make this article sticky.',
		'imageMain' => 'Please upload an image with the dimensions at least <code>960×543px</code>.',
		'metaTitle' => 'Recommended length up to 60 characters. You may leave this empty, in which case the article title will be used by default.',
		'metaDescription' => 'Recommended length up to 160 characters. You may leave this empty, in which case an excerpt from the article contents will be used by default.',
	],

	'confirmDelete' => [
		'message' => 'Are you sure you want to delete this article?',
		'confirm' => [
			'default' => 'Confirm',
			'loading' => 'Deleting...',
		],
		'cancel' => [
			'default' => 'Cancel',
			'loading' => 'Cancelling...',
		],
	],

	'successMessages' => [
		'create' => 'You have successfully created a new generation.',
		'edit' => 'You have successfully updated this generation.',
		'delete' => 'You have successfully deleted the generation ":title".',
	],

];
