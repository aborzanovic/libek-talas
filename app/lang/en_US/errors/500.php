<?php

return array(
	'title' => '500 - Libertarian Club Libek',
	'h1' => 'Libertarian Club Libek',
	'h2' => 'Error 500: Internal Server Error',
	'text' => array(
		'We\'re very sorry, but something went wrong on the server, and the administrator has been notified about it.',
		'Please, only use links within the site to find the desired content. If the link from our site brought you here, we would be very grateful if you notify the administrator about this, by sending an e-mail to <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>. Thank you for your patience.',
		'If you click <a href="/en">here</a>, you will be redirected to the home page.',
	),
);
