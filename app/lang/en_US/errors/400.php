<?php

return array(
	'title' => '400 - Libertarian Club Libek',
	'h1' => 'Libertarian Club Libek',
	'h2' => 'Error 400: Bad Request',
	'text' => array(
		'We\'re very sorry, but the request you have made is invalid.',
		'Please, only use links within the site to find the desired content. If the link from our site brought you here, we would be very grateful if you notify the administrator about this, by sending an e-mail to <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>. Thank you for your patience.',
		'If you click <a href="/en">here</a>, you will be redirected to the home page.',
	),
);
