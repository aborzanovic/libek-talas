<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/11/2016
 * Time: 5:21 PM
 */

return [
    'ayn-rand' => [
        'about' => 'ABOUT AYN RAND',
        'title' => 'Ayn Rand was born in 1905. in Sankt Petersburg in a Jewish family, as Alisa Rosenbaum.',
        'content' => 'She emigrated to the United States in 1926, after the revolutionary regime confiscated most of her family\'s property. After moving to America, she worked as a script writer in Hollywood. Her first novel, "We the Living" was published in 1936. Her most famous works are "The Fountainhead", published in 1943, and "Atlas Shrugged", which was published in 1957. These novels are based on Ayn Rand\'s philosophy, Objectivism. Besides fiction, Ayn Rand wrote book and essays about various philosophical and political topics. The non - fiction books include "The Virtue of Selfishness", "Philosophy, who needs it", "Capitalism: The Unknown Ideal", and many other books, letters and publications. She is one of the most prominent American authors and has had an immense influence on American political culture through her promotion of the principles of the laissez faire free market capitalism.',
        'details' => 'Read more &raquo;',
        'read-less' => 'Read less &laquo;'
    ],
    'history' => [
        'title' => 'Historical context',
        'content' => 'The historical context is very important for understanding of ideas and life path of Ayn Rand. As someone who was born before the revolution in Russia, she was able to witness all the negative consequences of the revolution and was in the position to defend the ideas of freedom and capitalism, in a time where it seemed that the world was turning towards collectivism. Before the cold war became the main factor in the way international relations operated, she decisively chose the United States and the American way of life. She was convinced that freedom was fading even in the United States and that people who believe in freedom needed to make an effort to preserve its foundations and promote the ideas of liberty.'
    ],
    'quotes' => [
        'content' => '"As other Libek\'s programs I have taken part in, the John Galt School was a phenomenal experience. I enjoyed the fact that the opinions were split on some issues, and that debate was constructive, with this difference in opinions making it more interesting. I especially liked Tara Smith\'s lecture called "Capitalism: Justice, Productivity and Money". I recommend everyone to apply for the Public Policy Academy, where the John Galt School is one of sub program.',
        'alumna' => 'alumna'
        /*'quote1' => 'Lorem ipsum dolor sit amet, ut fabulas delectus vel, sit dolore intellegebat id. Quem admodum corpora mea ad,
        id his nihil quodsi qualisque',
        'quote2' => 'Lorem ipsum dolor sit amet, ut fabulas delectus vel, sit dolore intellegebat id. Quem admodum corpora mea ad,
        id his nihil quodsi qualisque',
        'quote3' => 'Lorem ipsum dolor sit amet, ut fabulas delectus vel, sit dolore intellegebat id. Quem admodum corpora mea ad,
        id his nihil quodsi qualisque',
        'quote4' => 'Lorem ipsum dolor sit amet, ut fabulas delectus vel, sit dolore intellegebat id. Quem admodum corpora mea ad,
        id his nihil quodsi qualisque',
        'quote5' => 'Lorem ipsum dolor sit amet, ut fabulas delectus vel, sit dolore intellegebat id. Quem admodum corpora mea ad,
        id his nihil quodsi qualisque'*/
    ]
];