<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/14/2016
 * Time: 2:56 AM
 */

return [
    'content1' => 'The „Politics of the Individual“ seminars introduce high school students to the foundations of classical liberalism and libertarian philosophy. Special focus is on practical application of libertarian philosophy and solutions which libertarians offer to various social, political and economic questions.',
    'content2' => 'The seminars are interactive and consist of lectures and workshops which take place over two days, usually on weekends. So far, Libek has held 14 seminars for around 300 high school students and young poltical party leaders.',
    'content3' => 'The cooperation with high school students then continues through the <a href="alp"> Public Policy Academy </a>'
];