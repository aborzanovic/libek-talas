<?php

return array(

	'aboutUs' => 'about-us',
	'news' => 'news',
    'team' => 'team',
    'education' => 'education',
	'research' => 'research',
	'projects' => 'projects',
	'contact' => 'contact',
    'blog' => 'blog',
    'Q&A' =>  'Q&A',
    'works' => 'works',
    'advocating' => 'advocacy',
    'johnGalt' => 'john-galt',
	'alp' => 'alp',
    'support' => 'support',
	'lecturers' => 'lecturers',
    'individual-policy' => 'politics-of-the-individual',
	'mentors' => 'mentors',
	'alumni' => 'alumni',
	'executiveBoard' => 'executive-board',
	'communicationAndProtocol' => 'communication-and-protocol',
	'researchDepartment' => 'research-department',
	'subscriptions' => 'subscriptions',
	'applicationForms' => 'application-forms',
    'newsletter' => 'subscribe',
    'thanks' => 'thanks-for-following'

);
