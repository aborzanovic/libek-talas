<?php

return array(

	'siteName' => 'Libertarian Club Libek',
	'homeTagline' => '',
	'metaDescription' => 'Libertarian Club Libek',
	'noscriptWarning' => 'Oops - it seems your browser is blocking scripts from executing on this website! Most things should work fine without scripts, but we recommend you enable them for maximum performance.',
	'dateFormat' => 'm/d/Y',
	'dateTimeFormat' => 'm/d/Y H:i',
	'noArticles' => 'We\'re sorry, but we don\'t have any articles here yet. We promise to add them soon, so stay tuned!',
	'adminPanel' => 'Admin panel',
	'decimalSeparator' => '.',
	'thousandsSeparator' => ',',

);
