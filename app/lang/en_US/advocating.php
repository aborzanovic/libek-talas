<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/13/2016
 * Time: 7:34 PM
 */

return [
    'content1' => 'Libek promotes the principles of a healthy free market economy, consistent rule of law and limited government that does not interfere with the economy or individual liberty.',
    'content2' => 'We stimulate and drive the debate on the biggest challenges of public spending and accountability of the state to citizens. Thousands of attendees have attended our conferences, panels and street events.',
    'content3' => 'The motto of public advocacy activities of Libek is "Defend your wallet!", which is heard at the Taxpayers Marches we have been setting up since 2013',
    'content4' => 'We build partnerships with various stakeholders, such as organizations and individuals and work to create a new entrepreneurship culture in Serbia. Working with a variety of partners, we advocate for lower taxes, simple regulation and a balanced budget.'

];