<?php

return [
	'labels' => [
		'fullName' => 'Full name',
		'email' => 'Email address',
		'message' => 'Message',
		'send' => [
			'default' => 'Send',
			'loading' => 'Sending...',
		],
	],
	'errors' => [
		'fullName' => 'You must enter your full name.',
		'email' => 'You must enter a valid email address.',
		'message' => 'You must enter a message.',
	],
	'privacy' => '<strong>Privacy:</strong> In order to protect your privacy, we guarantee that your personal data will never be made available to third-parties, or used for any purpose other than communicating with you.',
	'success' => 'Your message has been sent successfully. Thanks for writing, we\'ll make sure to reply as soon as possible.',
	'subject' => 'Contact',
];
