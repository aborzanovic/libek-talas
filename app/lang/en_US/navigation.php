<?php

return array(


	'home' => 'HOME',
    'education' => 'Education',
	'team' => 'TEAM',
	'news' => 'NEWS',
	'research' => 'Research',
	'projects' => 'Projects',
	'gallery' => 'Gallery',
	'lecturers' => 'LECTURERS',
	'mentors' => 'MENTORS',
    'advocating' => 'ADVOCACY',
    'alp' => 'ALP',
    'john-galt' => "JOHN GALT",
	'alumni' => 'ALUMNI',
	'contact' => 'CONTACT',
    'support' => 'SUPPORT',
    'application' => 'APPLY',
    'blog' => 'BLOG',
    'Q&A' =>  'Q&A',
    'works' => 'WORKS',
    'individual-policy' => 'Politics of the Individual',
	'subscriptions' => 'Subscriptions',
	'applicationForms' => 'Application forms',
    'backToLibek' => '&laquo; Back to Libek',
    'newsletter' => 'Subscribe'

);
