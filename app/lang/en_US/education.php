<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/2/2016
 * Time: 1:46 PM
 */

return [
    'tiny' =>    'Education',
    'large' => [
        '1' => 'Education is the first pillar of Libek’s activities. We have education and training programs for high school students, university students and activists.',
        '2' =>  'As a leading educational center in the region, Libek provides the program participants with an environment for comprehensive discussion of the philosophy of freedom, market economy and related phenomena.'
    ],
    'medium' => [
        '1' => 'Over the eight years of work, we are increasingly recognized as an organization which brings together some of the most talented young people in Serbia.',
        '2' => 'Our flagship educational program is the Public Policy Academy, which is now attended by the fifth generation of students.',
        '3' => 'So far, Libek has organized 15 seminars for high school students in various cities in Serbia.',
        '4' => 'We have a network of over 300 alumni, and thousands of people have attended our conferences and other short events.'
    ],
    'titles' => [
        'alp' => 'PUBLIC POLICY ACADEMY',
        'john_galt' => 'JOHN GALT SCHOOL',
        'pop' => 'POP ECONOMICS',
        'firm_policy' => 'POLITICS OF THE INDIVIDUAL'
    ],
    'content' =>[
        'alp' => 'Education for freedom for students',
        'john_galt' => 'Introduction to the philosophy of Objectivism',
        'pop' => 'Short clips for understanding of economics',
        'firm_policy' => 'Program for high school students'
    ]
];

