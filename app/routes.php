<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 * Include our route patterns.
 */

require_once 'route-patterns.php';


/*
 * Resolve the language automatically before defining routes.
 */

$currentLanguage = LocaleResolver::getLanguage();

/*
 * Here are the actual route bindings. The route translations are found in the
 * relevant language files.
 */

Route::group(['before' => ['forceSsl'], 'namespace' => 'Libek\LibekOrgRs\Http\Controllers'], function() use ($currentLanguage)
{
	/*
	 * The default language redirect controller.
	 */
	Route::get('', 'Front\LanguageRedirectController@getRedirect');

	Route::post('newsletter', 'Front\NewsletterController@subscribe');

	/*
	 * Language specific routes for the website front-end.
	 */
	Route::group(['prefix' => $currentLanguage, 'namespace' => 'Front'], function() use ($currentLanguage)
	{
		/*
		 * Home page.
		 */

		Route::get('', 'HomeController@getIndex');

		Route::get('o-nama', 'AboutUsController@getIndex');		

        Route::group(['prefix' => Lang::get('routes.newsletter')], function()
        {
            Route::get('', 'NewsletterController@getIndex');

        });

		/*
		 * News article listing and single pages.
		 */
		Route::group(['prefix' => Lang::get('routes.news')], function()
		{
			Route::get('', 'NewsArticleController@getIndex');
			Route::get('tag/{tag}', 'NewsArticleController@getByTag');
			Route::get('tag/{tag}/{page}', 'NewsArticleController@getByTagAjax');
			Route::get('author/{author}', 'NewsArticleController@getByAuthor');
			Route::get('author/{author}/{page}', 'NewsArticleController@getByAuthorAjax');
			Route::get('pretraga', 'NewsArticleController@getBySearch');
			Route::get('pretraga/{page}', 'NewsArticleController@getBySearchAjax');
			Route::get('{page}', 'NewsArticleController@getPageAjax');
			Route::get('{year}/{month}/{day}/{slug}', 'NewsArticleController@getArticle');

			Route::get('feed/rss', 'NewsArticleController@getRssFeed');
		});

	});

	/*
	 * Admin panel login route.
	 */
	Route::group(['prefix' => 'admin', 'before' => 'guest', 'namespace' => 'Admin'], function()
	{
		Route::get('login', 'LoginController@getLogin');
		Route::post('login', 'LoginController@postLogin');
	});

	/*
	 * Admin panel routes.
	 */
	Route::group(['prefix' => 'admin', 'before' => ['auth', 'permissions'], 'namespace' => 'Admin'], function()
	{
		/*
		 * The admin panel home page.
		 */
		Route::get('', 'HomeController@index');

		/*
		 * News articles.
		 */
		Route::group(['prefix' => 'news-articles'], function()
		{
			Route::get('', 'NewsArticleController@index');

			Route::get('create', 'NewsArticleController@create');
			Route::post('', 'NewsArticleController@store');

			Route::get('{newsArticle}/edit', 'NewsArticleController@edit');
			Route::put('{newsArticle}', 'NewsArticleController@update');

			Route::get('{newsArticle}/delete', 'NewsArticleController@confirmDelete');
			Route::delete('{newsArticle}', 'NewsArticleController@delete');
		});

		/*
		 * User administration.
		 */
		Route::group(['prefix' => 'users'], function()
		{
			Route::get('', 'UserController@index');

			Route::get('create', 'UserController@create');
			Route::post('', 'UserController@store');

			Route::get('{user}/edit', 'UserController@edit');
			Route::put('{user}', 'UserController@update');

			Route::get('{user}/delete', 'UserController@confirmDelete');
			Route::delete('{user}', 'UserController@delete');
		});

		/*
		 * Redactor uploads.
		 */
		Route::group(['prefix' => 'redactor', 'namespace' => 'Redactor'], function()
		{
			Route::post('images', 'ImageController@store');
			Route::post('files', 'FileController@store');
		});

		/*
		 * The route for changing the password.
		 */
		Route::get('change-password', 'PasswordController@index');
		Route::post('change-password', 'PasswordController@postUpdate');

		/*
		 * Logout route.
		 */
		Route::post('logout', 'LoginController@postLogout');
	});
});
