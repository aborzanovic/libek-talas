<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdFieldToNewsArticles extends Migration {

	public function up()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->dropColumn('user_id');
		});
	}

}
