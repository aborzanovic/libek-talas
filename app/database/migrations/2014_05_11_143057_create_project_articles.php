<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectArticles extends Migration {

	public function up()
	{
		Schema::create('project_articles', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('language');

			$table->string('title');
			$table->string('slug');

			$table->text('contents');

			$table->string('meta_title')->nullable();
			$table->string('meta_description')->nullable();

			$table->string('image_main_full')->nullable();
			$table->string('image_main_cover')->nullable();
			$table->string('image_main_thumbnail')->nullable();
			$table->string('image_main_og')->nullable();

			$table->timestamp('published_at')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->engine = 'InnoDB';
		});
	}

	public function down()
	{
		Schema::drop('project_articles');
	}

}
