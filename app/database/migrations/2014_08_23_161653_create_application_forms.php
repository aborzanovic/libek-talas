<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationForms extends Migration {

	public function up()
	{
		Schema::create('application_forms', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('type');
			$table->string('language');

			$table->string('title');
			$table->string('slug');

			$table->timestamp('deadline');
			$table->timestamp('published_at')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->engine = 'InnoDB';
		});
	}

	public function down()
	{
		Schema::drop('application_forms');
	}

}
