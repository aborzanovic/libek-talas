<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResearchArticlesAddSticky extends Migration {

	public function up()
	{
		Schema::table('research_articles', function(Blueprint $table)
		{
			$table->integer('sticky')->after('image_main_og')->default(0);
		});
	}

	public function down()
	{
		Schema::table('research_articles', function(Blueprint $table)
		{
			$table->dropColumn('sticky');
		});
	}

}
