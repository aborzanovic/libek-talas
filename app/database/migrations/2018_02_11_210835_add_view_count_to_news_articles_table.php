<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewCountToNewsArticlesTable extends Migration {

	public function up()
	{
		// SET global sql_mode = "";
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->integer('view_count')->unsigned()->default(0);
		});
	}

	public function down()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->dropColumn('view_count');
		});
	}

}
