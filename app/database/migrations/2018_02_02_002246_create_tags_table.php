<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tags', function(Blueprint $table)
		{
			$table->string('name')->unique();
			$table->engine = 'InnoDB';
		});
	}

	public function down()
	{
		Schema::drop('tags');
	}

}
