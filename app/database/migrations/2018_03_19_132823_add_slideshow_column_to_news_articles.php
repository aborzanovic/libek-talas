<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlideshowColumnToNewsArticles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->boolean('slideshow')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->dropColumn('slideshow');
		});
	}

}
