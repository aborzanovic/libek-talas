<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmittedApplications extends Migration {

	public function up()
	{
		Schema::create('submitted_applications', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('application_form_id')->unsigned();

			$table->text('data');

			$table->timestamp('submitted_at')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->index('application_form_id');
			$table->foreign('application_form_id')->references('id')->on('application_forms')->onUpdate('cascade')->onDelete('restrict');

			$table->engine = 'InnoDB';
		});
	}

	public function down()
	{
		Schema::drop('submitted_applications');
	}

}
