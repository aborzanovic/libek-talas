<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsArticlesAddSticky extends Migration {

	public function up()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->integer('sticky')->after('image_main_og')->default(0);
		});
	}

	public function down()
	{
		Schema::table('news_articles', function(Blueprint $table)
		{
			$table->dropColumn('sticky');
		});
	}

}
