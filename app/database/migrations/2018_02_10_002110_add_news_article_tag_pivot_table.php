<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsArticleTagPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_article_tag', function(Blueprint $table)
		{
			$table->integer('news_article_id')->unsigned();
			$table->integer('tag_id')->unsigned();
			$table->engine = 'InnoDB';
		});
	}

	public function down()
	{
		Schema::drop('news_article_tag');
	}

}
