<?php

use Illuminate\Database\Seeder;
use Libek\LibekOrgRs\Auth\User;

class UserSeeder extends Seeder {

	/**
	 * Users to seed.
	 *
	 * @var array
	 */
	protected $users = [
		[
			'email' => 'admin@libek.org.rs',
			'password' => 'admin',
			'first_name' => 'Super',
			'last_name' => 'Administrator',
			'roles' => ['admin'],
		],
	];

	/**
	 * {@inheritDoc}
	 */
	public function run()
	{
		foreach ($this->users as $userConfig)
		{
			if ( ! $this->userExists($userConfig['email']))
			{
				$this->createUser($userConfig);
			}
		}
	}

	/**
	 * Checks whether a user with the given email exists.
	 *
	 * @param string $email
	 * @return boolean
	 */
	protected function userExists($email)
	{
		return User::whereEmail($email)->exists();
	}

	/**
	 * Creates a new user according to the specified config, and assigns the
	 * roles.
	 *
	 * @param array $userConfig
	 * @return void
	 */
	protected function createUser(array $userConfig)
	{
		$roles = $userConfig['roles'];

		unset($userConfig['roles']);

		$user = Sentinel::registerAndActivate($userConfig);

		foreach ($roles as $role)
		{
			$this->attachToRole($user, $role);
		}
	}

	/**
	 * Attaches a user to a role.
	 *
	 * @param \Bpharm\Bpharm\Models\User $user
	 * @param string $roleSlug
	 * @return void
	 */
	protected function attachToRole(User $user, $roleSlug)
	{
		$role = Sentinel::findRoleBySlug($roleSlug);

		$role->users()->attach($user);
	}

}
