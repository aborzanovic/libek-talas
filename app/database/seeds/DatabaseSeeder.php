<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * {@inheritDoc}
	 */
	public function run()
	{
		Model::unguard();

		$this->call('RoleSeeder');
		$this->call('UserSeeder');
	}

}
