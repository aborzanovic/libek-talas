<?php

use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder {

	/**
	 * Role configuration.
	 *
	 * @var array
	 */
	protected $roles = [
		[
			'name' => 'Admin',
			'slug' => 'admin',
			'permissions' => [
				'Libek\LibekOrgRs\Http\Controllers\Admin\HomeController@index' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\SlideshowImagesController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\ResearchArticleController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\JohnGaltArticleController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\TeamMemberController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\MentorController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\LecturerController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\GenerationsArticleController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\AlumnusController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@create' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@update' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@confirmDelete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\UserController@delete' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\Redactor\ImageController@store' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\Redactor\FileController@store' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@index' => true,
				'Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@postUpdate' => true,

				'Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@postLogout' => true,

				'Libek\LibekOrgRs\Http\Controllers\Api\V1\Admin\ImageController@delete' => true,
				'Libek\LibekOrgRs\Http\Controllers\Api\V1\Admin\ImageController@sort' => true,
			],
		],
		[
			'name' => 'Guest',
			'slug' => 'guest',
			'permissions' => [],
		],
	];

	/**
	 * {@inheritDoc}
	 */
	public function run()
	{
		foreach ($this->roles as $roleConfig)
		{
			$role = Sentinel::findRoleBySlug($roleConfig['slug']);

			if (is_null($role))
			{
				$this->createRole($roleConfig);
			}
			else
			{
				$this->updateRole($role, $roleConfig);
			}
		}
	}

	/**
	 * Creates the passed role.
	 *
	 * @param array $roleConfig
	 * @return void
	 */
	protected function createRole(array $roleConfig)
	{
		Sentinel::getRoleRepository()->createModel()->create($roleConfig);
	}

	/**
	 * Updates the passed role with the specified parameters.
	 *
	 * @param \Cartalyst\Sentinel\Roles\EloquentRole $role
	 * @param array $roleConfig
	 * @return void
	 */
	protected function updateRole(EloquentRole $role, array $roleConfig)
	{
		$role->name = $roleConfig['name'];
		$role->permissions = $roleConfig['permissions'];

		$role->save();
	}

}
