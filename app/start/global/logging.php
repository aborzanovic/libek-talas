<?php

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log', Config::get('app.logLevel', 'debug'));

$monolog = Log::getMonolog();

/**
 * If a Loggly token is configured, we'll send all log messages there as well.
 */

if ($logglyToken = Config::get('loggly.token'))
{
	if (!($logglyTag = Config::get('loggly.tag')))
	{
		throw new InvalidArgumentException('You must configure a Loggly tag in order to use logging');
	}

	$logglyHandler = new Monolog\Handler\LogglyHandler($logglyToken);
	$logglyHandler->setTag($logglyTag);

	$monolog->pushHandler($logglyHandler);
}

/**
 * We'll attach some additional debugging info as extra data, which will be
 * logged by Monolog appropriately.
 *
 * Note that artisan commands will always log the URL configured as the
 * `app.url`, but we can use the `console` entry to identify those.
 */

$extraDataProcessor = new Creitive\Monolog\Processor\ExtraDataProcessor([
	'httpMethod' => Request::method(),
	'url' => Request::url(),
	'environment' => App::environment(),
	'console' => App::runningInConsole(),
	'clientIps' => Request::getClientIps(),
	'referrer' => Request::server('HTTP_REFERER'),
	'userAgent' => Request::header('User-Agent'),
]);

$monolog->pushProcessor($extraDataProcessor);

/**
 * We also want to log the current Git branch/commit info.
 */

$monolog->pushProcessor(new Monolog\Processor\GitProcessor);
