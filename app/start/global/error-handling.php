<?php

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	/**
	 * Always log errors, no matter what happens.
	 */
	Log::error($exception);

	/**
	 * We don't want to explicitly handle errors during development; we'll just
	 * let them bubble up to Laravel's built-in exception handler, which pretty
	 * prints the errors using `filp/whoops`.
	 */
	if (App::environment() === 'local')
	{
		return;
	}

	/**
	 * Also, if we're running in the console, we won't return the pretty error
	 * pages, but just let Laravel format the exception as it does on its own.
	 */
	if (App::runningInConsole())
	{
		return;
	}

	/**
	 * The "staging" and "production" environments mustn't display any traces or
	 * error output for security reasons, so we'll handle those manually,
	 * returning the appropriate HTTP status code and a generic error page. The
	 * same goes for the "testing" environment, since we want to be able to
	 * actually test whether the appropriate responses are returned.
	 *
	 * We will explicitly check some specific exceptions we need to handle
	 * individually, and display nice error pages in those cases.
	 */
	if (
		($exception instanceof Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
		|| ($exception instanceof Illuminate\Database\Eloquent\ModelNotFoundException)
	)
	{
		return Response::make(
			View::make('errors.404'),
			404
		);
	}
	else if ($exception instanceof Symfony\Component\HttpKernel\Exception\BadRequestHttpException)
	{
		return Response::make(
			View::make('errors.400'),
			400
		);
	}
	else if (
		($exception instanceof Symfony\Component\HttpKernel\Exception\HttpException)
		&& ($exception->getStatusCode() === 403)
	)
	{
		return Response::make(
			View::make('errors.403'),
			403
		);
	}
	else if ($exception instanceof Creitive\Http\Exception\NonAjaxRequestException)
	{
		return Response::make(
			'Only AJAX requests are allowed at this endpoint.',
			400
		);
	}
	else if ($exception instanceof Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException)
	{
		return Response::make(
			'Too many requests, please wait for a little while and try again.',
			429
		);
	}
	else
	{
		return Response::make(
			View::make('errors.500'),
			500
		);
	}
});
