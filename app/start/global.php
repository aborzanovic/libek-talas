<?php

/**
 * This file is executed for all requests and artisan commands.
 *
 * It has been refactored from the original Laravel structure mostly into
 * separate files, for easier maintenance and extension.
 *
 * @copyright ©2014 CreITive (http://www.creitive.rs)
 */

/**
 * Initialize the class loader.
 */
require app_path().'/start/global/class-loader.php';

/**
 * Initialize application logging.
 */
require app_path().'/start/global/logging.php';

/**
 * Initialize application error handling.
 */
require app_path().'/start/global/error-handling.php';

/**
 * Maintenance mode handler.
 */
require app_path().'/start/global/maintenance-mode.php';

/**
 * Require the filters file.
 *
 * Next we will load the filters file for the application. This gives us a nice
 * separate location to store our route and application filter definitions
 * instead of putting them all in the main routes file.
 */
require app_path().'/filters.php';

require app_path().'/helpers.php';
