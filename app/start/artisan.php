<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

$commands = [
	Creitive\Config\Console\MakeEnvironmentConfigCommand::class,
	Creitive\Elospect\ElospectCommand::class,
	Libek\LibekOrgRs\Auth\Console\CreateUserCommand::class,
	Libek\LibekOrgRs\Auth\Console\ResetPasswordCommand::class,
	Libek\LibekOrgRs\News\Console\AddNewsArticleImageCommand::class,
	Libek\LibekOrgRs\News\Console\ImportOldNewsArticlesCommand::class,
	Libek\LibekOrgRs\Projects\Console\AddProjectArticleImageCommand::class,
	Libek\LibekOrgRs\Research\Console\AddResearchArticleImageCommand::class,
];

foreach ($commands as $command)
{
	Artisan::resolve($command);
}
