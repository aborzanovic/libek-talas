<?php

use Libek\LibekOrgRs\Auth\User;
use Libek\LibekOrgRs\Slideshow\Image as SlideshowImage;
use Libek\LibekOrgRs\News\Article as NewsArticle;
use Libek\LibekOrgRs\Generations\Article as GenerationsArticle;
use Libek\LibekOrgRs\JohnGalt\Article as JohnGaltArticle;
use Libek\LibekOrgRs\Research\Article as ResearchArticle;
use Libek\LibekOrgRs\TeamMembers\Person as TeamMember;
use Libek\LibekOrgRs\Mentors\Person as Mentor;
use Libek\LibekOrgRs\Lecturers\Person as Lecturer;
use Libek\LibekOrgRs\Alumni\Person as Alumnus;

/*
 * A generic ID pattern which can be used in pattern definitions.
 */
$idPattern = '[1-9][0-9]*';

/**
 * A pattern for a single URL segment.
 */
$segment = '[A-Za-z0-9\\-]+';

/**
 * General pattern definitions.
 */
Route::pattern('any', '[A-Za-z0-9/\\-]+');
Route::pattern('page', '[0-9]+');

/**
 * Slug pattern definition.
 */
Route::pattern('slug', $segment);

/**
 * Date pattern definitions.
 */
Route::pattern('year', '[0-9]{4}');
Route::pattern('month', '[0-9]{2}');
Route::pattern('day', '[0-9]{2}');

/**
 * Route model definitions.
 *
 * We're defining our own logic, because `Route::model()` unfortunately also
 * matches IDs like `03` (treating them as identical to `3`), which we want to
 * avoid.
 */

Route::pattern('slideshowImage', $segment);
Route::bind('slideshowImage', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$image = SlideshowImage::whereId($value)->first();

		if ( ! is_null($image))
		{
			return $image;
		}
	}

	return SlideshowImage::whereSlug($value)->firstOrFail();
});

Route::pattern('newsArticle', $segment);
Route::bind('newsArticle', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$article = NewsArticle::whereId($value)->first();

		if ( ! is_null($article))
		{
			return $article;
		}
	}

	return NewsArticle::whereSlug($value)->firstOrFail();
});

Route::pattern('researchArticle', $segment);
Route::bind('researchArticle', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$article = ResearchArticle::whereId($value)->first();

		if ( ! is_null($article))
		{
			return $article;
		}
	}

	return ResearchArticle::whereSlug($value)->firstOrFail();
});

Route::pattern('johnGaltArticle', $segment);
Route::bind('johnGaltArticle', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$article = JohnGaltArticle::whereId($value)->first();

		if ( ! is_null($article))
		{
			return $article;
		}
	}

	return JohnGaltArticle::whereSlug($value)->firstOrFail();
});

Route::pattern('teamMember', $segment);
Route::bind('teamMember', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$person = TeamMember::whereId($value)->first();

		if ( ! is_null($person))
		{
			return $person;
		}
	}

	return false;
});

Route::pattern('mentor', $segment);
Route::bind('mentor', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$person = Mentor::whereId($value)->first();

		if ( ! is_null($person))
		{
			return $person;
		}
	}

	return false;
});

Route::pattern('lecturer', $segment);
Route::bind('lecturer', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$person = Lecturer::whereId($value)->first();

		if ( ! is_null($person))
		{
			return $person;
		}
	}

	return false;
});

Route::pattern('generationsArticle', $segment);
Route::bind('generationsArticle', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$article = GenerationsArticle::whereId($value)->first();

		if ( ! is_null($article))
		{
			return $article;
		}
	}

	return GenerationsArticle::whereSlug($value)->firstOrFail();
});

Route::pattern('alumnus', $segment);
Route::bind('alumnus', function($value) use ($idPattern)
{
	if (preg_match("/^{$idPattern}$/D", $value))
	{
		$person = Alumnus::whereId($value)->first();

		if ( ! is_null($person))
		{
			return $person;
		}
	}

	return false;
});

Route::pattern('user', $idPattern);
Route::bind('user', function($value)
{
	return User::findOrFail($value);
});
