@if(count($articles) > 0)
        <?php $cnt = 0; ?>
        <div class="row ">
        @foreach ($articles as $article)
                <?php $url = $urlGenerator($article); ?>
                <?php $cnt++; ?>
                @include("article-listing-one")
                @if($cnt % 3 === 0)
                        </div>
                        <div class="row row-margin">
                @endif
        @endforeach
        </div>
@endif