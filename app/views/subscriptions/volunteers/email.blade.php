<html>
<head>
	<meta charset="utf-8">
	<title>{{{ $subject }}}</title>
</head>
<body>

<b>{{{ Lang::get('subscriptions.labels.fullName') }}}</b>
<br />
{{{ $inputData['full_name'] }}}
<br />
<br />

<b>{{{ Lang::get('subscriptions.labels.email') }}}</b>
<br />
{{{ $inputData['email'] }}}
<br />
<br />

<b>{{{ Lang::get('subscriptions.labels.message') }}}</b>
<br />
{{{ $inputData['message'] }}}
<br />
<br />

</body>
</html>
