<?php $url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\SubscriptionController@postVolunteerForm'); ?>

<div class="subscriptions-page--background"></div>
<div class="subscriptions-page--background-cover"></div>

<div class="subscriptions-page--container container-fluid">
	<div class="subscriptions-page--contents container">
		<article>
			@if ($successMessages->isEmpty())
				<div class="alert alert-info">{{ Lang::get('subscriptions.motivational') }}</div>

				<div class="subscriptions-page--form-container">
					{{ Form::open(['url' => $url, 'role' => 'form']) }}

						{{-- Full name --}}

						<div class="form-group">
							{{ Form::label('full_name', Lang::get('subscriptions.labels.fullName')) }}
							{{ Form::text('full_name', null, ['class' => 'form-control form-control--subscriptions']) }}
						</div>

						@if ($errors->has('full_name'))
							<div class="alert alert-danger">{{ Lang::get('subscriptions.errors.fullName') }}</div>
						@endif

						{{-- Email --}}

						<div class="form-group">
							{{ Form::label('email', Lang::get('subscriptions.labels.email')) }}
							{{ Form::email('email', null, ['class' => 'form-control form-control--subscriptions']) }}
						</div>

						@if ($errors->has('email'))
							<div class="alert alert-danger">{{ Lang::get('subscriptions.errors.email') }}</div>
						@endif

						{{-- Message --}}

						<div class="form-group">
							{{ Form::label('message', Lang::get('subscriptions.labels.message')) }}
							{{ Form::text('message', null, ['class' => 'form-control form-control--subscriptions']) }}
						</div>

						{{-- Submit button --}}

						<div class="form-group">
							{{
								Form::button(
									Lang::get('subscriptions.labels.send.default'),
									[
										'class' => 'btn btn-lg btn-primary',
										'type' => 'submit',
										'data-loading-text' => Lang::get('subscriptions.labels.send.loading'),
									]
								)
							}}
						</div>

					{{ Form::close() }}
				</div>

				<div class="alert alert-info">{{ Lang::get('subscriptions.privacy') }}</div>
			@else
				<div class="alert alert-success">{{ $successMessages->first() }}</div>
			@endif
		</article>
	</div>
</div>
