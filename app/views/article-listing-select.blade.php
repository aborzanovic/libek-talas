<div class="content-row">
    <div class="container news-padding">
        @if($listingType == 'none')
        <div class="row">
            <div class="col-md-6 mx-auto newest" style="display: inline-flex;"> <!-- TODO: check inline-flex -->
                <img src="{{ url('/images/talas/newest-wawe.png') }}" class="wave-left">
                <h3>Najnovije vesti</h3>
                <img src="{{ url('/images/talas/newest-wawe.png') }}" class="wave-right">
            </div>
        </div>
        @endif
        <?php $cnt = 0; $cnt2 = 0 ?>
        <div class="row ">
            @foreach ($articles as $article)
                <?php $url = $urlGenerator($article); ?>
                <?php $cnt++; $cnt2++; ?>
                @include("article-listing-one")
                @if($cnt % 3 === 0)
        </div>
        <div class="row row-margin">
                @endif
            @endforeach
        </div>
    </div>
</div>
<style>
    body {
        overflow-y: auto;
        overflow-x: hidden;
    }
    html {
        overflow: hidden;
        height: 100%;
    }
</style>