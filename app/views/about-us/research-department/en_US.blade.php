<h1>Research department</h1>

<p>The economic research department unit was established in October 2013 with the aim to enhance the process of public economic policy analysis and boost the process of public advocacy in Serbia. The idea behind the research department unit is to create public policy proposals and economic analysis, provide information regarding contemporary economic policy to citizens and other stakeholders with the aim to enhance dialogue and reform processes. The unit also provides services to individuals and organizations in the field of economic analysis.</p>

<p>The main topics of interest to research department unit are <em>public finance</em> and <em>fiscal policy</em>, <em>public sector economics</em>, <em>education policy</em> and <em>economics of regulation</em>.</p>

<p>Types of documents that the research department unit creates are:</p>

<ul>
	<li><strong>fact sheet</strong> (overview of a certain area pointing out strong and weak points but without any ellabaoration and concrete probelm solving). Its main goal is to aquaint wider public and other stakeholders with key issues in the area.</li>
	<li><strong>opinion brief</strong> (short overview of a problem with solution proposing). Its main goal is to galvanize disucussion among wider public and experts.</li>
	<li><strong>public policy proposal</strong> (analysis of a certain field with possible solutions and argumented proposal of one of them). Its main goal is to be used in public advocacy campaign, and implemented by the decision makers.</li>
</ul>

<?php
	$people = [
		'mihailo-gajic' => [
			'name' => 'Mihailo Gajić',
			'position' => 'Program Director of Research Department',
		],
	];
?>

@include('about-us/people', ['people' => $people])
