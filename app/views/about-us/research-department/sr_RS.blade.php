<h1>Istraživačka jedinica</h1>

<p>Ekonomska istraživačka jedinica je osnovana oktobra 2013. godine sa željom da se unapredi proces analize javnih ekonomskih politika i unapredi proces javnog zagovaranja u Srbiji. Ideja koja stoji iza istraživačke jedinice je da ona kreira predloge javnih politika i ekonomske analize, pruža više informacija o trenutnoj ekonomskoj politici građanima i zainteresovanim stranama sa željom da se pospeši dijalog i reformski procesi. Pored toga, istraživačka jedinica pruža usluge zainteresovanim pojedincima i organizacijama iz domena ekonomske analize.</p>

<p>Glavne teme kojima se bavi istraživačka jedinica su <em>javne finansije</em> i <em>fiskalna politika</em>, <em>ekonomija javnog sektora</em>, <em>obrazovanje</em>, <em>ekonomika regulacije</em>.</p>

<p>Vrste dokumenata koji nastaju u radu istraživačke jedinice su:</p>

<ul>
	<li><strong>pregled činjenica</strong> (pregled stanja u određenoj oblasti bez davanja konkretnih predloga ili rešenja). Cilj ovog dokumenta je da upozna javnost i zainteresovane strane sa stanjem u datoj oblasti.</li>
	<li><strong>esej</strong> (kraći pregled nekog problema sa davanjem adekvatnih rešenja). Cilj ovog dokumenta je da skrene pažnju na probleme u nekoj oblasti i zapodene javnu raspravu.</li>
	<li><strong>predlozi praktičnih politika</strong> (analiza stanja u određenoj oblasti uz navođenje mogućih rešenja i argumentovano odabiranje jednog od njih). Cilj ovog dokumenta je da bude korišćen u kampanji javnog zagovaranja i da preporuke navedene u njemu budu usvojene i sprovedene od strane nosilaca vlasti.</li>
</ul>

<?php
	$people = [
		'mihailo-gajic' => [
			'name' => 'Mihailo Gajić',
			'position' => 'Programski direktor istraživačke jedinice',
		],
	];
?>

@include('about-us/people', ['people' => $people])
