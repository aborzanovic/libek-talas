<h1>Izvršni Odbor</h1>

<p>Izvršni odbor odgovoran je za strateško i programsko planiranje, prikupljanje sredstava, projektno upravljanje i koordinaciju svih projektnih aktivnosti.</p>
<p>Izvršni odbor sastoji se od Predsednika, Izvršnog direktora, Programskog direktora i Direktora za međunarodnu saradnju.</p>

<?php
	$people = [
		'milos-nikolic' => [
			'name' => 'Miloš Nikolić',
			'position' => 'Predsednik',
		],
		'djordje-trikos' => [
			'name' => 'Đorđe Trikoš',
			'position' => 'Programski direktor',
		],
		'petar-cekerevac' => [
			'name' => 'Petar Čekerevac',
			'position' => 'Izvršni direktor',
		],
		'aleksandar-kokotovic' => [
			'name' => 'Aleksandar Kokotović',
			'position' => 'Direktor za međunarodnu saradnju',
		],
	];
?>

@include('about-us/people', ['people' => $people])
