<h1>Executive Board</h1>

<p>Executive Board is responsible for strategic and program planning, fundraising, project management and coordination of all projects.</p>
<p>Executive Board consists of President, Executive Director, Program Director and Director for International Outreach.</p>

<?php
	$people = [
		'milos-nikolic' => [
			'name' => 'Miloš Nikolić',
			'position' => 'President',
		],
		'djordje-trikos' => [
			'name' => 'Đorđe Trikoš',
			'position' => 'Program Director',
		],
		'petar-cekerevac' => [
			'name' => 'Petar Čekerevac',
			'position' => 'Executive Director',
		],
		'aleksandar-kokotovic' => [
			'name' => 'Aleksandar Kokotović',
			'position' => 'International Outreach Director',
		],
	];
?>

@include('about-us/people', ['people' => $people])
