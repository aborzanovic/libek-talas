<div class="about-us-subpage--links">
	<div class="container">
		@include('about-us/links')
	</div>
</div>

<div class="about-us-subpage--contents container">
	<article>
		{{ $subpageContent }}
	</article>
</div>
