<h1>About us</h1>

<p>Libek was established in 2008. We started as a small group of students from Faculty of Political Science in Belgrade and since then we became the major liberty promoting center both in Serbia and in our region.</p>
<p>Today Libek has three main strategic areas of activities: education for freedom, public advocacy for lower taxation and economic and social research.</p>
<p>Our mission started with the education of young people. We provide them with skills and knowledge so they can advocate individual liberty and personal responsibility in various ways in our society.</p>
<p>We create wide political movement oriented toward limited and fiscally responsible government. We believe in strong potential of advocacy associations and grassroots coalitions.</p>
<p>We want to influence decision makers in order to implement reforms. Our advocacy activism will be strongly relied on the work of our research units.</p>
