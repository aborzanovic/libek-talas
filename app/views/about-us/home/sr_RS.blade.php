<h1>O nama</h1>

<p>Libek je osnovan 2008. godine. Te godine počeli smo kao mala grupa studenata sa Fakulteta političkih nauka u Beogradu, da bismo od tada postali glavna organizacija za promociju slobode kako u Srbiji, tako i u čitavom našem regionu.</p>
<p>Danas Libek ima tri glavna strateška polja delovanja – obrazovanje, javno zagovaranje nižih poreza i ekonomsko i društveno istraživanje.</p>
<p>Naša misija započela je obrazovanjem mladih ljudi. Obezbeđujemo im znanja i veštine kako bi što uspešnije zagovarali ideje individualne slobode i lične odgovornosti na različite načine u našem društvu.</p>
<p>Stvaramo širok politički pokret koji se zalaže za strogo ograničenu i fiskalno odgovornu vladu. Verujemo u veliki potencijal koji postoji u udruženjima za javno zagovaranje i u građanskim koalicijama.</p>
<p>Želimo da utičemo na donosioce odluka da sprovode odgovarajuće reforme. Naš javni angažman nalaziće svoj stabilan oslonac u radovima i analizama naših istraživačkih timova.</p>
