<?php $separatedClass = (isset($separated) && $separated) ? 'about-us-page--links-separated' : ''; ?>
<div class="about-us-page--links {{ $separatedClass }}">
	<?php
		$links = [
			'executive-board' => 'executiveBoard',
			'communication-and-protocol' => 'communicationAndProtocol',
			'research-department' => 'researchDepartment',
		];

		foreach ($links as $dataPage => $entry)
		{
			$activeClass = ($bodyDataPage === $dataPage) ? 'about-us-page--link-active' : '';
			$url = '/'.$currentLanguage.'/'.Lang::get('routes.aboutUs').'/'.Lang::get("routes.{$entry}");
			$name = Lang::get("aboutUs.{$entry}");
	?>
			<div class="col-lg-4"><a href="{{ $url }}" class="about-us-page--link {{ $activeClass }}">{{ $name }}</a></div>
	<?php
		}
	?>
</div>
