<div class="row">
	@foreach ($people as $key => $data)
		@include('about-us/person', ['key' => $key, 'name' => $data['name'], 'position' => $data['position']])
	@endforeach
</div>
