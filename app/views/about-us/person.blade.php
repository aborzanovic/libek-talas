<div class="col-sm-6 col-lg-3">
	<div class="about-us-subpage--people-thumbnail thumbnail">
		<img src="/images/about-us-people/{{ $key }}.png" alt="{{{ $name }}}">
		<div class="about-us-subpage--people-caption caption">
			<p class="about-us-subpage--people-name">{{{ $name }}}</p>
			<p class="about-us-subpage--people-position">{{{ $position }}}</p>
		</div>
	</div>
</div>
