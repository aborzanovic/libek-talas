<h1>Komunikacija i Protokol</h1>

<p>Menadžer za komunikaciju u Libertarijanskom klubu Libek, bavi se pre svega internom komunikacijom, kako unutar same
    organizacije, tako i sa čitavom bazom alumnista sa programa Politika pojedinca i Akademija liberalne politike.
    Komunikacija je jedan od najvažnijih aspekata funkcionisanja svake organizacije, pa u Libeku posebno posvećujemo
    pažnju kako već pomenutoj internoj komunikaciji, tako i komunikaciji sa medijima i ljudima koji su na bilo koji
    način podržali neku od Libekovih aktivnosti.</p>

<?php
$people = [
        'milica-kostic' => [
                'name' => 'Milica Kostić',
                'position' => 'Menadžer za komunikacije',
        ],
];
?>

@include('about-us/people', ['people' => $people])
