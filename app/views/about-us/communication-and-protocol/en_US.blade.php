<h1>Communication and Protocol</h1>

<p>Communication manager in Libek is dealing with various kind of jobs. From communication within our team and  with our alumnis, to the extern communication which is usually communication with the media, or with our supporters. Since the communication is very important in every organization, we pay very much attention to this aspect.</p>

<?php
	$people = [
		'milica-kostic' => [
			'name' => 'Milica Kostić',
			'position' => 'Communications Manager',
		],
	];
?>

@include('about-us/people', ['people' => $people])
