<?php
$url = $urlGenerator($article);
$coverImage = $article->getImage('main', 'full');
$count = count($tags);

$num = str_word_count($article->contents,0);

$numOfMin = ceil($num / 200);

?>
@include('partials.navigation')

<style type="text/css">
    .article-signle-cover {
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    height: 620px;
    margin-top: 50px;
}

@media (max-width: 768px){
    .article-signle-cover {
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;
        height: 320px;
        margin-top: 50px;
    }
}

</style>


<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto article-signle-cover" style="background-image: url('{{$coverImage}}')">
        </div>
        <div class="col-md-6 mx-auto article-signle-title">
            <p class="article-signle-author">  <time pubdate
                          datetime="{{ $article->published_at->toATOMString() }}">{{ Str::title(Str::getDayName($article->published_at, $currentLanguage)) }}
                        , {{ $article->published_at->format(Lang::get('common.dateFormat')) }}</time>
                        |
                        Vreme čitanja: {{$numOfMin}} {{ $numOfMin == 1 ? 'minut' : 'minuta'}}
            </p>    
            <h1>{{ $article->title}}</h1>
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-9 article-signle-content">
            <div style="padding-left: 12%; padding-right: 12%;">
            {{ $article->contents }}
                <div class="row">
                    <div class="col-12">
                         <p class="article-signle-author" style="font-size: 18px;">
                         Autor : <a href="/sr/vesti/author/{{$author->id}}" class="normalize-link">{{$author->name}}</a>
                         <br/> Tagovi: @foreach($tags as $tag) <a href="/sr/vesti/tag/{{$tag->id}}" class="normalize-link">{{$tag->name}}</a> <?php if(--$count > 0) echo ","; ?>  @endforeach<br/>
                     </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 nav-articles">
                        <div class="row nav-margin-top">
                            <div class="col-md-4 mx-auto previous-article">
                                @if($prevArticle != null)
                                <a href="{{$prevArticle->url}}" class="normalize-link">
                                <img src="{{ url('/images/talas/arrow-left.png') }}"><p class="next-article-title">{{$prevArticle->title}}</p>
                                </a>
                                @endif
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-4 mx-auto next-article">
                                @if($nextArticle != null)
                                <a href="{{$nextArticle->url}}" class="normalize-link">
                                <img src="{{ url('/images/talas/arrow-right2.png') }}"><p class="next-article-title">{{$nextArticle->title}}</p>
                                </a>
                                @endif    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 hidden-xs-down article-single-sidebar">
            <div class="row">
                <div class="col-md-3 talas-logo-vertical" style="background-repeat: no-repeat;"></div><div class="col-md-19"></div>
            </div>
                <!--
                <div class="col-5">About:
Phasellus rhoncus eros et elit varius maximus. Nulla dictum risus interdum quam ornare vehicula. Duis cursus mi et arcu pellentesque, ac sagittis quam elementum. </div>

            </div>
            <div style="margin-top: 15px; width: 35%;">
                <p>
                Phasellus rhoncus eros et elit varius maximus. Nulla dictum risus interdum quam ornare vehicula. 
                Duis cursus mi et arcu pellentesque, ac sagittis quam elementum. Suspendisse vestibulum consequat turpis.
                sit amet rutrum felis congue non. </p>
            </div> 
            <div class="bg-stripes-2">
                  <img src="{{ url('/images/talas/bg-stripes.png') }}" class="bg-stripes-single">
            </div>-->
            <div class="row" >
                
                <div class="left-margin" style="margin-top: 25px;">
                    <p><b>newsletter</b></p>
                    <form name="newsletter-form">
                    <input type="text" name="newsletter" placeholder="enter your email here" class="newsletter" id="newsletter-email"> 
                    <button type="submit" value="send" class="send-button" id="newsletter-subscribe">Send</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-10 related">
                    <div class="rel-art"><b>SRODAN SADRŽAJ</b></div>
                    @foreach($recommended as $recomm)
                    <?php $url = $urlGenerator($recomm); ?>
                    <div class="rel-art">
                        <a href="{{$url}}" class="normalize-link">
                        <h6><b>{{$recomm->title}}</b></h6>
                        </a>
                        <time class="recomm-time" pubdate
                          datetime="{{ $recomm->published_at->toATOMString() }}">{{ Str::title(Str::getDayName($recomm->published_at, $currentLanguage)) }}, 
                          {{ $recomm->published_at->format(Lang::get('common.dateFormat')) }}</time>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>
        
    </div>
</div>