<?php $url = $urlGenerator($article); ?>

<div class="project-article--cover" style="background-image: url({{{ $article->getImage('main', 'cover') }}});"></div>

<article class="project-article container">
	<h1>{{{ $article->title }}}</h1>

	<div class="project-article--contents">
		<div class="project-article--social-links">
			{{ $socialPluginsGenerator($url) }}
		</div>

		{{ $article->contents }}
	</div>
</article>
