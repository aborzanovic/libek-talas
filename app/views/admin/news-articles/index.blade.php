<?php

$currentUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index');
$createUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@create');

?>

{{ Breadcrumbs::render('creitive/basic::admin.newsArticles') }}

@foreach ($successMessages->all() as $message)
	<div class="alert alert-success">{{{ $message }}}</div>
@endforeach

<div class="itemList">
	<div class="itemList-actions">
		<div class="pull-right">
			<a href="{{ $createUrl }}" class="itemList itemList-iconLink btn btn-xs btn-primary">
				<i class="fa fa-plus-square"></i>
				<span class="hidden-xs">{{ Lang::get('admin/newsArticles.index.links.create') }}</span>
			</a>
		</div>
	</div>
	<div class="itemList-head">
		<div class="col-xs-1 itemList-column">
			{{{ Lang::get('admin/newsArticles.labels.language') }}}
		</div>
		<div class="col-xs-2 itemList-column">
			{{{ Lang::get('admin/newsArticles.labels.createdAt') }}}
		</div>
		<div class="col-xs-2 itemList-column">
			{{{ Lang::get('admin/newsArticles.labels.publishedAt') }}}
		</div>
		<div class="col-xs-4 itemList-column">
			{{{ Lang::get('admin/newsArticles.labels.title') }}}
		</div>
	</div>
	<div class="itemList-rootContainer">
		{{ View::make('admin.news-articles.index.list', ['articles' => $articles])->render() }}
	</div>
</div>

{{ $pagination->links() }}
