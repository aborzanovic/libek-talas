<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@delete', ['article' => $article->id]);

?>

{{ Breadcrumbs::render('creitive/basic::admin.newsArticles.delete', $article) }}

<div class="alert alert-warning">{{ Lang::get('admin/newsArticles.confirmDelete.message') }}</div>

{{ Form::open(['url' => $url, 'method' => 'delete']) }}

	<div class="form-group">
		{{-- Confirm button --}}

		{{
			Form::button(
				'<i class="fa fa-trash-o"></i> '.Lang::get('admin/newsArticles.confirmDelete.confirm.default'),
				[
					'class' => 'btn btn-lg btn-danger',
					'type' => 'submit',
					'name' => 'action',
					'value' => 'confirm',
					'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.Lang::get('admin/newsArticles.confirmDelete.confirm.loading'),
				]
			)
		}}

		{{-- Cancel button --}}

		{{
			Form::button(
				'<i class="fa fa-times"></i> '.Lang::get('admin/newsArticles.confirmDelete.cancel.default'),
				[
					'class' => 'btn btn-lg btn-default',
					'type' => 'submit',
					'name' => 'action',
					'value' => 'cancel',
					'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.Lang::get('admin/newsArticles.confirmDelete.cancel.loading'),
				]
			)
		}}
	</div>

{{ Form::close() }}
