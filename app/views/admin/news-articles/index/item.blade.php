<?php

$editUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit', ['article' => $article->id]);
$deleteUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@confirmDelete', ['article' => $article->id]);

?>

<li class="itemList-item" data-id="{{ $article->id }}">
	<div class="itemList-item-contents">
		<div class="col-xs-1 itemList-column">
			{{{ LocaleResolver::getLanguageName($article->language) ?: Lang::get('admin/newsArticles.labels.notAvailable') }}}
		</div>
		<div class="col-xs-2 itemList-column">
			{{{ ( ! is_null($article->created_at)) ? $article->created_at->format(Lang::get('common.dateTimeFormat')) : Lang::get('admin/newsArticles.labels.notAvailable') }}}
		</div>
		<div class="col-xs-2 itemList-column">
			{{{ ( ! is_null($article->published_at)) ? $article->published_at->format(Lang::get('common.dateTimeFormat')) : Lang::get('admin/newsArticles.labels.notAvailable') }}}
		</div>
		<div class="col-xs-4 itemList-column" title="{{{ $article->title }}}">
			{{{ $article->title }}}
		</div>

		<div class="col-xs-3 itemList-column itemList-noDrag">
			<div class="pull-right">
				<a href="{{ $editUrl }}" class="itemList-iconLink">
					<i class="fa fa-pencil"></i>
					<span class="hidden-xs">{{ Lang::get('admin/newsArticles.index.links.edit') }}</span>
				</a>
				<a href="{{ $deleteUrl }}" class="itemList-iconLink">
					<i class="fa fa-trash-o"></i>
					<span class="hidden-xs">{{ Lang::get('admin/newsArticles.index.links.delete') }}</span>
				</a>
			</div>
		</div>
	</div>
</li>
