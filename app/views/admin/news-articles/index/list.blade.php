<ol class="itemList-list">
	@foreach ($articles as $article)
		{{ View::make('admin.news-articles.index.item', ['article' => $article])->render() }}
	@endforeach
</ol>
