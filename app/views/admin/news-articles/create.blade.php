<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@store');
$now = Carbon\Carbon::now();

?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap3.min.css" rel="stylesheet" type="text/css">

{{ Breadcrumbs::render('creitive/basic::admin.newsArticles.create') }}

@foreach ($successMessages->all() as $message)
	<div class="alert alert-success">{{ $message }}</div>
@endforeach

{{ Form::open(['url' => $url, 'files' => true]) }}

	{{-- Basic configuration --}}

	<div class="panel panel-default">
		<div class="panel-heading">{{ Lang::get('admin/newsArticles.panelTitles.basicConfiguration') }}</div>
		<div class="panel-body">

			{{-- Language --}}

			<div class="form-group">
				{{ Form::label('language', Lang::get('admin/newsArticles.labels.language'), ['class' => 'control-label']) }}
				{{ Form::select('language', $languageOptions, null, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('language'))
				<div class="alert alert-danger">{{ $errors->first('language') }}</div>
			@endif


			{{-- Title --}}

			<div class="form-group">
				{{ Form::label('title', Lang::get('admin/newsArticles.labels.title'), ['class' => 'control-label']) }}
				{{ Form::text('title', null, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('title'))
				<div class="alert alert-danger">{{ $errors->first('title') }}</div>
			@endif

			{{-- Tags --}}

			<div class="form-group">
				{{ Form::label('tags[]', Lang::get('admin/newsArticles.labels.tags'), ['class' => 'control-label']) }}
				{{ Form::select('tags[]', $tagOptions, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'tags']) }}
			</div>

			@if ($errors->has('tags'))
				<div class="alert alert-danger">{{ $errors->first('tags') }}</div>
			@endif


			{{-- Slug --}}

			<div class="form-group">
				{{ Form::label('slug', Lang::get('admin/newsArticles.labels.slug'), ['class' => 'control-label']) }}
				{{ Form::text('slug', null, ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.slug') }}</p>
			</div>

			@if ($errors->has('slug'))
				<div class="alert alert-danger">{{ $errors->first('slug') }}</div>
			@endif


			{{-- Published at --}}

			<div class="form-group">
				{{ Form::label('published_at', Lang::get('admin/newsArticles.labels.publishedAt'), ['class' => 'control-label']) }}
				{{ Form::datetime('published_at', $now->format(Lang::get('common.dateTimeFormat')), ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.publishedAt') }}</p>
			</div>

			@if ($errors->has('published_at'))
				<div class="alert alert-danger">{{ $errors->first('published_at') }}</div>
			@endif


			{{-- Sticky --}}

			<div class="form-group">
				{{ Form::label('sticky', Lang::get('admin/newsArticles.labels.sticky'), ['class' => 'control-label']) }}
				{{ Form::number('sticky', 0, 0, null, ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.sticky') }}</p>
			</div>

			@if ($errors->has('sticky'))
				<div class="alert alert-danger">{{ $errors->first('sticky') }}</div>
			@endif

		</div>
	</div>


	{{-- Content --}}

	<div class="panel panel-default">
		<div class="panel-heading">{{ Lang::get('admin/newsArticles.panelTitles.content') }}</div>
		<div class="panel-body">

			{{-- Lead --}}

			<div class="form-group form-group--redactor">
				{{ Form::label('lead', Lang::get('admin/newsArticles.labels.lead'), ['class' => 'control-label']) }}
				{{ Form::textarea('lead', null, ['class' => 'redactor form-control', 'data-min-height' => '150']) }}
			</div>

			@if ($errors->has('lead'))
				<div class="alert alert-danger">{{ $errors->first('lead') }}</div>
			@endif


			{{-- Content --}}

			<div class="form-group form-group--redactor">
				{{ Form::label('contents', Lang::get('admin/newsArticles.labels.contents'), ['class' => 'control-label']) }}
				{{ Form::textarea('contents', null, ['class' => 'redactor form-control']) }}
			</div>

			@if ($errors->has('contents'))
				<div class="alert alert-danger">{{ $errors->first('contents') }}</div>
			@endif


			{{-- Main image --}}

			<div class="form-group">
				{{ Form::label('image_main', Lang::get('admin/newsArticles.labels.imageMain'), ['class' => 'control-label']) }}
				{{ Form::file('image_main') }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.imageMain') }}</p>
			</div>

			@if ($errors->has('image_main'))
				<div class="alert alert-danger">{{ $errors->first('image_main') }}</div>
			@endif

		</div>
	</div>


	{{-- SEO --}}

	<div class="panel panel-default">
		<div class="panel-heading">{{ Lang::get('admin/newsArticles.panelTitles.seo') }}</div>
		<div class="panel-body">

			{{-- Title --}}

			<div class="form-group">
				{{ Form::label('meta_title', Lang::get('admin/newsArticles.labels.metaTitle'), ['class' => 'control-label']) }}
				{{ Form::text('meta_title', null, ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.metaTitle') }}</p>
			</div>


			{{-- Description --}}

			<div class="form-group">
				{{ Form::label('meta_description', Lang::get('admin/newsArticles.labels.metaDescription'), ['class' => 'control-label']) }}
				{{ Form::text('meta_description', null, ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/newsArticles.help.metaDescription') }}</p>
			</div>

		</div>
	</div>


	{{-- Submit button --}}

	<div class="form-group">
		{{
			Form::button(
				'<i class="fa fa-save"></i> '.Lang::get('admin/newsArticles.labels.save.default'),
				[
					'class' => 'btn btn-lg btn-primary',
					'type' => 'submit',
					'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.Lang::get('admin/newsArticles.labels.save.loading'),
				]
			)
		}}
	</div>

{{ Form::close() }}
