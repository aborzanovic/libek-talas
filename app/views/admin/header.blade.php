<header class="header">
	<a href="#" class="header-navToggleButton">
		<i class="fa fa-arrow-right"></i>
		<i class="fa fa-arrow-left"></i>
	</a>
	<div class="header-content">
		<p class="header-hello">{{{ Lang::get('admin/header.hello', ['name' => $currentUser->full_name ?: $currentUser->email]) }}}</p>

		@include('admin.header.logout')
	</div>
</header>
