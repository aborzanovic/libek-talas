<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@postUpdate');

?>

{{ Breadcrumbs::render('creitive/basic::admin.changePassword') }}

@foreach ($successMessages->all() as $message)
	<div class="alert alert-success">{{{ $message }}}</div>
@endforeach

{{ Form::open(['url' => $url, 'role' => 'form', 'autocomplete' => 'off']) }}

	{{-- Current password --}}

	<div class="form-group">
		{{ Form::label('current_password', Lang::get('admin/changePassword.labels.currentPassword')) }}

		{{ Form::password('current_password', ['autofocus', 'class' => 'form-control']) }}
	</div>

	@if ($errors->has('current_password'))
		<div class="alert alert-danger">{{ $errors->first('current_password') }}</div>
	@endif

	{{-- New password --}}

	<div class="form-group">
		{{ Form::label('new_password', Lang::get('admin/changePassword.labels.newPassword')) }}

		{{ Form::password('new_password', ['class' => 'form-control']) }}
	</div>

	@if ($errors->has('new_password'))
		<div class="alert alert-danger">{{ $errors->first('new_password') }}</div>
	@endif

	{{-- New password confirmation --}}

	<div class="form-group">
		{{ Form::label('new_password_confirmation', Lang::get('admin/changePassword.labels.newPasswordConfirmation')) }}

		{{ Form::password('new_password_confirmation', ['class' => 'form-control']) }}
	</div>

	@if ($errors->has('new_password_confirmation'))
		<div class="alert alert-danger">{{ $errors->first('new_password_confirmation') }}</div>
	@endif

	{{-- Submit button --}}

	<div class="form-group">
		{{
			Form::button(
				Lang::get('admin/changePassword.labels.update.default'),
				[
					'class' => 'btn btn-lg btn-primary',
					'type' => 'submit',
					'data-loading-text' => Lang::get('admin/changePassword.labels.update.loading'),
				]
			)
		}}
	</div>

{{ Form::close() }}
