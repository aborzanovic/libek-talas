<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@postLogout');

?>

{{ Form::open(['url' => $url, 'class' => 'header-logoutForm']) }}

	{{-- Submit button --}}

	<div class="form-group header-logoutButton--container">
		{{
			Form::button(
				'<i class="fa fa-sign-out"></i><span class="header-logoutButton-text"> '.Lang::get('admin/header.logout.default').'</span>',
				[
					'class' => 'header-logoutButton',
					'type' => 'submit',
					'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i><span class="header-logoutButton-text"> '.Lang::get('admin/header.logout.loading').'</span>',
				]
			)
		}}
	</div>

{{ Form::close() }}
