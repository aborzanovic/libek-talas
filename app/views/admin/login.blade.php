<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\LoginController@postLogin');

?>

<section class="container admin-login--container">
	<header class="admin-login--header">
		<h1><i class="fa fa-lock icon"></i> {{ Lang::get('login.title') }}</h1>
	</header>

	{{ Form::open(['url' => $url, 'role' => 'form', 'autocomplete' => 'off']) }}

		{{-- Email address --}}

		<div class="form-group">
			{{ Form::label('email', Lang::get('login.labels.email')) }}

			<i class="fa fa-envelope email-icon"></i>
			{{ Form::email('email', null, ['autofocus', 'class' => 'form-control', 'placeholder' => Lang::get('login.placeholders.email'), 'tabIndex' => 1]) }}
		</div>

		@if ($errors->has('email'))
			<div class="alert alert-danger">{{ $errors->first('email') }}</div>
		@endif

		{{-- Password --}}

		<div class="form-group">
			{{ Form::label('password', Lang::get('login.labels.password')) }}

			<i class="fa fa-lock password-icon"></i>
			{{ Form::password('password', ['class' => 'form-control', 'tabIndex' => 2]) }}
		</div>

		@if ($errors->has('password'))
			<div class="alert alert-danger">{{ $errors->first('password') }}</div>
		@endif

		{{-- Remember me --}}

		<div class="checkbox">
			<label>
				{{ Form::checkbox('remember_me', '1', Input::has('remember_me'), ['tabIndex' => 3]) }}
				{{ Lang::get('login.labels.rememberMe') }}
			</label>
		</div>

		{{-- Submit button --}}

		<div class="form-group">
			{{
				Form::button(
					Lang::get('login.labels.send.default'),
					[
						'class' => 'btn btn-lg btn-primary',
						'tabIndex' => 4,
						'type' => 'submit',
						'data-loading-text' => Lang::get('login.labels.send.loading'),
					]
				)
			}}
		</div>

	{{ Form::close() }}

</section>
