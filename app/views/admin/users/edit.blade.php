<?php

$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@update', ['user' => $user->id]);

?>

{{ Breadcrumbs::render('creitive/basic::admin.users.edit', $user) }}

@foreach ($successMessages->all() as $message)
	<div class="alert alert-success">{{ $message }}</div>
@endforeach

{{ Form::open(['url' => $url, 'method' => 'put']) }}

	{{-- Basic configuration --}}

	<div class="panel panel-default">
		<div class="panel-heading">{{{ Lang::get('admin/users.panelTitles.basicConfiguration') }}}</div>
		<div class="panel-body">

			{{-- Role --}}

			<div class="form-group">
				{{ Form::label('role', Lang::get('admin/users.labels.role'), ['class' => 'control-label']) }}
				{{ Form::select('role', $roleOptions, $user->role->slug, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('role'))
				<div class="alert alert-danger">{{ $errors->first('role') }}</div>
			@endif


			{{-- Email --}}

			<div class="form-group">
				{{ Form::label('email', Lang::get('admin/users.labels.email'), ['class' => 'control-label']) }}
				{{ Form::email('email', $user->email, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('email'))
				<div class="alert alert-danger">{{ $errors->first('email') }}</div>
			@endif


			{{-- Password --}}

			<div class="form-group">
				{{ Form::label('password', Lang::get('admin/users.labels.password'), ['class' => 'control-label']) }}
				{{ Form::password('password', ['class' => 'form-control']) }}
				<p class="help-block">{{ Lang::get('admin/users.help.password') }}</p>
			</div>

			@if ($errors->has('password'))
				<div class="alert alert-danger">{{ $errors->first('password') }}</div>
			@endif


			{{-- First name --}}

			<div class="form-group">
				{{ Form::label('first_name', Lang::get('admin/users.labels.firstName'), ['class' => 'control-label']) }}
				{{ Form::text('first_name', $user->first_name, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('first_name'))
				<div class="alert alert-danger">{{ $errors->first('first_name') }}</div>
			@endif


			{{-- Last name --}}

			<div class="form-group">
				{{ Form::label('last_name', Lang::get('admin/users.labels.lastName'), ['class' => 'control-label']) }}
				{{ Form::text('last_name', $user->last_name, ['class' => 'form-control']) }}
			</div>

			@if ($errors->has('last_name'))
				<div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
			@endif

		</div>
	</div>

	{{-- Submit button --}}

	<div class="form-group">
		{{
			Form::button(
				'<i class="fa fa-save"></i> '.Lang::get('admin/users.labels.save.default'),
				[
					'class' => 'btn btn-lg btn-primary',
					'type' => 'submit',
					'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.Lang::get('admin/users.labels.save.loading'),
				]
			)
		}}
	</div>

{{ Form::close() }}
