<?php

$currentUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index');
$createUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@create');

?>

{{ Breadcrumbs::render('creitive/basic::admin.users') }}

@foreach ($successMessages->all() as $message)
	<div class="alert alert-success">{{{ $message }}}</div>
@endforeach

<div>
	<a href="{{{ $currentUrl }}}" class="btn btn-xs btn-primary"><i class="fa fa-filter"></i> {{{ Lang::get('admin/users.index.links.roles.all') }}}</a>
	<a href="{{{ $currentUrl }}}?roles[]=admin" class="btn btn-xs btn-primary"><i class="fa fa-filter"></i> {{{ Lang::get('admin/users.index.links.roles.admins') }}}</a>
	<a href="{{{ $currentUrl }}}?roles[]=guest" class="btn btn-xs btn-primary"><i class="fa fa-filter"></i> {{{ Lang::get('admin/users.index.links.roles.guests') }}}</a>
</div>

<div class="itemList">
	<div class="itemList-actions">
		<div class="pull-right">
			<a href="{{ $createUrl }}" class="itemList-iconLink btn btn-xs btn-primary">
				<i class="fa fa-fw fa-plus-square"></i>
				<span class="hidden-xs">{{ Lang::get('admin/users.index.links.create') }}</span>
			</a>
		</div>
	</div>
	<div class="itemList-head">
		<div class="col-xs-1 itemList-column">
			{{ Lang::get('admin/users.labels.id') }}
		</div>
		<div class="col-xs-3 itemList-column">
			{{ Lang::get('admin/users.labels.fullName') }}
		</div>
		<div class="col-xs-3 itemList-column">
			{{ Lang::get('admin/users.labels.email') }}
		</div>
		<div class="col-xs-2 itemList-column">
			{{ Lang::get('admin/users.labels.roles') }}
		</div>
	</div>
	<div class="itemList-rootContainer">
		{{ View::make('admin.users.index.list', ['users' => $users])->render() }}
	</div>
</div>
