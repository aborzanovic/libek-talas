<ol class="itemList-list">
	@foreach ($users as $user)
		{{ View::make('admin.users.index.item', ['user' => $user])->render() }}
	@endforeach
</ol>
