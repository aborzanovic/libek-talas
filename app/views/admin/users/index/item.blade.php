<?php

$editUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit', ['user' => $user->id]);
$deleteUrl = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@delete', ['user' => $user->id]);

$roles = $user->roles->implode('name', ', ');

?>

<li class="itemList-item" data-id="{{ $user->id }}">
	<div class="itemList-item-contents">
		<div class="col-xs-1 itemList-column">
			{{{ $user->id }}}
		</div>
		<div class="col-xs-3 itemList-column">
			{{{ $user->full_name }}}
		</div>
		<div class="col-xs-3 itemList-column">
			{{{ $user->email }}}
		</div>
		<div class="col-xs-2 itemList-column">
			{{{ $roles }}}
		</div>

		<div class="col-xs-3 itemList-column itemList-noDrag">
			<div class="pull-right">
				<a href="{{ $editUrl }}" class="itemList-iconLink">
					<i class="fa fa-pencil"></i>
					<span class="hidden-xs">{{ Lang::get('admin/users.index.links.edit') }}</span>
				</a>
				<a href="{{ $deleteUrl }}" class="itemList-iconLink">
					<i class="fa fa-trash-o"></i>
					<span class="hidden-xs">{{ Lang::get('admin/users.index.links.delete') }}</span>
				</a>
			</div>
		</div>
	</div>
</li>
