<div class="container d-none d-sm-block">
    <div class="slideshow">
        <div id="homeCarousel" class="slideshow-box carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($slideshowArticles as $index => $topArticle)
                    <li data-target="#homeCarousel" data-slide-to="{{ $index }}" class="{{ $index === 0 ? 'active' : '' }}">0{{ $index + 1 }}</li>
                @endforeach
            </ol>
            

            <div class="slideshow-container carousel-inner">  
            @foreach($slideshowArticles as $index => $topArticle)
                <?php $url = $urlGenerator($topArticle); ?>  
                <div class="carousel-item {{ $index === 0 ? 'active' : '' }}" style="height: 100%">
                    <div class="slideshow-text">
                        <div class="slideshow-title">
                           <a href="{{$url}}" class="normalize-link"> <h4 style="font-size: 36px; !important; font-weight: bold;">{{$topArticle->title}}</h4></a>
                        </div>
                        <!--
                        <div class="slideshow-author">
                            Autor : <a class="normalize-link" href="/sr/vesti/author/{{$mostPopularAuthors[$topArticle->id]->id}}">{{ $mostPopularAuthors[$topArticle->id]->first_name.' '. $mostPopularAuthors[$topArticle->id]->last_name }}</a>
                        </div>-->
                            <?php $cnt = count($mostPopularTags[$topArticle->id]); ?>
                            <!--
                          <div class="slideshow-tags">
                            Tag : @foreach($mostPopularTags[$topArticle->id] as $mpt) <a class="normalize-link" href="/sr/vesti/tag/{{$mpt->id}}"> {{ $mpt->name }} </a> @if(--$cnt>0){{', '}}@endif @endforeach
                        </div>-->
                        <?php
                            if (isset($topArticle->lead) && !empty($topArticle->lead)) {
                                $lead = $topArticle->lead;
                            } else {
                                $lead = $topArticle->contents;
                            }
                        ?>
                        <p class="slideshow-lead">{{ str_limit(strip_tags($lead), 340) }}</p>
                        
                        <a href="{{$url}}">
                            <img src="{{ url('/images/talas/arrow.png') }}" class="slideshow-arrow">
                        </a>
                        <img src="{{ url('/images/talas/text-bg-shape.png') }}" class="slidehow-text-shape"> 
                    </div>
                    <div class="slideshow-image" style="background-image: url('{{$topArticle->getImage('main', 'full')}}')"></div>
                </div> 
                 @endforeach
            </div>
        </div>
        <div class="bg-stripes">
            <img src="../../images/talas/bg3.png" class="bg-stripes">
        </div>
    </div>
</div>