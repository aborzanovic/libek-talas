<style type="text/css">
	.carousel-indicators {
   bottom: 10px !important;
}
</style>


<div class="row most-popular d-none d-sm-block">
	<div class="pop-invisible">
		<img src="{{ url('images/talas/most-pop-stripes.png') }}" class="most-pop-stripes">
	</div>
	<div class="col-md-3 mx-auto most-popular-sticker">
		<h4><b>Najčitanije vesti</b></h4>
	</div>
	<div id="mostPopularCarousel" class="slideshow-box carousel slide" data-ride="carousel">
		<div class="slideshow-container carousel-inner">
			<ol class="carousel-indicators">
				@foreach($mostPopularArticles as $index => $mostPopularArticlePage)
					<li data-target="#mostPopularCarousel" data-slide-to="{{ $index }}" class="{{ $index === 0 ? 'active' : '' }}">0{{ $index + 1 }}</li>
				@endforeach
			</ol>
			@foreach($mostPopularArticles as $index => $mostPopularArticlePage)
			<div class="container carousel-item {{ $index === 0 ? 'active' : '' }}" style="height: 100%">
				<div class="row" style="width: 100%">
				@foreach($mostPopularArticlePage as $mostPopularArticle)
				<div class="col-md-3 mx-auto pop-news-holder" style="overflow: hidden">
					<a href="{{ $urlGenerator($mostPopularArticle) }}" class="home-news-link normalize-link">
							 <div class="news-image pop-news-image"
								  style="background-image: url('{{$mostPopularArticle->getImage('main', 'thumbnail')}}')">
							</div>
 							<div class="pop-news-content">
							<article class="">
								 <h4><b>{{$mostPopularArticle->title}}</b></h4>								 
							 </article>
						 </div>
					</a> 
				 </div>
				@endforeach
				</div>
			</div>
			@endforeach
		</div>
	</div>			
</div>
