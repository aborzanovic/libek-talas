<?php $url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@postApplicationForm', ['slug' => $applicationForm->slug]); ?>

<div class="application-form--background"></div>
<div class="application-form--background-cover"></div>

<div class="application-form--container container-fluid">
	<div class="application-form--contents container">
		<article>
			@if ($successMessages->isEmpty())
				<div class="application-form--form-container">
					<h1>{{{ $applicationForm->title }}}</h1>

					{{ Form::open(['url' => $url, 'role' => 'form', 'files' => true]) }}

						{{-- Personal info --}}

						<h2>{{ Lang::get('applicationForms.academy.titles.personalInfo') }}</h2>

							{{-- Full name --}}

							<div class="form-group">
								{{ Form::label('full_name', Lang::get('applicationForms.academy.labels.fullName')) }}
								{{ Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('full_name'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.academy.errors.fullName') }}</div>
							@endif

							{{-- Birthdate --}}

							<div class="form-group">
								{{ Form::label('birthdate', Lang::get('applicationForms.academy.labels.birthdate')) }}
								{{ Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- Sex --}}

							<div class="form-group">
								<?php
									$sexes = [
										'm' => Lang::get('applicationForms.academy.sexes.m'),
										'f' => Lang::get('applicationForms.academy.sexes.f'),
									];
								?>
								{{ Lang::get('applicationForms.academy.labels.sex') }}
								{{ Form::inlineRadios('sex', $sexes, 'm') }}
							</div>

							{{-- CV --}}

							<div class="form-group">
								{{ Form::label('cv', Lang::get('applicationForms.academy.labels.cv')) }}
								{{ Form::hidden('MAX_FILE_SIZE', 10485760) }}
								{{ Form::file('cv') }}
							</div>

							@if ($errors->has('cv'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.academy.errors.cv') }}</div>
							@endif

						{{-- Contact info --}}

						<h2>{{ Lang::get('applicationForms.academy.titles.contactInfo') }}</h2>

							{{-- Email address --}}

							<div class="form-group">
								{{ Form::label('email', Lang::get('applicationForms.academy.labels.email')) }}
								{{ Form::email('email', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('email'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.academy.errors.email') }}</div>
							@endif

							{{-- Phone --}}

							<div class="form-group">
								{{ Form::label('phone', Lang::get('applicationForms.academy.labels.phone')) }}
								{{ Form::text('phone', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Education --}}

						<h2>{{ Lang::get('applicationForms.academy.titles.education') }}</h2>

							{{-- Faculty --}}

							<div class="form-group">
								{{ Form::label('faculty', Lang::get('applicationForms.academy.labels.faculty')) }}
								{{ Form::text('faculty', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- Area of study --}}

							<div class="form-group">
								{{ Form::label('area_of_study', Lang::get('applicationForms.academy.labels.areaOfStudy')) }}
								{{ Form::textarea('area_of_study', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- History --}}

							<div class="form-group">
								{{ Form::label('history', Lang::get('applicationForms.academy.labels.history')) }}
								{{ Form::textarea('history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Impressions --}}

							<div class="form-group">
								{{ Form::label('impressions', Lang::get('applicationForms.academy.labels.impressions')) }}
								{{ Form::textarea('impressions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Libek history --}}

							<div class="form-group">
								{{ Form::label('libek_history', Lang::get('applicationForms.academy.labels.libekHistory')) }}
								{{ Form::textarea('libek_history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Memberships --}}

							<div class="form-group">
								{{ Form::label('memberships', Lang::get('applicationForms.academy.labels.memberships')) }}
								{{ Form::textarea('memberships', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Personal attitude --}}

						<h2>{{ Lang::get('applicationForms.academy.titles.personalAttitude') }}</h2>

							{{-- Problems --}}

							<div class="form-group">
								{{ Form::label('problems', Lang::get('applicationForms.academy.labels.problems')) }}
								{{ Form::textarea('problems', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Solutions --}}

							<div class="form-group">
								{{ Form::label('solutions', Lang::get('applicationForms.academy.labels.solutions')) }}
								{{ Form::textarea('solutions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Future --}}

							<div class="form-group">
								{{ Form::label('future', Lang::get('applicationForms.academy.labels.future')) }}
								{{ Form::textarea('future', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Ego --}}

							<div class="form-group">
								{{ Form::label('ego', Lang::get('applicationForms.academy.labels.ego')) }}
								{{ Form::textarea('ego', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Other --}}

						<h2>{{ Lang::get('applicationForms.academy.titles.other') }}</h2>

							{{-- Expectations --}}

							<div class="form-group">
								{{ Form::label('expectations', Lang::get('applicationForms.academy.labels.expectations')) }}
								{{ Form::textarea('expectations', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Reference --}}

							<div class="form-group">
								{{ Form::label('reference', Lang::get('applicationForms.academy.labels.reference')) }}
								{{ Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Needs --}}

							<div class="form-group">
								{{ Form::label('needs', Lang::get('applicationForms.academy.labels.needs')) }}
								{{ Form::textarea('needs', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Submit button --}}

						<div class="form-group">
							{{
								Form::button(
									Lang::get('applicationForms.academy.labels.send.default'),
									[
										'class' => 'btn btn-lg btn-primary',
										'type' => 'submit',
										'data-loading-text' => Lang::get('applicationForms.academy.labels.send.loading'),
									]
								)
							}}
						</div>

					{{ Form::close() }}

					<div class="alert alert-info">{{ Lang::get('applicationForms.academy.notes.problems') }}</div>
					<div class="alert alert-info">{{ Lang::get('applicationForms.academy.notes.privacy') }}</div>

				</div>
			@else
				<div class="alert alert-success">{{ $successMessages->first() }}</div>
			@endif
		</article>
	</div>
</div>
