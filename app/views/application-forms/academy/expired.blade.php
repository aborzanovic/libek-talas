<div class="application-form--background"></div>
<div class="application-form--background-cover"></div>

<div class="application-form--container container-fluid">
	<div class="application-form--contents container">
		<article>
			<div class="application-form--form-container">
				<h1>{{{ $applicationForm->title }}}</h1>

				<div class="alert alert-danger">{{ Lang::get('applicationForms.academy.expiredNote') }}</div>
			</div>
		</article>
	</div>
</div>
