<?php $data = $submittedApplication->data; ?>
<html>
<head>
	<meta charset="utf-8">
	<title>{{{ $subject }}}</title>
</head>
<body>

<h1>{{{ Lang::get('applicationForms.academy.applicationTitle', ['title' => $applicationForm->title]) }}}</h1>


<h2>{{ Lang::get('applicationForms.academy.titles.personalInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.academy.labels.fullName') }}}</b>
<br />
{{{ $data['full_name'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.birthdate') }}}</b>
<br />
{{{ $data['birthdate'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.sex') }}}</b>
<br />
{{{ Lang::get('applicationForms.academy.sexes.'.$data['sex']) }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.cvInAttachment') }}}</b>
<br />


<h2>{{ Lang::get('applicationForms.academy.titles.contactInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.academy.labels.email') }}}</b>
<br />
{{{ $data['email'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.phone') }}}</b>
<br />
{{{ $data['phone'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.academy.titles.education') }}</h2>

<b>{{{ Lang::get('applicationForms.academy.labels.faculty') }}}</b>
<br />
{{{ $data['faculty'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.areaOfStudy') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['area_of_study'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.history') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['history'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.impressions') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['impressions'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.libekHistory') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['libek_history'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.memberships') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['memberships'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.academy.titles.personalAttitude') }}</h2>

<b>{{{ Lang::get('applicationForms.academy.labels.problems') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['problems'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.solutions') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['solutions'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.future') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['future'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.ego') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['ego'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.academy.titles.other') }}</h2>

<b>{{{ Lang::get('applicationForms.academy.labels.expectations') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['expectations'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.reference') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['reference'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.academy.labels.needs') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['needs'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

</body>
</html>
