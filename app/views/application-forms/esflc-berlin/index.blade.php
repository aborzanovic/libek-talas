<?php $url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@postApplicationForm', ['slug' => $applicationForm->slug]); ?>

<div class="application-form--background"></div>
<div class="application-form--background-cover"></div>

<div class="application-form--container container-fluid">
	<div class="application-form--contents container">
		<article>
			@if ($successMessages->isEmpty())
				<div class="application-form--form-container">
					<h1>{{{ $applicationForm->title }}}</h1>

					{{ Form::open(['url' => $url, 'role' => 'form', 'files' => true]) }}

						{{-- Personal info --}}

						<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.personalInfo') }}</h2>

							{{-- Full name --}}

							<div class="form-group">
								{{ Form::label('full_name', Lang::get('applicationForms.esflcBerlin.labels.fullName')) }}
								{{ Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('full_name'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.esflcBerlin.errors.fullName') }}</div>
							@endif

							{{-- Birthdate --}}

							<div class="form-group">
								{{ Form::label('birthdate', Lang::get('applicationForms.esflcBerlin.labels.birthdate')) }}
								{{ Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Contact info --}}

						<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.contactInfo') }}</h2>

							{{-- Email address --}}

							<div class="form-group">
								{{ Form::label('email', Lang::get('applicationForms.esflcBerlin.labels.email')) }}
								{{ Form::email('email', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('email'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.esflcBerlin.errors.email') }}</div>
							@endif

							{{-- Phone --}}

							<div class="form-group">
								{{ Form::label('phone', Lang::get('applicationForms.esflcBerlin.labels.phone')) }}
								{{ Form::text('phone', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Education --}}

						<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.education') }}</h2>

							{{-- Faculty --}}

							<div class="form-group">
								{{ Form::label('faculty', Lang::get('applicationForms.esflcBerlin.labels.faculty')) }}
								{{ Form::text('faculty', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- Memberships --}}

							<div class="form-group">
								{{ Form::label('memberships', Lang::get('applicationForms.esflcBerlin.labels.memberships')) }}
								{{ Form::textarea('memberships', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Libek history --}}

							<div class="form-group">
								{{ Form::label('libek_history', Lang::get('applicationForms.esflcBerlin.labels.libekHistory')) }}
								{{ Form::textarea('libek_history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Other --}}

						<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.other') }}</h2>

							{{-- Reference --}}

							<div class="form-group">
								{{ Form::label('reference', Lang::get('applicationForms.esflcBerlin.labels.reference')) }}
								{{ Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Motivation --}}

							<div class="form-group">
								{{ Form::label('motivation', Lang::get('applicationForms.esflcBerlin.labels.motivation')) }}
								{{ Form::textarea('motivation', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Future --}}

							<div class="form-group">
								{{ Form::label('future', Lang::get('applicationForms.esflcBerlin.labels.future')) }}
								{{ Form::textarea('future', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Submit button --}}

						<div class="form-group">
							{{
								Form::button(
									Lang::get('applicationForms.esflcBerlin.labels.send.default'),
									[
										'class' => 'btn btn-lg btn-primary',
										'type' => 'submit',
										'data-loading-text' => Lang::get('applicationForms.esflcBerlin.labels.send.loading'),
									]
								)
							}}
						</div>

					{{ Form::close() }}

					<div class="alert alert-info">{{ Lang::get('applicationForms.esflcBerlin.notes.problems') }}</div>
					<div class="alert alert-info">{{ Lang::get('applicationForms.esflcBerlin.notes.privacy') }}</div>

				</div>
			@else
				<div class="alert alert-success">{{ $successMessages->first() }}</div>
			@endif
		</article>
	</div>
</div>
