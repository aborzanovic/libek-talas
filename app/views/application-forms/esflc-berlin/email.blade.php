<?php $data = $submittedApplication->data; ?>
<html>
<head>
	<meta charset="utf-8">
	<title>{{{ $subject }}}</title>
</head>
<body>

<h1>{{{ Lang::get('applicationForms.esflcBerlin.applicationTitle', ['title' => $applicationForm->title]) }}}</h1>


<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.personalInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.fullName') }}}</b>
<br />
{{{ $data['full_name'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.birthdate') }}}</b>
<br />
{{{ $data['birthdate'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.contactInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.email') }}}</b>
<br />
{{{ $data['email'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.phone') }}}</b>
<br />
{{{ $data['phone'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.education') }}</h2>

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.faculty') }}}</b>
<br />
{{{ $data['faculty'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.memberships') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['memberships'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.libekHistory') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['libek_history'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.esflcBerlin.titles.other') }}</h2>

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.reference') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['reference'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.motivation') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['motivation'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.esflcBerlin.labels.future') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['future'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

</body>
</html>
