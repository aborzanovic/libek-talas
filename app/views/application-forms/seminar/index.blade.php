<?php $url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@postApplicationForm', ['slug' => $applicationForm->slug]); ?>

<div class="application-form--background"></div>
<div class="application-form--background-cover"></div>

<div class="application-form--container container-fluid">
	<div class="application-form--contents container">
		<article>
			@if ($successMessages->isEmpty())
				<div class="application-form--form-container">
					<h1>{{{ $applicationForm->title }}}</h1>

					{{ Form::open(['url' => $url, 'role' => 'form', 'files' => true]) }}

						{{-- Personal info --}}

						<h2>{{ Lang::get('applicationForms.seminar.titles.personalInfo') }}</h2>

							{{-- Full name --}}

							<div class="form-group">
								{{ Form::label('full_name', Lang::get('applicationForms.seminar.labels.fullName')) }}
								{{ Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('full_name'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.seminar.errors.fullName') }}</div>
							@endif

							{{-- Birthdate --}}

							<div class="form-group">
								{{ Form::label('birthdate', Lang::get('applicationForms.seminar.labels.birthdate')) }}
								{{ Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- Sex --}}

							<div class="form-group">
								<?php
									$sexes = [
										'm' => Lang::get('applicationForms.seminar.sexes.m'),
										'f' => Lang::get('applicationForms.seminar.sexes.f'),
									];
								?>
								{{ Lang::get('applicationForms.seminar.labels.sex') }}
								{{ Form::inlineRadios('sex', $sexes, 'm') }}
							</div>

						{{-- Contact info --}}

						<h2>{{ Lang::get('applicationForms.seminar.titles.contactInfo') }}</h2>

							{{-- Email address --}}

							<div class="form-group">
								{{ Form::label('email', Lang::get('applicationForms.seminar.labels.email')) }}
								{{ Form::email('email', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('email'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.seminar.errors.email') }}</div>
							@endif

							{{-- Phone --}}

							<div class="form-group">
								{{ Form::label('phone', Lang::get('applicationForms.seminar.labels.phone')) }}
								{{ Form::text('phone', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Education --}}

						<h2>{{ Lang::get('applicationForms.seminar.titles.education') }}</h2>

							{{-- School --}}

							<div class="form-group">
								{{ Form::label('school', Lang::get('applicationForms.seminar.labels.school')) }}
								{{ Form::text('school', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- History --}}

							<div class="form-group">
								{{ Form::label('history', Lang::get('applicationForms.seminar.labels.history')) }}
								{{ Form::textarea('history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Memberships --}}

							<div class="form-group">
								{{ Form::label('memberships', Lang::get('applicationForms.seminar.labels.memberships')) }}
								{{ Form::textarea('memberships', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Faculty --}}

							<div class="form-group">
								{{ Form::label('faculty', Lang::get('applicationForms.seminar.labels.faculty')) }}
								{{ Form::textarea('faculty', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Personal attitude --}}

						<h2>{{ Lang::get('applicationForms.seminar.titles.personalAttitude') }}</h2>

							{{-- Problems --}}

							<div class="form-group">
								{{ Form::label('problems', Lang::get('applicationForms.seminar.labels.problems')) }}
								{{ Form::textarea('problems', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Solutions --}}

							<div class="form-group">
								{{ Form::label('solutions', Lang::get('applicationForms.seminar.labels.solutions')) }}
								{{ Form::textarea('solutions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Ego --}}

							<div class="form-group">
								{{ Form::label('ego', Lang::get('applicationForms.seminar.labels.ego')) }}
								{{ Form::textarea('ego', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Other --}}

						<h2>{{ Lang::get('applicationForms.seminar.titles.other') }}</h2>

							{{-- Expectations --}}

							<div class="form-group">
								{{ Form::label('expectations', Lang::get('applicationForms.seminar.labels.expectations')) }}
								{{ Form::textarea('expectations', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Reference --}}

							<div class="form-group">
								{{ Form::label('reference', Lang::get('applicationForms.seminar.labels.reference')) }}
								{{ Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Needs --}}

							<div class="form-group">
								{{ Form::label('needs', Lang::get('applicationForms.seminar.labels.needs')) }}
								{{ Form::textarea('needs', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Submit button --}}

						<div class="form-group">
							{{
								Form::button(
									Lang::get('applicationForms.seminar.labels.send.default'),
									[
										'class' => 'btn btn-lg btn-primary',
										'type' => 'submit',
										'data-loading-text' => Lang::get('applicationForms.seminar.labels.send.loading'),
									]
								)
							}}
						</div>

					{{ Form::close() }}

					<div class="alert alert-info">{{ Lang::get('applicationForms.seminar.notes.problems') }}</div>
					<div class="alert alert-info">{{ Lang::get('applicationForms.seminar.notes.privacy') }}</div>

				</div>
			@else
				<div class="alert alert-success">{{ $successMessages->first() }}</div>
			@endif
		</article>
	</div>
</div>
