<?php $data = $submittedApplication->data; ?>
<html>
<head>
	<meta charset="utf-8">
	<title>{{{ $subject }}}</title>
</head>
<body>

<h1>{{{ Lang::get('applicationForms.seminar.applicationTitle', ['title' => $applicationForm->title]) }}}</h1>


<h2>{{ Lang::get('applicationForms.seminar.titles.personalInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.seminar.labels.fullName') }}}</b>
<br />
{{{ $data['full_name'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.birthdate') }}}</b>
<br />
{{{ $data['birthdate'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.sex') }}}</b>
<br />
{{{ Lang::get('applicationForms.seminar.sexes.'.$data['sex']) }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.seminar.titles.contactInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.seminar.labels.email') }}}</b>
<br />
{{{ $data['email'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.phone') }}}</b>
<br />
{{{ $data['phone'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.seminar.titles.education') }}</h2>

<b>{{{ Lang::get('applicationForms.seminar.labels.school') }}}</b>
<br />
{{{ $data['school'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.history') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['history'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.memberships') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['memberships'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.faculty') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['faculty'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.seminar.titles.personalAttitude') }}</h2>

<b>{{{ Lang::get('applicationForms.seminar.labels.problems') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['problems'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.solutions') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['solutions'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.ego') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['ego'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.seminar.titles.other') }}</h2>

<b>{{{ Lang::get('applicationForms.seminar.labels.expectations') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['expectations'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.reference') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['reference'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.seminar.labels.needs') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['needs'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

</body>
</html>
