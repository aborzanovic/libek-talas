<?php $data = $submittedApplication->data; ?>
<html>
<head>
	<meta charset="utf-8">
	<title>{{{ $subject }}}</title>
</head>
<body>

<h1>{{{ Lang::get('applicationForms.johnGalt.applicationTitle', ['title' => $applicationForm->title]) }}}</h1>


<h2>{{ Lang::get('applicationForms.johnGalt.titles.personalInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.johnGalt.labels.fullName') }}}</b>
<br />
{{{ $data['full_name'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.birthdate') }}}</b>
<br />
{{{ $data['birthdate'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.sex') }}}</b>
<br />
{{{ Lang::get('applicationForms.johnGalt.sexes.'.$data['sex']) }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.cvInAttachment') }}}</b>
<br />


<h2>{{ Lang::get('applicationForms.johnGalt.titles.contactInfo') }}</h2>

<b>{{{ Lang::get('applicationForms.johnGalt.labels.email') }}}</b>
<br />
{{{ $data['email'] }}}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.phone') }}}</b>
<br />
{{{ $data['phone'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.johnGalt.titles.education') }}</h2>

<b>{{{ Lang::get('applicationForms.johnGalt.labels.faculty') }}}</b>
<br />
{{{ $data['faculty'] }}}
<br />
<br />


<h2>{{ Lang::get('applicationForms.johnGalt.titles.personalAttitude') }}</h2>

<b>{{{ Lang::get('applicationForms.johnGalt.labels.motivation') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['motivation'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.future') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['future'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.ego') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['ego'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.quote') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['quote'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />


<h2>{{ Lang::get('applicationForms.johnGalt.titles.other') }}</h2>

<b>{{{ Lang::get('applicationForms.johnGalt.labels.reference') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['reference'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.expectations') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['expectations'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

<b>{{{ Lang::get('applicationForms.johnGalt.labels.needs') }}}</b>
<br />
{{ nl2br(htmlspecialchars($data['needs'], ENT_QUOTES | ENT_HTML401)) }}
<br />
<br />

</body>
</html>
