<?php $url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\ApplicationFormController@postApplicationForm', ['slug' => $applicationForm->slug]); ?>

<div class="application-form--background"></div>
<div class="application-form--background-cover"></div>

<div class="application-form--container container-fluid">
	<div class="application-form--contents container">
		<article>
			@if ($successMessages->isEmpty())
				<div class="application-form--form-container">
					<h1>{{{ $applicationForm->title }}}</h1>

					{{ Form::open(['url' => $url, 'role' => 'form', 'files' => true]) }}

						{{-- Personal info --}}

						<h2>{{ Lang::get('applicationForms.johnGalt.titles.personalInfo') }}</h2>

							{{-- Full name --}}

							<div class="form-group">
								{{ Form::label('full_name', Lang::get('applicationForms.johnGalt.labels.fullName')) }}
								{{ Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('full_name'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.johnGalt.errors.fullName') }}</div>
							@endif

							{{-- Birthdate --}}

							<div class="form-group">
								{{ Form::label('birthdate', Lang::get('applicationForms.johnGalt.labels.birthdate')) }}
								{{ Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							{{-- Sex --}}

							<div class="form-group">
								<?php
									$sexes = [
										'm' => Lang::get('applicationForms.johnGalt.sexes.m'),
										'f' => Lang::get('applicationForms.johnGalt.sexes.f'),
									];
								?>
								{{ Lang::get('applicationForms.johnGalt.labels.sex') }}
								{{ Form::inlineRadios('sex', $sexes, 'm') }}
							</div>

							{{-- CV --}}

							<div class="form-group">
								{{ Form::label('cv', Lang::get('applicationForms.johnGalt.labels.cv')) }}
								{{ Form::hidden('MAX_FILE_SIZE', 10485760) }}
								{{ Form::file('cv') }}
							</div>

							@if ($errors->has('cv'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.johnGalt.errors.cv') }}</div>
							@endif

						{{-- Contact info --}}

						<h2>{{ Lang::get('applicationForms.johnGalt.titles.contactInfo') }}</h2>

							{{-- Email address --}}

							<div class="form-group">
								{{ Form::label('email', Lang::get('applicationForms.johnGalt.labels.email')) }}
								{{ Form::email('email', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

							@if ($errors->has('email'))
								<div class="alert alert-danger">{{ Lang::get('applicationForms.johnGalt.errors.email') }}</div>
							@endif

							{{-- Phone --}}

							<div class="form-group">
								{{ Form::label('phone', Lang::get('applicationForms.johnGalt.labels.phone')) }}
								{{ Form::text('phone', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Education --}}

						<h2>{{ Lang::get('applicationForms.johnGalt.titles.education') }}</h2>

							{{-- Faculty --}}

							<div class="form-group">
								{{ Form::label('faculty', Lang::get('applicationForms.johnGalt.labels.faculty')) }}
								{{ Form::text('faculty', null, ['class' => 'form-control form-control--application-form']) }}
							</div>

						{{-- Personal attitude --}}

						<h2>{{ Lang::get('applicationForms.johnGalt.titles.personalAttitude') }}</h2>

							{{-- Motivation --}}

							<div class="form-group">
								{{ Form::label('motivation', Lang::get('applicationForms.johnGalt.labels.motivation')) }}
								{{ Form::textarea('motivation', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Future --}}

							<div class="form-group">
								{{ Form::label('future', Lang::get('applicationForms.johnGalt.labels.future')) }}
								{{ Form::textarea('future', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Ego --}}

							<div class="form-group">
								{{ Form::label('ego', Lang::get('applicationForms.johnGalt.labels.ego')) }}
								{{ Form::textarea('ego', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Quote --}}

							<div class="form-group">
								{{ Form::label('quote', Lang::get('applicationForms.johnGalt.labels.quote')) }}
								{{ Form::textarea('quote', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Other --}}

						<h2>{{ Lang::get('applicationForms.johnGalt.titles.other') }}</h2>

							{{-- Reference --}}

							<div class="form-group">
								{{ Form::label('reference', Lang::get('applicationForms.johnGalt.labels.reference')) }}
								{{ Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

							{{-- Expectations --}}

							<div class="form-group">
								{{ Form::label('expectations', Lang::get('applicationForms.johnGalt.labels.expectations')) }}
								{{ Form::textarea('expectations', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>


							{{-- Needs --}}

							<div class="form-group">
								{{ Form::label('needs', Lang::get('applicationForms.johnGalt.labels.needs')) }}
								{{ Form::textarea('needs', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) }}
							</div>

						{{-- Submit button --}}

						<div class="form-group">
							{{
								Form::button(
									Lang::get('applicationForms.johnGalt.labels.send.default'),
									[
										'class' => 'btn btn-lg btn-primary',
										'type' => 'submit',
										'data-loading-text' => Lang::get('applicationForms.johnGalt.labels.send.loading'),
									]
								)
							}}
						</div>

					{{ Form::close() }}

					<div class="alert alert-info">{{ Lang::get('applicationForms.johnGalt.notes.problems') }}</div>
					<div class="alert alert-info">{{ Lang::get('applicationForms.johnGalt.notes.privacy') }}</div>

				</div>
			@else
				<div class="alert alert-success">{{ $successMessages->first() }}</div>
			@endif
		</article>
	</div>
</div>
