@extends('partials.hero.hero')
@section('heroTitle')
	@if($listingType == 'author')
    {{ $listingAuthor->name }}
    @elseif($listingType == 'tag')
    {{ $listingTag->name }}
    @endif
@stop