<div class="content-row">
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-12 supp-desc1">
                <article>

                    <p>U upornoj izgradnji vizije kojoj težimo od Libekovog osnivanja pre više od osam godina, stalno
                        unapređujemo svoje aktivnosti i dopiremo do što većeg broja ljudi. </p>

                    <p>Ovo ne bismo uspevali bez vaše podrške - podrške mnogih prijatelja, domaćih i međunarodnih
                        saradnika, partnera kao i predavača, mentora i naših alumnista. </p>

                    <p>Hvala vam na stalnoj podršci.</p>


                    <p>Postanite, ukoliko već niste, prijatelj Libeka podržavajući dalje unapređenje obrazovnih,
                        istraživačkih i izdavačkih aktivnosti.</p>

                    <p>Zajedno u borbi za slobodno društvo odgovornih pojedinaca.</p>

                    <p>Podrži Libek svojom ličnom ili donacijom svog preduzeća, u RSD ili kriptovaluti.</p>



                </article>

            </div>
            <p>Model uplatnice:</p>
            <img src= "/images/uplatnica.png" />
            <div class="col-md-12 supp-desc2">




                <p>Instrukcija za plaćanje u evrima EUR:</p>

                <p>

                    Bank name: Unicredit Bank</br>
                    Bank Address: Rajiceva 27-29, 11000 Belgrade, Serbia</br>
                    Account Name: Udruzenje gradjana Libertarijanski Klub</br>
                    IBAN: RS35170003000560800126</br>
                    SWIFT/BIC: BACXRSBG
                </p>

                <p>

                    Instrukcija za plaćenje u dolarima USD:
                </p>

                <p>

                    Bank name: Unicredit Bank</br>
                    Bank Address: Rajiceva 27-29, 11000 Belgrade, Serbia</br>
                    Account Name: Udruzenje gradjana Libertarijanski Klub</br>
                    IBAN: RS35170003000560800223</br>
                    SWIFT/BIC: BACXRSBG
                </p>
					
					<p>
					Kriptovalute:</br>

					Bitcoin (BTC) adresa:</br>

					1KKN2eg6CAAs7yUMbNZk8bWUxPeqkVyu4f</br>

					Ethereum (ETH) adresa:</br>

					0xAD00D0737D819475Fb4Fd68485e2ef1D8845FbCa</br>

					Litecoin (LTC) adresa:</br>

				    LKWNMEFEZueLi3SKnfvNc1Gk6Fj2ZVeUNH</br>
					</p>
				
            </div>


        </div>
    </div>
</div>