<div class="content-row">
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-12 supp-desc1">
                <article>

                    <p>Libek has been promoting the values of free and responsible society in Serbia for over eight
                        years, constantly improving activities and ensuring impact on political and social debate.</p>

                    <p>We would have not been able to do this without support from you, our friends and donors, partners
                        from around the world and Serbia, as well as the support from our lecturers, mentors and
                        alumni.</p>

                    <p>Thank you for your continuous support..</p>


                    <p>If you would like to become a friend of Libek through a donation, you are welcome to do so. We
                        focus on funding from private sources, and your contribution would support educational programs,
                        economic and social research work and publishing of books which influence the debate on the
                        ideas of individual liberty, responsible civil society and free market reforms.</p>

					<p>
					It is possible to support Libek through a wire transfer or cryptocurrency.
					</p>
                </article>

            </div>
            <div class="col-md-12 supp-desc2">


                <p>Payment instruction for wire transfers (EUR): </p>

                <p>

                    Bank name: Unicredit Bank</br>
                    Bank Address: Rajiceva 27-29, 11000 Belgrade, Serbia</br>
                    Account Name: Udruzenje gradjana Libertarijanski Klub</br>
                    IBAN: RS35170003000560800126</br>
                    SWIFT/BIC: BACXRSBG

                </p>

                <p>

                    Payment instruction for wire transfers (USD):
                </p>

                <p>

                    Bank name: Unicredit Bank</br>
                    Bank Address: Rajiceva 27-29, 11000 Belgrade, Serbia</br>
                    Account Name: Udruzenje gradjana Libertarijanski Klub</br>
                    IBAN: RS35170003000560800223</br>
                    SWIFT/BIC: BACXRSBG

                </p>
				
				<p>
					Cryptocurrency:</br>

					Bicoin (BTC) address:</br>

					1KKN2eg6CAAs7yUMbNZk8bWUxPeqkVyu4f</br>

					Ethereum (ETH) address:</br>

					0xAD00D0737D819475Fb4Fd68485e2ef1D8845FbCa</br>

					Litecoin (LTC) address:</br>

				    LKWNMEFEZueLi3SKnfvNc1Gk6Fj2ZVeUNH</br>
					</p>

                <p>
                    US citizens interested in making a tax deductible donation, please contact: <a
                            href="mailto:petar.cekerevac@libek.org.rs">petar.cekerevac@libek.org.rs</a>
                </p>


            </div>


        </div>
    </div>
</div>