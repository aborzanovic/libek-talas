@extends('layouts.master')
@section('layoutContent')


	@yield('content')

	@include('partials.scripts.google-analytics')

	@include("partials.footer.{$currentLocale}")

@stop
