<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="{{ $currentLocale }}" class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="{{ $currentLocale }}" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="{{ $currentLocale }}" class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="{{ $currentLocale }}"> <!--<![endif]-->
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116129609-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-116129609-1');
	</script>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta content="True" name="HandheldFriendly">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<title>{{{ $pageTitle }}}</title>
	<meta name="description" content="{{{ $meta->description }}}">
	<link 
  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
  rel="stylesheet"  type='text/css'>
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<meta name="csrf-token" content="{{{ Session::token() }}}">



	@if ( ! App::environment('production'))
		<meta name="environment" content="{{{ App::environment() }}}">
	@endif

	@include('partials.meta.og')
	@include('partials.shortcut-icons')

	@if (isset($feedUrl))
		<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="{{{ $feedUrl }}}">
	@endif
	{{ Asset::container('head') }}
</head>
<body class="{{ $bodyClasses }}" data-page="{{ $bodyDataPage }}">

@include('partials.noscript.bottom')

{{ Asset::container('bodyStart') }}

@yield('layoutContent')

{{ Asset::container('bodyEnd') }}
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ab25eb15c6b4bd4"></script>

</body>
<?php /*if($bodyDataPage === 'research' || $bodyDataPage === 'news') echo*/
        //if($meta->og['og:type'] === 'article') echo
'<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56ec78498b326b78"></script>
';?>
</html>
