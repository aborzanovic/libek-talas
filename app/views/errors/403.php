<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo Lang::get( 'errors/403.title' ); ?></title>

	<style type="text/css">

		body {
			background: #aaa;
			color: #666;
			font-family: Arial, sans-serif;
			font-size: 14px;
			font-style: italic;
			font-weight: bold;
			text-align: justify;
		}

		section#error-403 {
			background: #ddd;
			border-radius: 8px;
			box-shadow: 0px 0px 4px rgba( 0, 0, 0, 0.8 ), 0px 0px 256px #fff;
			margin: 40px auto;
			padding: 20px 20px 30px;
			text-shadow: 0px 1px 0px #fff;
			width: 760px;
		}

		h1 {
			color: #930544;
			font-size: 48px;
			margin: 0;
			padding: 0;
		}

		h2 {
			color: #930544;
			font-size: 20px;
			margin: 10px 0 0;
			padding: 0;
		}

		p {
			margin: 20px 0 0;
			padding: 0;
		}

		a {
			color: #c33574;
			text-decoration: none;
		}

		a:hover {
			color: #930544;
			text-decoration: underline;
		}

	</style>
</head>
<body>
	<section id="error-403">
		<h1><?php echo Lang::get( 'errors/403.h1' ); ?></h1>
		<h2><?php echo Lang::get( 'errors/403.h2' ); ?></h2>

		<?php
			foreach (Lang::get( 'errors/403.text' ) as $line)
			{
		?>
				<p><?php echo $line; ?></p>
		<?php
			}
		?>
	</section>
</body>
</html>
