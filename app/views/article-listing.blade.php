<?php
$mobile_pops = $slideshowArticles->slice(0, 3);
?>

<style type="text/css">
.news-holder-mob-1
{
  width: 80%
}

.news-holder-mob-2
{
  width: 40%
}

.news-image-mob
{
width: 100%;
    background-image: url(/uploads/images/NewsArticle/1518817809.7400e/main/thumbnail.jpg);
    height: 150px;
    background-size: cover;
    background-position: center;
}

.newest-mob
{
    margin-top: 50px;

}

.news-content-mob
{   
    margin-top: 10px;
    text-align: center;
}

</style>

<div class="content-row">
    <div class="container news-padding">
                <div class="row">
                    <div class="col-12 hidden-sm-up newest-mob" style="text-align: center;;">
                    <h3><b>Popularne vesti</b></h3>
                    </div>
                </div>
                <div class="row" >
                        <?php $i=0; ?>
                        @foreach ($mobile_pops as $index => $mobile_pop)
                        
                        <?php $url = $urlGenerator($mobile_pop);  ?>
                        <div class="hidden-sm-up col-<?php if($i==0) echo'10 offset-1'; else echo'5 mx-auto'; ?>">
                        <a href="{{ $url }}" class="home-news-link normalize-link">
                             <div class="news-sticker-mob">
                                 <time class="news-time"
                                           datetime="{{ $mobile_pop->published_at->toATOMString() }}">{{ Str::title(Str::getDayName($mobile_pop->published_at, $currentLanguage)) }},
                                          {{ $mobile_pop->published_at->format(Lang::get('common.dateFormat')) }}</time>
                             </div> 
                             <div class="">
                                 <div class="news-image-mob"
                                      style="background-image: url('{{$mobile_pop->getImage('main', 'thumbnail')}}')"></div>
                                 <article class="news-content-mob">
                                     <h4><b>{{$mobile_pop->title}}</b></h4>
                                     <?php
                                     if (isset($mobile_pop->lead) && !empty($mobile_pop->lead)) {
                                         $lead = $mobile_pop->lead;
                                     } else {
                                         $lead = $mobile_pop->contents;
                                     }
                                     ?>
                                     <!--<p class="news-lead-mob">{{ str_limit(strip_tags($lead), 140) }}</p>-->
                                     
                                 </article>
                             </div>
                        </a> 
                     </div>
                     <?php $i++; ?>
                     @endforeach
                </div>
             <div class="row">    
            <div class="hidden-xs-down mx-auto newest" style="display: inline-flex;">
                <img src="{{ url('/images/talas/newest-wawe.png') }}" class="wave-left">
                <h3><b>Najnovije vesti</b></h3>
                <img src="{{ url('/images/talas/newest-wawe.png') }}" class="wave-right">
            </div>
            <div class="col-xs-2 hidden-sm-up"></div><div class="col-xs-10 hidden-sm-up" style="height: 50px; width: 100%; margin-bottom: 20px; margin-top: 25px; margin-bottom: 20px; text-align: center;" > <h3>Najnovije vesti</h3> </div>
        </div>
        <?php $cnt = 0; $cnt2 = 0 ?>
        <div class="row ">
            @foreach ($articles as $article)
                <?php $url = $urlGenerator($article); ?>
                <?php $cnt++; $cnt2++; ?>
                @include("article-listing-one")
                @if ($cnt2 == 6 ) 
                    </div>
                    @include("home.mostpopular")
                    <div class="row row-margin">
                @endif
                @if($cnt % 3 === 0 && $cnt2 !== 6)
        </div>
        <div class="row row-margin">
                @endif
            @endforeach
        </div>
    </div>
</div>
<style>
    body {
        overflow-y: auto;
        overflow-x: hidden;
    }
    html {
        overflow: hidden;
        height: 100%;
    }
</style>