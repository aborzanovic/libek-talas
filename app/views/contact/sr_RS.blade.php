<h1>Kontakt</h1>

<p>Možete nam pisati emailom na <a href="mailto:kontakt@libek.org.rs">kontakt@libek.org.rs</a>. Takođe možete da nas kontaktirate popunjavanjem kontakt forme.</p>

<p>Obično odgovaramo na sve email poruke u roku od 24 sata, ali se može desiti da nam je potrebno više vremena. Ukoliko ne dobijete odgovor u razumnom roku, slobodno nas kontaktirajte ponovo.</p>
