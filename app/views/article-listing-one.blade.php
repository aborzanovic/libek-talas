<div class="col-md-3 mx-auto news-holder">
    <a href="{{ $url }}" class="home-news-link normalize-link">
         <div class="news-sticker">
             <time class="news-time"
                       datetime="{{ $article->published_at->toATOMString() }}">{{ Str::title(Str::getDayName($article->published_at, $currentLanguage)) }},
                      {{ $article->published_at->format(Lang::get('common.dateFormat')) }}</time>
         </div> 
         <div class="">
             <div class="news-image"
                  style="background-image: url('{{$article->getImage('main', 'thumbnail')}}')"></div>
             <article class="">
                 <h4><b>{{$article->title}}</b></h4>
                 <?php
                 if (isset($article->lead) && !empty($article->lead)) {
                     $lead = $article->lead;
                 } else {
                     $lead = $article->contents;
                 }
                 ?>
                 <p class="news-lead hidden-sm-up">{{ str_limit(strip_tags($lead), 140) }}</p>
                 <p class="news-lead hidden-lg-down">{{ str_limit(strip_tags($lead), 140) }}</p>
                 
             </article>
         </div>
    </a> 
 </div>