<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap3.min.css" rel="stylesheet" type="text/css">

<style type="text/css">
#tags-selectized
{
  font-size: 13px;
}

.img-responsive {
    display: block;
    max-width: 100%;
    height: auto;
}

.sidebar-header {
 left: 15px !important;
}

.slideshow-title {
  max-height: 260px !important;
}

</style>

<script type="text/javascript">
  
  function hover(elem)
  {
     elem.setAttribute('src', '{{ url('images/talas/talas-hov.png') }}');
  }

  function unhover(elem)
  {
     elem.setAttribute('src', '{{ url('images/talas/menu-talas.png') }}');
  }



</script>

<!-- onmouseover="hover(this)" onmouseout="unhover(this)" style="width: 22px; height: 91px;"-->

<nav style="position: fixed !important; z-index: 50;">
  	<div id="side-bar" class="sidebar-header">
            <img src="{{ url('images/talas/glyphicons-114-justify.png') }}" id="ham" class="hamburger" onclick="hide_show_menu()">
              <a class="normalize-link" href="/"><img src="{{ url('images/talas/menu-talas.png') }}"  id="img" class="row-margin hamburger hidden-xs-down"></a>
              <img src="{{ url('images/talas/menu-search.png') }}" class="hidden-xs-down" style="margin-top: 260px; display: block;" id="icon" onclick="search()">
             
                <div id="search" style="display: none; margin-top: 320px;" >
                   <form name="searchform" method="POST" action="javascript:redirect()">
                  <div class="form-group" style="width: 190px;">
                    {{ Form::select('tags[]', $tagOptions, [], ['class' => 'form-control', 'multiple' => 'multiple', 'placeholder' => 'Unesite reč za pretragu', 'id' => 'tags']) }}
                     <button type="submit" value="send" class="send-button">Search</button>
                  </div>
                 
                 <!-- <input type="button" name="close" class="X-button" value="X" onclick="close_div()"> -->
                 </form>
                </div>
                
              <a class="normalize-link hidden-xs-down" target="_blank"  href="https://twitter.com/Talas_rs"><img src="{{ url('images/talas/menu-twitter.png') }}" style="margin-top: 15px;"></a>
               <a target="_blank" href="https://www.instagram.com/talasrs/" class="hidden-xs-down normalize-link" style="font-size: 30px; margin-top: 14px; color: black;">
                        <i class="fab fa-instagram"></i>
                    </a>
              <a class="normalize-link hidden-xs-down" target="_blank"  href="https://www.facebook.com/talas.rs/"><img src="{{ url('images/talas/menu-facebook.png') }}" style="margin-top: 5px;vertical-align:  middle;margin-left: 5px;"></a>
      </div>
</nav>


<div id="menu_open_shadow" class="showmenu" style="display: none; position: fixed !important;">
    <div class="menu_style" id="menu-content">
      <div class="row menu-height">
          <div class="col-9" style="padding-right: 0px;">
            <div class="link-list">
              <ul style="list-style-type: initial; padding-left: 20px;">
                <a href="/" class="normalize-link"><li><h2><strong>Početna</strong></h2></li></a><br/>
                <a href="/sr/vesti/tag/1" class="normalize-link"><li><h2><strong>Uspeh</strong></h2></li></a><br/>
                <a href="/sr/vesti/tag/2" class="normalize-link"><li><h2><strong>Lični razvoj</strong></h2></li></a><br/>
                <a href="http://slobodniugao.talas.rs" target="_blank" class="normalize-link"><li><h2><strong>Slobodni ugao</strong></h2></li></a><br/>           
                <a href="/sr/o-nama" class="normalize-link"><li><h2><strong>O Talasu</strong></h2></li></a>
              </ul>
            </div>
            <!--
            <div class="link-list">
              <ul style="list-style-type: none;">
                <a href="/sr/o-nama" class="normalize-link"><li><h5><strong>O Talasu</strong></h5></li></a>
              </ul>
            </div>-->

            <!--
            <div class="menu-most-pop">
                
                <?php $url = $urlGenerator($mostPopularArticle); ?>
                <div class="menu-news-holder">
                    <a href="{{ $url }}" class="home-news-link normalize-link">
                         <div class="news-sticker menu-news-sticker" style="width: 66%;">
                             Najčitanija vest
                         </div> 
                         <div class="">
                             <div class="menu-news-image"
                                  style="background-image: url('{{$mostPopularArticle->getImage('main', 'thumbnail')}}')"></div>
                             <article class="news-content">
                                 <h5>{{$mostPopularArticle->title}}</h5>
                                 <a href="{{$url}}">
                                  <img src="{{url('images/talas/slide-arrow.png')}}" class="menu-arrow">
                               </a>
                             </article>
                         </div>
                    </a> 
                 </div>
            </div> -->
          </div>
          <div class="col-3" style="border-left: 1px solid gray;"> 
            
              <nav>
                <div id="side-bar" class="sidebar-header-2">
                        <img src="{{ url('images/talas/close.png') }}" id="close" class="cancel" onclick="hide_show_menu()">
                        <img src="{{ url('images/talas/menu-talas.png') }}" class="hamburger" style="margin-top: 25px;" onclick="hide_show_menu()">
                        <!--<img src="{{ url('images/talas/menu-search.png') }}" style="margin-top: 365px;">-->
                        <a href="https://twitter.com/Talas_rs" class="hidden-xs-down"><img src="{{ url('images/talas/menu-twitter.png') }}" style="margin-top: 370px;"></a>
                        <a target="_blank" href="https://www.instagram.com/talasrs/" class="hidden-xs-down normalize-link" style="font-size: 30px; margin-top: 14px; color: black;">
                        <i class="fab fa-instagram"></i>
                    </a>
                        <a href="https://www.facebook.com/talas.rs/" class="hidden-xs-down"><img src="{{ url('images/talas/menu-facebook.png') }}" style="margin-top: 5px;vertical-align:  middle;margin-left: 5px;"></a>
                  </div>
              </nav>

          </div>
      </div>
    </div>
    <div  class="shadow" >
    </div>
 </div>



<script type="text/javascript">

function hide_show_menu2()
{
  $("#side-bar").hide();
  $("#menu_open_shadow").show(1000);
}


function redirect()
{
  var $x = document.getElementById("tags").childNodes["0"].value; 
  window.location.href = "/sr/vesti/tag/"+$x;
}

function hide_show_menu()
{
  var $x = document.getElementById("menu_open_shadow");
  var $sidebar = document.getElementById("side-bar");
if ($x.style.display === "block") 
    {
      document.getElementById("menu_open_shadow").style.display = "none";
      $sidebar.style.setProperty('display', 'inline-grid');
    }
    else //none
    {
      $x.style.setProperty('display', 'block');
       $sidebar.style.setProperty('display', 'none');
    }
    close_div();
};


function search()
{
  var $x = document.getElementById("icon");
  var $y = document.getElementById("search");
  if ($x.style.display === "block") 
  {
    $x.style.display = "none";
    $y.style.display = "block";
  }
}

  function close_div(){
  var $x = document.getElementById("icon");
  var $y = document.getElementById("search");
  $x.style.display = "block";
  $y.style.display = "none";
  }
</script>
