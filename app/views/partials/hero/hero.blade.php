<?php
    $heroClass = 'hero-'.$bodyDataPage;
    $containerClass = 'hero-'.$bodyDataPage.'-container';
    $slideshow = isset($backgroundImages) ? 'slideshow' : '';
?>
<div class="hero-container {{$heroClass}} {{ $slideshow }}">
    @if(isset($backgroundImages))
        <?php $backgroundCnt = 0; ?>
        @foreach($backgroundImages as $backgroundImage)
            <div class="hero-slideshow {{ $backgroundCnt === 0 ? 'active' : '' }}" style="background-image: url('{{ $backgroundImage->getImage('main', 'full') }}')"></div>
            <?php $backgroundCnt++; ?>
        @endforeach
    @else
        <div class="hero-slideshow active {{$heroClass}}"></div>
    @endif
    <div class="hero-slideshow-overlay"></div>
    <div class="jumbotron hero">
      <div class="container {{$containerClass}}">
          <h1>@yield('heroTitle')</h1>
          <div class="underline"></div>
          <h2>@yield('heroSubtitle')</h2>
      </div>
    </div>
</div>