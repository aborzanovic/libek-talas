<?php foreach ( $meta->og as $property => $content ) : ?>
	<meta property="{{ $property }}" content="{{ $content }}">
<?php endforeach; ?>
