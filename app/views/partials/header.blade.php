<?php $headerContainerBackgroundClass = $headerContainerHasBackground ? 'header-container--with-background' : ''; ?>
<header>
    <div class="header-absolute">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default">

                    <div class="container container-relative">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header ">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>


                            @if ( $subdomain === 'johngalt')
                                @include('partials.header.logoJohn')
                                @include('partials.header.navigationJohn')
                            @elseif( $subdomain === 'alp')

                                @include('partials.header.logoAlp')
                                @include('partials.header.navigationAlp')

                            @else

                                @include('partials.header.logo')
                                @include('partials.header.navigation')


                            @endif
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>