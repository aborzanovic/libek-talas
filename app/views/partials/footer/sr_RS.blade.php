<style type="text/css">

.pull-right2
{
    float: right; 
    margin-top: -70px;
}

@media (max-width: 768px){
    .pull-right2 {
        float: none;
        margin-top: 0px;
    }
}    

</style>

<footer>
    <div class="row home-footer full-row text-center">
        <div class="col-md-12 footer-content">
           <!--<div class="pull-left">
                <h1 class="footer-logo"><a href="/{{ $currentLanguage }}" class="footer-logo--link" style='<?php echo 'background-image: url("/images/talas/footer.png");' ?>'>{{{ Lang::get('common.siteName') }}}</a></h1>
            </div>-->
            <div class="hidden-xs-down">
            <p>
                Talas je trend uspeha u svetu promena
            </p>
            <p>
              Kontakt: <a class="normalize-link" href="mailto:kontakt@talas.rs " style="font-size: 15px;">kontakt@talas.rs </a>
            </p>
            <p>
                Copyright © <a href="https://libek.org.rs/" class="normalize-link" target="_blank" style="font-size: 15px;"> Libertarijanski klub Libek </a>
            </p>
            </div>
            <div class="pull-right2" style="">
                <p>Pratite nas na</p>
                <p>društvenim mrežama</p>
                <p>
                    <a target="_blank" href="https://www.facebook.com/talas.rs/" class="normalize-link">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a target="_blank" href="https://twitter.com/Talas_rs" class="normalize-link">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a target="_blank" href="https://www.instagram.com/talasrs/" class="normalize-link">
                        <i class="fab fa-instagram"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>

<style type="text/css">
    .home-footer {
    background-image: url(/images/social-footer.png);
    background-size: cover !important;
    background-repeat: no-repeat !important;
    background-position: center !important;
}

.home-footer p {
    color: white;
    font-size: 15px;
    font-family: "ralewaylight", sans-serif;
    margin: 0;
}

.full-row {
    margin: 0 !important;
}

.text-center {
    text-align: center;
}

.home-footer .footer-content {
    padding: 20px 30px;
    background-color: transparent;
}

.pull-left {
    float: left !important;
}
.pull-right {
    float: right !important;
}

.footer-logo {
    display: block;
    margin: 0;
    padding: 0;
    margin-top: 18px;
}

.home-footer a {
    font-size: 32px;
}
.footer-logo--link {
    display: block;
    width: 230px;
    height: 50px;
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    text-indent: -9999px;
}


</style>