<footer>
    <div class="row home-footer full-row text-center">
        <div class="col-md-12 footer-content">
            <div class="pull-left">
                <h1 class="footer-logo"><a href="/{{ $currentLanguage }}" class="footer-logo--link" style='<?php echo 'background-image: url("/images/libek-logo-en.png");' ?>'>{{{ Lang::get('common.siteName') }}}</a></h1>
            </div>
            <div class="pull-right">
                <p>Follow us</p>
                <p>on social networks</p>
                <p>
                    <a target="_blank" href="https://www.facebook.com/libertarijanski.klub" class="normalize-link">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a target="_blank" href="https://twitter.com/libekbg" class="normalize-link">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a target="_blank" href="https://www.linkedin.com/company/libertarian-club-libek" class="normalize-link">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>