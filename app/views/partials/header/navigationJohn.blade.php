<div class="collapse navbar-collapse" id="myNavbar">
    <nav class="header-navigation">
        <ul class="header-navigation--list nav navbar-nav">
            <!-- <?php $edu = Lang::get('routes.education');$extension= ''.$edu.'/john-galt'; $activeClass = ($bodyDataPage === 'home') ? 'header-navigation--link--active' : ''; ?>
                    <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{$extension}}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.john-galt') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'blog') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.blog') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.blog') }}</a>
            </li>
            <li class="header-navigation--item"><a href="#"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.blog') }}</a>
            </li>-->

            <?php $activeClass = ($bodyDataPage === 'works') ? 'header-navigation--link--active' : ''; ?>
            <!--<li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.works') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.works') }}</a>
            </li>-->
            <li class="header-navigation--item"><a href="http://albionbooks.rs/ekskluzivno-izabrana-dela-ajn-rend/" target="_blank"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.works') }}</a>
            </li>

            <!--
		<?php $activeClass = ($bodyDataPage === 'research') ? 'header-navigation--link--active' : ''; ?>
                    <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.research') }}" class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.research') }}</a></li>

		<?php $activeClass = ($bodyDataPage === 'projects') ? 'header-navigation--link--active' : ''; ?>
                    <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.projects') }}" class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.projects') }}</a></li>

		<!--<li class="header-navigation--item"><a href="https://www.facebook.com/libertarijanski.klub/photos_stream?tab=photos_albums" target="_blank" class="header-navigation--link">{{ Lang::get('navigation.gallery') }}</a></li>
		-->
            <?php $activeClass = ($bodyDataPage === 'Q&A') ? 'header-navigation--link--active' : ''; ?>
            <!--<li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.Q&A') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.Q&A') }}</a>
            </li>-->
            <li class="header-navigation--item"><a href="http://rs.aynrandeurope.org/" target="_blank"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.Q&A') }}</a>
            </li>

        </ul>
    </nav>
</div>