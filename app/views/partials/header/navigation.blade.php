<div class="collapse navbar-collapse" id="myNavbar">
    <nav class="header-navigation">
        <ul class="header-navigation--list nav navbar-nav">
            <?php $activeClass = ($bodyDataPage === 'home') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.home') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'team') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.team') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.team') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'news') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.news') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.news') }}</a>
            </li>

            <!--
		<?php $activeClass = ($bodyDataPage === 'research') ? 'header-navigation--link--active' : ''; ?>
                    <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.research') }}" class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.research') }}</a></li>

		<?php $activeClass = ($bodyDataPage === 'projects') ? 'header-navigation--link--active' : ''; ?>
                    <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.projects') }}" class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.projects') }}</a></li>

		<!--<li class="header-navigation--item"><a href="https://www.facebook.com/libertarijanski.klub/photos_stream?tab=photos_albums" target="_blank" class="header-navigation--link">{{ Lang::get('navigation.gallery') }}</a></li>
		-->

          <?php $activeClass = ($bodyDataPage === 'support') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.support') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.support') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'contact') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{ Lang::get('routes.contact') }}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.contact') }}</a>
            </li>

            @include("partials.header.languages")
        </ul>
    </nav>
</div>