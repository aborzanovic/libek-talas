<h1 class="header-logo"><a href="/{{ $currentLanguage }}" class="header-logo--link"
                           style='<?php if ($meta->og['og:locale'] === "sr_RS") echo 'background-image: url("/images/header-logo.png");';
                           else echo 'background-image: url("/images/libek-logo-en.png");' ?>'>
        {{{ Lang::get('common.siteName') }}}</a></h1>
