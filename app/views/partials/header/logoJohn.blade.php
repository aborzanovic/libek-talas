<h1 class="header-logo"><a href="/{{ $currentLanguage }}/{{Lang::get('routes.education')}}/john-galt" class="header-logo--link"
                           style="background-image: url('/images/john-galt-logo.png');";
    >
        {{{ Lang::get('common.siteName') }}}</a></h1><a href="/"><button type="button" class="btn btn-default back-logo">{{Lang::get('navigation.backToLibek')}}</button></a>
