<div class="collapse navbar-collapse" id="myNavbar">
    <nav class="header-navigation">
        <ul class="header-navigation--list nav navbar-nav">
            <?php $edu = Lang::get('routes.education');$extension = '' . $edu . '/alp'; $activeClass = ($bodyDataPage === 'home') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a href="/{{ $currentLanguage }}/{{$extension}}"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.alp') }}</a>
            </li>

         <!--   <li class="header-navigation--item"><a href="http://alp6.libek.org.rs/"
                                                   class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.application') }}</a>
            </li> -->

            <?php $activeClass = ($bodyDataPage === 'lecturers') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a
                        href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.lecturers') }}"
                        class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.lecturers') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'mentors') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a
                        href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.mentors') }}"
                        class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.mentors') }}</a>
            </li>

            <?php $activeClass = ($bodyDataPage === 'alumni') ? 'header-navigation--link--active' : ''; ?>
            <li class="header-navigation--item"><a
                        href="/{{ $currentLanguage }}/{{$extension}}/{{ Lang::get('routes.alumni') }}"
                        class="header-navigation--link {{ $activeClass }}">{{ Lang::get('navigation.alumni') }}</a>
            </li>

            @include("partials.header.languages")
        </ul>
    </nav>
</div>