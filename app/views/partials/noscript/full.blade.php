<noscript class="noscript-cover">
	<div class="noscript-cover-outer">
		<div class="noscript-cover-middle">
			<div class="noscript-cover-inner">
				{{{ Lang::get('common.noscriptWarning') }}}
			</div>
		</div>
	</div>
</noscript>
