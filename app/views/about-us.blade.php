@include('partials.navigation');
<style type="text/css">
	
.about
{
	text-align: center;
	margin-top: 50px;
	
	color: #7046c6;
}

.about-p
{
	font-size: 22px;
	text-align: center;
	margin-top: 100px;
	margin-bottom: 100px;
}

.about-stripes
{
	position: absolute;
    right: 350px;
    top: 200px;
}

.about-bg-stripes-single
{
	opacity: 0.3;
   	width: 500px;
}

</style>

<div class="row">
	<div class="col-12 about">
		<h2 style="font-size: 60px !important;"><b> O Talasu </b></h2>
	</div>
</div>
<div class="row">
	<div class="col-6 mx-auto about-p">
		<p>
			Talas je portal koji vam približava važne globalne trendove koji oblikuju naš svet. Savremena društva menjaju se svakodnevno. Težimo da te promene unaprede naše okruženje. Obrađujemo važne informacije i donosimo inspirativne priče. Nudimo korisne savete jer verujemo u snagu ličnog uspeha i moć primenjivog znanja. 
		</p>
		<p>Talas je trend uspeha u svetu promena.</p>
	</div>
</div>
<!--
  <div class="about-stripes">
                  <img src="{{ url('/images/talas/bg-stripes.png') }}" class="about-bg-stripes-single">
 </div>

 <div >
 	<img class="img-responsive" src="{{ url('/images/talas/talas_talas.png') }}" style="width: 100%; position: absolute;">
 </div>-->

<div class="row" style="width: 100%; z-index: -5; position: relative;">
	<img class="img-responsive hidden-xs-down" src="{{ url('/images/talas/talas_talas.png') }}" style="width: 100%; height: 80%; top: 20%; position: absolute;">
	<div class="col-6 mx-auto about-p">
		<p>
			 Talas tim:
		</p>
		<p>Urednica: Lana Avakumović <a href="mailto:lana@talas.rs">lana@talas.rs</a> <br/>
Koordinatori: Miloš Nikolić <a href="mailto:milos@talas.rs">milos@talas.rs</a> <br/>
i Petar Čekerevac <a href="mailto:petar@talas.rs">petar@talas.rs</a><br/>
Social media: Jovana Stanisavljević <a href="mailto:jovana@talas.rs">jovana@talas.rs</a></p>
	</div>
</div>


