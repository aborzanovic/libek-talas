<div class="container" style="width: 100% !important;">
    <div class="row edu-text">

        <div class="container">
            <div class="col-md-12 edu-tiny">  <!-- hardcode  -->

            </div>
            <div class="col-md-12 edu-large ">
                <p>
                    {{Lang::get('education.large.1')}}
                </p>

            </div>
            <div class=" col-md-12 edu-medium ">
                <p>
                    {{Lang::get('education.large.2')}}
                </p>

                <p>
                    {{Lang::get('education.medium.1')}}
                </p>

                <p>
                    {{Lang::get('education.medium.2')}}
                </p>

                <p>
                    {{Lang::get('education.medium.3')}}
                </p>

                <p>
                    {{Lang::get('education.medium.4')}}
                </p>
            </div>
        </div>

    </div>

    <div class="row edu-alp animation-holder">
        <div class="animation-overlay">
            <span class="overlay"></span>
        </div>
        <a href="/{{ $currentLanguage }}/{{ Lang::get('routes.education') }}/alp">
            <div class="link">
                <div class="container">
                    <div class="col-md-12">
                        <p class="edu-title">{{Lang::get('education.titles.alp')}}</p>

                        <p class="edu-desc">{{Lang::get('education.content.alp')}}
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="row edu-pop animation-holder">
        <div class="animation-overlay">
            <span class="overlay"></span>
        </div>
        <a href="http://www.popekonomija.rs/" target="_blank">
            <div class="link">
                <div class="container">
                    <div class="col-md-12">
                        <p class="edu-title">{{Lang::get('education.titles.pop')}}</p>

                        <p class="edu-desc">{{Lang::get('education.content.pop')}}
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="row edu-john animation-holder">
        <div class="animation-overlay">
            <span class="overlay"></span>
        </div>
        <a href="/{{ $currentLanguage }}/{{ Lang::get('routes.education') }}/john-galt">
            <div class="link">
                <div class="container">
                    <div class="col-md-12">
                        <p class="edu-title">{{Lang::get('education.titles.john_galt')}}</p>

                        <p class="edu-desc">{{Lang::get('education.content.john_galt')}}

                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="row edu-pp animation-holder">
        <div class="animation-overlay">
            <span class="overlay"></span>
        </div>
        <a href="/{{ $currentLanguage }}/{{ Lang::get('routes.education') }}/{{Lang::get('routes.individual-policy')}}">
            <div class="link">
                <div class="container">
                    <div class="col-md-12">
                        <p class="edu-title">{{Lang::get('education.titles.firm_policy')}}</p>

                        <p class="edu-desc">{{Lang::get('education.content.firm_policy')}}

                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
