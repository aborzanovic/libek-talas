<?php

/*
 * Breadcrumbs configuration for the `davejamesmiller/breadcrumbs` package.
 */

use DaveJamesMiller\Breadcrumbs\Generator;
use Libek\LibekOrgRs\Auth\User;
use Libek\LibekOrgRs\Slideshow\Image as SlideshowImage;
use Libek\LibekOrgRs\News\Article as NewsArticle;
use Libek\LibekOrgRs\JohnGalt\Article as JohnGaltNewsArticle;
use Libek\LibekOrgRs\Generations\Article as GenerationsArticle;
use Libek\LibekOrgRs\Research\Article as ResearchArticle;
use Libek\LibekOrgRs\TeamMembers\Person as TeamMember;
use Libek\LibekOrgRs\Mentors\Person as Mentor;
use Libek\LibekOrgRs\Lecturers\Person as Lecturer;
use Libek\LibekOrgRs\Alumni\Person as Alumnus;

/*
 * Front-end breadcrumbs.
 */

Breadcrumbs::register('front.home', function(Generator $breadcrumbs)
{
	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Front\HomeController@index');

	$breadcrumbs->push(Lang::get('navigation.home'), $url);
});


/**
 * Basic CMS admin breadcrumbs.
 */

Breadcrumbs::register('creitive/basic::admin.home', function(Generator $breadcrumbs)
{
	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\HomeController@index');

	$breadcrumbs->push(Lang::get('admin/navigation.home'), $url);
});

/**
 * News article administration.
 */

Breadcrumbs::register('creitive/basic::admin.newsArticles', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('creitive/basic::admin.home');

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@index');

	$breadcrumbs->push(Lang::get('admin/newsArticles.titles.index'), $url);
});

Breadcrumbs::register('creitive/basic::admin.newsArticles.show', function(Generator $breadcrumbs, NewsArticle $newsArticle)
{
	$breadcrumbs->parent('creitive/basic::admin.newsArticles');

	$breadcrumbs->push($newsArticle->title);
});

Breadcrumbs::register('creitive/basic::admin.newsArticles.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('creitive/basic::admin.newsArticles');

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@create');

	$breadcrumbs->push(Lang::get('admin/newsArticles.titles.create'), $url);
});

Breadcrumbs::register('creitive/basic::admin.newsArticles.edit', function(Generator $breadcrumbs, NewsArticle $newsArticle)
{
	$breadcrumbs->parent('creitive/basic::admin.newsArticles.show', $newsArticle);

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@edit', ['newsArticle' => $newsArticle->id]);

	$breadcrumbs->push(Lang::get('admin/newsArticles.titles.edit'), $url);
});

Breadcrumbs::register('creitive/basic::admin.newsArticles.delete', function(Generator $breadcrumbs, NewsArticle $newsArticle)
{
	$breadcrumbs->parent('creitive/basic::admin.newsArticles.show', $newsArticle);

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\NewsArticleController@confirmDelete', ['newsArticle' => $newsArticle->id]);

	$breadcrumbs->push(Lang::get('admin/newsArticles.titles.delete'), $url);
});

/**
 * User administration.
 */

Breadcrumbs::register('creitive/basic::admin.users', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('creitive/basic::admin.home');

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@index');

	$breadcrumbs->push(Lang::get('admin/navigation.users'), $url);
});

Breadcrumbs::register('creitive/basic::admin.users.show', function(Generator $breadcrumbs, User $user)
{
	$breadcrumbs->parent('creitive/basic::admin.users');

	$breadcrumbs->push($user->full_name);
});

Breadcrumbs::register('creitive/basic::admin.users.create', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('creitive/basic::admin.users');

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@create');

	$breadcrumbs->push(Lang::get('admin/users.titles.create'), $url);
});

Breadcrumbs::register('creitive/basic::admin.users.edit', function(Generator $breadcrumbs, User $user)
{
	$breadcrumbs->parent('creitive/basic::admin.users.show', $user);

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@edit', ['user' => $user->id]);

	$breadcrumbs->push(Lang::get('admin/users.titles.edit'), $url);
});

Breadcrumbs::register('creitive/basic::admin.users.delete', function(Generator $breadcrumbs, User $user)
{
	$breadcrumbs->parent('creitive/basic::admin.users.show', $user);

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\UserController@confirmDelete', ['user' => $user->id]);

	$breadcrumbs->push(Lang::get('admin/users.titles.delete'), $url);
});


/**
 * Password change.
 */

Breadcrumbs::register('creitive/basic::admin.changePassword', function(Generator $breadcrumbs)
{
	$breadcrumbs->parent('creitive/basic::admin.home');

	$url = URL::action('Libek\LibekOrgRs\Http\Controllers\Admin\PasswordController@index');

	$breadcrumbs->push(Lang::get('admin/navigation.changePassword'), $url);
});
