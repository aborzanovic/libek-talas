<?php

return [

	/**
	 * How many items to return per page in the admin panel.
	 *
	 * Only applies to entities that are actually paginated - some items are
	 * always shown without pagination (ie. all results are displayed), for
	 * example, when sorting is required.
	 *
	 * @var integer
	 */
	'perPage' => 50,

];
