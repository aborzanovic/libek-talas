<?php

return [

	/**
	 * The Google Analytics tracking ID for this website.
	 */
	'trackingId' => getenv('GOOGLE_ANALYTICS_TRACKING_ID'),

	/**
	 * The domain tracked for this website.
	 *
	 * This should be the `production` domain, as the Analytics code isn't
	 * loaded in any other environment.
	 */
	'domain' => getenv('GOOGLE_ANALYTICS_DOMAIN'),

];
