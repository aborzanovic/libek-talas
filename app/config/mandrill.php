<?php

/**
 * Configuration for the wrapper class for the Mandrill PHP API.
 *
 * For more detailed documentation on specific options, check Mandrill's
 * official documentation.
 *
 * @link http://help.mandrill.com
 */

return array(

	/**
	 * Mandrill API key.
	 */
	'apiKey' => getenv('MANDRILL_API_KEY'),

	/**
	 * The e-mail address from which the e-mail will appear to be sent.
	 */
	'fromEmail' => 'noreply@libek.org.rs',

	/**
	 * The name from which the e-mail will appear be sent.
	 */
	'fromName' => 'Talas',

	/**
	 * The e-mail address to which the user should reply to. If left empty, it
	 * will default to the "From:" e-mail address.
	 */
	'replyToEmail' => 'mandrill@libek.org.rs',

	/**
	 * The name of the recipient listed as the "replyToEmail".
	 */
	'replyToName' => 'Talas',

	/**
	 * Whether to send the e-mail asynchronously or not.
	 */
	'async' => false,

	/**
	 * Which IP pool to use for sending the e-mail.
	 */
	'ipPool' => 'Main Pool',

);
