<?php

/**
 * Configuration for the wrapper class for the MailChimp PHP API.
 *
 * For more detailed documentation on specific options, check MailChimp's
 * official documentation.
 *
 * @link http://apidocs.mailchimp.com/api/2.0/
 */

return array(

	/**
	 * MailChimp API key.
	 */
	'apiKey' => getenv('MAILCHIMP_API_KEY'),

	/**
	 * The IDs of all the subscriber lists used within the application.
	 */
	'listIds' => [

		/**
		 * A list for volunteers for the September 2014 Taxpayers' March.
		 */
		'volunteers' => getenv('MAILCHIMP_LIST_IDS_VOLUNTEERS'),

	],

);
