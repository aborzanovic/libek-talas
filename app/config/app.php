<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => true,

	/*
	|--------------------------------------------------------------------------
	| Application Log level
	|--------------------------------------------------------------------------
	|
	| The minimum level which will be logged to log files.
	|
	*/

	'logLevel' => 'debug',

	/*
	|--------------------------------------------------------------------------
	| Force SSL
	|--------------------------------------------------------------------------
	|
	| Whether to always force SSL connections.
	|
	*/

	'forceSsl' => (getenv('FORCE_SSL') === 'true'),

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'http://www.libek.org.rs',
	/*
	 * WEBMONKS:TODO
	 * promeniti kada se sajt postavi na produkcijski server
	 */

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Europe/Belgrade',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'sr_RS',

	/*
	|--------------------------------------------------------------------------
	| Application Allowed Locales
	|--------------------------------------------------------------------------
	|
	| The allowed locales are those that may be used by the application.
	|
	| For each locale, the key must be the full locale code (ie. including the
	| teritorry, for example `en_US`), while the value must be an array
	| containing the keys `language` (which should hold the link that will be
	| used within routes, and for determining the current application locale),
	| and `name` (which is the name of the language, in that language).
	|
	*/

	'locales' => [
		[
			'locale' => 'sr_RS',
			'language' => 'sr',
			'name' => 'Srpski',
		],
		[
			'locale' => 'en_US',
			'language' => 'en',
			'name' => 'English',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Application Fallback Locale
	|--------------------------------------------------------------------------
	|
	| The fallback locale determines the locale to use when the current one
	| is not available. You may change the value to correspond to any of
	| the language folders that are provided through your application.
	|
	*/

	'fallback_locale' => 'en_US',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => getenv('APP_KEY'),

	'cipher' => MCRYPT_RIJNDAEL_128,

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => [

		/*
		 * Illuminate providers.
		 *
		 * The commented-out providers were intentionally left here, so we can
		 * easily see which default Illuminate providers are disabled.
		 */

		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Session\CommandsServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Log\LogServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Database\MigrationServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Remote\RemoteServiceProvider',
		'Illuminate\Auth\Reminders\ReminderServiceProvider',
		'Illuminate\Database\SeedServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Workbench\WorkbenchServiceProvider',

		/*
		 * Third-party providers.
		 *
		 * These are usually service providers that ship with packages created
		 * for Laravel.
		 */

		'Cartalyst\Sentinel\Laravel\SentinelServiceProvider',
		'DaveJamesMiller\Breadcrumbs\ServiceProvider',
		'Levacic\LaravelWhoopsEditorLink\LaravelWhoopsEditorLinkServiceProvider',

		/*
		 * Creitive providers.
		 *
		 * Ideally, these would all be extracted to packages and added to the
		 * previous list, but as that requires us to either open-source the code
		 * or set up custom private repositories, we're currently just manually
		 * adding these classes to each each individual project.
		 */

		'Creitive\Asset\AssetServiceProvider',
		'Creitive\Database\Query\Grammars\MySqlGrammarServiceProvider',
		'Creitive\Dropzone\DropzoneServiceProvider',
		'Creitive\Html\HtmlServiceProvider',
		'Creitive\Image\UploaderServiceProvider',
		'Creitive\Mandrill\MailerServiceProvider',
		'Creitive\Pagination\PaginationServiceProvider',
		'Creitive\Robots\ServiceProvider',
		'Creitive\Routing\LocaleResolverServiceProvider',
		'Creitive\ScriptHandler\ScriptHandlerServiceProvider',
		'Creitive\Sentinel\Laravel\SentinelServiceProvider',
		'Creitive\ViewData\ViewDataServiceProvider',

		/*
		 * Project-specific providers.
		 *
		 * Any providers specifically associated with this project, and created
		 * for that purpose.
		 */

		'Libek\LibekOrgRs\AssetManager\AssetManagerServiceProvider',
		'Libek\LibekOrgRs\Http\Filters\FilterServiceProvider',
		'Libek\LibekOrgRs\Validation\ValidatorServiceProvider',

	],

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => storage_path().'/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => [

		/*
		 * Illuminate aliases.
		 *
		 * As with the service providers, some entries have intentionally been
		 * commented out (as opposed to deleted), to have an easy overview of
		 * default stuff that we aren't using.
		 */

		'App' => 'Illuminate\Support\Facades\App',
		'Artisan' => 'Illuminate\Support\Facades\Artisan',
		'Auth' => 'Illuminate\Support\Facades\Auth',
		'Blade' => 'Illuminate\Support\Facades\Blade',
		'Cache' => 'Illuminate\Support\Facades\Cache',
		'ClassLoader' => 'Illuminate\Support\ClassLoader',
		'Config' => 'Illuminate\Support\Facades\Config',
		'Controller' => 'Illuminate\Routing\Controller',
		'Cookie' => 'Illuminate\Support\Facades\Cookie',
		'Crypt' => 'Illuminate\Support\Facades\Crypt',
		'DB' => 'Illuminate\Support\Facades\DB',
		'Eloquent' => 'Illuminate\Database\Eloquent\Model',
		'Event' => 'Illuminate\Support\Facades\Event',
		'File' => 'Illuminate\Support\Facades\File',
		'Form' => 'Illuminate\Support\Facades\Form',
		'Hash' => 'Illuminate\Support\Facades\Hash',
		'HTML' => 'Illuminate\Support\Facades\HTML',
		'Input' => 'Illuminate\Support\Facades\Input',
		'Lang' => 'Illuminate\Support\Facades\Lang',
		'Log' => 'Illuminate\Support\Facades\Log',
		'Mail' => 'Illuminate\Support\Facades\Mail',
		'Paginator' => 'Illuminate\Support\Facades\Paginator',
		'Password' => 'Illuminate\Support\Facades\Password',
		'Queue' => 'Illuminate\Support\Facades\Queue',
		'Redirect' => 'Illuminate\Support\Facades\Redirect',
		'Redis' => 'Illuminate\Support\Facades\Redis',
		'Request' => 'Illuminate\Support\Facades\Request',
		//'Response' => 'Illuminate\Support\Facades\Response',
		'Route' => 'Illuminate\Support\Facades\Route',
		'Schema' => 'Illuminate\Support\Facades\Schema',
		//'Seeder' => 'Illuminate\Database\Seeder',
		'Session' => 'Illuminate\Support\Facades\Session',
		//'SoftDeletingTrait' => 'Illuminate\Database\Eloquent\SoftDeletingTrait',
		'SSH' => 'Illuminate\Support\Facades\SSH',
		//'Str' => 'Illuminate\Support\Str',
		'URL' => 'Illuminate\Support\Facades\URL',
		'Validator' => 'Illuminate\Support\Facades\Validator',
		'View' => 'Illuminate\Support\Facades\View',

		/*
		 * Third-party aliases.
		 */

		'Activation' => 'Cartalyst\Sentinel\Laravel\Facades\Activation',
		'Breadcrumbs' => 'DaveJamesMiller\Breadcrumbs\Facade',
		'Reminder' => 'Cartalyst\Sentinel\Laravel\Facades\Reminder',
		'Sentinel' => 'Cartalyst\Sentinel\Laravel\Facades\Sentinel',

		/*
		 * Creitive aliases.
		 */

		'Asset' => 'Creitive\Asset\Facades\Asset',
		'Dropzone' => 'Creitive\Dropzone\Facades\Dropzone',
		'ImageUploader' => 'Creitive\Image\Facades\ImageUploader',
		'LocaleResolver' => 'Creitive\Routing\Facades\LocaleResolver',
		'LoremPixel' => 'Creitive\Support\LoremPixel',
		'Pagination' => 'Creitive\Pagination\Facades\Pagination',
		'Response' => 'Creitive\Support\Facades\Response',
		'Str' => 'Creitive\Support\Str',

		/*
		 * Project-specific aliases.
		 */

		'AssetManager' => 'Libek\LibekOrgRs\AssetManager\Facades\AssetManager',

	],

	'domain' => 'libek.org.rs'
	/*
	 * WEBMONKS:TODO
	 * promeniti kada se sajt postavi na produkcijski server
	 */

];
