<?php

return [

	/**
	 * Facebook app ID.
	 *
	 * @var string
	 */
	'appId' => getenv('FACEBOOK_APP_ID'),

	/**
	 * Facebook app secret.
	 *
	 * @var string
	 */
	'appSecret' => getenv('FACEBOOK_SECRET'),

	/**
	 * Options for initializing the JS SDK.
	 *
	 * @link https://developers.facebook.com/docs/javascript/reference/FB.init/v2.2
	 */
	'jsOptions' => [

		/**
		 * The Graph API version in use.
		 *
		 * @var string
		 */
		'version' => 'v2.2',

		/**
		 * Whether a cookie is created for the session.
		 *
		 * @var boolean
		 */
		'cookie' => false,

		/**
		 * Whether to check the current login status of the user on every page
		 * load.
		 *
		 * @var boolean
		 */
		'status' => false,

		/**
		 * Whether to parse XFBML tags on the page.
		 *
		 * @var boolean
		 */
		'xfbml' => true,

		/**
		 * Whether to enable frictionless requests.
		 *
		 * @link https://developers.facebook.com/docs/games/requests/v2.1#frictionless_requests
		 * @var boolean
		 */
		'frictionlessRequests' => false,

	],

];
